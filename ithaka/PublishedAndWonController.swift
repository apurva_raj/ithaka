//
//  PublishedAndWonController.swift
//  ithaka
//
//  Created by Apurva Raj on 18/03/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit


class PublishedAndWonController: UICollectionViewController, UICollectionViewDelegateFlowLayout, collectedFromAddIthaka {
    
    static let updateFeedNotificationNameForCollect = NSNotification.Name(rawValue: "UpdateFeedForCollect")
    static let updateFeedNotificationNameForCollectBig = NSNotification.Name(rawValue: "UpdateFeedForCollectBig")


    func collectedTapped() {
        print("delegate works")
            sendNotificationCenter()
            self.dismiss(animated: true, completion: nil)

    }
    
    func sendNotificationCenter(){
        NotificationCenter.default.post(name: AddIthakaFinalController.updateFeedNotificationName, object: nil)
        
        NotificationCenter.default.post(name: PublishedAndWonController.updateFeedNotificationNameForCollect, object: nil)

    }
    
   
    
    
    var postStory: PostStoryModel? {
        didSet {
            let st = postStory?.storyText ?? ""
            let ic = postStory?.itemCode ?? ""

            if ic.hasPrefix("ci"){
                let randomnumber = Int.random(in: 1...26)
                let makename = "ci" + String(randomnumber)
                self.itemcode = makename
            }
            
            if ic.hasPrefix("uci"){
                let randomnumber = Int.random(in: 1...47)
                let makename = "uci" + String(randomnumber)
                self.itemcode = makename
                
            }
            
            if ic.hasPrefix("ri"){
                let randomnumber = Int.random(in: 1...11)
                let makename = "ri" + String(randomnumber)
                self.itemcode = makename
                
                
            }

            print("icqqq is ", ic )
            let qt = postStory?.questTag ?? ""
            let sstrimed = st.trimmingCharacters(in: .whitespacesAndNewlines)
            if sstrimed.count == 0 {
                print("nothing")
                self.myActivityIndicator.stopAnimating()
                self.errorInfoLabel.text = "Ooops! You can't publish empty story."
            } else {
                
                
                let qid = postStory?.questId ?? nil
                let qobid = postStory?.questObjectId ?? nil
                let ll = postStory?.level ?? nil

                

                Api.MyPosts.publishMyStory(storytext: st, itemcode: ic, wonItemCode:self.itemcode ?? "", questTag: qt, questId: qid, questObjId: qobid, level: ll, username: nil) { (success) in
                    
                    
                    if success == "uploaded" {
                        
        
                        self.frontview.isHidden = true
                        self.myActivityIndicator.stopAnimating()

                        DispatchQueue.main.async {
                            self.collectionView?.reloadData()
                        }
                        
                        
                        print("do this")
                    } else {
                        print("this n this")
                        self.myActivityIndicator.stopAnimating()
                        self.errorInfoLabel.text = "Ooops! Something went wrong. Please try again."
                        
                        
                    }
                    
                    if success == nil {
                        print("there we are again")
                        self.myActivityIndicator.stopAnimating()
                        
                    }
                }
                
            }

        }
    }
    
    
    var fromQuestCompelted: Int? {
        didSet{
            if fromQuestCompelted == 42 {
                
                
                let randomnumber = Int.random(in: 1...11)
                let makename = "ri" + String(randomnumber)
                self.itemcode = makename
                
                self.newpoints = 100
                
                self.frontview.isHidden = true
                self.myActivityIndicator.stopAnimating()
                
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                }
                


            }
        }
    }
    
    var newpoints: Int?
    
    var questTag: String?
    
    var itemcode: String?
    
    lazy var lbutton: UIButton = {
       let button = UIButton()
        button.setTitle("Click Me", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(handleButton), for: .touchUpInside)
        return button
    }()
    
    

    @objc func handleButton(sender: UIButton){
        print("Working")
        

    }
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.backgroundColor = UIColor.clear
        
 collectionView?.register(PublishAndWonBodyCell.self, forCellWithReuseIdentifier: "PublishAndWonBodyCell")

        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)

        self.publishTheStory()

        
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                self.sendNotificationCenter()
                self.handleDismiss()
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    lazy var frontview = UIView()
    lazy var errorInfoLabel: UILabel = {
       let label = UILabel()
        label.numberOfLines = 0
        label.sizeToFit()
        label.font = UIFont.systemFont(ofSize: 21)
        label.textAlignment = .center
        label.text = "Publishing..."
        return label
    }()
    
    func publishTheStory() {
        print("publishing it")
        
        frontview.backgroundColor = .white
        self.view.addSubview(frontview)
        frontview.anchor(top: self.view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        frontview.addSubview(errorInfoLabel)
        errorInfoLabel.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 15, rightConstant: 15, widthConstant: 0, heightConstant: 0)

        frontview.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: frontview.leftAnchor, bottom: frontview.bottomAnchor, right: frontview.rightAnchor, topConstant:0, leftConstant: 0, bottomConstant: 40, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        myActivityIndicator.color = .black
        myActivityIndicator.startAnimating()


    }
    
    
    @objc func handleDismiss(){
        self.dismiss(animated: true, completion: nil)
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height - 100)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PublishAndWonBodyCell", for: indexPath) as! PublishAndWonBodyCell
        cell.delegate = self
        
        guard let ic = self.itemcode else { return cell }
        
        print("ic ic ic is", ic)
        
        cell.wonItemCode = ic
        
        guard let mm = self.newpoints else { return cell}
        
        if mm == 100 {
            cell.pointsLabel.text = "100 \nPoints"
            cell.publishedLabel.text = "Congratulations!"
            cell.captionLabel.text = "You've finished all required writing task for this Mini Quest."
            cell.collectButton.isEnabled = true
        }
        
        return cell

    }
    
}

// big "Published" label
// Small "Your story has been published" label
// Medium "And you have earned" label
// 10 points - Collect icon
// An Item - Only if you have - Collect Icon

protocol collectedFromAddIthaka: class {
    func collectedTapped()
}

class PublishAndWonBodyCell: UICollectionViewCell {
    
    
    var wonItemCode: String? {
        didSet{
            guard let uic = wonItemCode else { return }
            self.wonItemImageView.image = UIImage(named: uic + ".png")
            
            if uic.hasPrefix("ci"){
                let itemname = Api.Items.commonItemName[uic]
                self.itemNameLabel.text = itemname
            }
            
            if uic.hasPrefix("uci"){
                let itemname = Api.Items.unCommonItemsName[uic]
                self.itemNameLabel.text = itemname

            }
            
            if uic.hasPrefix("ri"){
                let itemname = Api.Items.rareItemsName[uic]
                self.itemNameLabel.text = itemname

            }

        }
    }
    
    weak var delegate: collectedFromAddIthaka?
    
    let stopButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        
        return button
    }()
    @objc func handleDismiss(){
        print("handle dismiss")
    }
    lazy var publishedLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.text = "Published!"
        label.textColor = UIColor.black
        label.textAlignment = .center
        return label
    }()
    lazy var captionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.text = "You story is published. Collect your rewards and Keep Writing."
        label.textColor = UIColor.gray
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        
        return label
    }()
    
    let mainFrameView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
        
        return view
    }()

    
    let bgframeview: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        
        return view
    }()
    
    let bgframeview2: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        
        return view
    }()

    
    lazy var wonItemImageView: UIImageView = {
        let imageview = UIImageView()
        
        return imageview
    }()
    
    lazy var pointsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 27)
        label.text = "10 \nPoints"
        label.numberOfLines = 2
        label.textAlignment = .center
        return label
    }()
    
    lazy var collectButton: UIButton = {
        let button = UIButton()
        button.setTitle("Collect", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = UIColor.rgb(red: 186, green: 186, blue: 191, alpha: 1)


        button.layer.cornerRadius = 15
        button.clipsToBounds = true
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.addTarget(self, action: #selector(collectButtonTapped), for: .touchUpInside)
        
        return button
    }()
    
    @objc func collectButtonTapped() {
        print("tpaped")
        
        self.collectButton.setTitle("Collected", for: .normal)
        self.collectButton.setTitleColor(UIColor.gray, for: .normal)
        self.collectButton.isEnabled = false
        
        self.delegate?.collectedTapped()
    }
    
    lazy var footerLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 11)
//        label.text = "If you delete your story. You will loose your earned items and points. If your post doesn't follow community guidelines(plagrising) we have right to remove it and you will also lose the items which you "
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        
        
        return label
    }()

    lazy var itemNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        
        
        return label
    }()
    


    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .clear
        
        addSubview(mainFrameView)
        
        addSubview(stopButton)
        
        addSubview(publishedLabel)
        addSubview(captionLabel)
        
        
        addSubview(wonItemImageView)
        addSubview(pointsLabel)
        addSubview(collectButton)
        addSubview(itemNameLabel)
        addSubview(footerLabel)
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.frame.width*0.05
            
            mainFrameView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: self.rightAnchor, topConstant: ph, leftConstant: ph, bottomConstant: ph, rightConstant:ph, widthConstant: 0, heightConstant: 0)
            
            stopButton.anchor(top: mainFrameView.topAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            
            publishedLabel.anchor(top: stopButton.bottomAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: mainFrameView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            captionLabel.anchor(top: publishedLabel.bottomAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: mainFrameView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
            
            
            wonItemImageView.anchor(top: captionLabel.bottomAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: nil, topConstant: 25, leftConstant: 55, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 100)
            
            pointsLabel.anchor(top: captionLabel.bottomAnchor, left: nil, bottom: nil, right: mainFrameView.rightAnchor, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 55, widthConstant: 150, heightConstant: 100)
            
            itemNameLabel.anchor(top: pointsLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 40)

            
            collectButton.anchor(top: itemNameLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 35, leftConstant: frame.width/2 - 100, bottomConstant: 0, rightConstant: 0, widthConstant: 200, heightConstant: 40)
            
            footerLabel.anchor(top: nil, left: mainFrameView.leftAnchor, bottom: mainFrameView.bottomAnchor, right: mainFrameView.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 100)

            footerLabel.font = UIFont.systemFont(ofSize: 14)

        default:
            print("i am Everyone except ipad")
            let mh = UIDevice().type
            let device: String = mh.rawValue
            
            switch device {
            case "iPhone 5", "iPhone 5S":
                
                mainFrameView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: self.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 15, rightConstant:15, widthConstant: 0, heightConstant: 0)
                
                stopButton.anchor(top: mainFrameView.topAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                
                publishedLabel.anchor(top: stopButton.bottomAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: mainFrameView.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                captionLabel.anchor(top: publishedLabel.bottomAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: mainFrameView.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 50)
                
                
                wonItemImageView.anchor(top: captionLabel.bottomAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: nil, topConstant: 25, leftConstant: 35, bottomConstant: 0, rightConstant: 0, widthConstant: 80, heightConstant: 80)
                
                pointsLabel.anchor(top: captionLabel.bottomAnchor, left: nil, bottom: nil, right: mainFrameView.rightAnchor, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 35, widthConstant: 100, heightConstant: 80)
                pointsLabel.textAlignment = .right
                
                itemNameLabel.anchor(top: pointsLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: -5, leftConstant: 35, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
                
                
                collectButton.anchor(top: itemNameLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: frame.width/2 - 100, bottomConstant: 0, rightConstant: 0, widthConstant: 200, heightConstant: 40)
                
                footerLabel.anchor(top: nil, left: mainFrameView.leftAnchor, bottom: mainFrameView.bottomAnchor, right: mainFrameView.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 100)
                

            default:
                
                mainFrameView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: self.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 15, rightConstant:15, widthConstant: 0, heightConstant: 0)
                
                stopButton.anchor(top: mainFrameView.topAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                
                publishedLabel.anchor(top: stopButton.bottomAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: mainFrameView.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                captionLabel.anchor(top: publishedLabel.bottomAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: mainFrameView.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 50)
                
                
                wonItemImageView.anchor(top: captionLabel.bottomAnchor, left: mainFrameView.leftAnchor, bottom: nil, right: nil, topConstant: 25, leftConstant: 35, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 100)
                
                pointsLabel.anchor(top: captionLabel.bottomAnchor, left: nil, bottom: nil, right: mainFrameView.rightAnchor, topConstant: 25, leftConstant: 0, bottomConstant: 0, rightConstant: 35, widthConstant: 150, heightConstant: 100)
                
                itemNameLabel.anchor(top: pointsLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 35, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
                
                
                collectButton.anchor(top: itemNameLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: frame.width/2 - 100, bottomConstant: 0, rightConstant: 0, widthConstant: 200, heightConstant: 40)
                
                footerLabel.anchor(top: nil, left: mainFrameView.leftAnchor, bottom: mainFrameView.bottomAnchor, right: mainFrameView.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 100)
                

            }
            
        }

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}




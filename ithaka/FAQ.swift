//
//  FAQ.swift
//  ithaka
//
//  Created by Apurva Raj on 09/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation

struct FAQ {
    
    let user: UserModel
    
    let question: String
    let answer: String
    
//    
//    init(user: User, dictionary: [String: Any]) {
//        self.user = user
//        self.question = dictionary["question"] as? String ?? ""
//        self.answer = dictionary["answer"] as? String ?? ""
//    }
}

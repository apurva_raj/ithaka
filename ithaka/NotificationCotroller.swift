//
//  NotificationCotroller.swift
//  ithaka
//
//  Created by Apurva Raj on 19/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase

class NotificationController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIViewControllerTransitioningDelegate {
    
    var notification: Notification?

    let cellId = "cellId"
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    let size = CGSize(width: 30, height: 30)
    

    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        collectionView?.backgroundColor = .white
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()


        navigationItem.title = "Notifications"
        collectionView?.backgroundColor = .white
        collectionView?.register(NotificationCell.self, forCellWithReuseIdentifier: cellId)
        
        fetchNotiicationFeed()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
//
        UIApplication.shared.applicationIconBadgeNumber = 0

        
        let values = ["badge": 0]
        guard let cuid = Auth.auth().currentUser?.uid else { return }

        Api.User.REF_USERS.child(cuid).updateChildValues(values)

        
        

        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.05
            
            self.collectionView?.contentInset = UIEdgeInsetsMake(30, ph, 100, ph)

            
        default:
            print("i am Everyone except ipad")
            self.collectionView?.contentInset = UIEdgeInsetsMake(10, 15, 100, 15)

            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.applicationIconBadgeNumber = 0

        self.tabBarController?.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)
        
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = false
//        reloadnewnotifications()

    }
    
    func reloadnewnotifications() {
        guard let cuid = Auth.auth().currentUser?.uid else { return }
 Database.database().reference().child("notifications").child(cuid).queryLimited(toLast: 1).observe(.childAdded) { (snapshot) in
    
    Api.Notification.observeSingleNotification(withUId: cuid, withPid: snapshot.key, completion: { (notificationew) in
        
        let notuid = notificationew.fromUserId ?? ""
        Api.User.observeUser(withId: notuid, completion: { (usernew) in
            
            self.notificationNew.append(notificationew)
            self.usersNew.append(usernew!)
            self.collectionView?.reloadData()
            
            Database.database().reference().child("notifications").child(cuid).removeAllObservers()
        })
    })
        }
    }
    

    
    func StartListeningForBadgeUpdates() {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.BadgeWasUpdated), name: NSNotification.Name(rawValue: "com.api4242.ithaka"), object: nil)
    }
 

    @objc func BadgeWasUpdated() {
    }

    
    @objc func handleRefresh() {

        notificationNew.removeAll()
        fetchNotiicationFeed()
    
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.1
            
            return CGSize(width: view.frame.width - ph - 30, height: 85)

            
        default:
            print("i am Everyone except ipad")
            return CGSize(width: view.frame.width - 30, height: 85)

            
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notificationNew.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
   
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! NotificationCell
        
        cell.notification = self.notificationNew[indexPath.item]
        cell.user = usersNew[indexPath.item]
        return cell
    }
    
    var forNotificationPost: NotificationPostLoadModel?
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let notification = notificationNew[indexPath.item]
        
        guard let fromuserId = notification.uid else { return }
        guard let pid = notification.pid else { return }
        
        guard let notifcsType = notification.type else { return }
        print("notivs type", notifcsType)
        switch notifcsType {
        case "23":
            let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
            profileController.userId = notification.fromUserId
          self.navigationController?.pushViewController(profileController, animated: true)
            
        case "12":
            print("pow pow")
        case "11", "42", "2312":
            
            let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
            
            guard let uid = Auth.auth().currentUser?.uid else { return }
            guard let vid = notification.fromUserId  else { return }
            let npp = NotificationPostLoadModel.transfromPostNotification(vid: vid, pid: pid)
        
            previewpostController.visitorUid = uid
            
            previewpostController.comingFromNotificationId = npp

            previewpostController.modalPresentationStyle = .fullScreen
            previewpostController.popoverPresentationController?.sourceView = self.view
            previewpostController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            
            previewpostController.popoverPresentationController?.permittedArrowDirections = []
            
            previewpostController.transitioningDelegate = self
            previewpostController.modalPresentationStyle = .custom
            previewpostController.modalPresentationCapturesStatusBarAppearance = true
            self.present(previewpostController, animated: true, completion: nil)

            
        case "1112",  "111":
            
            //presnt postui n forward to comment
            
            
        let previewpostMiniController = NotificationPostController(collectionViewLayout: UICollectionViewFlowLayout())
            
        
        guard let vid = notification.uid  else { return }
        let npp = NotificationPostLoadModel.transfromPostNotification(vid: vid, pid: pid)
        
        previewpostMiniController.fromUserId = vid
        previewpostMiniController.comingFromNotificationId = npp
        self.navigationController?.pushViewController(previewpostMiniController, animated: true)

            
        case "111232":
            let previewpostMiniController = NotificationPostController(collectionViewLayout: UICollectionViewFlowLayout())
            
            
            guard let vid = notification.fromUserId  else { return }
            let npp = NotificationPostLoadModel.transfromPostNotification(vid: vid, pid: pid)
            
            previewpostMiniController.fromUserId = vid
            
            previewpostMiniController.comingFromNotificationId = npp
            previewpostMiniController.formComment = true
            
            self.navigationController?.pushViewController(previewpostMiniController, animated: true)

            
        case "424223" :
            let questController =  DailyQuestNewController(collectionViewLayout: UICollectionViewFlowLayout())
            
            questController.hidesBottomBarWhenPushed = true

            self.navigationController?.pushViewController(questController, animated: true)
            

        default:
            print("champi")
        }
        
    
//       else {
//
//            let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
//            postController.userId = notification.uid
//            postController.postId = notification.pid
//            navigationController?.pushViewController(postController, animated: true)
//        }
    }
    var isFinishedPaging = false

    var notifications = [Notification]()
    var users = [UserModel]()

    var notificationNew = [NotificationNew]()
    var usersNew = [UserModelNew]()

    fileprivate var isLoadingNotification = false

    fileprivate func fetchNotiicationFeed() {
        
        self.tabBarController?.tabBarItem.badgeValue = String(0)
        
        isLoadingNotification = true
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        print("startttttt")
        
        Api.Notification.getRecentNotification(withId: uid, limit: 10) { (results) in
            guard let results = results else { self.myActivityIndicator.stopAnimating()
                return }

            if results.count == 0 {
                self.myActivityIndicator.stopAnimating()
                return
            }
            
            if results.count > 0 {
                results.forEach({ (result) in
                    self.notificationNew.append(result.0)
                    self.usersNew.append(result.1)
                    
                    print("new posts are", result)
                    
                })
            }
            self.isLoadingNotification = false
            self.myActivityIndicator.stopAnimating()
            self.collectionView?.reloadData()

        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
            guard let uid = Auth.auth().currentUser?.uid else { return }
            
            guard !isLoadingNotification else {
                return
            }
            isLoadingNotification = true
            print("Scrollview started")
            guard let lastPostTimestamp = self.notificationNew.last?.timestamp else {
                isLoadingNotification = false
                return
            }
            
            self.myActivityIndicator.startAnimating()
            
            let startfrom = notificationNew.last?.timestamp ?? 1
            print("last post timestamp is", lastPostTimestamp)
            Api.Notification.getOldNotificationFeed(withId: uid, start: startfrom, limit: 10, completionHandler: { (results) in
                
                guard let results = results else { self.myActivityIndicator.stopAnimating()
                    return }

                
                if results.count == 0 {
                    return
                }
                for result in results {
                    self.notificationNew.append(result.0)
                    self.usersNew.append(result.1)
                }
                self.myActivityIndicator.stopAnimating()
                self.collectionView?.reloadData()
                self.isLoadingNotification = false
            })
            
            
        }
    }
    
    
}



extension NotificationController {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? PreviewPostController {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal,
                               interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard
                let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController,
                interactionController.inProgress
                else {
                    return nil
            }
            return interactionController
    }
}



extension NotificationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return SystemPushAnimator(type: .navigation)
        case .pop:
            return SystemPopAnimator(type: .navigation)
        default:
            return nil
        }
    }
    
}

//
//  NotificationPostController.swift
//  ithaka
//
//  Created by Apurva Raj on 04/09/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import Firebase
import SwiftMessages

class NotificationPostController: UICollectionViewController, UICollectionViewDelegateFlowLayout, PostDelegate, UIViewControllerTransitioningDelegate {
    
    var pid: String?
    
    var fromUserId: String?{
        didSet{
            

        }
    }
    
    var formComment: Bool?{
        didSet{
            if formComment == true {
                
                
            }
        }
    }
    
    var comingFromNotificationId: NotificationPostLoadModel?{
        didSet{
            
            guard let pId = comingFromNotificationId?.postId else { return }
            guard let uid = comingFromNotificationId?.visitorId else { return }
            
            print("yes is ", pId)
            Api.NewPost.observeSinglePost1(withUId: uid, withPid: pId, completion: { (postnew) in
                
                
                
                Api.User.observeUser(withId: uid) { (usermodelnew) in
                    
                    if postnew?.creationDate == nil {
                        print("yahho")
                        
                        let redview: UIView = {
                            let view = UIView()
                            view.backgroundColor = .white
                            
                            return view
                        }()
                        
                        let message: UILabel = {
                            let label = UILabel()
                            label.text = "Oops! Content Removed By the Owner"
                            label.font = UIFont.systemFont(ofSize: 23)
                            label.textColor = .gray
                            label.numberOfLines = 0
                            label.lineBreakMode = .byWordWrapping
                            label.textAlignment = .center
                            
                            return label
                        }()
                        
                        self.view.addSubview(redview)
                        self.view.addSubview(message)
                        
                        redview.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 50, rightConstant: 0, widthConstant: self.view.frame.width, heightConstant: self.view.frame.height)
                        message.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: (self.view.frame.height/2) - 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 100)
                        self.myActivityIndicator.stopAnimating()
                        
                    } else {
                        self.user = usermodelnew
                        self.post = postnew
                        self.collectionView?.reloadData()
                        self.myActivityIndicator.stopAnimating()
                        
                        if self.formComment == true {
                            let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
                            commentsController.post = postnew
                            self.navigationController?.pushViewController(commentsController, animated: false)

                        }
                    }
                    
                }
            })
            
            
        }
    }

    
    func onRocketCountButtonTapped(for cell: PostPreviewHeader) {
        let rocketListController = RocketListController(collectionViewLayout: UICollectionViewFlowLayout())
        rocketListController.popoverPresentationController?.sourceView = self.view
        rocketListController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        rocketListController.popoverPresentationController?.permittedArrowDirections = []
        
        rocketListController.backbutton = "lost"
        rocketListController.post = post
        self.present(rocketListController, animated: true, completion: nil)
        
        
    }
    
    
    func didTapComment(post: PostNew) {
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.post = post
        
        commentsController.popoverPresentationController?.sourceView = self.view
        commentsController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        commentsController.backbutton = "lost"
        commentsController.popoverPresentationController?.permittedArrowDirections = []
        
        self.present(commentsController, animated: true, completion: nil)
    }
    
    func onShareButtonTapped(for cell: PostPreviewHeader) {
        print("yaho")
        
        let downloadImgaesController = DownloadImagesController()
        downloadImgaesController.post = post
        downloadImgaesController.user = user
        
        downloadImgaesController.popoverPresentationController?.sourceView = self.view
        downloadImgaesController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        downloadImgaesController.popoverPresentationController?.permittedArrowDirections = []
        
        self.present(downloadImgaesController, animated: true, completion: nil)
        
    }
    
    func onNameIdLabelTapped(userId: String) {
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        
        print("bc na", userId)
        
        profileController.popoverPresentationController?.sourceView = self.view
        profileController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        profileController.popoverPresentationController?.permittedArrowDirections = []
        profileController.userId = userId
        profileController.oneview.backgroundColor = .white
        present(profileController, animated: true, completion: nil)
    }
    
    
    func onNameLabelTapped(for cell: PostPreviewHeader) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.posts[indexPath.item]
        
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = post.user.uid
        self.present(profileController, animated: true, completion: nil)
        
        //        self.navigationController?.pushViewController(profileController,
        //                                                      animated: true)
        
    }
    
    
    
    func didTapComment(post: Post) {
        
    }
    
    func onRocketButtonTapped(for cell: PostPreviewHeader) {
        print("pritn cbc")
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        DispatchQueue.main.async {
            self.collectionView?.reloadItems(at: [indexPath])
        }

    }
    
    
    // define a variable to store initial touch position
    
    let postCellId = "postCellId"
    
    var postId: String?
    var userId: String?
    var post: PostNew?
    var user: UserModelNew?

    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()

        
        
        collectionView?.alwaysBounceVertical = true
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.backgroundColor = .white
        collectionView?.showsVerticalScrollIndicator = false
        
        collectionView?.register(PostUi.self, forCellWithReuseIdentifier: postCellId)

        let swipeDown = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(onSwipeDown))
        swipeDown.edges = .top
        
        self.view.addGestureRecognizer(swipeDown)
        
        fetchPost()
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.05
            
            collectionView?.contentInset = UIEdgeInsetsMake(30, ph, 100, ph)
            
            
        default:
            print("i am Everyone except ipad")
            collectionView?.contentInset = UIEdgeInsetsMake(20, 15, 100, 15)
            
            
        }

        
    }
    var newPosts = [PostNew]()
    var posts = [Post]()
    

    
    func fetchPost(){
        
        guard let pId = pid else { return }
        guard let uid = fromUserId else { return }
        
        
        
        Api.NewPost.observeSinglePost1(withUId: uid, withPid: pId, completion: { (postnew) in
            
            
            Api.User.observeUser(withId: uid) { (usermodelnew) in
                
                if postnew?.creationDate == nil {
                    print("yahho")
                    
                    let redview: UIView = {
                       let view = UIView()
                        view.backgroundColor = .white
                        
                        return view
                    }()
                    
                    let message: UILabel = {
                        let label = UILabel()
                        label.text = "Oops! Content Removed By the Owner"
                        label.font = UIFont.systemFont(ofSize: 23)
                        label.textColor = .gray
                        label.numberOfLines = 0
                        label.lineBreakMode = .byWordWrapping
                        label.textAlignment = .center
                        
                        return label
                    }()
                    
                    self.view.addSubview(redview)
                    self.view.addSubview(message)

                    redview.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: self.view.frame.width, heightConstant: self.view.frame.height)
                    message.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: (self.view.frame.height/2) - 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 100)
                    self.myActivityIndicator.stopAnimating()

                } else {
                    self.user = usermodelnew
                    self.post = postnew
                    self.collectionView?.reloadData()
                    self.myActivityIndicator.stopAnimating()
                }
                
            }
        })

        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postCellId, for: indexPath) as! PostUi
        
    
        
        cell.post = post
        cell.user = user
//        cell.textViewTap = self
//        cell.delegate = self
        
        
        //   collectionView.collectionViewLayout.invalidateLayout()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.10
            
            return CGSize(width: view.frame.width - ph, height: CGFloat(330))
            
            
        default:
            print("i am Everyone except ipad")
            return CGSize(width: view.frame.width - 30, height: CGFloat(330))
            
            
        }

    }
    
    @objc func onSwipeDown(_ recognizer: UISwipeGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    let currentIndex = 4
    @objc func handleDismiss(){
        self.tabBarController?.selectedIndex = currentIndex
        dismiss(animated: true, completion: nil)
        
    }
    
    fileprivate func setupNavigationButtons() {
        navigationController?.navigationBar.tintColor = .black
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(handleCancel))
        
    }
    @objc func handleCancel() {
        navigationController?.popViewController(animated: true)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
}





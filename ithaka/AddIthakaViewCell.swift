//
//  AddIthakaViewCell.swift
//  ithaka
//
//  Created by Apurva Raj on 17/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import SwiftMessages
import UITextView_Placeholder
import Firebase

class AddIthakaViewCell: BaseCollectionViewCell, UITextViewDelegate {
    
    
    weak var IthakaAddDelegate: IthakaAddDelegate?
    

    lazy var ShareLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = true
        label.text = "Share"
        label.textAlignment = .center
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: 17.5)
        label.backgroundColor = .white
        
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleShare))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
        
    }()
    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .black
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        button.isUserInteractionEnabled = true
        return button
    }()
    
    @objc func handleDismiss(){
        self.IthakaAddDelegate?.onDimissTapped()
        
    }
    let yview: UIView = {
       let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
        return view
    }()
    
    let TitleTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.boldSystemFont(ofSize: 20)
        tv.isEditable = true
        tv.backgroundColor = .white
        tv.textAlignment = .left
        tv.placeholder = "Title"
        tv.isScrollEnabled = false
        tv.tintColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.layoutSubviews()
        tv.sizeToFit()
        tv.layoutIfNeeded()
        tv.contentInset = UIEdgeInsetsMake(30, 0, 0, 0)

        return tv
    }()
    
    
    let StoryTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 20)
        tv.backgroundColor = .white
        tv.placeholder = "Start writing"
        tv.isScrollEnabled = false
        tv.tintColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        tv.contentInset = UIEdgeInsetsMake(30, 0, 0, 0)
        tv.layoutIfNeeded()
        return tv
    }()
    
    
    
    override func setupViews() {
        super.setupViews()
        TitleTextView.delegate = self
        StoryTextView.delegate = self
//        TitleTextView.becomeFirstResponder()

        addSubview(ShareLabel)
        addSubview(TitleTextView)
        addSubview(StoryTextView)
        addSubview(dismissButton)
        

        
        TitleTextView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        

        let storyHeight = (frame.height/2)

        StoryTextView.anchor(top: TitleTextView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
    }
    

    @objc func handleShare(){
        
        print("sarting to share")
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        myActivityIndicator.center = self.center
        
        self.addSubview(myActivityIndicator)
        
        myActivityIndicator.startAnimating()
        
        guard let newPostId = Api.NewPost.REF_POSTS.childByAutoId().key else { return }
        let cd = Date().timeIntervalSince1970
        guard let uid = Auth.auth().currentUser?.uid else { return }

        guard let title = TitleTextView.text, title.count > 0 else { return }
        guard let story = StoryTextView.text, story.count > 0 else { return }
        
        let titlewords = title.components(separatedBy: CharacterSet(charactersIn: "#@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_").inverted)
        let storywords = story.components(separatedBy: CharacterSet(charactersIn: "#@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_").inverted)
        
        // Adding hashtag entry to database
        for var titleword in titlewords {
            if titleword.hasPrefix("#") {
                titleword = titleword.trimmingCharacters(in: CharacterSet.punctuationCharacters)
                print(titleword)
                let hashref = Database.database().reference().child("Hashtags").child(titleword.lowercased()).child(newPostId)
                let hashtagvalue = ["uid": uid, "creationDate":cd] as [String : Any]

                hashref.updateChildValues(hashtagvalue)
            }
        }
        for var storyword in storywords {
            if storyword.hasPrefix("#") {
                storyword = storyword.trimmingCharacters(in: CharacterSet.punctuationCharacters)
                print(storyword)
                let hashref = Database.database().reference().child("Hashtags").child(storyword.lowercased()).child(newPostId)
                let hashtagvalue = ["uid": uid , "creationDate":cd] as [String : Any]

                hashref.updateChildValues(hashtagvalue)
            }
        }
        
        // adding post in auth users feed
        Api.Feed.REF_FEED.child(uid).child(newPostId).setValue(["uid":uid, "creationDate": cd])

        // adding post in every followers feed
        let followref = Database.database().reference().child("following").child(uid)
        followref.observeSingleEvent(of: .value, with: { (snapshot) in
            let arraySnapshot = snapshot.children.allObjects as! [DataSnapshot]
            arraySnapshot.forEach({ (child) in
                Api.Feed.REF_FEED.child(child.key).child(newPostId).setValue(["uid":uid, "creationDate": cd])
            })
        })
        
        // adding post to quest feed

        
        // adding post data to post tabel
        
        let userPostRef = Database.database().reference().child("posts").child(uid)
        let ref = userPostRef.child(newPostId)
        let ref3 = Database.database().reference().child("users").child(uid)
        
        let values = ["title": title, "story": story, "rocketCount": 0, "creationDate": cd, "questTag": "testing", "levelNumber": 3, "uid": uid] as [String : Any]

        ref.updateChildValues(values)
        
        //adding total post count to auth user table
        
        userPostRef.observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            
            let cc = value?.count ?? 0
            
            let postCount = ["postCount": cc]
            ref3.updateChildValues(postCount)
            
        })
        
        //showing earned points
        
        
        SwiftMessages.show {
            let view = MessageView.viewFromNib(layout: .CardView)
            view.configureTheme(.success)
            view.configureDropShadow()
            let iconText = "😍"
            view.configureContent(title: "Awesome", body: "You have earned 5 points for sharing this story", iconText: iconText)
            view.button?.isHidden = true
            
            return view
        }
        
        let notificationMessage =  "Awesome, you have earned 5 points for sharing a story: " + TitleTextView.text
        let ref4 = Database.database().reference().child("notifications").child(uid).childByAutoId()
        let notifValues = ["Message": notificationMessage, "uid":uid, "type":"12", "creationDate": Date().timeIntervalSince1970] as [String : Any]
        ref4.updateChildValues(notifValues)
        
        
        
        ref.updateChildValues(values) { (err, ref) in
            if let err = err {
//                self.navigationItem.rightBarButtonItem?.isEnabled = true
                print("Failed to save post to DB", err)
                return
            }
            
            let questTitleId = "-LHEtPJMr5FRmnKZmVOj"
            let questlevelId = "-LHFJlsgh6QCfG7xpbUP"
            let value_quest = [questlevelId:1]
            let levelNumber = "level1"
            let pLN = 1
            let ref_quest = Database.database().reference().child("QuestUserData").child(uid).child(questTitleId).child(levelNumber)
            
            ref_quest.observeSingleEvent(of: .value, with: { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                
                let count = value?.count
                print("count is ABCD", count)
                print("myids",value)
                
                let refff = Database.database().reference().child("QuestUserData").child(uid).child(questTitleId)
                
                if count == 2 {
                    let valuefornumber = ["PresentLevel": pLN + 1]
                    refff.updateChildValues(valuefornumber)
                    ref_quest.updateChildValues(value_quest)
                    
                } else {
                    let valuefornumber = ["PresentLevel": pLN]
                    refff.updateChildValues(valuefornumber)
                    
                    ref_quest.updateChildValues(value_quest)
                    
                }
                
            })
            
            
            
//            let homeController = HomeController(collectionViewLayout: UICollectionViewFlowLayout())
            
            //
            //            print("newpostId is", newPostId)
            //            let myPostRef = Api.MyPosts.REF_MYPOSTS.child(uid).child(newPostId)
            //            myPostRef.setValue(["creationDate": cd], withCompletionBlock: { (error, ref) in
            //                if error != nil {
            //                    return
            //                }
            //            })
            
//            let when = DispatchTime.now() + 2
//
//            DispatchQueue.main.asyncAfter(deadline: when, execute: {
//                homeController.newPosts.removeAll()
//                homeController.userModelNew.removeAll()
//                homeController.loadPosts()
//                DispatchQueue.main.async {
//                    homeController.collectionView?.reloadData()
//                    homeController.collectionView?.refreshControl?.endRefreshing()
//                    homeController.myActivityIndicator.stopAnimating()
//                }
//                self.navigationController?.popToRootViewController(animated: true)
                
                
//            })
            
            NotificationCenter.default.post(name: AddIthakaFinalController.updateFeedNotificationName, object: nil)
            
        }
        
        
        
    }
   
    
    func textViewDidChange(_ textView: UITextView) {
        
        self.IthakaAddDelegate?.onShareTapped()
    
    
        let titleCount = TitleTextView.text.count
        let storyCount = StoryTextView.text.count
        
        
        if (1...150).contains(titleCount){
            if storyCount == 0 {
                ShareLabel.isEnabled = false
                ShareLabel.textColor = .black
                ShareLabel.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
                
            } else {
                ShareLabel.isEnabled = true
                ShareLabel.textColor = .white
                ShareLabel.backgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
                
            }
            
            
        } else if titleCount == 0 {
            ShareLabel.isEnabled = false
            ShareLabel.textColor = .black
            ShareLabel.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
            
            
        } else {
            ShareLabel.isEnabled = false
            ShareLabel.textColor = .black
            ShareLabel.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
            
            
            SwiftMessages.show {
                let view = MessageView.viewFromNib(layout: .CardView)
                view.configureTheme(.warning)
                view.configureDropShadow()
                let iconText = "🙄"
                view.configureContent(title: "Sorry", body: "Only 150 characters are allowed", iconText: iconText)
                view.button?.isHidden = true
                
                return view
            }
            
        }
    }
}

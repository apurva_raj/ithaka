//
//  MyCustomDelegate.swift
//  ithaka
//
//  Created by Apurva Raj on 24/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit

protocol MyCustomDelegate: class {
    func onStoryViewTapped(user: UserModelNew, post: PostNew)
    func onloveButtonTapped(loc: CGPoint, for cell: PostUi)
//    func onTopicButtonTapped()
    func onTitleLabelTapped(for cell: PostUi)
    func onNameLabelTapped(for cell: PostUi)
    
    func onNameIdLabelTapped(userId: String)

    func didTapNewComment(post: PostNew)

    func onOptionButtonTapped(for cell: PostUi)
    func onEditPorfileTap(user: UserModel)
    func onEditPorfileTap(user: UserModelNew)

    func onRocketButtonTapped(for cell: PostUi)
    func onRocketCountButtonTapped(for cell: PostUi)

    func onShareButtonTapped(for cell: PostUi)

    func onEmojiTap(for cell: ProfileHeader)
    func onLogoutTapped(for cell: ProfileHeader)
    func onLevelCountTapped(user: UserModel)
    func onswipetButtonTapped(for cell: PostUi)

    func swipetoTop()
    
    func onUnblock()
    
    func onFollowersLabelTapped(user: UserModelNew)
    func onFollowingLabelTapped(user: UserModelNew)

    func onTagLabelTapped(post: Post)

    
    func onOptionButtonTappedFromProfile(for cell: PostProfileUi)
    func onTitleLabelTappedFromProfile(for cell: PostProfileUi)
    func onRocketButtonTappedFromProfile(for cell: PostProfileUi)

}

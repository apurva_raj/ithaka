//
//  PostStoryModel.swift
//  ithaka
//
//  Created by Apurva Raj on 29/03/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit


class PostStoryModel {
    var storyText: String?
    var itemCode: String?
    var questTag: String?
    var questId: String?
    var questObjectId: String?
    var level: Int?
    
}

extension PostStoryModel {
    static func transfromPostStory(st: String?, ic: String?, qt: String?, questId: String?, questObjectId: String?, level: Int?) -> PostStoryModel {
        
        let postStoryModel = PostStoryModel()
        
        postStoryModel.storyText = st ?? ""
        postStoryModel.itemCode = ic ?? ""
        postStoryModel.questTag = qt ?? ""
        postStoryModel.questId = questId ?? ""
        postStoryModel.questObjectId = questObjectId ?? ""
        postStoryModel.level = level ?? 0
        
        return postStoryModel
    }
}




class NotificationPostLoadModel {
    var visitorId: String?
    var postId: String?
    
}

extension NotificationPostLoadModel {
    static func transfromPostNotification(vid: String?, pid: String?) -> NotificationPostLoadModel {
        
        let notificationPostLoadModel = NotificationPostLoadModel()
        
        notificationPostLoadModel.visitorId = vid ?? ""
        notificationPostLoadModel.postId = pid ?? ""
        
        return notificationPostLoadModel
    }
}

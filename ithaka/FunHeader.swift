//
//  ExploreHeader.swift
//  ithaka
//
//  Created by Apurva Raj on 20/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import SwiftMessages
import SwiftyGif

class FunHeader: UICollectionViewCell {
    
    weak var delegate: QuestDelegate?

    var user: UserModel?

    var questLevel: Int? {
        didSet{

            let ql = questLevel ?? 1
            
            switch ql {
                case 1:
                secondStage.isEnabled = false
                thirdStage.isEnabled = false
//                button.setImage(#imageLiteral(resourceName: "blu1").withRenderingMode(.alwaysOriginal), for: .normal)

            case 2:
                secondStage.isEnabled = true
                secondStage.setImage(#imageLiteral(resourceName: "blu2"), for: .normal)
                thirdStage.isEnabled = false
            case 3:
                secondStage.isEnabled = true
                secondStage.setImage(#imageLiteral(resourceName: "blu2"), for: .normal)

                thirdStage.isEnabled = true
                thirdStage.setImage(#imageLiteral(resourceName: "blu3"), for: .normal)


            default:
                secondStage.isEnabled = false

                thirdStage.isEnabled = false

            }
            
        }
        
        
    }
    
    lazy var MainLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 26)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        
        textView.layoutIfNeeded()
        
        textView.text = "Ithaka Writing Quest"
        textView.textAlignment = .center
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTest))
        tap.numberOfTapsRequired = 1
        textView.addGestureRecognizer(tap)

        return textView
    }()
    
    lazy var firstStage: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "blu1").withRenderingMode(.alwaysOriginal), for: .normal)
        button.tag = 1
        let tap = UITapGestureRecognizer(target: self, action: #selector(stageTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        button.isUserInteractionEnabled = true
        return button
    }()
    
   
    
    lazy var secondStage: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icon-512@x").withRenderingMode(.alwaysOriginal), for: .normal)
        button.isEnabled = true
        button.isUserInteractionEnabled = true
        button.tag = 2
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(stageTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    lazy var thirdStage: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icon-512@x").withRenderingMode(.alwaysOriginal), for: .normal)
        button.isEnabled = false
        button.isUserInteractionEnabled = true
        button.tag = 3
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(stageTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)

        return button
    }()
    
    let firstline: UIView = {
        let uv = UIView()
        uv.backgroundColor = .red
        
        return uv
    }()
    
    @objc func stageTapped(sender: UITapGestureRecognizer) {
        guard let level = sender.view?.tag else { return }
        print("level is levedl", level)
        self.delegate?.didTapStage(level: level)
    }
    
 
    

    func checkAvatar() {
        let avatar = ProfileEditController.defaults.integer(forKey: "HeroSelectionThing")
        
        if avatar == 2 {
            
            let gif = UIImage(gifName:"Boat_with_male_Sailor")
            self.mainImage.setGifImage(gif, loopCount: -1)

        } else if avatar == 1 {
            let gif = UIImage(gifName:"Boat_with_Sailor")
            self.mainImage.setGifImage(gif, loopCount: -1)

        } else {
            return
        }
        
    }
    lazy var mainImage: UIImageView = {
       let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false

        return image
        
    }()
    
    let redimageview: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "redline").withRenderingMode(.alwaysTemplate)
        image.tintColor = .red
        return image
    }()
    
    lazy var stairImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "broken_stair")
        return imageView
    }()
    
    lazy var tentOnTheRockIV: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "tent_on_the_rock").withRenderingMode(.alwaysOriginal)
        return image
    }()

    @objc func didTest() {
        
    print("its working")
//        self.delegate?.didTapTest()
    }
    
    lazy var SecLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 36)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        textView.layoutIfNeeded()
        textView.text = "Unlock at profile level 3"
        textView.textAlignment = .center
        
        
        return textView
    }()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.white, for: .normal)
        button.tintColor = UIColor.white
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        
        return button
    }()

    @objc func handleBack(){
        self.delegate?.exit()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        checkAvatar()
//        backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)
        
        addSubview(backButton)
        backButton.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        addSubview(mainImage)
        
        
        addSubview(firstStage)
        addSubview(secondStage)
        addSubview(thirdStage)
        
        let mh = UIDevice().type
        
        let device: String = mh.rawValue

        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
                mainImage.anchor(top: nil, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: frame.height*0.35)
                
                firstStage.anchor(top: mainImage.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 55, bottomConstant: 30, rightConstant: 0, widthConstant: 60, heightConstant: 60)
                
                secondStage.anchor(top: nil, left: nil, bottom: firstStage.topAnchor, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 45, rightConstant: 50, widthConstant: 60, heightConstant: 60)
                
                
                self.addSubview(tentOnTheRockIV)
                
                tentOnTheRockIV.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 50, widthConstant: 300, heightConstant: 320)
                
                thirdStage.anchor(top: nil, left: nil, bottom: tentOnTheRockIV.bottomAnchor, right: tentOnTheRockIV.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -75, rightConstant: -60, widthConstant: 80, heightConstant: 80)
                
            
            
        
        default:
            print("i am Everyone except ipad")
            switch device {
            
                
            case "iPhone 5","iPhone 5S" :
                mainImage.anchor(top: nil, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: frame.height*0.35)
                
                firstStage.anchor(top: mainImage.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 55, bottomConstant: 30, rightConstant: 0, widthConstant: 45, heightConstant: 45)
                
                secondStage.anchor(top: nil, left: nil, bottom: firstStage.topAnchor, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 45, rightConstant: 50, widthConstant: 45, heightConstant: 45)
                
                
                self.addSubview(tentOnTheRockIV)
                
                tentOnTheRockIV.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 30, widthConstant: 200, heightConstant: 230)
                
                thirdStage.anchor(top: nil, left: nil, bottom: tentOnTheRockIV.bottomAnchor, right: tentOnTheRockIV.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -30, rightConstant: -35, widthConstant: 45, heightConstant: 45)
            case "iPhone 6S Plus", "iPhone 7 Plus", "iPhone 8 Plus":
                mainImage.anchor(top: nil, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: frame.height*0.35)
                
                firstStage.anchor(top: mainImage.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 55, bottomConstant: 30, rightConstant: 0, widthConstant: 60, heightConstant: 60)
                
                secondStage.anchor(top: nil, left: nil, bottom: firstStage.topAnchor, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 45, rightConstant: 50, widthConstant: 60, heightConstant: 60)
                
                
                self.addSubview(tentOnTheRockIV)
                
                tentOnTheRockIV.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 30, widthConstant: 200, heightConstant: 230)
                
                thirdStage.anchor(top: nil, left: nil, bottom: tentOnTheRockIV.bottomAnchor, right: tentOnTheRockIV.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -55, rightConstant: -55, widthConstant: 80, heightConstant: 80)
                
                
                
            default:
                mainImage.anchor(top: nil, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: frame.height*0.35)
                
                firstStage.anchor(top: mainImage.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 55, bottomConstant: 30, rightConstant: 0, widthConstant: 60, heightConstant: 60)
                
                secondStage.anchor(top: nil, left: nil, bottom: firstStage.topAnchor, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 45, rightConstant: 50, widthConstant: 60, heightConstant: 60)
                
                
                self.addSubview(tentOnTheRockIV)
                
                tentOnTheRockIV.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 30, widthConstant: 200, heightConstant: 230)
                
                thirdStage.anchor(top: nil, left: nil, bottom: tentOnTheRockIV.bottomAnchor, right: tentOnTheRockIV.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: -55, rightConstant: -55, widthConstant: 80, heightConstant: 80)
                
                
            }
            
            
            

            
        }

        setGradientBackground()
        self.bringSubview(toFront: backButton)

    }
    
    
    func setGradientBackground() {
        let colorTop = UIColor.rgb(red: 40, green: 84, blue: 99, alpha: 1).cgColor
        let colorBottom = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1).cgColor

        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 0.3]
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  PointsAPI.swift
//  ithaka
//
//  Created by Apurva Raj on 01/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase


class PointsAPI {
    
    let Points_REF = Database.database().reference().child("Points")
    
    
    func fetchUserPoints(uid: String, completion: @escaping (Points) -> Void) {
        let ref = Api.PointsAPII.Points_REF.child(uid)

        ref.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists(){
                guard let value = snapshot.value else { return }
                
                let points = Points.transform(dict: value as! [String : Any], key: uid)
                
                
                completion(points)

            } else {
                
                let valueRock = ["CollectedPoints": 0]
        
                ref.updateChildValues(valueRock)

            }
        }
    }
    
    func increasePoints(n_points: Int, UserId: String, catagory: Int){
        
        let points_user_ref = Points_REF.child(UserId)
        
        points_user_ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let dict = snapshot.value as? [String: Any] else { return }
            
            let points = Points.transform(dict: dict, key: snapshot.key)
        
            switch catagory {
            case 0:
                print("like points")
                var likepoints = points.likePoints ?? 0
                likepoints = likepoints + 1
                
                let value = ["likePoints": likepoints]
                
                points_user_ref.updateChildValues(value)
                
            case 1:
                print("comment points")
                var commentPoints = points.commentPoints ?? 0
                commentPoints = commentPoints + 3
                
                let value = ["commentPoints": commentPoints]
                
                points_user_ref.updateChildValues(value)

            case 3:
                print("story points")
                var storyPoints = points.storyPoints ?? 0
                storyPoints = storyPoints + 3
                
                let value = ["storyPoints": storyPoints]
                
                points_user_ref.updateChildValues(value)

            case 4:
//                questPoints
                
                var questPoints = points.collectedPoints ?? 0
                questPoints = questPoints + 3
                
                let value = ["CollectedPoints": questPoints]
                
                points_user_ref.updateChildValues(value)

                
            default:
                print("boo points")

            }

            
        }
        
    }
    
    func collectPoints(uid: String, catagory: Int){
        
        let ref = Api.PointsAPII.Points_REF.child(uid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value else { return }
            
            let points = Points.transform(dict: value as! [String : Any]
                , key: uid)
            
            switch catagory {
            case 0:
                guard let storyPoints = points.storyPoints else { return }
                var collectedPoints = points.collectedPoints ?? 0
                
                collectedPoints = collectedPoints + storyPoints
                
                let newValue = ["storyPoints": 0, "CollectedPoints": collectedPoints]
                
                ref.updateChildValues(newValue)
            case 1:
                guard let likePoints = points.likePoints else { return }
                var collectedPoints = points.collectedPoints ?? 0
                
                collectedPoints = collectedPoints + likePoints
                
                let newValue = ["likePoints": 0, "CollectedPoints": collectedPoints]
                
                ref.updateChildValues(newValue)
            case 2:
                guard let commentPoints = points.commentPoints else { return }
                var collectedPoints = points.collectedPoints ?? 0
                
                collectedPoints = collectedPoints + commentPoints
                
                let newValue = ["commentPoints": 0, "CollectedPoints": collectedPoints]
                
                ref.updateChildValues(newValue)
                
            default:
                print("You know I am on the verge of giving up")
            }

            
        }
    
    }
    
    
    
    
    // User Performs actions
    // Points are generated
    // Points are sent to the owner
    // Points are saved in uncollected count
    // Points tabel
    // UserId
    //   - Like Points
    //     - Comment Points
    //    - Story Points
    //  - Collected Points
    
    // Owener collects it and saved in total count
    
    
}

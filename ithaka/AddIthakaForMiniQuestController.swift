//
//  AddIthakaForMiniQuestController.swift
//  ithaka
//
//  Created by Apurva Raj on 15/03/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class AddIthakaForMiniQuestController: UICollectionViewController, UICollectionViewDelegateFlowLayout, AddIthakaItemSlect {
    
    
    var currentItemCodeNeeded: String?
    var questTag: String?

    var imageData: UIImage?
    var charlimit: Int?
    
    
    var currentLockCode: Int?
    var storytext: String?
    
    
    var questId: String?
    var level: Int?
    var questObjId: String?
    var questMainTitle: String?

    
    func onItemimagetapped(story: String) {
        let newcontroller = AddIthakaItemSelectionController(collectionViewLayout: UICollectionViewFlowLayout())
        newcontroller.preWrittenText = story
        newcontroller.modalPresentationStyle = .fullScreen
        newcontroller.popoverPresentationController?.sourceView = self.view
        newcontroller.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        newcontroller.popoverPresentationController?.permittedArrowDirections = []
        
        newcontroller.transitioningDelegate = self
        newcontroller.modalPresentationStyle = .custom
        newcontroller.modalPresentationCapturesStatusBarAppearance = true
        
        
        self.present(newcontroller, animated: true, completion: nil)
        
        self.view.endEditing(true)

    }
    
    var halfModalTransitioningDelegateForIthakaAdd: HalfModalTransitioningDelegateForIthakaAdd?

    
    func onShareTapped(storytext: String, itemCode: String) {
        print("needed")
        
        
        let qqt = ""
        self.storytext = ""
        self.imageData = nil
        
        
        let qid = self.questId ?? nil
        let qobid = self.questObjId ?? nil
        let ll = self.level ?? nil
        
        let ps = PostStoryModel.transfromPostStory(st: storytext, ic: itemCode, qt: qqt, questId: qid, questObjectId: qobid, level: ll)
        
        let publishController = PublishedAndWonController(collectionViewLayout: UICollectionViewFlowLayout())
        
        publishController.postStory = ps
        
        self.halfModalTransitioningDelegateForIthakaAdd = HalfModalTransitioningDelegateForIthakaAdd(viewController: self, presentingViewController: publishController)
        publishController.popoverPresentationController?.sourceView = self.view
        publishController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        publishController.popoverPresentationController?.permittedArrowDirections = []
        
        
        publishController.modalPresentationStyle = .custom
        publishController.transitioningDelegate = self.halfModalTransitioningDelegateForIthakaAdd
        
        self.present(publishController, animated: true, completion: nil)
        self.view.endEditing(true)
        

    }
    
    func onItemimagetapped() {
        let newcontroller = AddIthakaItemSelectionController(collectionViewLayout: UICollectionViewFlowLayout())
        newcontroller.modalPresentationStyle = .fullScreen
        newcontroller.popoverPresentationController?.sourceView = self.view
        newcontroller.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        newcontroller.popoverPresentationController?.permittedArrowDirections = []
        
        newcontroller.transitioningDelegate = self
        newcontroller.modalPresentationStyle = .custom
        newcontroller.modalPresentationCapturesStatusBarAppearance = true
        
        
        self.present(newcontroller, animated: true, completion: nil)
        
        self.view.endEditing(true)
    }
    
    func removeItempage() {
        print("nn")
    }
    
    func dailyQuestCardTapped(tag: Int) {
        print("nn")
    }
    
    
    
    
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        
        return button
    }()
    
    @objc func handleBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        collectionView?.register(AddIthakaStartCellForDailyQuest.self, forCellWithReuseIdentifier: "AddIthakaStartCellForDailyQuest")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        collectionView?.delegate = self
        
        setupBackButton()
        
        fetchCurrentPostImage()

        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateData), name: AddIthakaItemSelectionController.updateItemNotificationName, object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateData2), name: UnlockItemSelectionController.updateUnlockItemNotificationName, object: nil)
        
        UserDefaults.standard.set(1, forKey: "DailyQuestThing")  //Integer
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateData3), name: PublishedAndWonController.updateFeedNotificationNameForCollect, object: nil)
        
        fetchCurrentPostImage()

        
    }
    
    
    

    func fetchCurrentPostImage(){
        guard let uid = Auth.auth().currentUser?.uid else  { return }
        
        let ref = Database.database().reference().child("myItems").child(uid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            
            var ff = 1
            
            value.forEach({ (snapshot) in
                
                if ff == 1 {
                    let key = snapshot.key as! String
                    let image = UIImage(named: key + ".png")
                    self.currentItemCodeNeeded = key
                    self.imageData = image
                    ff += 1
                    if key.hasPrefix("ci"){
                        let cl = Api.Items.commonItemsRequiredChar[key]
                        self.charlimit = cl
                    }
                    
                    if key.hasPrefix("uci"){
                        let cl = Api.Items.unCommonItemsRequiredChar[key]
                        self.charlimit = cl
                        
                    }
                    
                    if key.hasPrefix("ri"){
                        let cl = Api.Items.rareItemsRequiredChar[key]
                        self.charlimit = cl
                    }

                }
                
            })
            
            self.collectionView?.reloadSections([0])
            
        }
        
        
    }

    
    @objc func handleUpdateData3(){
        DispatchQueue.main.async {
            
            
            self.fetchCurrentPostImage()
            
            
            self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: true)
            
        }
        
    }
    @objc func handleUpdateData(_ notification: NSNotification){
        print("yes")
        if let itemName = notification.userInfo?["itemName"] as? String {
            
            
            // do something with your image
            let iinm = itemName + ".png"
            
            self.imageData = UIImage(named: iinm)
            
            if itemName.hasPrefix("ci"){
                let cl = Api.Items.commonItemsRequiredChar[itemName]
                self.charlimit = cl
            }
            
            if itemName.hasPrefix("uci"){
                let cl = Api.Items.unCommonItemsRequiredChar[itemName]
                self.charlimit = cl
                
            }
            
            if itemName.hasPrefix("ri"){
                let cl = Api.Items.rareItemsRequiredChar[itemName]
                self.charlimit = cl
            }
            
            let story = notification.userInfo?["story"] as! String
            self.storytext = story
            self.currentItemCodeNeeded = itemName
            
            print("final pretext is ", story)
            
            DispatchQueue.main.async {
                self.collectionView?.reloadSections([0])
                
            }
        }
        
    }
    
    @objc func handleUpdateData2(_ notification: NSNotification){
        if let itemName = notification.userInfo?["itemName"] as? String {
            
            print("oiiasdkasbfsajbf,jsa ", itemName)
            
            if itemName.hasPrefix("unlocki") {
                print("yey, unlcoked mini quest")
                
                self.currentLockCode = 1
                
            }
            
            DispatchQueue.main.async {
                self.collectionView?.reloadSections([1])
            }
        }
        
    }
    
    
    var hhh: CGFloat?
    
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    

    func setupBackButton() {
        
        view.addSubview(backButton)
        backButton.anchor(top: nil, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 40, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaStartCellForDailyQuest", for: indexPath) as! AddIthakaStartCellForDailyQuest
        
        cell.requireChars = 55
        cell.delegate = self
        cell.StoryTextView.text = storytext ?? ""
        let im = self.imageData
        let cl = self.charlimit ?? 40
        cell.profileItemView.image = im
        cell.minimumWordsRequired.text = String(cl)
        cell.requireChars = cl
        cell.currentItemCode = self.currentItemCodeNeeded ?? ""

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var finalHeight: CGFloat?
        
        let tabbarHEIGHT = self.tabBarController?.tabBar.frame.height ?? 49.0

        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("yey")
            let ph = self.view.frame.width*0.1

            if  UserDefaults.standard.object(forKey: "keyboardHeight") != nil {
                finalHeight = UserDefaults.standard.object(forKey: "keyboardHeight") as? CGFloat
                
            } else {
                finalHeight = 300
                
                NotificationCenter.default.addObserver(self, selector: .keyboardWillShow, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
                
            }
            
            let fh = finalHeight ?? 250
            

            return CGSize(width: view.frame.width - ph + 30, height: view.frame.height + 20 - fh - tabbarHEIGHT)

        default:
        
            
            if  UserDefaults.standard.object(forKey: "keyboardHeight") != nil {
                finalHeight = UserDefaults.standard.object(forKey: "keyboardHeight") as? CGFloat
                
                print("finalheight is ", finalHeight)
            } else {
                finalHeight = 300
                
                NotificationCenter.default.addObserver(self, selector: .keyboardWillShow, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
                
            }
            
            let fh = finalHeight ?? 250
            return CGSize(width: view.frame.width, height: view.frame.height + 30 - fh - tabbarHEIGHT)

        
        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
}


extension AddIthakaForMiniQuestController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? PreviewPostController {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal,
                               interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard
                let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController,
                interactionController.inProgress
                else {
                    return nil
            }
            return interactionController
    }
}



extension AddIthakaForMiniQuestController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return SystemPushAnimator(type: .navigation)
        case .pop:
            return SystemPopAnimator(type: .navigation)
        default:
            return nil
        }
    }
    
}


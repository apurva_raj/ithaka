//
//  StageDelegate.swift
//  ithaka
//
//  Created by Apurva Raj on 30/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//


import UIKit

protocol StageDelegate: class {
    
    func onQ1Tapped()
    func onQTapped(level: Int, checkpoint: Int, title: String)
    func onQ1Tapped(title: String, id: String)
}


//
//  AddIthakaStoryContainer.swift
//  ithaka
//  Created by Apurva Raj on 29/09/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import Foundation
import Firebase
import SwiftMessages
import UITextView_Placeholder


protocol AddIthakaDelegate: class {
    
    func onTypeTapped(for cell: QuestMain)
    
    func onTtapped(for cell: QuestCards)
    
    func onTapQuestTitle(id: String)
    
    func didTapStage(level: Int)
    
    func stopLoadingAnimation()
}

protocol IthakaAddDelegate: class {
    func onShareTapped()
    func onDimissTapped()
}
class AddIthakaStoryController: UIViewController, UITextViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, IthakaAddDelegate, UITextFieldDelegate {
    
    
    var questDaily: String? {
        didSet{
            if questDaily == "daily" {
                let dailymsg = WriteTodaysQuest()
                self.addChildViewController(dailymsg)
                self.view.addSubview(dailymsg.view)
                dailymsg.didMove(toParentViewController: self)
                
            }
        }
    }
    
//    func hideKeyboardWhenTappedAround() {
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("oh bc")
        StoryTextView.becomeFirstResponder()
        return true
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    

    func onShareTapped() {
        return
        
    }
    
    func onDimissTapped() {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    var level: Int?
    var checkPoint: Int?
    var questMainTitle: String?

    var questId: String?
    var questObjId: String?
    var questTag: String? {
        didSet{
            questTagLabel.text = questTag
            questTagLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 20)

        }
        
    }
    
    var tofix: String?{
        didSet{
            questTagLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)

        }
    }
    var addedTag: String? {
        didSet {

        }
    }
    
    let addTitleLabel: UILabel = {
       let label = UILabel()
       label.isUserInteractionEnabled = false
       label.text = "WRITE TITLE"
       label.textAlignment = .center
       label.textColor = .gray
       label.font = UIFont.systemFont(ofSize: 14)
       label.tintColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)

       return label
    
    }()
    
    
    
    let TitleTextView: UITextField = {
        let tv = UITextField()
        tv.font = UIFont.boldSystemFont(ofSize: 20)
//        tv.isEditable = true
        tv.backgroundColor = .white
        tv.textAlignment = .left
        tv.placeholder = "Title"
        tv.tintColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        tv.backgroundColor = .white
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.sizeToFit()
        tv.layoutIfNeeded()
//        tv.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
//        tv.isScrollEnabled = true
        tv.addTarget(self, action: #selector(AddIthakaStoryController.textFieldDidChange(_:)), for: UIControlEvents.editingChanged)

        
        return tv
    }()
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        let titleCount = TitleTextView.text?.count ?? 0
       
        if (1...150).contains(titleCount){
            sharevalue = 25
            if sharevalue2 == 23 {
                navigationItem.rightBarButtonItem?.isEnabled = true
            }
            
        } else {
            sharevalue = 0
            if titleCount == 0 {
                navigationItem.rightBarButtonItem?.isEnabled = true
            } else {
                navigationItem.rightBarButtonItem?.isEnabled = true
                
                SwiftMessages.show {
                    let view = MessageView.viewFromNib(layout: .CardView)
                    view.configureTheme(.warning)
                    view.configureDropShadow()
                    let iconText = "🙄"
                    view.configureContent(title: "Sorry", body: "Only 150 characters are allowed", iconText: iconText)
                    view.button?.isHidden = true
                    
                    return view
                }
            }
            
            
            
        }

    }

    let StoryTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 20)
        tv.backgroundColor = .white
        tv.placeholder = "Start writing..."
        tv.isScrollEnabled = true
        tv.tintColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.layer.cornerRadius = 15
        tv.clipsToBounds = true
        tv.sizeToFit()
        tv.layoutIfNeeded()
        tv.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)
        tv.backgroundColor = .white
        tv.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5)
        return tv
    }()
    
    let questTagLabel: UILabel = {
        let textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 11)
        textView.textAlignment = .center
        textView.textColor = UIColor.gray
        textView.isUserInteractionEnabled = false
        return textView
    }()

    
   let scrollView = UIScrollView()
    let views = UIView()
    var num = CGFloat(12)
    
    var sharevalue: Int?
    var sharevalue2: Int?
    
    
    
    func textViewDidChange(_ textView: UITextView) {
        let storyCount = StoryTextView.text.count
        
        if (1...5000).contains(storyCount){
            sharevalue2 = 23

            if sharevalue == 25 {
                navigationItem.rightBarButtonItem?.isEnabled = true
            }
            
            
        } else {
           if storyCount == 0 {
                print("kai nai")
            sharevalue2 = 0

                self.navigationItem.rightBarButtonItem?.isEnabled = false

            } else {
                self.navigationItem.rightBarButtonItem?.isEnabled = false
                sharevalue2 = 0
                SwiftMessages.show {
                    let view = MessageView.viewFromNib(layout: .CardView)
                    view.configureTheme(.warning)
                    view.configureDropShadow()
                    let iconText = "🙄"
                    view.configureContent(title: "Sorry", body: "Only 5000 characters are allowed", iconText: iconText)
                    view.button?.isHidden = true
                    
                    return view
                }

            }
            
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
//        hideKeyboardWhenTappedAround()
        
        
//        view.addSubview(colorPickerView)

        
        scrollView.isScrollEnabled = true
        num = CGFloat(10)
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 242)
        view.addSubview(questTagLabel)
        view.addSubview(StoryTextView)
        StoryTextView.delegate = self

            let gg = view.frame.height/2 - 75
            
        StoryTextView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: gg)
        
//        colorPickerView.anchor(top: StoryTextView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
    
        
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self
            , action: #selector(handleShare))
        navigationItem.rightBarButtonItem?.isEnabled = true
        fetchUser()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(AddIthakaStoryController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        
        

    }
    

    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    
    

    var username: String?
    
    func fetchUser() {
        Api.User.observeCurrentUser { (usermodelNew) in
            let uname = usermodelNew.u_username
            self.username = uname
        }
    }
    
    fileprivate func setupNavigationButton() {
        navigationController?.navigationBar.tintColor = .blue
       
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self
            , action: #selector(handleShare))
        navigationItem.rightBarButtonItem?.isEnabled = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        tabBarController?.tabBar.isHidden = false
        self.questTagLabel.text = ""
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.isHidden = false

    }

    @objc func handleShare(){
        
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        myActivityIndicator.center = view.center
        
        view.addSubview(myActivityIndicator)
        
        
        myActivityIndicator.startAnimating()

        guard let newPostId = Api.NewPost.REF_POSTS.childByAutoId().key else { return }
        let cd = Date().timeIntervalSince1970
        guard let uid = Auth.auth().currentUser?.uid else { return }

        guard let title = TitleTextView.text, title.count > 0 else { return }
        guard let story = StoryTextView.text, story.count > 0 else { return }
        
        let titlewords = title.components(separatedBy: CharacterSet(charactersIn: "#@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_").inverted)
        let storywords = story.components(separatedBy: CharacterSet(charactersIn: "#@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_").inverted)

        // adding to rocket tabel
        
        let rocketValue = ["rocketCount": 0]
        Database.database().reference().child("rockets").child(newPostId).updateChildValues(rocketValue)

        // adding post data to post tabel
                 let userPostRef = Database.database().reference().child("posts").child(uid)
        let ref = userPostRef.child(newPostId)
        let ref3 = Database.database().reference().child("users").child(uid)
        
        let values = ["title": title, "story": story, "rocketCount": 0, "creationDate": cd, "questTag": questTagLabel.text ?? "", "questId": questId ?? "", "uid": uid] as [String : Any]
        
        ref.updateChildValues(values)
        
        
        // Adding hashtag entry to database
        
        let hashrefs = Database.database().reference().child("HashtagsSearch")

        for var titleword in titlewords {
            if titleword.hasPrefix("#") {
                titleword = titleword.trimmingCharacters(in: CharacterSet.punctuationCharacters).lowercased()
                print(titleword)
                let hashref = Database.database().reference().child("Hashtags").child(titleword).child(newPostId)
                let hashtagvalue = ["uid": uid, "creationDate":cd] as [String : Any]
                
                let hhs = ["tag": titleword]

                hashref.updateChildValues(hashtagvalue)
                hashrefs.child(titleword).updateChildValues(hhs)
            }
        }
        for var storyword in storywords {
            if storyword.hasPrefix("#") {
                storyword = storyword.trimmingCharacters(in: CharacterSet.punctuationCharacters).lowercased()
                print(storyword)
                let hashref = Database.database().reference().child("Hashtags").child(storyword).child(newPostId)
                let hashtagvalue = ["uid": uid , "creationDate":cd] as [String : Any]
                let hhs = ["tag": storyword]

                hashref.updateChildValues(hashtagvalue)
                hashrefs.child(storyword).updateChildValues(hhs)

            }
        }
        
        
        // adding post in auth users feed
        Api.Feed.REF_FEED.child(uid).child(newPostId).setValue(["uid":uid, "creationDate": cd])
        // adding post in every followers feed
        let followref = Database.database().reference().child("following").child(uid)
        followref.observeSingleEvent(of: .value, with: { (snapshot) in
            let arraySnapshot = snapshot.children.allObjects as! [DataSnapshot]
            arraySnapshot.forEach({ (child) in
                Api.Feed.REF_FEED.child(child.key).child(newPostId).setValue(["uid":uid, "creationDate": cd])
            })
        })


        // adding post to quest feed
        let questObjId = self.questObjId ?? ""

        let questTitleId = questId ?? ""

        if questTitleId.count == 0 {
            print("bye bye")
            
        } else {
            let QuestTagValue = ["uid": uid, "creationDate":cd] as [String : Any]
            let questFeedREF = Database.database().reference().child("questFeed").child(questTitleId).child(newPostId)

            questFeedREF.updateChildValues(QuestTagValue)

        }
        
        //adding quest data to questuserdata
        
//        levelNumber
//        questobjid
//        questiD
        var mainp = 0

         let questLevel = level ?? 0
        
        //        let qlevelstring = String(questLevel)
        
        print(questTitleId,questObjId,questLevel)
        if questTitleId.count == 0 {
            print("waay good")
            
        } else {
            let value_quest = [questObjId:true]
            let levelnumber = "level\(questLevel)"
            
            let refff = Database.database().reference().child("QuestUserData").child(uid).child(questTitleId)
            
            let ref_quest = Database.database().reference().child("QuestUserData").child(uid).child(questTitleId).child(levelnumber)
            
            refff.observeSingleEvent(of: .value) { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                var presentLevel = 1
                if value == nil {
                     presentLevel =  1
                } else {
                     presentLevel = value!["PresentLevel"] as? Int ?? 1

                }
                
                
                //okay this should work
                ref_quest.observeSingleEvent(of: .value, with: { (snapshot) in
                    if snapshot.exists() {
                        guard let refvalue = snapshot.value as? NSDictionary else { return }
                        
                        print("count is sdsa, ", refvalue.count)
                        if refvalue.count == 2 {
                            
                            if presentLevel == 7 {
                                mainp = 23
                                ref_quest.updateChildValues(value_quest)
                                self.congretsCard1()
                                
                                let notificationMessage =  "Awesome, you have earned 100 for completing this quest"
                                
                                let ref4 = Database.database().reference().child("notifications").child(uid).childByAutoId()
                                
                                let notifValues = ["Message": notificationMessage, "uid":uid, "type":"12", "creationDate": cd] as [String : Any]
                                
                                ref4.updateChildValues(notifValues)


                            } else {
                                let valuefornumber = ["PresentLevel": presentLevel + 1]
                                refff.updateChildValues(valuefornumber)
                                ref_quest.updateChildValues(value_quest)
                                self.congretsCard()

                            }
                            
                        } else {
                            
                            let valuefornumber = ["PresentLevel": presentLevel]
                            refff.updateChildValues(valuefornumber)
                            ref_quest.updateChildValues(value_quest)
                            
                        }

                    } else {
                        let valuefornumber = ["PresentLevel": presentLevel]
                        refff.updateChildValues(valuefornumber)
                        ref_quest.updateChildValues(value_quest)

                    }
                })
                
              
                
            }
            
        }
        
        
        //updating total post count
        
        let refcountall = Database.database().reference().child("allpostcount")
        
        refcountall.observeSingleEvent(of: .value) { (snapshot) in
            
                guard let value = snapshot.value as? NSDictionary else { return }
                
                var allcount = value["count"] as? Int ?? 0
                
                allcount = allcount + 1
                
                let ccvalue = ["count": allcount]
                refcountall.updateChildValues(ccvalue)
            
            
        }
        
        
        //adding total post count to auth user table

        ref3.observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary

            let cc = value?["postCount"] as? Int ?? 0
            let count = cc + 1
            
            // adding points to my profile

            var points = value?["profileProgress"] as? Int ?? 0
            
            //suppose point is 5 . then add =
            
            let badge = value?["badge"] as? Int ?? 0
            let bv = badge + 1

            let bcc = String(bv)
            if mainp == 23 {
                var questcount = value?["questCompleted"] as? Int ?? 0
                questcount = questcount + 1
                points = points + 110
                let tp = ["profileProgress": points, "questCompleted": questcount, "postCount": count, "badge":bv ]
                ref3.updateChildValues(tp)
                
            } else {
                points = points + 10
                let tp = ["profileProgress": points, "postCount": count, "badge":bv]
                ref3.updateChildValues(tp)
                
            }

            
            //         Sending user self notification
            
            
            let notificationMessage =  "Awesome, you have earned 10 points for sharing story: " + title
            
            let ref4 = Database.database().reference().child("notifications").child(uid).childByAutoId()
            
            
            let notifValues = ["Message": notificationMessage, "fromUserId":"b5MKdFi5mkcJgQC12hEOjJOT3MB3", "uid":uid, "badge":bcc, "type":"12", "creationDate": cd] as [String : Any]
            
            ref4.updateChildValues(notifValues)
            


        })
        


        //showing earned points
        SwiftMessages.show {
            let view = MessageView.viewFromNib(layout: .CardView)
            view.configureTheme(.success)
            view.configureDropShadow()
            let iconText = "😍"
            view.configureContent(title: "Awesome", body: "You have earned 10 points", iconText: iconText)
            view.button?.isHidden = true
        
            return view
         }


        
        
        // sending mention notification
        
        let username = self.username ?? ""
        let ref_username = Database.database().reference().child("users")
        
        for var titleword in titlewords {
            if titleword.hasPrefix("@") {
                titleword = titleword.trimmingCharacters(in: CharacterSet.punctuationCharacters).lowercased()
                print("the word is,", titleword)
                if titleword.count != 0 {
                    ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
                        
                        if snapshot.exists() {
                            
                            Api.User.observeUserByUsername(username: titleword, completion: { (usermodelnew) in
                                
                                guard let uuid = usermodelnew.uid else { return }
                                
                                if uuid == uid {
                                    
                                    print("bye bye")
                                } else {
                                    let notificationMessage = username + " has mentioned you in post: " + title
                                    
                                    let notifValues = ["Message": notificationMessage, "uid":uuid, "fromUserId":uid, "pid":newPostId, "type":"42", "creationDate": cd] as [String : Any]
                                    
                                    let ref10 = Database.database().reference().child("notifications").child(uuid).childByAutoId()
                                    
                                    ref10.updateChildValues(notifValues)
                                }
                                
                            })
                            
                            
                        } else {
                            print("north remembers")
                            
                        }
                    }
                    
                }
                
            }
        }

        
        for var storyword in storywords {
            if storyword.hasPrefix("@") {
                storyword = storyword.trimmingCharacters(in: CharacterSet.punctuationCharacters).lowercased()
                print("the word is,", storyword)
                if storyword.count != 0 {
                    ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
                        
                        if snapshot.exists() {
                            
                            Api.User.observeUserByUsername(username: storyword, completion: { (usermodelnew) in
                                
                                guard let uuid = usermodelnew.uid else { return }
                                
                                if uuid == uid {
                                    
                                    print("bye bye")
                                } else {
                                    let notificationMessage = username + " has mentioned you in post: " + title
                                    
                                    let notifValues = ["Message": notificationMessage, "uid":uuid, "fromUserId":uid, "pid":newPostId, "type":"42", "creationDate": cd] as [String : Any]
                                    
                                    let ref10 = Database.database().reference().child("notifications").child(uuid).childByAutoId()
                                    
                                    ref10.updateChildValues(notifValues)
                                }

                            })
                            
                            
                        } else {
                            print("north remembers")
                            
                        }
                    }

                }
    
            }
        }

        
        TitleTextView.text = ""
        StoryTextView.text = ""

        DispatchQueue.main.async {
            
            self.tabBarController?.selectedIndex = 0
        self.navigationController?.popToRootViewController(animated: true)
            
            NotificationCenter.default.post(name: AddIthakaFinalController.updateFeedNotificationName, object: nil)
            
        }

    }
    
    fileprivate func congretsCard() {
        SwiftMessages.show {
            let view = MessageView.viewFromNib(layout: .CenteredView)
            view.configureTheme(.warning)
            view.configureDropShadow()
            let iconText = "😍"
            view.configureContent(title: "Congratulations", body: "You now have unlocked next level", iconText: iconText)
            view.button?.isHidden = true
            
            return view
        }

    }
    
    fileprivate func congretsCard1() {
        SwiftMessages.show {
            let view = MessageView.viewFromNib(layout: .CenteredView)
            view.configureTheme(.warning)
            view.configureDropShadow()
            let iconText = "😍"
            view.configureContent(title: "+100 Points - EPIC", body: "You now completed this Quest", iconText: iconText)
            view.button?.isHidden = true
            
            return view
        }
        
    }

    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        self.StoryTextView.becomeFirstResponder()
        self.navigationController?.navigationBar.isHidden = true


//        self.collectionView?.contentOffset.x = 0
    }
    
//
//    func textViewDidEndEditing(_ textView: UITextView) {
//        re
//    }
    static let updateFeedNotificationName = NSNotification.Name(rawValue: "UpdateFeed")

    
    fileprivate func saveToDatabaseWithoutImage() {
        
        
    }
    
}




//
//  PostDelegate.swift
//  ithaka
//
//  Created by Apurva Raj on 03/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

protocol PostDelegate: class {
    
    func didTapComment(post: PostNew)
    func onNameLabelTapped(for cell: PostPreviewHeader)
    func onNameIdLabelTapped(userId: String)

    func onRocketButtonTapped(for cell: PostPreviewHeader)
    func onShareButtonTapped(for cell: PostPreviewHeader)
    func onRocketCountButtonTapped(for cell: PostPreviewHeader)

}

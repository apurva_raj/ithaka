//
//  FollowersController.swift
//  ithaka
//
//  Created by Apurva Raj on 21/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase

class FollowerController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    
    let cellId = "cellId"
    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 20, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()

        collectionView?.delegate = self
        collectionView?.backgroundColor = .white
        
        collectionView?.register(FollowersList.self, forCellWithReuseIdentifier: cellId)
       
        collectionView?.register(TitleHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        

        collectionView?.contentInset = UIEdgeInsetsMake(10, 0, 15, 15)
        fetchUser()
        print("me", users.count)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 55)
    }
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! TitleHeader
        header.titleLabel.text = "Following"
        header.backgroundColor = .white
        
        return header
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 60)

    }
  
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! FollowersList
        
        cell.user = users[indexPath.item]

        return cell

    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return users.count
        
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let userNew = users[indexPath.item]
        print(userNew)
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = userNew.uid
     navigationController?.pushViewController(profileController,animated: true)

    }
  
    
    
    var users = [UserModelNew]()

    var filteredUsers = [UserModelNew]()

    var user: UserModelNew?
    
    fileprivate func fetchUser() {
        print("Fetching users..")
        guard let uid = user?.uid else { return }
        
        let ref = Database.database().reference().child("followers").child(uid)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let dictionaries = snapshot.value as? [String: Any] else { return }
            
            print("Dd is", dictionaries)
            dictionaries.forEach({ (key, value) in
                print("key is", key)
                
                Database.database().reference().child("users").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    guard let userDictionary = snapshot.value as? [String: Any] else { return }
                    let user = UserModelNew.transformUser(dictionary: userDictionary, key: uid)
//                    var user = UserModel(uid: uid, dictionary: userDictionary)
                    user.uid = snapshot.key
                    self.users.append(user)
                    self.collectionView?.reloadData()


            })
        
            self.myActivityIndicator.stopAnimating()
            self.collectionView?.reloadData()


        })

    }

}
}

//
//  HashtagAPI.swift
//  ithaka
//
//  Created by Apurva Raj on 23/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseDatabase

class HashtagAPI {
    var REF_HASHTAGS = Database.database().reference().child("Hashtags")
    
    func getRecentHashtagfeed(withTag hashtag: String, start timestamp: TimeInterval? = nil, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]) -> Void) {
        
        var feedQuery = REF_HASHTAGS.child(hashtag).queryOrdered(byChild: "creationDate")

        if let latestPostTimestamp = timestamp, latestPostTimestamp > 0 {
            feedQuery = feedQuery.queryStarting(atValue: latestPostTimestamp + 1 , childKey: "creationDate").queryLimited(toLast: limit)
        } else {
            feedQuery = feedQuery.queryLimited(toLast: limit)
        }

        feedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects
            let myGroup = DispatchGroup()
            
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in (items as! [DataSnapshot]).enumerated() {
                myGroup.enter()
                let value = item.value as? NSDictionary
                let uid = value!["uid"] as? String ?? ""

                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                        if PostNew?.uid == UserModelNew?.uid {
                            results.append((PostNew!, UserModelNew!))
                            //                            results.insert((PostNew, UserModelNew), at: 0)
                            
                        }
                        myGroup.leave()
                        
                    })
                })
            }
            myGroup.notify(queue: .main) {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            }
            
            
        })
    }
    
    
    func getOldHashtagFeed(withId id: String, start timestamp: TimeInterval, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]) -> Void) {
        
        let feedOrderQuery = REF_HASHTAGS.child(id).queryOrdered(byChild: "creationDate")
        
        let feedLimitedQuery = feedOrderQuery.queryEnding(atValue: timestamp - 1, childKey: "creationDate").queryLimited(toLast: limit)
        
        
        
        feedLimitedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects as! [DataSnapshot]
            
            let myGroup = DispatchGroup()
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in items.enumerated() {
                print(item)
                
                myGroup.enter()
                let value = item.value as? NSDictionary
                let uid = value!["uid"] as? String ?? ""

                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                        if PostNew?.uid == UserModelNew?.uid {
                            results.append((PostNew!, UserModelNew!))
                            
                        }
                        myGroup.leave()
                        
                    })
                })
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            })
            
        })
        
    }

}

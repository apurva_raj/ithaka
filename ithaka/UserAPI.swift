//
//  UserAPI.swift
//  ithaka
//
//  Created by Apurva Raj on 04/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class UserApi {
    var REF_USERS = Database.database().reference().child("users")
    

    func observeUserByUsername(username: String, completion: @escaping (UserModelNew) -> Void) {
        REF_USERS.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .childAdded, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                let user = UserModelNew.transformUser(dictionary: dict, key: snapshot.key)
                completion(user)
            }
        })
    }

    
    func observeUser(withId uid: String, completion: @escaping (UserModelNew?) -> Void) {
        REF_USERS.child(uid).observeSingleEvent(of: .value, with: {
            snapshot in
            
            if snapshot.exists() {
                if let dict = snapshot.value as? [String: Any] {
                    let user = UserModelNew.transformUser(dictionary: dict, key: snapshot.key)
                    completion(user)
                }
            }

        })
    }
    
    func observeCurrentUser(completion: @escaping (UserModelNew) -> Void) {
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        REF_USERS.child(currentUser.uid).observeSingleEvent(of: .value, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                let user = UserModelNew.transformUser(dictionary: dict, key: snapshot.key)
                completion(user)
            }
        })
    }
    
    func observeUsers(completion: @escaping (UserModelNew) -> Void) {
        REF_USERS.observe(.childAdded, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                let user = UserModelNew.transformUser(dictionary: dict, key: snapshot.key)
                completion(user)
            }
        })
    }
    
    var CURRENT_USER: User? {
        if let currentUser = Auth.auth().currentUser {
            return currentUser
        }
        return nil
    }
    
    var REF_CURRENT_USER: DatabaseReference? {
        guard let currentUser = Auth.auth().currentUser else {
            return nil
        }
        
        return REF_USERS.child(currentUser.uid)
    }
    
    
    
}

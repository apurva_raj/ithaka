//
//  SuggestedPeople.swift
//  ithaka
//
//  Created by Apurva Raj on 25/01/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit

class SuggestedPeople: UICollectionViewController, UICollectionViewDelegateFlowLayout, test1023, ExploreBodyDelegate {
    func openUserProfile(id: String) {
        print("DAmed")
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = id
        self.navigationController?.pushViewController(profileController, animated: true)
    }
    
    
    func tap12(){
        print("Reached here")
        self.collectionView?.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

//        setupTopBar()
        setupCollectionView()
        setupMenuBar()
        

        self.navigationItem.title = "Ithakians"
        

    }
    let newview = UIView()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        return button
    }()
    
    func setupTopBar() {
        
        newview.backgroundColor = .white
        
        let newview1 = UIView()
        newview1.backgroundColor = .white
        self.view.addSubview(newview1)
        newview1.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        view.addSubview(newview)
        
        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        
        newview.addSubview(backButton)
        backButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        view.bringSubview(toFront: backButton)
        
        
    }
    
    @objc func handleBack(){
        self.navigationController?.popViewController(animated: true)
    }

    func setupCollectionView() {
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        
        collectionView?.delegate = self
        self.collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.backgroundColor = UIColor.white
        collectionView?.register(AmazingWritersCell.self, forCellWithReuseIdentifier: "AmazingWritersCell")
        
        collectionView?.isPagingEnabled = true
        collectionView?.isUserInteractionEnabled = true

    }
    
    lazy var menuBar: NewMeWritersMenuBar = {
        let mb = NewMeWritersMenuBar()
        mb.homeController = self
        return mb
    }()
    
    
    fileprivate func setupMenuBar() {
        
        view.addSubview(menuBar)
        menuBar.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
        
    }
    
    let titles = ["Daily Quest", "Monster Quest", "ada", "me"]

    
    fileprivate func setTitleForIndex(_ index: Int) {
        if let titleLabel = navigationItem.titleView as? UILabel {
            titleLabel.text = "  \(titles[index])"
        }
        
    }
    
    
    func scrollToMenuIndex(_ menuIndex: Int) {
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        setTitleForIndex(menuIndex)
    }
    

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AmazingWritersCell", for: indexPath) as! AmazingWritersCell
        
        cell.delegate = self
        
        if indexPath.item == 0 {
            cell.tableTitle = "TopWritersLongList"
        } else {
            cell.tableTitle = "NewWritersList"
        }

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height - 250)
    }
    let topConstraintRange = (CGFloat(0)..<CGFloat(140))

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        var previousOffset = CGFloat(0)
//
//        var rect = self.view.frame
//        rect.origin.y += previousOffset - scrollView.contentOffset.y
//        previousOffset = scrollView.contentOffset.y
//        self.view.frame = rect
        
        menuBar.horizontalBarLeftAnchorConstraint?.constant = scrollView.contentOffset.x / 2
        
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let index = targetContentOffset.pointee.x / view.frame.width
        
        let indexPath = IndexPath(item: Int(index), section: 0)
        menuBar.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionViewScrollPosition())
        
        setTitleForIndex(Int(index))
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    
}

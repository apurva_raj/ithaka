////
////  CommentsController.swift
////  ithaka
////
////  Created by Apurva Raj on 31/08/17.
////  Copyright © 2017 Apurva Raj. All rights reserved.
////
//
//
//import UIKit
//import Firebase
//
//class CommentsController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
//    
//    var post: Post?
//    
//    let cellId = "cellId"
//    var timer:Timer? = nil
//    var lpgr:UILongPressGestureRecognizer? = nil
//    
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        
//        navigationItem.title = "Comments"
//        
//        collectionView?.backgroundColor = .white
//        
//        
//        collectionView?.register(CommentCell.self, forCellWithReuseIdentifier: cellId)
//        fetchComments()
//        
//    }
//    
//    var comments = [Comment]()
//    fileprivate func fetchComments() {
//        guard let postId = self.post?.id else { return }
//        let ref = Database.database().reference().child("comments").child(postId)
//        ref.observe(.childAdded, with: { (snapshot) in
//            
//            guard let dictionary = snapshot.value as? [String: Any] else { return }
//            
//            guard let uid = dictionary["uid"] as? String else { return }
//            
//            Database.fetchUserWithUID(uid: uid, completion: { (user) in
//                
//                var comment = Comment(user: user, dictionary: dictionary)
//                comment.id = snapshot.key
//                self.comments.append(comment)
//                self.collectionView?.reloadData()
//            })
//            
//        }) { (err) in
//            print("Failed to observe comments")
//        }
//    }
//    
//    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return comments.count
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        
//        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
//        let dummyCell = CommentCell(frame: frame)
//        dummyCell.comment = comments[indexPath.item]
//        dummyCell.layoutIfNeeded()
//        
//        let targetSize = CGSize(width: view.frame.width, height: 1000)
//        let estimatedSize = dummyCell.systemLayoutSizeFitting(targetSize)
//        
//        let height = max(40 + 8 + 8, estimatedSize.height)
//        return CGSize(width: view.frame.width, height: height)
//    }
//    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
//    
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CommentCell
//        
//        cell.comment = self.comments[indexPath.item]
//        return cell
//    }
//    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        tabBarController?.tabBar.isHidden = true
//    }
//    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        tabBarController?.tabBar.isHidden = false
//    }
//    
//    lazy var submitButton: UIButton = {
//        let button = UIButton(type: .custom)
//        button.isEnabled = false
//        button.setTitle("Submit", for: .normal)
//        button.setTitleColor(.gray, for: .normal)
//        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
//        button.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
//        
//        return button
//    }()
//    
//    lazy var containerView: UIView = {
//        let containerView = UIView()
//        containerView.backgroundColor = .white
//        containerView.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
//        
//        
//        containerView.addSubview(submitButton)
//        submitButton.anchor(top: containerView.topAnchor, left: nil, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 12, widthConstant: 50, heightConstant: 0)
//        
//        containerView.addSubview(self.commentTextField)
//        self.commentTextField.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: submitButton.leftAnchor, topConstant: 0, leftConstant: 12, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        
//        let lineSeparatorView = UIView()
//        lineSeparatorView.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230, alpha: 1)
//        containerView.addSubview(lineSeparatorView)
//        lineSeparatorView.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: nil, right: containerView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
//        
//        return containerView
//    }()
//    
//    let commentTextField: UITextField = {
//        let textField = UITextField()
//        textField.placeholder = "Enter Comment"
//        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
//        
//        return textField
//    }()
//    
//    @objc func textFieldDidChange(_ textField: UITextField) {
//        let commentCount = commentTextField.text?.characters.count
//        
//        if (commentCount == 0){
//            submitButton.isEnabled = false
//            submitButton.setTitleColor(.gray, for: .normal)
//            
//            
//        } else {
//            submitButton.isEnabled = true
//            submitButton.setTitleColor(.black, for: .normal)
//        }
//    }
//    
//    
//    @objc func handleSubmit() {
//        guard let uid = Auth.auth().currentUser?.uid else { return }
//        guard let toUserId = post?.user.uid else { return }
//        
//        
//        print("post id:", self.post?.id ?? "")
//        
//        print("Inserting comment:", commentTextField.text ?? "")
//        
//        let postId = self.post?.id ?? ""
//        let values = ["text": commentTextField.text ?? "", "creationDate": Date().timeIntervalSince1970, "uid": uid] as [String : Any]
//        
//        Database.database().reference().child("comments").child(postId).childByAutoId().updateChildValues(values) { (err, ref) in
//            
//            if let err = err {
//                print("Failed to insert comment:", err)
//                return
//            }
//            
//            print("Successfully inserted comment.")
//            self.view.window?.endEditing(true)
//            self.commentTextField.placeholder = " "
//            self.commentTextField.text = " "
//            if(uid == toUserId) {
//                return
//            } else {
//                guard let username = Auth.auth().currentUser?.displayName else { return }
//                guard let postname = self.post?.title else { return }
//                let notificationMessage = username + " commented on your post: " + postname
//                
//                let ref = Database.database().reference().child("notifications").child(toUserId).childByAutoId()
//                
//                let notifValues = ["Message": notificationMessage, "uid":toUserId, "pid":postId]
//                
//                ref.updateChildValues(notifValues) { (err, ref) in
//                    if let err = err {
//                        print("failed to save notification", err)
//                        return
//                    }
//                }
//            }
//        }
//        
//        let ref2 = Database.database().reference().child("comments").child(postId)
//        let ref3 = Database.database().reference().child("posts").child(toUserId).child(postId)
//        
//        ref2.observeSingleEvent(of: .value) { (snapshot) in
//            let value = snapshot.value as? NSDictionary
//            let cc = value?.count
//            
//            let updates = ["commentsCount": cc]
//            ref3.updateChildValues(updates)
//        }
//        
//        
//    }
//    
//    override var inputAccessoryView: UIView? {
//        get {
//            return containerView
//        }
//    }
//    
//    override var canBecomeFirstResponder: Bool {
//        return true
//    }
//    
//    
//    
//}
//
//

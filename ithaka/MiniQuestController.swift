//
//  MiniQuestController.swift
//  ithaka
//
//  Created by Apurva Raj on 11/03/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase


class MiniQuestController: UICollectionViewController, UICollectionViewDelegateFlowLayout, QuestDelegate {
    
    
    func onTypeTapped(for cell: QuestMain) {
        return
    }
    
    func onTtapped(for cell: QuestCards) {
        return
    }
    
    func onTapQuestTitle(id: String) {
        return
    }
    
    func onTapQuestTitle(id: String, title: String) {
        return
    }
    
    var questmaintitle = "-LHEtPJRa3J9ezajpwVj"

    func didTapStage(level: Int) {
        print("level id is", level)
        let cv = QuestLevelController(collectionViewLayout: UICollectionViewFlowLayout())
        cv.questId = questTitleId
        cv.level = level
        cv.questMainTitle = questmaintitle
        
        cv.hidesBottomBarWhenPushed = true
        navigationController?.pushViewController(cv, animated: true)
        
    }
    
    func stopLoadingAnimation() {
        return
    }
    
    func exit() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    // There we are again. First need to define logic for selection and playing and story around it. Yeh, that sounds great. Only three level. Now Design the three level mini quest super fast, as soon as you can...
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant:0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()

        self.collectionView?.backgroundColor = UIColor.rgb(red: 40, green: 84, blue: 99, alpha: 1)
        collectionView?.register(FunHeader.self, forCellWithReuseIdentifier: "cellId")
        collectionView?.isScrollEnabled = false

        fetchLevel()

        fetchUser()
        
        checkIfQuestComepleted()

    }
    var halfModalTransitioningDelegateForIthakaAdd: HalfModalTransitioningDelegateForIthakaAdd?

    fileprivate func checkIfQuestComepleted(){
        
        if UserDefaults.standard.integer(forKey: "questCompleted") ==  23 {
        
            
            
            myActivityIndicator.startAnimating()
            Api.User.observeCurrentUser { (user) in
                let qc = user.questCompletedModel
                
                if qc == true {
                    
                
                    
                    let publishController = PublishedAndWonController(collectionViewLayout: UICollectionViewFlowLayout())
                    publishController.fromQuestCompelted = 42

                    guard let uid = user.uid else { return }
                    Api.PointsAPII.increasePoints(n_points: 0, UserId: uid, catagory: 4)

                    self.halfModalTransitioningDelegateForIthakaAdd = HalfModalTransitioningDelegateForIthakaAdd(viewController: self, presentingViewController: publishController)
                    publishController.popoverPresentationController?.sourceView = self.view
                    publishController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                    publishController.popoverPresentationController?.permittedArrowDirections = []
                    
                    
                    publishController.modalPresentationStyle = .custom
                    publishController.transitioningDelegate = self.halfModalTransitioningDelegateForIthakaAdd
                    
                    self.present(publishController, animated: true, completion: nil)
                    
                    // setup questComeltedModel and plist
                    
                    UserDefaults.standard.set(22, forKey: "questCompleted")  //Integer
                    let userref = Database.database().reference().child("users").child(uid)
                    
                    let vvmm = ["questComepleted": false]
                    
                    userref.updateChildValues(vvmm)

                    
                }
                
            }
            
            myActivityIndicator.stopAnimating()
        }
        
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 1
    }
    
    var level: Int?

    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! FunHeader
        cell.user = self.user
        cell.questLevel = level
        cell.delegate = self
        
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }
    
    var user: UserModel?
    
    var questTitleId = "-LHEtPJRa3J9ezajpwVj"
    
  

    func fetchLevel() {
        
        let uid = Auth.auth().currentUser?.uid ?? ""
        let questTitleID = questTitleId
        let ref = Database.database().reference().child("QuestUserData").child(uid).child(questTitleID)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let values = snapshot.value as? [String: Any] else { return }
            let levelnumber = values["PresentLevel"] as? Int ?? 0
            self.level = levelnumber
            
            print("levelNumber is", levelnumber)
        }
    }
    
    
    fileprivate func fetchUser() {
        
        let uid = Auth.auth().currentUser?.uid ?? ""
        
        Database.fetchUserWithUID(uid: uid) { (user) in
            self.user = user
            self.collectionView?.reloadData()
            
        }
        myActivityIndicator.stopAnimating()
    }



}

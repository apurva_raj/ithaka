//
//  MonsterQuestDurationCell.swift
//  ithaka
//
//  Created by Apurva Raj on 04/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit


class MonsterQuestDurationCell: UICollectionViewCell {
    

    lazy var timeSelect1: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(16)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
//        label.layer.borderColor = UIColor.black.cgColor
//        label.layer.borderWidth = 1
        label.layer.cornerRadius = 5
        label.backgroundColor = .white
        label.clipsToBounds = true
        return label
    }()
    
    lazy var timeSelect2: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(16)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.layer.cornerRadius = 5
        label.backgroundColor = .white
        label.clipsToBounds = true

        return label
    }()
    
    lazy var timeSelect3: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(16)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.backgroundColor = .white

        return label
    }()
    lazy var timeSelect4: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(16)
        label.textAlignment = .center
        label.layer.cornerRadius = 5
        label.clipsToBounds = true
        label.backgroundColor = .white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()

        return label
    }()
    
    lazy var bg1: UIView = {
        let iv = UIView()
        iv.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        iv.layer.cornerRadius = 5
        return iv
    }()

    
    
    fileprivate func setupStackView() {
        
        let timeStackView = UIStackView(arrangedSubviews: [timeSelect1, timeSelect2, timeSelect3, timeSelect4])
        timeStackView.axis = .horizontal
        timeStackView.distribution = .fillEqually
        timeStackView.spacing = 10
        addSubview(timeStackView)
        
        
        timeStackView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 70)
        
        
    }
    
    func setupTime() {
        let a1text = NSMutableAttributedString(string: "40", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18)])
        
        a1text.append(NSAttributedString(string: "\nWords", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
    
        let a2text = NSMutableAttributedString(string: "100", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18)])
        
        a2text.append(NSAttributedString(string: "\nWords", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
        
        let a3text = NSMutableAttributedString(string: "300", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18)])
        
        a3text.append(NSAttributedString(string: "\nWords", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
        
        let a4text = NSMutableAttributedString(string: "1000", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18)])
        
        a4text.append(NSAttributedString(string: "\nWords", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))

        
        timeSelect1.attributedText = a1text
        timeSelect2.attributedText = a2text
        timeSelect3.attributedText = a3text
        timeSelect4.attributedText = a4text

        
        print("zz is ", a2text)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupStackView()
        setupTime()
        
        self.backgroundColor = UIColor.clear
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

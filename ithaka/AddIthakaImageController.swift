//
//  AddIthakaImageController.swift
//  ithaka
//
//  Created by Apurva Raj on 01/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import Photos

private extension UICollectionView {
    func indexPathsForElements(in rect: CGRect) -> [IndexPath] {
        let allLayoutAttributes = collectionViewLayout.layoutAttributesForElements(in: rect)!
        return allLayoutAttributes.map { $0.indexPath }
    }
}

class AddIthakaImageController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var addedStory: String?
    var addedTitle: String?

    let cellId = "cellId"
    let headerId = "headerId"
   

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        
        setupNavigationButtons()
    
        collectionView?.register(PhotoSelectorCell.self, forCellWithReuseIdentifier: cellId)
        
        collectionView?.register(PhotoSelectorHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)

        
    }

    var selectedImage: UIImage?


    fileprivate func assetsFetchOptions() -> PHFetchOptions {
        let fetchOptions = PHFetchOptions()
        fetchOptions.fetchLimit = 30
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        fetchOptions.sortDescriptors = [sortDescriptor]
        return fetchOptions
    }


    fileprivate func setupNavigationButtons() {
        navigationController?.navigationBar.tintColor = .black
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancel))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(handleNext))
    }
    
    @objc func handleNext() {
        let addIthakaFinalController = AddIthakaFinalController()
        addIthakaFinalController.addedStory = addedStory
        addIthakaFinalController.addedTitle = addedTitle
//        addIthakaFinalController.selectedImage = header?.photoImageView.image
        navigationController?.pushViewController(addIthakaFinalController, animated: true)
    }
    
    @objc func handleCancel() {
        navigationController?.popViewController(animated: true)
    }
}



//
//  TrophyQuestController.swift
//  ithaka
//
//  Created by Apurva Raj on 24/09/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import GTProgressBar
import FirebaseDatabase
import FirebaseAuth

class TrophyQuestController: UIViewController {
    
    
    var userId: String?{
        didSet{
            guard let userId = userId else { return }
            Api.User.observeUser(withId: userId) { (user) in
                guard let profileProgress = user?.profileProgress else { return }
                
                guard let uid = user?.uid else { return }
                
                let ref = Database.database().reference().child("users").child(uid)
                
                ref.observeSingleEvent(of: .value) { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    
                    let avatar = value?["Avatar"] as? Int ?? 0
                    
                    if avatar == 11 {
                        self.boatImageView.image = #imageLiteral(resourceName: "boatMan")
                    } else if avatar == 23 {
                        self.boatImageView.image = #imageLiteral(resourceName: "boatGirl")
                        
                    } else {
                        return
                    }
                    
                }
                
                let pp = CGFloat(profileProgress)
                
                let approximateCount = pp
                let naturalCount: Int
                let pProgress: CGFloat
                let number = ((4*approximateCount-95).squareRoot() + 15)/10
                print("number is", number)
                
                switch approximateCount {
                case 0...29:
                    naturalCount = 1
                    pProgress = (approximateCount)/29
                    self.levelProgressBar.progress = pProgress
                    
                case 30...105:
                    naturalCount = 2
                    pProgress = (approximateCount-30)/75
                    self.levelProgressBar.progress = pProgress
                    
                case 106...995030:
                    naturalCount = Int(((4*approximateCount-95).squareRoot() + 15)/10)
                    print("numberInt is", naturalCount)
                    let newm = CGFloat(naturalCount)
                    pProgress = number - newm
                    self.levelProgressBar.progress = pProgress
                    
                    
                default:
                    naturalCount = 1000
                }
                
                print("naturalcount ", naturalCount)
                
                let profileLevelString = String(describing: naturalCount)
                
                let levelCountText = NSMutableAttributedString(string: "Level ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: UIColor.white])
                
                levelCountText.append(NSAttributedString(string: profileLevelString, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)]))
                //            levelCountText.append(attachmentString)
                
                self.levelCount.attributedText = levelCountText
                
                
            
            }
        }
    }
    
    var user: UserModelNew? {
        didSet {
            guard let profileProgress = user?.profileProgress else { return }

            guard let uid = user?.uid else { return }

            let ref = Database.database().reference().child("users").child(uid)

            ref.observeSingleEvent(of: .value) { (snapshot) in
                let value = snapshot.value as? NSDictionary
                
                let avatar = value?["Avatar"] as? Int ?? 0
                
                if avatar == 11 {
                    self.boatImageView.image = #imageLiteral(resourceName: "boatMan")
                } else if avatar == 23 {
                    self.boatImageView.image = #imageLiteral(resourceName: "boatGirl")
                    
                } else {
                    return
                }
                
            }
            
            let pp = CGFloat(profileProgress)
            
            let approximateCount = pp
            let naturalCount: Int
            let pProgress: CGFloat
            let number = ((4*approximateCount-95).squareRoot() + 15)/10
            print("number is", number)
            
            switch approximateCount {
            case 0...29:
                naturalCount = 1
                pProgress = (approximateCount)/29
                levelProgressBar.progress = pProgress
                
            case 30...105:
                naturalCount = 2
                pProgress = (approximateCount-30)/75
                levelProgressBar.progress = pProgress
                
            case 106...995030:
                naturalCount = Int(((4*approximateCount-95).squareRoot() + 15)/10)
                print("numberInt is", naturalCount)
                let newm = CGFloat(naturalCount)
                pProgress = number - newm
                levelProgressBar.progress = pProgress
                
                
            default:
                naturalCount = 1000
            }
            
            print("naturalcount ", naturalCount)
            
            let profileLevelString = String(describing: naturalCount)
            
            let levelCountText = NSMutableAttributedString(string: "Level ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: UIColor.white])
            
        levelCountText.append(NSAttributedString(string: profileLevelString, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)]))
            //            levelCountText.append(attachmentString)
            
            levelCount.attributedText = levelCountText


        }
    }
   
    let bg1: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        
        return view
    }()
    
    let bg2: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        
        return view
    }()
    
    lazy var yourProgress: UILabel = {
        let label = UILabel()
        label.text = "Your Progress"
        label.font = UIFont.boldSystemFont(ofSize: 23)
        label.textAlignment = .center
        return label
    }()

    
    lazy var reachlevel: UILabel = {
        let label = UILabel()
        label.text = "Reach LEVEL 10 And Win"
        label.font = UIFont.boldSystemFont(ofSize: 22)
        label.textAlignment = .center
        return label
    }()
    
    let winList: UILabel = {
        let label = UILabel()
        label.text =  "- Bonus 1000 points\n- Two real copies of novels \n   (For first 5)\n- A trophy \n   (It’ll be shown to your profile)\n- A virtual pet for your Avatar."
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        
        return label
    }()

    
    let trophyImage: UIImageView = {
            let image = UIImageView()
            image.image = #imageLiteral(resourceName: "Armor_10").withRenderingMode(.alwaysOriginal)
        
            return image
    }()
    
    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        
        return button
    }()
    
    @objc func handleDismiss(){
        dismiss(animated: true, completion: nil)
        
    }

    let oneview = UIView()
    lazy var progressBackground: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 3
        return view
    }()

    let levelBar: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return view
    }()
    
    let levelProgressBar: GTProgressBar = {
        let bar = GTProgressBar()
        // bar.progress = 0.3
        bar.barBorderColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        bar.barFillColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        bar.barBackgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 0.2)
        bar.barBorderWidth = 1
        bar.barFillInset = 2
        bar.labelTextColor = UIColor.darkGray
        bar.progressLabelInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        bar.font = UIFont.boldSystemFont(ofSize: 16)
        bar.labelPosition = GTProgressBarLabelPosition.right
        bar.barMaxHeight = 12
        //        bar.direction = GTProgressBarDirection.anticlockwise
        
        return bar
    }()
    
    lazy var levelCount: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(10)
        label.numberOfLines = 0
        label.textColor = .white
        label.layer.borderWidth = 2
        label.layer.cornerRadius = 15
        label.layer.borderColor = UIColor.gray.cgColor
        
        label.backgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.isUserInteractionEnabled = true
        
        return label
        
    }()

    let boatImageView: UIImageView = {
        let imageview = UIImageView()
        
        return imageview
    }()
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)

        oneview.backgroundColor = .black
        
        view.addSubview(trophyImage)
        trophyImage.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 30, leftConstant: (view.frame.width/2) - 50, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 100)

        view.addSubview(bg1)
        
        bg1.anchor(top: trophyImage.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 200)
        
        bg1.addSubview(reachlevel)
        bg1.addSubview(winList)
        
        reachlevel.anchor(top: bg1.topAnchor, left: bg1.leftAnchor, bottom: nil, right: bg1.rightAnchor, topConstant: 10, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)

        let mh = UIDevice().type

        let device: String = mh.rawValue
        
        switch device {
        case "iPhone 5", "iPhone 5S":
            winList.anchor(top: reachlevel.bottomAnchor, left: bg1.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 130)

            winList.font = UIFont.systemFont(ofSize: 17)
        default:
            winList.anchor(top: reachlevel.bottomAnchor, left: bg1.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 150)

            winList.font = UIFont.systemFont(ofSize: 20)

        }
        
        view.addSubview(progressBackground)
        view.addSubview(yourProgress)
        view.addSubview(levelProgressBar)
        view.addSubview(levelCount)
        view.addSubview(levelBar)
        view.addSubview(boatImageView)

        view.addSubview(dismissButton)
        view.addSubview(oneview)
        
        dismissButton.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        oneview.anchor(top: view.safeAreaLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        progressBackground.anchor(top: nil, left: view.leftAnchor, bottom: dismissButton.topAnchor, right: view.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 20, rightConstant: 20 , widthConstant: 0, heightConstant: 120)
        
        yourProgress.anchor(top: progressBackground.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        levelCount.anchor(top: yourProgress.bottomAnchor, left: progressBackground.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 30)
        
        levelProgressBar.anchor(top: levelCount.bottomAnchor, left: progressBackground.leftAnchor, bottom: nil, right: progressBackground.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)

    }
    
}

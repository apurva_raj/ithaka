//
//  QuestMainDelegate.swift
//  ithaka
//
//  Created by Apurva Raj on 10/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation


protocol QuestMainDelegate: class {
    
    func didTapQuestType(for cell: QuestCards)
    func didTapQuestTitle(id: String)
    func didTapQuestTitle(id: String, title: String)

}

//
//  MonsterPlaygroundFirstHalf.swift
//  ithaka
//
//  Created by Apurva Raj on 11/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit


class MonsterPlaygroundFirstHalf: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.backgroundColor = .green
    }
    
}

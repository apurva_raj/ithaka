//
//  Direction1.swift
//  ZLSwipeableViewSwift
//
//  Created by Andrew Breckenridge on 5/17/16.
//  Copyright © 2016 Andrew Breckenridge. All rights reserved.
//

import UIKit


//HalfModalInteractiveTransitionForIthakaAdd
class HalfModalInteractiveTransition: UIPercentDrivenInteractiveTransition {
    var viewController: UIViewController
    var presentingViewController: UIViewController?
    var panGestureRecognizer: UIPanGestureRecognizer
    
    var shouldComplete: Bool = false
    
    init(viewController: UIViewController, withView view:UIView, presentingViewController: UIViewController?) {
        self.viewController = viewController
        self.presentingViewController = presentingViewController
        self.panGestureRecognizer = UIPanGestureRecognizer()
        
        super.init()
        
//        self.panGestureRecognizer.addTarget(self, action: #selector(onPan))
//        view.addGestureRecognizer(panGestureRecognizer)
    }
    
    override func startInteractiveTransition(_ transitionContext: UIViewControllerContextTransitioning) {
        super.startInteractiveTransition(transitionContext)
        print("start interactive")
    }
    
    override var completionSpeed: CGFloat {
        get {
            return 1.0 - self.percentComplete
        }
        set {}
    }
    
    @objc func onPan(pan: UIPanGestureRecognizer) -> Void {
        let translation = pan.translation(in: pan.view?.superview)
        
        switch pan.state {
        case .began:
            self.presentingViewController?.dismiss(animated: true, completion: nil)
            
            break
            
        case .changed:
            let screenHeight = UIScreen.main.bounds.size.height - 50
            let dragAmount = screenHeight
            let threshold: Float = 0.2
            var percent: Float = Float(translation.y) / Float(dragAmount)
            
            percent = fmaxf(percent, 0.0)
            percent = fminf(percent, 1.0)
            
            update(CGFloat(percent))
            
            shouldComplete = percent > threshold
            
            break
            
        case .ended, .cancelled:
            if pan.state == .cancelled || !shouldComplete {
                cancel()
                
                print("cancel transition")
            }
            else {
                finish()
                
                print("finished transition")
            }
            
            break
            
        default:
            cancel()
            
            break
        }
    }
}


class HalfModalTransitionAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    
    var type: HalfModalTransitionAnimatorType
    
    init(type:HalfModalTransitionAnimatorType) {
        self.type = type
    }
    
    @objc func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let _ = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let from = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: { () -> Void in
            
            from!.view.frame.origin.y = 800
            
            print("animating...")
            
        }) { (completed) -> Void in
            print("animate completed")
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.4
    }
}

internal enum HalfModalTransitionAnimatorType {
    case Present
    case Dismiss
}

class HalfModalTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    var viewController: UIViewController
    var presentingViewController: UIViewController
    var interactionController: HalfModalInteractiveTransition
    
    var interactiveDismiss = false
    
    init(viewController: UIViewController, presentingViewController: UIViewController) {
        self.viewController = viewController
        self.presentingViewController = presentingViewController
        self.interactionController = HalfModalInteractiveTransition(viewController: self.viewController, withView: self.presentingViewController.view, presentingViewController: self.presentingViewController)
        
        super.init()
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return HalfModalTransitionAnimator(type: .Dismiss)
    }
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfModalPresentationController(presentedViewController: presented, presenting: presenting)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        if interactiveDismiss {
            return self.interactionController
        }
        
        return nil
    }
    
}









public typealias ZLSwipeableViewDirection1 = Direction1

extension Direction1: Equatable {}
public func ==(lhs: Direction1, rhs: Direction1) -> Bool {
    return lhs.rawValue == rhs.rawValue
}

/**
 *  Swiped Direction1.
 */
public struct Direction1 : OptionSet, CustomStringConvertible {
    
    public var rawValue: UInt
    
    public init(rawValue: UInt) {
        self.rawValue = rawValue
    }
    
    public static let None = Direction1(rawValue: 0b0000)
    public static let Left = Direction1(rawValue: 0b0001)
    public static let Right = Direction1(rawValue: 0b0010)
    public static let Up = Direction1(rawValue: 0b0100)
    public static let Down = Direction1(rawValue: 0b1000)
    public static let Horizontal: Direction1 = [Left, Right]
    public static let Vertical: Direction1 = [Up, Down]
    public static let All: Direction1 = [Horizontal, Vertical]
    
    public static func fromPoint(point: CGPoint) -> Direction1 {
        switch (point.x, point.y) {
        case let (x, y) where abs(x) >= abs(y) && x > 0:
            return .Right
        case let (x, y) where abs(x) >= abs(y) && x < 0:
            return .Left
        case let (x, y) where abs(x) < abs(y) && y < 0:
            return .Up
        case let (x, y) where abs(x) < abs(y) && y > 0:
            return .Down
        case (_, _):
            return .None
        }
    }
    
    public var description: String {
        switch self {
        case Direction1.None:
            return "None"
        case Direction1.Left:
            return "Left"
        case Direction1.Right:
            return "Right"
        case Direction1.Up:
            return "Up"
        case Direction1.Down:
            return "Down"
        case Direction1.Horizontal:
            return "Horizontal"
        case Direction1.Vertical:
            return "Vertical"
        case Direction1.All:
            return "All"
        default:
            return "Unknown"
        }
    }
    
}

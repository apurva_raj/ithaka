//
//  CommentAPI.swift
//  ithaka
//
//  Created by Apurva Raj on 03/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class CommentAPI {
    
    
    func incrementLikes(userId: String, commentId: String, postId: String, onSucess: @escaping (CommentNew) -> Void, onError: @escaping (_ errorMessage: String?) -> Void) {

        let commentRef = Database.database().reference().child("comment_likes").child(postId).child(commentId)
        let commentRef1 = Api.Comment.REF_COMMENT.child(postId).child(commentId)
        let usersRef = Database.database().reference().child("users").child(userId)
        
        commentRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            let uui = Auth.auth().currentUser?.uid
            if var comment = currentData.value as? [String : AnyObject], let uid = uui {
                
                
                var likeCount = comment["likeCount"] as? Int ?? 0
                if let _ = comment[uid] {
                    likeCount -= 1
                    comment.removeValue(forKey: uid)
                } else {
                    likeCount += 1
                    comment[uid] = true as AnyObject
                }
                comment["likeCount"] = likeCount as AnyObject?
                print("dd", comment)
                
                let rocketValue = ["likeCount": likeCount]
                commentRef1.updateChildValues(rocketValue)
                currentData.value = comment
                
                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
        }) { (error, committed, snapshot) in
            if let error = error {
                onError(error.localizedDescription)
            }
            if let dict = snapshot?.value as? [String: Any] {
                let comment = CommentNew.transformComment(dict: dict, key: (snapshot!.key))
                
                onSucess(comment)
            }
        }
        
        
    }

    
    var REF_COMMENT = Database.database().reference().child("comments")
    
    func observeSingleComment(withPId  pid: String, withCid cid: String,  completion: @escaping (CommentNew) -> Void) {
    
        
        REF_COMMENT.child(pid).child(cid).observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.exists() {
                if let dict = snapshot.value as? [String: Any] {
                    let commentNew = CommentNew.transformComment(dict: dict, key: snapshot.key)
                    
                    
                    guard let current_uid = Auth.auth().currentUser?.uid else { return }
                    
                    Database.database().reference().child("comment_likes").child(pid).child(cid).child(current_uid).observeSingleEvent(of: .value, with: { (snapshot) in
                        
                        print("sadasdasdasdasd 12321", snapshot.value)
                        if let value = snapshot.value as? Bool, value == true {
                            commentNew.isLiked = true
                        } else {
                            commentNew.isLiked = false
                        }

                    })
                    completion(commentNew)

                }
            }
            
        }
    }
    
    
    
    func getRecentComments(withId id: String, start timestamp: TimeInterval? = nil, limit: UInt, completionHandler: @escaping ([(CommentNew, UserModelNew)]?) -> Void){
        
        var feedQuery = REF_COMMENT.child(id).queryOrdered(byChild: "creationDate")
        
        if let latestPostTimestamp = timestamp, latestPostTimestamp > 0 {
            feedQuery = feedQuery.queryStarting(atValue: latestPostTimestamp + 1 , childKey: "creationDate").queryLimited(toLast: limit)
        } else {
            feedQuery = feedQuery.queryLimited(toLast: limit)
        }
        
        
        feedQuery.observeSingleEvent(of: .value) { (snapshot) in
            let items = snapshot.children.allObjects
            let myGroup = DispatchGroup()

            var results: [(commnet: CommentNew, user: UserModelNew)] = []
            
            for (_, item) in (items as! [DataSnapshot]).enumerated() {
                myGroup.enter()
                
                self.observeSingleComment(withPId: id, withCid: item.key, completion: { (commentNew) in
                    
                    guard let fromuserid = commentNew.uid else { return }

                    Api.User.observeUser(withId: fromuserid, completion: { (UserModelNew) in
                        results.append((commentNew,UserModelNew!))
                        
                        myGroup.leave()
                    })
                })
            }
            myGroup.notify(queue: .main) {
                results.sort(by: {$0.0.timestamp ?? 0 > $1.0.timestamp ?? 0 })
                results.reverse()
                completionHandler(results)

        }


    }
    
  }
}

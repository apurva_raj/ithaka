//
//  MonsterQuestCard.swift
//  ithaka
//
//  Created by Apurva Raj on 22/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit

class MonsterQuestCard: UICollectionViewCell {
    
    
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.image =  #imageLiteral(resourceName: "book")
        
        return iv
    }()
    
    lazy var questLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(30)
        label.textAlignment = .center
        label.text = "Daily Quest"
        return label
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        
        self.backgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        
        questLabel.clipsToBounds = true
        //        addSubview(imageView)
        
        addSubview(questLabel)
        questLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 20, rightConstant: 20, widthConstant: 0, heightConstant: 50)

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



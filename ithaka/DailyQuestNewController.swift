//
//  DailyQuestNewController.swift
//  ithaka
//
//  Created by Apurva Raj on 22/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase


class DailyQuestNewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, dailyQuestHeaderProtocol {
    
    func backbuttonTapped() {
        print("Step 2")
        self.navigationController?.popViewController(animated: true)
    }
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)

        
        return button
    }()

    @objc func handleBack(){
        self.backbuttonTapped()
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        collectionView?.register(dailyQuestHeaderCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "dailyQuestHeaderCell")
        
        collectionView?.register(viewfordailyquest.self, forCellWithReuseIdentifier: "viewfordailyquest")
        navigationItem.title = "Daily Quest"
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.05
            
            collectionView?.contentInset = UIEdgeInsetsMake(55, ph, 0, ph)

            
        default:
            print("i am Everyone except ipad")
            collectionView?.contentInset = UIEdgeInsetsMake(55, 0, 0, 0)

            
        }
        fetchQuests()

        
        setupTopbar()
    }
    
    
    func setupTopbar(){
        let newview = UIView()
        newview.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        
        let newview1 = UIView()
        newview1.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        self.view.addSubview(newview1)
        newview1.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)


        view.addSubview(newview)

        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)

        
        newview.addSubview(backButton)
        backButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        view.bringSubview(toFront: backButton)

    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dailyQuest.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "dailyQuestHeaderCell", for: indexPath) as! dailyQuestHeaderCell
        
        header.delegate = self
        
        return header

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.1
            
            return CGSize(width: view.frame.width - ph, height: 280)

            
        default:
            print("i am Everyone except ipad")
            return CGSize(width: view.frame.width, height: 280)

            
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "viewfordailyquest", for: indexPath) as! viewfordailyquest
        cell.dailyQuest = dailyQuest[indexPath.item]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        let quest = dailyQuest[indexPath.item]
        let qtitle = quest.quest?.height(withConstrainedWidth:  view.frame.width - 50, font: UIFont.systemFont(ofSize: 20))

        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.1

            return CGSize(width: view.frame.width - ph, height: qtitle! + 60)

            
        default:
            
            return CGSize(width: view.frame.width, height: qtitle! + 60)

        }

        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Selected ")
        
        
        let title = dailyQuest[indexPath.item]

        let quest = title.quest ?? "NA"
        
        let addIthakaController = AddIthakaForMiniQuestController(collectionViewLayout: UICollectionViewFlowLayout())
        addIthakaController.hidesBottomBarWhenPushed = true
        addIthakaController.questTag = "Daily Quest: " + quest
        
        self.navigationController?.pushViewController(addIthakaController, animated: true)

    }
    
    var dailyQuest = [DailyQuestModel]()
    var dailyQuest1 = [DailyQuestModel]()
    
    var todayQuest: String?
    var fquestheight: CGFloat?
    
    func fetchQuests() {
        let ref = Database.database().reference().child("dailyQuest")
        
        var qqs = [DailyQuestModel]()
        
        
        ref.queryLimited(toLast: 7).observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            
            
            value.forEach({ (snapshot) in
                let vv = snapshot.value
                print(snapshot.key)
                print("snapshot.key", vv)
                
                let quest = DailyQuestModel.init(key: snapshot.key as! String, dictionary: vv as! [String : Any])
                qqs.append(quest)
                self.dailyQuest.append(quest)
                
                
            })
            
            DispatchQueue.main.async {
                qqs.sort(by: {$0.timestamp! > $1.timestamp!})
                self.dailyQuest = qqs
                self.collectionView?.reloadData()
//                self.myActivityIndicator.stopAnimating()
            }
            
            
        }
        
        
        
    }
          
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            self.tabBarController?.tabBar.isHidden = true
            self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false
    }

}

protocol dailyQuestHeaderProtocol: class {
    func backbuttonTapped()
}

class dailyQuestHeaderCell: UICollectionViewCell {
    
    
    weak var delegate: dailyQuestHeaderProtocol?
    
    let mainview = UIView()
    
    
    
    @objc func handleBack(){
        print("Step 1")
        self.delegate?.backbuttonTapped()
    }

    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "book")
        
        return iv
    }()
    
    
    lazy var questLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.text = "Daily Quest"
        
        return label
    }()
    
    
    lazy var footerLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textAlignment = .center
        label.text = "Having a writer's block? Try this daily quest. Writing something or anything daily improves your skills. Do this everyday and see the magic."
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.textColor = UIColor.gray
        return label
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        mainview.backgroundColor = UIColor.white
        mainview.layer.cornerRadius = 15
        mainview.clipsToBounds = true
        
        
        addSubview(mainview)
        mainview.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 20, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        
        mainview.addSubview(imageView)
        mainview.addSubview(questLabel)
        mainview.addSubview(footerLabel)
        
        imageView.anchor(top: mainview.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: (frame.width/2) - 75, bottomConstant: 0, rightConstant: 0, widthConstant: 150, heightConstant: 0)
        
        questLabel.anchor(top: imageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        
        footerLabel.anchor(top: nil, left: mainview.leftAnchor, bottom: mainview.bottomAnchor, right: mainview.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 70 )


    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

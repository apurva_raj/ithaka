//
//  MyItems.swift
//  ithaka
//
//  Created by Apurva Raj on 22/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import Foundation

class MyItems {
    
    var uid: String?
    
    var collectedPoints: Int?
    var likePoints: Int?
    var storyPoints: Int?
    var commentPoints: Int?
    var totalNumberOfItemsUserHave: Int?
    
}


extension MyItems {
    static func transform(dict: [String: Any], key: String) -> MyItems {
        
        let myItems = MyItems()
        myItems.uid = key
        myItems.collectedPoints = dict["CollectedPoints"] as? Int? ?? 0
        myItems.storyPoints = dict["storyPoints"] as? Int? ?? 0
        myItems.commentPoints = dict["commentPoints"] as? Int? ?? 0
        myItems.likePoints = dict["likePoints"] as? Int? ?? 0
        
        return myItems
    }
}



class ItemNameAndCount {
    
    var itemName: String?
    var itemCount: Int?
}

extension ItemNameAndCount {
    static func transfrom(key: String, count: Int) -> ItemNameAndCount {
        
        let itemNamesAndCount = ItemNameAndCount()
        itemNamesAndCount.itemName = key
        itemNamesAndCount.itemCount = count
        
        return itemNamesAndCount
    }
}

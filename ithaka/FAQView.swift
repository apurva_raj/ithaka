//
//  FAQView.swift
//  ithaka
//
//  Created by Apurva Raj on 09/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class FAQViewsss: UICollectionViewCell {
    
    var faq: FAQ? {
        didSet {
            guard let questions = faq?.question else { return }
            guard let answers = faq?.answer else { return }
        }
    }
    
    
    let taglineLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(17)
        label.text = "Keep ithaka always in your mind"
        label.textAlignment = .center
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        addSubview(taglineLabel)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

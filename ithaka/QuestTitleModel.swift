//
//  QuestTitleModel.swift
//  ithaka
//
//  Created by Apurva Raj on 13/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation


struct QuestTitleModel {
    
    var id: String
    var title: String
    var questImage: String
    var creationDate: TimeInterval
    
    init(id: String, dictionary: [String: Any]) {
        self.id = id
        self.title = dictionary["title"] as? String ?? ""
        self.questImage = dictionary["questImage"] as? String ?? ""

        self.creationDate = dictionary["creationDate"]  as? TimeInterval ?? 0
        
    }
}

struct QuestStageObjectives {
    var id: String
    var title: String
    
    var isWritten = false
    
    init(id: String, dictionary: [String: Any]) {
        self.id = id
        self.title = dictionary["title"] as? String ?? ""

    }
}

struct QuestUserData {
    var uid: String?
    var id: String?
    var PresentLevel: Int
    var levelInfo: Dictionary<String, Any>?
    
    init(uid:String?, id: String, dictionary: [String: Any]) {
        self.id = id
        self.uid = uid
        self.PresentLevel = dictionary["PresentLevel"] as? Int ?? 0
        self.levelInfo = dictionary["level1"] as? Dictionary<String, Any>
        
    }
}


struct QuestTitleLevelModel {
    var id: String
    var levelNumber: String
    var objective: String
    var objectiveNumber: Int
}

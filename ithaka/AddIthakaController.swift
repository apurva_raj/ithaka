//
//  addIthakaController.swift
//  ithaka
//
//  Created by Apurva Raj on 19/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//


// Remained Work



// Done


import UIKit
import Firebase
import SwiftMessages

class AddIthakaController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UITextViewDelegate {
    
    
    let cellId = "cellId"
    let headerId = "headerId"
//    var header: AddTitle?
    var addedTitle: String?
    
    
    let titleView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 23)
        tv.layer.borderColor = UIColor.gray.cgColor
        tv.layer.borderWidth = 1
        tv.layer.cornerRadius = 5
        tv.isUserInteractionEnabled = true
        tv.text = "asddsadasdasdas"
        return tv
    }()
    

    
    func textViewDidChange(_ textView: UITextView) {
        
        let titleCount = textView.text.count
        
        if (1 ... 150).contains(titleCount){
            navigationItem.rightBarButtonItem?.isEnabled = true
            
        } else if(titleCount == 0) {
            navigationItem.rightBarButtonItem?.isEnabled = false
            SwiftMessages.show {
                let view = MessageView.viewFromNib(layout: .CardView)
                view.configureTheme(.warning)
                view.configureDropShadow()
                let iconText = "🙄"
                view.configureContent(title: "Oops", body: "You must atleast enter one character!", iconText: iconText)
                view.button?.isHidden = true
                
                return view
            }
            
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            SwiftMessages.show {
                let view = MessageView.viewFromNib(layout: .CardView)
                view.configureTheme(.warning)
                view.configureDropShadow()
                let iconText = "🙄"
                view.configureContent(title: "Sorry", body: "Only 150 characters are allowed", iconText: iconText)
                view.button?.isHidden = true
                
                return view
            }
        }
    
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        myActivityIndicator.center = view.center
        
        view.addSubview(myActivityIndicator)
        
        myActivityIndicator.startAnimating()

        collectionView?.backgroundColor = UIColor.rgb(red: 255, green:255, blue: 0, alpha: 1)
        navigationItem.title = "EmojiAdd"
        
//        collectionView?.register(AddTitle.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
//        collectionView?.register(AddEmoji.self, forCellWithReuseIdentifier: "emojiId")


        titleView.delegate = self
        
        
        setupNavigationButton()
        myActivityIndicator.stopAnimating()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
//    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
////        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "emojiId", for: indexPath) as! AddEmoji
////
////        return cell
//    }
//    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
    }

    fileprivate func setupNavigationButton() {
        navigationController?.navigationBar.tintColor = .blue
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self
            , action: #selector(handleCancel))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self
            , action: #selector(handleNext))
        navigationItem.rightBarButtonItem?.isEnabled = false


    }
    
    @objc func handleCancel() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func handleNext() {
        let addIthakaStoryController = AddIthakaStoryController()
        let titleT = String(titleView.text.filter { !"\n".contains($0) })
//        addIthakaStoryController.addedTitle = titleT
        navigationController?.pushViewController(addIthakaStoryController, animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }

    
}

//
//  QuestController.swift
//  ithaka
//
//  Created by Apurva Raj on 09/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase

protocol textViewTap: class {
    func hashTagTapped(hash: String)
    func usernameTapped(username: String)

}

class QuestController: UICollectionViewController, UICollectionViewDelegateFlowLayout, QuestDelegate, UIViewControllerTransitioningDelegate,textViewTap, IthakaAddDelegate, DailyQuestDelegate{
    func exit() {
        return
    }
    
    func onTrophyTapped() {
        
        let trophyController = TrophyQuestController()
        trophyController.user = self.user
        
        trophyController.popoverPresentationController?.sourceView = self.view
        trophyController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        trophyController.popoverPresentationController?.permittedArrowDirections = []

        present(trophyController, animated: true, completion: nil)
    }
    
  
    func onTresureTapped() {
        let treasureController = TreasureQuestController()
    self.navigationController?.pushViewController(treasureController, animated: true)

    }
    
    
    func ontodaytapped() {
        let addstory = WriteTodaysQuest(collectionViewLayout: UICollectionViewFlowLayout())
//        addstory.questDaily = "daily"
//        addstory.questTag = "there we go agqin"
        self.navigationController?.pushViewController(addstory, animated: true)
    }
    
    func onShareTapped() {
        print("dabbu dobi che")
        let addIthakaController = AddIthakaAPI.addIthakaController
    self.navigationController?.pushViewController(addIthakaController, animated: true)

    }
    
    func onDimissTapped() {
        self.dismiss(animated: true, completion: nil)

    }
    
    func onTapQuestTitle(id: String, title: String) {
        let funController = FunController(collectionViewLayout: UICollectionViewFlowLayout())
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        self.navigationItem.backBarButtonItem = backItem
        funController.navigationItem.title = title
        funController.questTitleId = id
        funController.questmaintitle = title
        self.navigationController?.pushViewController(funController, animated: true)

    }
    
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    func hashTagTapped(hash: String) {
        print("bithcplease")
        let hh = HashTagPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        hh.hashtag = hash
        self.navigationController?.pushViewController(hh, animated: false)

    }
    
    func usernameTapped(username: String) {
        return
    }
    
    func stopLoadingAnimation() {
       self.myActivityIndicator.stopAnimating()

    }
    
    func didTapStage(level: Int) {
        return
    }
    
    func onTapQuestTitle(id: String) {
        print("fianlly", id)
//        let funController = FunController(collectionViewLayout: UICollectionViewFlowLayout())
//        let backItem = UIBarButtonItem()
//        backItem.title = "Back"
//        self.navigationItem.backBarButtonItem = backItem
//
//        funController.questTitleId = id
//        self.navigationController?.pushViewController(funController, animated: true)


    }
    
    func onTtapped(for cell: QuestCards) {
        print("fianlly")
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        print("should not work", indexPath.item)

    }
    
    
    
    func onTypeTapped(for cell: QuestMain) {
        print("at what price. at what price !!!")
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        

        let previewpostController = FunController(collectionViewLayout: UICollectionViewFlowLayout())
        self.navigationController?.pushViewController(previewpostController, animated: true)

    }
    
   
    
    lazy var AvatarImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "boy_sailer").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .gray
        button.imageView?.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    let frames = CGRect(x: 200, y: 100, width: 50, height: 50)

    override func viewDidLoad() {
        super.viewDidLoad()

        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()
//        hideKeyboardWhenTappedAround()
        self.collectionView?.backgroundColor = .white

        collectionView?.delegate = self
        collectionView?.dataSource = self

        collectionView?.showsVerticalScrollIndicator = false
        collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.register(TitleHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        collectionView?.register(QuestHashtag.self, forCellWithReuseIdentifier: "questHashtag")
        collectionView?.register(QuestMain.self, forCellWithReuseIdentifier: "questMain")
        collectionView?.register(c1.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerC1")
        collectionView?.register(QuestAvatar.self, forCellWithReuseIdentifier: "questAvatar")
        collectionView?.register(AddIthakaNewCell.self, forCellWithReuseIdentifier: "cellId")
        collectionView?.register(DailyQuestCard.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "DailyQuestCard")


//        createQuestLevelDatabase()
        
//        createQuestDatabase()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue", size: 20)!]

//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "STHeitiSC-", size: 20) ?? UIFont.systemFont(ofSize: 20)]

        fetchQuests()
        collectionView?.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        navigationController?.navigationBar.isTranslucent = false
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
                NotificationCenter.default.addObserver(self, selector: #selector(self.BadgeWasUpdated), name: NSNotification.Name(rawValue: "com.api4242.ithaka"), object: nil)
        

//        createQuestLevelDatabase()
        
        fetchuser()
//        dailyquestadd()
    }
    var user: UserModelNew?
    
    func fetchuser(){
        Api.User.observeCurrentUser { (usermodelnew) in
            self.user = usermodelnew
        }
    }

    
    func dailyquestadd() {
        let ref = Database.database().reference().child("dailyQuest")
        let refkey = Date().timeIntervalSince1970
        let refk = Int(refkey)
        let reff = String(refk)
        let values = ["quest": "*Now doing public release. Currently iOS exclusive."]
        print("Refk", refkey)
        print("Refk", refk)

        ref.child(reff).updateChildValues(values)
    }
    
    
    @objc func addButtonTapped() {
        print("bc")
    }

    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 1 {
            
            let dailyQuestController = DailyQuestController(collectionViewLayout: UICollectionViewFlowLayout())
            navigationController?.navigationItem.backBarButtonItem?.title = "Back"
            self.navigationController?.pushViewController(dailyQuestController, animated: true)

        }
    }
    
    func mainff() {
        Database.database().reference().child("users").observeSingleEvent(of: .value) { (snapshot) in
            
            guard let value = snapshot.value as? NSDictionary else { return }
            
            value.forEach({ (snapshot) in
                 let uid = snapshot.key
                Database.database().reference().child("posts").child(uid as! String).observeSingleEvent(of: .value, with: { (snapshot) in
                    guard let value1 = snapshot.value as? NSDictionary else { return }
                    
                    value1.forEach({ (snapshot) in
                        Api.NewPost.observeSinglePost(withUId: uid as! String, withPid: snapshot.key as! String, completion: { (post) in
                            
                            
                        
                            
                            let pid = snapshot.key as! String
                        
                            switch pid {
                            case "-LKh-qClTZ8hA-5QorI0", "-LKh-fxXHAygAUD64NyD", "-LKgzyaZxUTq43Gy3_1_", "-LKgx64QCE-oweS-8W2v", "-LKgwRdTBDBQ9YCEk8dT", "-LKgw8amZUNsZhQV81V4", "-LISirkM51DjEfQbuzAi", "-LIShzRt6NHrCqxhmgYA", "-LISh9qkLumke0n4ANUr" :
                                print("me hu na")
                                
                            default:
                                print("my work")
                                
                                let cd = post?.timestamp ?? 0
                                
                                let nv = ["uid": uid, "creationDate":cd]
                                
                                Database.database().reference().child("users").observeSingleEvent(of: .value, with: { (snapshot) in
                                    guard let value3 = snapshot.value as? NSDictionary else { return }
                                    value3.forEach({ (snapshot) in
                                        let uuid = snapshot.key as! String
                                        Database.database().reference().child("feed").child(uuid).child(pid ).updateChildValues(nv)
                                        
                                    })
                                    
                                })

                            }
                            
                            
                        })
                    })
                })
                
            })
            
        }
    }
    
    @objc func BadgeWasUpdated() {
        self.tabBarController?.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)
    }
    
    @objc func handleRefresh() {
        questTitle.removeAll()
        fetchQuests()
        
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
        }
    }
    

    
    // sections
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }

    
    
//    Header Start
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch indexPath.section {
        case 0:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! TitleHeader
            header.titleLabel.text = "Start writing"
            header.backgroundColor = .white
            
            return header
        case 1:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "DailyQuestCard", for: indexPath) as! DailyQuestCard
            header.delegate = self
            header.TitleTextView.text = "Or Choose A Quest"
            header.backgroundColor = .white
            header.TitleTextView.isHidden = false
            header.grayLine.isHidden = false
            
            return header

        default:
            return UICollectionViewCell()
        }
        
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        switch section {
        case 0:
            return .zero
        case 1:
            return CGSize(width: view.frame.width, height: 125)

        default:
            return .zero
        }
    
    
    }
    //Header end
    
    
    // body start
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! AddIthakaNewCell
            cell.delegate = self
            
            return cell
            
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "questMain", for: indexPath) as! QuestMain
            cell.backgroundColor = .white
            cell.delegate = self
            cell.questTitle = questTitle
            
            return cell

        default:
            return UICollectionViewCell()
        }
     

    }
    
    var questTitle = [QuestTitleModel]()
    fileprivate func fetchQuests() {
        let ref = Database.database().reference().child("QuestTitles")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let values = snapshot.value as? [String: Any] else { return }
            
            values.forEach({ (snapshot) in
                
                guard let value = snapshot.value as? [String: Any] else { return }
                let key = snapshot.key
                let quest = QuestTitleModel(id: key, dictionary:value)
                
                self.questTitle.append(quest)
                self.collectionView?.reloadData()
                self.myActivityIndicator.stopAnimating()
                self.collectionView?.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)

            })
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.width, height: view.frame.height/2)
        case 1:
            let height = questTitle.count * Int((collectionView.frame.width-60)/2 - 60)
            return CGSize(width: view.frame.width, height: CGFloat(height))

        default:
            return .zero
        }
      

    }
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    
    func createQuestLevelDatabase() {
        
        let list2 = ["Who/What (in physical form or otherwise) scares the hell out of you?", "Your visit to a haunted place", "Scariest and realest thing you have ever experienced", "A scary fictional story on - VALAK (THE Nun)", "You are married to love your life. Now he/she tells you things you don't see nor believe"]
        
        let list3 = ["If you were to write a movie script, what would be the plot?", "Which movie makes you cry most? Why?", "If you could change one thing from any of the most popular movies, what would it be?", "In your opinion, which film adaptation has stayed true its book?", "Share an interesting short story you read in any book"]
        
        let list0 = ["About your most favourite philosopher", "A poem on - Existential crisis", "The most interesting philosophical ideologies", "Explain the word - philosophy to 5 year old kid", "List your favourite books on philosophy"]

        let list4 = ["Best and worst thing about your crush", "Similarity or difference with your significant other", "How did you meet love of your life?", "Write a poetry dedicating to someone you loved most", "Your thoughts on - open relationships"]
        
        let list5 = ["Your dream travel destination", "Your Best travel experience", "Your worst travel experience", "One unforgettable thing that happening during your travel", "Do you think everyone should travel? Why?"]

        let list11 = ["What does it means to be alive for you?", "The craziest experience you’ve had with your roommate", "A poetic love letter to your own life", "That one thing noone should ever do?", "Have a bucket list? Share"]
        
        let list1 = ["Travelling solo - with partner, friends, or groups tours? What’s your personal favourite?", "Ever done road trip with a sibling? Share your real experience. If not, share your imagined experience.", "You are given $10K to use for travelling. Valid is only for 3 days. What would you do? (You can only spend it on your travelling-exploring-eating)", "Your thoughts on living DigitalNomad life", "About the dream country you want to visit."]


        
        let ref = Database.database().reference().child("QuestLevels").child("-LHEtPJRa3J9ezajpwVj").child("level2")
        
        let ref1 = ref.childByAutoId()
        let ref2 = ref.childByAutoId()
        let ref3 = ref.childByAutoId()
        let ref4 = ref.childByAutoId()
        let ref5 = ref.childByAutoId()
        
        let value1 = ["title": list1[0], "number":0] as [String : Any]
        let value2 = ["title": list1[1], "number":1] as [String : Any]
        let value3 = ["title": list1[2], "number":2] as [String : Any]
        let value4 = ["title": list1[3], "number":3] as [String : Any]
        let value5 = ["title": list1[4], "number":4] as [String : Any]
        
        
        ref1.updateChildValues(value1)
        ref2.updateChildValues(value2)
        ref3.updateChildValues(value3)
        ref4.updateChildValues(value4)
        ref5.updateChildValues(value5)

//        ref.observeSingleEvent(of: .value) { (snapshot) in
//            guard let values = snapshot.value as? [String: Any] else { return }
//            values.forEach({ (snapshot) in
//
//                let questTitleId = snapshot.key
//
//
//            })
//        }

        

    }
    
    func createQuestDatabase() {
        let ref = Database.database().reference().child("QuestTitles")
        
        let questTitles = ["Horror", "Love story", "Movies", "Infinity war", "Motivation", "Lost", "Ithaka", "War"]
        let textColor = [".red", ".yellow", ".blue", ".green"]
        let bgcolor = [".white", ".black", ".pink"]
        let creationDate = Date().timeIntervalSince1970

        let quest0 = ["title": questTitles[0], "textcolor": textColor[1],"bgcolor": bgcolor[1], "creationDate": creationDate ] as [String : Any]
        let quest1 = ["title": questTitles[1], "textcolor": textColor[2],"bgcolor": bgcolor[2], "creationDate": creationDate ] as [String : Any]
        let quest2 = ["title": questTitles[2], "textcolor": textColor[3],"bgcolor": bgcolor[1], "creationDate": creationDate ] as [String : Any]
        let quest3 = ["title": questTitles[3], "textcolor": textColor[1],"bgcolor": bgcolor[2], "creationDate": creationDate ] as [String : Any]
        let quest4 = ["title": questTitles[4], "textcolor": textColor[2],"bgcolor": bgcolor[1], "creationDate": creationDate ] as [String : Any]
        let quest5 = ["title": questTitles[5], "textcolor": textColor[3],"bgcolor": bgcolor[2], "creationDate": creationDate ] as [String : Any]
        let quest6 = ["title": questTitles[6], "textcolor": textColor[1],"bgcolor": bgcolor[1], "creationDate": creationDate ] as [String : Any]
        let quest7 = ["title": questTitles[7], "textcolor": textColor[2],"bgcolor": bgcolor[2], "creationDate": creationDate ] as [String : Any]

        ref.childByAutoId().updateChildValues(quest0)
        ref.childByAutoId().updateChildValues(quest1)
        ref.childByAutoId().updateChildValues(quest2)
        ref.childByAutoId().updateChildValues(quest3)
        ref.childByAutoId().updateChildValues(quest4)
        ref.childByAutoId().updateChildValues(quest5)
        ref.childByAutoId().updateChildValues(quest6)
        ref.childByAutoId().updateChildValues(quest7)

    }
    
        //body end
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.statusBarView?.isHidden = true

        tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = true
        self.tabBarController?.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.shared.statusBarView?.isHidden = false

        UIApplication.shared.statusBarView?.backgroundColor = UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)

    }
}

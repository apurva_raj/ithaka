//
//  UtilityAPI.swift
//  ithaka
//
//  Created by Apurva Raj on 19/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class UtilityAPI {

    static var sizeofIndicator = CGSize(width: 30, height: 30)
    static let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)


    static func deletePost(post: PostNew){
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard let postId = post.id else { return }
        let ref = Database.database().reference()
        
        let ref3 = Database.database().reference().child("users").child(uid)

        
        //deleting from notifications
        //updating total post count
        
        let refcountall = Database.database().reference().child("allpostcount")
        
        refcountall.observeSingleEvent(of: .value) { (snapshot) in
            
            guard let value = snapshot.value as? NSDictionary else { return }
            
            var allcount = value["count"] as? Int ?? 0
            
            allcount = allcount - 1
            
            let ccvalue = ["count": allcount]
            refcountall.updateChildValues(ccvalue)
            
            
        }

        //removing total post count to auth user table
        
        ref3.observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            
            let cc = value?["postCount"] as? Int ?? 0
            let count = cc - 1
            let postCount = ["postCount": count]
            ref3.updateChildValues(postCount)
            
        })
        //removing post from quest feed
        guard let questTitleId = post.questId else { return }
        
        if questTitleId.count > 2 {
            let questFeedREF = Database.database().reference().child("questFeed").child(questTitleId).child(postId)
            
            questFeedREF.removeValue()
            
        }

        //removing from explore feed
        
        Database.database().reference().child("explore").child(postId).removeValue()
        
        //removing my points from profile
        ref3.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            var points = value?["profileProgress"] as? Int ?? 0
            points = points - 10
            let tp = ["profileProgress": points]
            ref3.updateChildValues(tp)
            
        }
        
        //remove from followers feed
        let followref = Database.database().reference().child("following").child(uid)
        followref.observeSingleEvent(of: .value, with: { (snapshot) in
            let arraySnapshot = snapshot.children.allObjects as! [DataSnapshot]
            arraySnapshot.forEach({ (child) in
                Api.Feed.REF_FEED.child(child.key).child(postId).removeValue()
            })
        })

        //removing quest data from questuserdata
        
        //remove from user auth feed
        Api.Feed.REF_FEED.child(uid).child(postId).removeValue()
        
  
        
        //remove from explore feed
        
        Api.Feed.REF_EXPLORE_POSTS.child(postId).removeValue()
        
        //remove from hashtagfeed
        guard let story = post.story else { return }
        
        let storywords = story.components(separatedBy: CharacterSet(charactersIn: "#@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_").inverted)
        for var storyword in storywords {
            if storyword.hasPrefix("#") {
                storyword = storyword.trimmingCharacters(in: CharacterSet.punctuationCharacters)
                print(storyword)
                let hashref = Database.database().reference().child("Hashtags").child(storyword.lowercased()).child(postId)
                
                hashref.removeValue()
                
            }
        }
        
        //remove form allpostiaqyests feed
        let questfeedref = Database.database().reference().child("AllPostsViaQuests")
        questfeedref.child(postId).removeValue()

        //remvoe from admin table
        let adminref = Database.database().reference().child("AllPostsForAdmin")
        adminref.child(postId).removeValue()

        //remove from notification
        //remove from post table
        ref.child("posts").child(uid).child(postId).removeValue(completionBlock: { (err, ref) in
            if let err = err {
                print(err)
            } else {
                return
            }
        })
        

    }

}

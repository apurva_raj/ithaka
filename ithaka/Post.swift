//
//  Post.swift
//  ithaka
//
//  Created by Apurva Raj on 02/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import Foundation

struct Post {
    
    var id: String?
    
    let user: UserModel
    let imageUrl: String
    let title: String
    let story: String
    let tag: String
    let emoji: String
    
    let creationDate: Date
    var loveCount: Int
    var commentsCount: Int
    var rocketCount: Int
    
    var hasLiked = false

    
    init(user: UserModel, dictionary: [String: Any]) {
        self.user = user
        self.imageUrl = dictionary["imageUrl"] as? String ?? ""
        self.title = dictionary["title"] as? String ?? ""
        self.story = dictionary["story"] as? String ?? ""
        self.tag = dictionary["questTag"] as? String ?? ""

        self.emoji = dictionary["emoji"] as? String ?? ""

        self.loveCount = dictionary["loveCount"] as? Int ?? 0
        self.commentsCount = dictionary["commentsCount"] as? Int ?? 0
        self.rocketCount = dictionary["rocketCount"] as? Int ?? 0
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)
    }
}


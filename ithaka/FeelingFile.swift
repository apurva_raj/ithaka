//
//  FeelingFile.swift
//  ithaka
//
//  Created by Apurva Raj on 12/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class FeelingFile: UICollectionViewCell {
    
    
    var title: String? {
        didSet {
            titleLabel.text = title
        }
        
    }
    var emoji: String? {
        didSet {
            emojiLabel.text = emoji
        }
    }
    
    lazy var emojiLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 40)
        
        return label
    }()

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = UIFont(name: "STHeitiTC-Medium", size: CGFloat(14))
        
        return label
    }()
    
    let whiteview: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 7
        view.clipsToBounds = true
        
        return view
    }()
    
    override var isSelected: Bool{
        didSet {
            self.whiteview.backgroundColor = isSelected ?  UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
 : UIColor.white
            self.titleLabel.backgroundColor = isSelected ?  UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
 : UIColor.white
            self.emojiLabel.backgroundColor = isSelected ?  UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
 : UIColor.white

        }

    }
    
    override var isHighlighted: Bool{
        didSet {
            self.whiteview.backgroundColor = isSelected ?  UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
                : UIColor.white
            self.titleLabel.backgroundColor = isSelected ?  UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
                : UIColor.white
            self.emojiLabel.backgroundColor = isSelected ?  UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
                : UIColor.white
            
        }

    }
    

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(emojiLabel)
        addSubview(titleLabel)
        
        addSubview(whiteview)
        
        emojiLabel.anchor(top: safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        titleLabel.anchor(top: emojiLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        whiteview.anchor(top: emojiLabel.topAnchor, left: self.leftAnchor, bottom: titleLabel.bottomAnchor, right: self.rightAnchor, topConstant: -10, leftConstant: 10, bottomConstant: -10, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        self.sendSubview(toBack: whiteview)
        
        

    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


class FeelingHeaderFile: UICollectionViewCell {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "What are you feeling right now?"
        label.font = UIFont(name: "STHeitiTC-Medium", size: CGFloat(23))
        label.sizeToFit()
        label.numberOfLines = 0
        label.padding = UIEdgeInsetsMake(15, 15, 15, 15)
        label.clipsToBounds = true
        label.layer.cornerRadius = 7
        
        return label
    }()
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(titleLabel)
        
        titleLabel.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 15, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

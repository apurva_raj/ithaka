//
//  SeeMore.swift
//  ithaka
//
//  Created by Apurva Raj on 24/01/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit


protocol oneone: class {
    func tapped()
}

class SeeMoreLabel: UICollectionViewCell {
    
    weak var delegate: oneone?
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.text = "See More"
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.padding = UIEdgeInsetsMake(10, 10, 10, 10)
        label.textAlignment = .center
        label.isUserInteractionEnabled = true

        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        label.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//
//  PostNew.swift
//  ithaka
//
//  Created by Apurva Raj on 05/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseAuth

class PostNew {
    

    var id: String?
    var uid: String?
    var imageUrl: String?
    var title: String?
    var story: String?
    var tag: String?
    var questId: String?
    var itemCode: String?

    var emoji: String?
    
    var creationDate: Date?
    var loveCount: Int?
    var commentsCount: Int?
    var rocketCount: Int?
    
    var likeCount: Int?
    var likes: Dictionary<String, Any>?
    var isLiked: Bool?
    var timestamp: TimeInterval?
    
    var postSetItem: String?
    var wonItemCode: String?

}

extension PostNew {
    static func transformPostPhoto(dictionary: [String: Any], key: String) -> PostNew {
        let postNew = PostNew()
        postNew.id = key
        
        postNew.imageUrl = dictionary["imageUrl"] as? String ?? ""
        postNew.title = dictionary["title"] as? String ?? ""
        postNew.story = dictionary["story"] as? String ?? ""
        postNew.tag = dictionary["questTag"] as? String ?? ""
        postNew.questId = dictionary["questId"] as? String ?? ""
        postNew.itemCode = dictionary["itemCode"] as? String ?? ""


        postNew.uid = dictionary["uid"] as?  String ?? ""
        postNew.emoji = dictionary["emoji"] as? String ?? ""
        
        postNew.loveCount = dictionary["loveCount"] as? Int ?? 0
        postNew.commentsCount = dictionary["commentsCount"] as? Int ?? 0
        postNew.rocketCount = dictionary["rocketCount"] as? Int ?? 0
        
        postNew.likeCount = dictionary["likeCount"] as? Int
        postNew.likes = dictionary["likes"] as? Dictionary<String, Any>

        if postNew.rocketCount == nil {
            postNew.rocketCount = 0
        }
        

        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        postNew.creationDate = Date(timeIntervalSince1970: secondsFrom1970)

        postNew.timestamp = dictionary["creationDate"] as? TimeInterval
        
        
        postNew.postSetItem = dictionary["itemcode"] as? String ?? ""
        postNew.wonItemCode = dictionary["wonItemCode"] as? String ?? ""

        return postNew
    }

}


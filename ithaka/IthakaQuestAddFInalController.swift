//
//  IthakaQuestAddFInalController.swift
//  ithaka
//
//  Created by Apurva Raj on 04/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import SwiftEntryKit
import FirebaseAuth
import FirebaseDatabase

class IthakaQuestAddFInalController: UICollectionViewController, UICollectionViewDelegateFlowLayout, AddIthakaItemSlect{
    
    func itemselectPrompt(){
        
    }
    
    var halfModalTransitioningDelegateForIthakaAdd: HalfModalTransitioningDelegateForIthakaAdd?

    func onShareTapped(storytext: String, itemCode: String) {
        
        let qqt = ""
        self.storytext = ""
        self.imageData = nil
        
        let ps = PostStoryModel.transfromPostStory(st: storytext, ic: itemCode, qt: qqt, questId: nil, questObjectId: nil, level: nil)

        let publishController = PublishedAndWonController(collectionViewLayout: UICollectionViewFlowLayout())
        
        publishController.postStory = ps

        self.halfModalTransitioningDelegateForIthakaAdd = HalfModalTransitioningDelegateForIthakaAdd(viewController: self, presentingViewController: publishController)
        publishController.popoverPresentationController?.sourceView = self.view
        publishController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
    publishController.popoverPresentationController?.permittedArrowDirections = []
        
        
        publishController.modalPresentationStyle = .custom
        publishController.transitioningDelegate = self.halfModalTransitioningDelegateForIthakaAdd
        
        self.present(publishController, animated: true, completion: nil)
        self.view.endEditing(true)

        
    }
    
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?


    func dailyQuestCardTapped(tag: Int) {
        switch tag {
        case 1:
            print("tapped 1")
            let dailyQuestController = DailyQuestNewController(collectionViewLayout: UICollectionViewFlowLayout())
            dailyQuestController.hidesBottomBarWhenPushed = true

            self.navigationController?.pushViewController(dailyQuestController, animated: true)
        case 2:
            print("tapped 2")
            
            if self.checkIfDefaultKeyContains(key: "MiniQuestThings") {
                
                if self.ccLockCode == 1 {
                    if self.checkIfDefaultKeyContains(key: "HeroSelectionThing") {
                        let dailyQuestController = MiniQuestController(collectionViewLayout: UICollectionViewFlowLayout())
                        
                        dailyQuestController.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(dailyQuestController, animated: true)
                        
                    } else {
                        let dailyQuestController = MiniQuestIntroController(collectionViewLayout: UICollectionViewFlowLayout())
                        dailyQuestController.hidesBottomBarWhenPushed = true
                        self.navigationController?.pushViewController(dailyQuestController, animated: true)


                    }

                }
                

            } else {
                print("it does not exist")

                let dailyQuestController = QuestIsLockedController(collectionViewLayout: UICollectionViewFlowLayout())
                
                dailyQuestController.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(dailyQuestController, animated: true)

                
                self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: dailyQuestController)
                
                dailyQuestController.popoverPresentationController?.sourceView = self.view
                dailyQuestController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                
                dailyQuestController.popoverPresentationController?.permittedArrowDirections = []
                
                let attachment:NSTextAttachment = NSTextAttachment()
                attachment.image = #imageLiteral(resourceName: "decor_4")
                
                attachment.setImageHeight(height: 40)
                
                let attachmentString:NSAttributedString = NSAttributedString(string:" Mini Quest")
                let attributedString:NSMutableAttributedString = NSMutableAttributedString(attachment: attachment)
                
                
                attributedString.append(attachmentString)
                
                dailyQuestController.questLabel.attributedText = attributedString
                dailyQuestController.lockImageView.image = #imageLiteral(resourceName: "basic_lock")
                dailyQuestController.questLabel.backgroundColor = UIColor.rgb(red: 72, green: 207, blue: 250, alpha: 1)
                dailyQuestController.infoLabel.text = "This mini quest is all about you sailing through sea on a small boat."
                dailyQuestController.infoLabel1.text = "You need a key to unlock this quest."
                dailyQuestController.infoLabel2.text = "You will discover the if you keep writing and reading more."
                
                dailyQuestController.footer.text = "Tap on the lock if you have got the key"
                
                let mh = UIDevice().type
                let device: String = mh.rawValue

                switch device {
                case "iPhone 5S":
                    print("Decis is ", device)
                    self.present(dailyQuestController, animated: true, completion: nil)


                default:
                    dailyQuestController.modalPresentationStyle = .custom
                    dailyQuestController.transitioningDelegate = self.halfModalTransitioningDelegate
                    self.present(dailyQuestController, animated: true, completion: nil)


                }
                


            }
            
        case 4:
            print("case 4 tpped - help")
            let newController = FAQController(collectionViewLayout: UICollectionViewFlowLayout())
            self.navigationController?.pushViewController(newController, animated: true)
            
        case 42:
            print("case 42 tpped - help")
            let feedbackController = FeedbackController()
            self.navigationController?.pushViewController(feedbackController, animated: true)




        case 3:
            print("tapped 3")
            
            if self.checkIfDefaultKeyContains(key: "MonsterQuestThing") {
                print("waht???")
            } else {
                let dailyQuestController = QuestIsLockedController(collectionViewLayout: UICollectionViewFlowLayout())
                
                self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: dailyQuestController)
                
                dailyQuestController.popoverPresentationController?.sourceView = self.view
                dailyQuestController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                
                dailyQuestController.popoverPresentationController?.permittedArrowDirections = []
                
                let attachment:NSTextAttachment = NSTextAttachment()
                attachment.image = #imageLiteral(resourceName: "red_monster")
                
                attachment.setImageHeight(height: 60)
                
                let attachmentString:NSAttributedString = NSAttributedString(string:" Monster Battle")
                let attributedString:NSMutableAttributedString = NSMutableAttributedString(attachment: attachment)
                
                
                attributedString.append(attachmentString)
                
                dailyQuestController.questLabel.attributedText = attributedString
                dailyQuestController.lockImageView.image = #imageLiteral(resourceName: "red_crystals")
                dailyQuestController.questLabel.backgroundColor = UIColor.rgb(red: 231, green: 76, blue: 60, alpha: 1)
                dailyQuestController.infoLabel.text = "This is all about defeating monsters by spontaneous writing within given time."
                dailyQuestController.infoLabel1.text = "To fight bigger monsters, invite your friends and team up."
                
                dailyQuestController.infoLabel2.text = "Unlocking monster quest requires a red rune and 100+ followers."
                
                dailyQuestController.footer.text = "Tap on the stone if you have got the rune"
                
                
                let mh = UIDevice().type
                let device: String = mh.rawValue
                
                switch device {
                case "iPhone 5S":
                    print("Decis is ", device)
                    self.present(dailyQuestController, animated: true, completion: nil)
                    
                    
                default:
                    dailyQuestController.modalPresentationStyle = .custom
                    dailyQuestController.transitioningDelegate = self.halfModalTransitioningDelegate
                    self.present(dailyQuestController, animated: true, completion: nil)
                    
                    
                }


            }


        default:
            print("nothing")
        }
    }
    
    
    
    func removeItempage() {
        print("removing")
        
    }
    func dailyQuestCardTapped() {
        print("opne daily page")
//
    }
   
    
    let customView = UIView()
    var attributes = EKAttributes()

    
    func onItemimagetapped(story: String){
        print("tapped xis ")

        let newcontroller = AddIthakaItemSelectionController(collectionViewLayout: UICollectionViewFlowLayout())
        
        newcontroller.preWrittenText = story
        
        newcontroller.modalPresentationStyle = .fullScreen
        newcontroller.popoverPresentationController?.sourceView = self.view
        newcontroller.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        newcontroller.popoverPresentationController?.permittedArrowDirections = []
        
        newcontroller.transitioningDelegate = self
        newcontroller.modalPresentationStyle = .custom
        newcontroller.modalPresentationCapturesStatusBarAppearance = true

        
        self.present(newcontroller, animated: true, completion: nil)

        self.view.endEditing(true)

    }


    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        
        collectionView?.register(AddIthakaStartCell.self, forCellWithReuseIdentifier: "AddIthakaStartCell")
        collectionView?.register(QuestNewCardCell.self, forCellWithReuseIdentifier: "QuestNewCardCell")
//        collectionView?.register(MonsterQuestCard.self, forCellWithReuseIdentifier: "MonsterQuestCard")
        collectionView?.register(helpCard.self, forCellWithReuseIdentifier: "helpCard")

        self.collectionView?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        collectionView?.delegate = self
// rgba(231, 76, 60,1.0)
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateData), name: AddIthakaItemSelectionController.updateItemNotificationName, object: nil)
        

        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateData2), name: UnlockItemSelectionController.updateUnlockItemNotificationName, object: nil)
        
        UserDefaults.standard.set(1, forKey: "DailyQuestThing")  //Integer

        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateData3), name: PublishedAndWonController.updateFeedNotificationNameForCollect, object: nil)
        

        fetchCurrentPostImage()
        NotificationCenter.default.addObserver(self, selector: .keyboardWillShow, name: NSNotification.Name.UIKeyboardWillShow, object: nil)

    }
    
    
    var currentItemCodeNeeded: String?
    
    func fetchCurrentPostImage(){
        guard let uid = Auth.auth().currentUser?.uid else  { return }
        
        let ref = Database.database().reference().child("myItems").child(uid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            
            var ff = 1
            
            value.forEach({ (snapshot) in
                
                if ff == 1 {
                    let key = snapshot.key as! String
                    let image = UIImage(named: key + ".png")
                    self.currentItemCodeNeeded = key
                    self.imageData = image
                    ff += 1
                    

                    if key.hasPrefix("ci"){
                        let cl = Api.Items.commonItemsRequiredChar[key]
                        self.charlimit = cl
                    }
                    
                    if key.hasPrefix("uci"){
                        let cl = Api.Items.unCommonItemsRequiredChar[key]
                        self.charlimit = cl
                        
                    }
                    
                    if key.hasPrefix("ri"){
                        let cl = Api.Items.rareItemsRequiredChar[key]
                        self.charlimit = cl
                    }

                    
                    
                }
                
            })
            
            DispatchQueue.main.async {
                self.collectionView?.reloadSections([0])
                
            }

        }
        
       
    }
    
    @objc func handleUpdateData3(){
        DispatchQueue.main.async {
            
            
            self.fetchCurrentPostImage()
        
        
           self.tabBarController?.selectedIndex = 0
            self.navigationController?.popToRootViewController(animated: true)
            
        }

    }
    

    
    @objc func handleUpdateData(_ notification: NSNotification){
        print("yes")
        if let itemName = notification.userInfo?["itemName"] as? String {
            

            // do something with your image
            let iinm = itemName + ".png"
            
            self.imageData = UIImage(named: iinm)
            
            if itemName.hasPrefix("ci"){
                let cl = Api.Items.commonItemsRequiredChar[itemName]
                self.charlimit = cl
            }
            
            if itemName.hasPrefix("uci"){
                let cl = Api.Items.unCommonItemsRequiredChar[itemName]
                self.charlimit = cl

            }
            
            if itemName.hasPrefix("ri"){
                let cl = Api.Items.rareItemsRequiredChar[itemName]
                self.charlimit = cl
            }

            let story = notification.userInfo?["story"] as! String
            self.storytext = story
            self.currentItemCodeNeeded = itemName

            print("final pretext is ", story)

            DispatchQueue.main.async {
                self.collectionView?.reloadSections([0])
                
            }
        }

    }
    
    @objc func handleUpdateData2(_ notification: NSNotification){
        if let itemName = notification.userInfo?["itemName"] as? String {
            
            print("oiiasdkasbfsajbf,jsa ", itemName)
            
            if itemName.hasPrefix("unlocki") {
                print("yey, unlcoked mini quest")
                
                self.currentLockCode = 1
                
            }
            
            DispatchQueue.main.async {
                self.collectionView?.reloadSections([1])
            }
        }
        
    }

    
    var hhh: CGFloat?
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 5
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    var imageData: UIImage?
    var charlimit: Int?
    
     let ccLockCode =  UserDefaults.standard.integer(forKey: "DailyQuestThing")

    
    var currentLockCode: Int?
    var storytext: String?
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaStartCell", for: indexPath) as! AddIthakaStartCell
            
            print("super final pre text is ", storytext ?? "")
            cell.delegate = self
            cell.StoryTextView.text = storytext ?? ""
            let im = self.imageData
            let cl = self.charlimit ?? 40
            cell.postItem = im
            cell.profileItemView.image = im
            cell.minimumWordsRequired.text = String(cl)
            cell.requireChars = cl
            cell.currentItemCode = self.currentItemCodeNeeded ?? ""
            
//            if self.imageData == nil{
//                cell.imageisEmpty = true
//            } else {
//                cell.imageisEmpty = false
//            }
//            
            return cell
            
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestNewCardCell", for: indexPath) as! QuestNewCardCell
            cell.imageView.isHidden = true
            let attachment:NSTextAttachment = NSTextAttachment()
            attachment.image =  #imageLiteral(resourceName: "book")
            
            attachment.setImageHeight(height: 55)
            
            let attachmentString:NSAttributedString = NSAttributedString(string:"Daily Quest")
            let attributedString:NSMutableAttributedString = NSMutableAttributedString(attachment: attachment)
            
            
            attributedString.append(attachmentString)
            
            cell.questLabel.attributedText = attributedString
            cell.tag = 1
            cell.questLabel.tag = 1
            cell.currentLock = 1
            cell.backgroundColor = UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)

            cell.delegate = self
            return cell
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestNewCardCell", for: indexPath) as! QuestNewCardCell
            
            let attachment:NSTextAttachment = NSTextAttachment()
            attachment.image = #imageLiteral(resourceName: "red_monster")
            
            attachment.setImageHeight(height: 60)
            
            let attachmentString:NSAttributedString = NSAttributedString(string:" Monster Battle")
            let attributedString:NSMutableAttributedString = NSMutableAttributedString(attachment: attachment)
            
            
            attributedString.append(attachmentString)
            
            cell.questLabel.attributedText = attributedString
            cell.backgroundColor = UIColor.rgb(red: 231, green: 76, blue: 60, alpha: 1)
            cell.tag = 3
            cell.questLabel.tag = 3
            if self.checkIfDefaultKeyContains(key: "MonsterQuestThing") {
                cell.currentLock = self.ccLockCode
                
                print("print the ", self.ccLockCode)
                
            } else {
                
                print("print the 2", self.ccLockCode)
                
                cell.currentLock = 23
                
            }
            cell.imageView.image = #imageLiteral(resourceName: "red_crystals").withRenderingMode(.alwaysOriginal)
            cell.delegate = self
            
            return cell

        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "QuestNewCardCell", for: indexPath) as! QuestNewCardCell
            
            let attachment:NSTextAttachment = NSTextAttachment()
            attachment.image = #imageLiteral(resourceName: "decor_4")
            
            attachment.setImageHeight(height: 40)
            
            let attachmentString:NSAttributedString = NSAttributedString(string:" Mini Quest")
            let attributedString:NSMutableAttributedString = NSMutableAttributedString(attachment: attachment)
            
            
            attributedString.append(attachmentString)
            
            cell.questLabel.attributedText = attributedString
            cell.backgroundColor = UIColor.rgb(red: 72, green: 207, blue: 250, alpha: 1)

            if self.checkIfDefaultKeyContains(key: "MiniQuestThings") {
                cell.currentLock = self.ccLockCode
                
                print("print the ", self.ccLockCode)
                
            } else {
                
                print("print the 2", self.ccLockCode)

                cell.currentLock = 23
                
            }
            cell.tag = 2
            cell.questLabel.tag = 2
            cell.imageView.image = #imageLiteral(resourceName: "basic_lock").withRenderingMode(.alwaysOriginal)

            cell.delegate = self
            
            return cell
            
        case 4:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "helpCard", for: indexPath) as! helpCard
            cell.tag = 4
            cell.questLabel.tag = 4
            cell.backgroundColor = UIColor.clear
            
            cell.delegate = self
            return cell


        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaStartCell", for: indexPath) as! AddIthakaStartCell
            
            return cell

        }
    }
    
    func checkIfDefaultKeyContains(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.1
            
            switch indexPath.section {
                
                
            case 0:
                
                var finalHeight: CGFloat?
                
                if  UserDefaults.standard.object(forKey: "keyboardHeight") != nil {
                    finalHeight = UserDefaults.standard.object(forKey: "keyboardHeight") as? CGFloat
                    
                } else {
                    finalHeight = 300
                    
                    NotificationCenter.default.addObserver(self, selector: .keyboardWillShow, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
                    
                }
                
                let fh = finalHeight ?? 250
                return CGSize(width: view.frame.width - ph + 30, height: view.frame.height + 20 - fh)
                
            case 1:
                return CGSize(width: view.frame.width - ph, height: 95)
            case 2:
                return CGSize(width: view.frame.width - ph, height: 95)
            case 3:
                return CGSize(width: view.frame.width - ph, height: 95)
            case 4:
                return CGSize(width: view.frame.width - ph, height: 75)
                
            default:
                return .zero
            }

            
        default:
            switch indexPath.section {
                
                
            case 0:
                
                var finalHeight: CGFloat?
                
                if  UserDefaults.standard.object(forKey: "keyboardHeight") != nil {
                    finalHeight = UserDefaults.standard.object(forKey: "keyboardHeight") as? CGFloat
                    print("finalheigh23t is ", finalHeight)

                } else {
                    finalHeight = 300
                    
                    NotificationCenter.default.addObserver(self, selector: .keyboardWillShow, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
                    
                }
                
                let fh = finalHeight ?? 350
                return CGSize(width: view.frame.width, height: view.frame.height + 30 - fh)
                
            case 1:
                return CGSize(width: view.frame.width - 30, height: 95)
            case 2:
                return CGSize(width: view.frame.width - 30, height: 95)
            case 3:
                return CGSize(width: view.frame.width - 30, height: 95)
            case 4:
                return CGSize(width: view.frame.width - 30, height: 75)
                
            default:
                return .zero
            }

            
        }
        
    }
    
    @objc fileprivate func keyboardWillShow(notification:NSNotification) {
        if let keyboardRectValue = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardRectValue.height
            
            UserDefaults.standard.set(keyboardHeight, forKey: "keyboardHeight")
            print("freaking height is", keyboardHeight)
        }
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.05
            
            switch section {
            case 0:
                return UIEdgeInsetsMake(0, ph, 0, ph)
            case 1:
                return UIEdgeInsetsMake(20, ph, 10, ph)
            case 2:
                return UIEdgeInsetsMake(10, ph, 10, ph)
            case 3:
                return UIEdgeInsetsMake(10, ph, 10, ph)
            case 4:
                return UIEdgeInsetsMake(10, ph, 10, ph)
                
                
            default:
                return UIEdgeInsetsMake(0, ph, 0, ph)
                
            }

            
        default:
            switch section {
            case 0:
                return UIEdgeInsetsMake(0, 0, 0, 0)
            case 1:
                return UIEdgeInsetsMake(20, 15, 10, 15)
            case 2:
                return UIEdgeInsetsMake(10, 15, 10, 15)
            case 3:
                return UIEdgeInsetsMake(10, 15, 10, 15)
            case 4:
                return UIEdgeInsetsMake(10, 15, 10, 15)
                
                
            default:
                return UIEdgeInsetsMake(0, 0, 0, 0)
                
            }

            
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            print("0 selected")
        case 1:
            print("1 selected")
        case 2:
            print("1 selected")
        case 3:
            print("1 selected")


        default:
            print("Something slected")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true

    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        self.navigationController?.navigationBar.isHidden = false
//
//    }
    
}

extension IthakaQuestAddFInalController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? PreviewPostController {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal,
                               interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard
                let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController,
                interactionController.inProgress
                else {
                    return nil
            }
            return interactionController
    }
}



extension IthakaQuestAddFInalController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return SystemPushAnimator(type: .navigation)
        case .pop:
            return SystemPopAnimator(type: .navigation)
        default:
            return nil
        }
    }
    
}


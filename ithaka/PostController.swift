//
//  PostController.swift
//  ithaka
//
//  Created by Apurva Raj on 31/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
import SwiftMessages

class PostController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIViewControllerTransitioningDelegate, PostDelegate {
    func onRocketCountButtonTapped(for cell: PostPreviewHeader) {
        return
    }
    
    func didTapComment(post: PostNew) {
        return
    }
    
    func didTapNewComment(post: PostNew) {
        return
    }
    
    func onShareButtonTapped(for cell: PostPreviewHeader) {
        return
    }
    
    func onNameIdLabelTapped(userId: String) {
        return
    }
    
    func onNameLabelTapped(for cell: PostPreviewHeader) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.posts[indexPath.item]
        
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = post.user.uid
        self.navigationController?.pushViewController(profileController,
                                                      animated: true)

    }
    
    func didTapComment(post: Post) {
        print("bc")
    }
    
    func onRocketButtonTapped(for cell: PostPreviewHeader) {
        print("pritn cbc")
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        DispatchQueue.main.async {
            self.collectionView?.reloadItems(at: [indexPath])
        }
    }
    
    let postCellId = "postCellId"
    var userId: String?
    
    var postId: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        


        collectionView?.backgroundColor = .white
        
        collectionView?.register(PostPreviewHeader.self, forCellWithReuseIdentifier: postCellId)
        collectionView?.register(ProfileHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")

        fePost()
        fetchUser()
        
    }
    
    
    var posts = [Post]()
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    var post: Post?

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postCellId, for: indexPath) as! PostPreviewHeader
//        cell.post = self.post
//        cell.post = posts[indexPath.item]
//
//         let postC = self.posts[indexPath.item]
//
//        postss = postC.id
        
        cell.delegate = self
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let storyView:UITextView = UITextView(frame: CGRect(x: 0, y: 0, width: collectionView.frame.width-10, height: CGFloat.greatestFiniteMagnitude))
        storyView.font = UIFont.systemFont(ofSize: 16)
        let post = posts[indexPath.item]
        storyView.text = post.story
        storyView.sizeToFit()
        storyView.isScrollEnabled = false
        storyView.layoutIfNeeded()
        

        let width = view.frame.width
        let hh = (storyView.frame.height)+width + 65
    
        return CGSize(width: width, height: hh)

    }
    
   
    fileprivate func fetchPost(){
        guard let uid = self.user?.uid else { return }
        let ref = Database.database().reference().child("posts").child(uid)
        print(ref)
        
//
//       let refHandle = ref.observe(DataEventType.value, with: { (snapshot) in
//            let postDict = snapshot.value as? [String : AnyObject] ?? [:]
//        })

    }
    
    var user: UserModel?
    fileprivate func fetchUser() {
        
        guard let uid = userId else { return }
        //guard let uid = FIRAuth.auth()?.currentUser?.uid else { return }
        
        Database.fetchUserWithUID(uid: uid) { (user) in
            self.user = user
            
            self.collectionView?.reloadData()
            self.fetchPost()

        }
    }
    
    fileprivate func fePost() {
        guard let uid = userId else { return }
//        guard let pid = postId else { return }
        Database.fetchUserWithUID(uid: uid) { (user) in
            self.fetchPostsWithUser(user: user)
        }
    }
    
    fileprivate func fetchPostsWithUser(user: UserModel){
        guard let pid = postId else { return }
        let ref = Database.database().reference().child("posts").child(user.uid).child(pid)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            self.collectionView?.refreshControl?.endRefreshing()
            
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            
            var post = Post(user: user, dictionary: dictionary)

            print("love")
            post.id = snapshot.key

                self.posts.append(post)
                self.collectionView?.reloadData()
        }
    }
    

    
    var postss: String?
    
    @objc func didTapComment() {
        
        

    }
    

    @objc func didTapRocket() {
        let gifcontroller = GifController()
        self.addChildViewController(gifcontroller)
        self.view.addSubview(gifcontroller.view)
        gifcontroller.didMove(toParentViewController: self)
        
        let when = DispatchTime.now() + 4
        DispatchQueue.main.asyncAfter(deadline: when) {
            print("Start remove sibview")
            gifcontroller.willMove(toParentViewController: nil)
            
            // Remove the child
            gifcontroller.removeFromParentViewController()
            
            // Remove the child view controller's view from its parent
            gifcontroller.view.removeFromSuperview()
            
        }
        
        guard let indexPath = collectionView?.indexPathForItem(at: CGPoint(x: view.frame.width-30, y: view.frame.height-80)) else { return }
        
        var post = self.posts[indexPath.item]
        guard let postId = post.id else { return }
        let postUserId = post.user.uid
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Database.database().reference().child("rockets").child(postId)
        
        let ref3 = Database.database().reference().child("posts").child(postUserId).child(postId)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            let taps = 1
            
            let cc = value?.count ?? 0
            
            let updates = [uid: taps]
            let rocketCount = ["rocketCount":cc]
            
            ref.updateChildValues(updates)
            
            ref3.updateChildValues(rocketCount, withCompletionBlock: { (err, ref) in
                
                post.rocketCount = cc
                self.posts[indexPath.item] = post
                self.collectionView?.reloadItems(at: [indexPath])
                
            })
            
        }
    
}
    
}

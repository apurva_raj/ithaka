//
//  PostProfileUi.swift
//  ithaka
//
//  Created by Apurva Raj on 05/04/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//


import UIKit
import FirebaseAuth
import FirebaseDatabase
//import ActiveLabel

class PostProfileUi: UICollectionViewCell {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            print(currentPoint)
            // do something with your currentPoint
        }
    }
    var forScroll = SLPagingViewSwift2()
    
    //    func nextCardView() -> UIView? {
    //        return UIView
    //    }
    
    weak var delegate: MyCustomDelegate?
    var post: PostNew? {
        didSet {
            
            blockedCheck()
            guard let title = post?.title else { return }
            guard let story = post?.story else { return }
            guard let tag = post?.tag else { return }

//            guard let profileImageUrl = post?.user.profileImageUrl else { return }
//            guard let username = post?.user.username else { return }
            
            guard let emoji = post?.emoji else { return }

            guard let commetsCount = post?.commentsCount else { return }
            guard let rocketCount = post?.rocketCount else { return }
            
            guard let postId = post?.id else { return }
            
            guard let currentuid = Auth.auth().currentUser?.uid else { return }

            let ref = Database.database().reference().child("rockets").child(postId)
            
            ref.observeSingleEvent(of: .value) { (snapshot) in
                //                let value = snapshot.value as? NSDictionary
                if snapshot.hasChild(currentuid) {
                    self.rocketImageView.setImage(#imageLiteral(resourceName: "icons8-rocket-filled-50").withRenderingMode(.alwaysOriginal), for: .normal)
                } else {
                    self.rocketImageView.setImage(#imageLiteral(resourceName: "rocket_unselected").withRenderingMode(.alwaysOriginal), for: .normal)
                }
            }
            
            let commentsCountString = String(commetsCount)
            let rokcketCountString = String(rocketCount)
            
        
            //            mainpicView.loadImage(urlString: postImageUrl)
            titleLabel.text = title
            storyLabel.text = story
            
            emojiLabel.text = emoji
            
            CommentLabel.text = commentsCountString
            rocketCountLabel.text = rokcketCountString
            
//            nameLabel.text = username
//            profileImageView.loadImage(urlString: profileImageUrl)
            
            
//            guard let uid = post?.user.uid else { return }
            
//            if(uid != cuid) {
//                optionButton.isEnabled = false
//                optionButton.isHidden = true
//            } else {
//                optionButton.isEnabled = true
//                optionButton.isHidden = false
//            }
            
            questTagLabel.text = tag

            
            setupAttributedDate()
            
        }
    }
    
    fileprivate func setupAttributedDate() {
        guard let post = self.post else { return }
        
//        let attributedText = NSMutableAttributedString(string: post.user.username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
//        
//        attributedText.append(NSAttributedString(string: "  ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
//        
//        let timeAgoDisplay = post.creationDate.timeAgoDisplay()
//        attributedText.append(NSAttributedString(string: timeAgoDisplay, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor.gray]))
        
//        nameLabel.attributedText = attributedText
    }
    
    //    lazy var ss: UIScrollView = {
    //        let ss = UIScrollView()
    //        ss.isScrollEnabled
    //    }()
    
    lazy var questTagLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textAlignment = .center
        label.textColor = .white
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        
        label.isUserInteractionEnabled = false
        label.backgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)

        
        return label
    }()
    
    lazy var titleLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 18)
        textView.isUserInteractionEnabled = true
//        textView.sizeToFit()
        textView.isScrollEnabled = true
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        //        textView.backgroundColor = .red
        textView.contentInset = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 5)
//        textView.textAlignment = .center
        
        
        textView.layoutIfNeeded()
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
        tap.numberOfTapsRequired = 1
        textView.addGestureRecognizer(tap)
        
        
        return textView
    }()
    
    
    @objc func titleLabelTapped() {
        self.delegate?.onTitleLabelTappedFromProfile(for: self)
    }
    
    lazy var emojiLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 18)
        textView.isUserInteractionEnabled = false
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        //        textView.backgroundColor = .red
        textView.contentInset = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
        textView.textAlignment = .center
        textView.text = "😂😎🍕😄"
        
        return textView
    }()
    
    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
//    lazy var nameLabel: UILabel = {
//        let label = UILabel()
//        label.font = label.font.withSize(13)
//        label.numberOfLines = 0
////        label.textAlignment = .center
//        label.textColor = .gray
//        label.isUserInteractionEnabled = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
//        tap.numberOfTapsRequired = 1
//        label.addGestureRecognizer(tap)
//
//
//        return label
//    }()
    
    @objc func nameLabelTapped(_ recognizer: UITapGestureRecognizer) {
//        self.delegate?.onNameLabelTapped(for: self)
    }
    
    lazy var optionButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_expand_more"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOptionButton))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        button.isEnabled = false
        button.isHidden = true
        return button
    }()
    
    @objc func handleOptionButton() {
        self.delegate?.onOptionButtonTappedFromProfile(for: self)
    }
    
    lazy var storyLabel: UITextView = {
        let textview = UITextView()
        textview.textColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.85)
        textview.font = UIFont.systemFont(ofSize: 17)
        
        textview.isUserInteractionEnabled = true
        textview.isEditable = false
        textview.isScrollEnabled = false
        textview.sizeToFit()
        textview.translatesAutoresizingMaskIntoConstraints = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
        tap.numberOfTapsRequired = 1
        textview.addGestureRecognizer(tap)
        return textview
    }()
    
    
    
    @objc func storyViewTapped(_ recognizer: UITapGestureRecognizer) {
        print("Story tv tapped")
        
//        self.delegate?.onStoryViewTapped(post: post)
        
    }

    @objc func handleComment() {
        print("Trying to show comments...")
        
//        delegate?.didTapComment(post: post)
    }
    
    lazy var rocketImageView: UIButton = {
        let button = UIButton(type: .custom)
        let tap = UITapGestureRecognizer(target: self, action: #selector(rocketButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    
    lazy var commentImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "comment"), for: .normal)
        button.addTarget(self, action: #selector(handleComment), for: .touchUpInside)
        
        return button
    }()
    
    
    @objc func rocketButtonTapped(sender: UITapGestureRecognizer){
        self.delegate?.onRocketButtonTappedFromProfile(for: self)
        print("flyyy")
        
    }
    
    let rocketCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .center
        
        return label
    }()
    let CommentLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textAlignment = .center
        label.textColor = .gray
        
        return label
    }()
    
    let bottomDividerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        return view
    }()
    
    func blockedCheck(){
        guard let currentLoggedInUserId = Auth.auth().currentUser?.uid else { return }
        
        guard let userId = post?.uid else { return }
        
        print("fuc", userId)
        let ref = Database.database().reference().child("BlockedProfile").child(currentLoggedInUserId).child(userId)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                print("it works")
            } else {
                self.notBlocked()
                
            }
        }
    }
    
    fileprivate func notBlocked() {
    
    addSubview(questTagLabel)
    addSubview(titleLabel)
    
    addSubview(profileImageView)
    //        addSubview(nameLabel)
    addSubview(optionButton)
    addSubview(emojiLabel)
    addSubview(storyLabel)
    
    
    addSubview(rocketImageView)
    addSubview(commentImageView)
    
    addSubview(rocketCountLabel)
    addSubview(CommentLabel)
    
    
    addSubview(bottomDividerView)
    //        let dpl = (frame.width/2)-12.5
    self.bringSubview(toFront: bottomDividerView)
        
        questTagLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant:10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)

    
    profileImageView.anchor(top: questTagLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)
    titleLabel.anchor(top: questTagLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 37)
    
    //        nameLabel.anchor(top: titleLabel.bottomAnchor, left: profileImageView.rightAnchor, bottom: nil, right: self.rightAnchor, topConstant: -10, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
    
    optionButton.anchor(top: questTagLabel.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    
    emojiLabel.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: -6, leftConstant: 10, bottomConstant: 5, rightConstant: 10, widthConstant: 0, heightConstant: 30)
    
    storyLabel.anchor(top: emojiLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 190)
    
    let wh = (frame.width*0.20)
    commentImageView.anchor(top: storyLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant:-10, leftConstant: wh, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
    rocketImageView.anchor(top: storyLabel.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant:-10, leftConstant: 0, bottomConstant: 0, rightConstant: wh, widthConstant: 0, heightConstant: 30)
    
    CommentLabel.anchor(top: rocketImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: wh+5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    rocketCountLabel.anchor(top: rocketImageView.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: wh+10, widthConstant: 0, heightConstant: 0)
    
    bottomDividerView.anchor(top: CommentLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 3, leftConstant: 0, bottomConstant: -20, rightConstant: 0, widthConstant: 0, heightConstant: 20)
    }
    
        
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        backgroundColor = UIColor.rgb(red: 232, green: 126, blue: 4, alpha: 0.5)
        blockedCheck()
        
    }
    
    func LogoutbuttonTapped() {
        print(42)
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


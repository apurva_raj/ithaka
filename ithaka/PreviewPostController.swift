//
//  PreviewPostController.swift
//  ithaka
//
//  Created by Apurva Raj on 09/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import Foundation
import Firebase
import SwiftMessages
import Pulsator


class PreviewPostController: UICollectionViewController, UICollectionViewDelegateFlowLayout, PostDelegate, UIViewControllerTransitioningDelegate, textViewTap, removeChild, UIGestureRecognizerDelegate {
    
    static let updateReadStoryNotificationName = NSNotification.Name(rawValue: "UpdateForReadingBackStory")

    
    var comingFromNotificationId: NotificationPostLoadModel?{
        didSet{
            
            guard let pId = comingFromNotificationId?.postId else { return }
            guard let uid = comingFromNotificationId?.visitorId else { return }
            
            print("yes is ", pId)
            Api.NewPost.observeSinglePost1(withUId: uid, withPid: pId, completion: { (postnew) in
                
            
                
                Api.User.observeUser(withId: uid) { (usermodelnew) in
                    
                    if postnew?.creationDate == nil {
                        print("yahho")
                        
                        let redview: UIView = {
                            let view = UIView()
                            view.backgroundColor = .white
                            
                            return view
                        }()
                        
                        let message: UILabel = {
                            let label = UILabel()
                            label.text = "Oops! Content Removed By the Owner"
                            label.font = UIFont.systemFont(ofSize: 23)
                            label.textColor = .gray
                            label.numberOfLines = 0
                            label.lineBreakMode = .byWordWrapping
                            label.textAlignment = .center
                            
                            return label
                        }()
                        
                        self.view.addSubview(redview)
                        self.view.addSubview(message)
                        
                        redview.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 50, rightConstant: 0, widthConstant: self.view.frame.width, heightConstant: self.view.frame.height)
                        message.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: (self.view.frame.height/2) - 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 100)
                        self.myActivityIndicator.stopAnimating()
                        
                    } else {
                        self.user = usermodelnew
                        self.post = postnew
                        self.collectionView?.reloadData()
                        self.myActivityIndicator.stopAnimating()
                    }
                    
                }
            })

            
        }
    }
    
    
    func readbackstory(itemCode: String?) {

        
        removeChild()
        self.dismiss(animated: true, completion: nil)
        
        let dataDict: [String: Any] = ["itemName": itemCode]

        NotificationCenter.default.post(name: PreviewPostController.updateReadStoryNotificationName, object: nil, userInfo: dataDict)

       
    }
    
    
    var interactionController: LeftEdgeInteractionController?

    
    func removeChild() {
        print("working??")
        
        if self.childViewControllers.count > 0{
            let viewControllers:[UIViewController] = self.childViewControllers
            for viewContoller in viewControllers{
                viewContoller.willMove(toParentViewController: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParentViewController()
            }
        }
    }

    static let PushHashtagPage = NSNotification.Name(rawValue: "PushHashtagPage")
    static let PushProfilePage = NSNotification.Name(rawValue: "PushProfilePage")

    func hashTagTapped(hash: String) {
        
        let dataDict: [String: Any] = ["hashtag": hash]
        
        NotificationCenter.default.post(name: PreviewPostController.PushHashtagPage, object: nil, userInfo: dataDict)
        
        self.dismiss(animated: true, completion: nil)

    }
    
    func usernameTapped(username: String) {
        
        let dataDict: [String: Any] = ["username": username]
        
        NotificationCenter.default.post(name: PreviewPostController.PushProfilePage, object: nil, userInfo: dataDict)
        
        self.dismiss(animated: true, completion: nil)


    }
    
    
    func onRocketCountButtonTapped(for cell: PostPreviewHeader) {
        let rocketListController = RocketListController(collectionViewLayout: UICollectionViewFlowLayout())
        rocketListController.popoverPresentationController?.sourceView = self.view
        rocketListController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        rocketListController.popoverPresentationController?.permittedArrowDirections = []
        
        rocketListController.backbutton = "lost"
        rocketListController.post = post
        self.present(rocketListController, animated: true, completion: nil)


    }
    
    
    func didTapComment(post: PostNew) {
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.post = post
        
        commentsController.popoverPresentationController?.sourceView = self.view
        commentsController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        commentsController.backbutton = "lost"
        commentsController.popoverPresentationController?.permittedArrowDirections = []
        
        self.present(commentsController, animated: true, completion: nil)
    }
    
    func onShareButtonTapped(for cell: PostPreviewHeader) {
        print("yaho")

        let downloadImgaesController = DownloadImagesController()
        downloadImgaesController.post = post
        downloadImgaesController.user = user
        
        downloadImgaesController.popoverPresentationController?.sourceView = self.view
        downloadImgaesController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        downloadImgaesController.popoverPresentationController?.permittedArrowDirections = []

        self.present(downloadImgaesController, animated: true, completion: nil)

    }
    
    func onNameIdLabelTapped(userId: String) {
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        
        print("bc na", userId)
        
        profileController.popoverPresentationController?.sourceView = self.view
        profileController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        profileController.popoverPresentationController?.permittedArrowDirections = []
        profileController.userId = userId
        profileController.oneview.backgroundColor = .white
        present(profileController, animated: true, completion: nil)
    }
    
    
    func onNameLabelTapped(for cell: PostPreviewHeader) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.posts[indexPath.item]
        
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = post.user.uid
        self.present(profileController, animated: true, completion: nil)

//        self.navigationController?.pushViewController(profileController,
//                                                      animated: true)

    }
    
    
    
    func didTapComment(post: Post) {
        
    }
    
    func onRocketButtonTapped(for cell: PostPreviewHeader) {
        print("pritn cbc")
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        DispatchQueue.main.async {
            self.collectionView?.reloadItems(at: [indexPath])
        }

    }
    
    
    // define a variable to store initial touch position

    let postCellId = "postCellId"
    
    var postId: String?
    var post: PostNew?
    var user: UserModelNew?
    
    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)

        return button
    }()
    
    lazy var testButton: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.clear
        v.isUserInteractionEnabled = false
        v.isHidden = true
        return v
    }()
    
    lazy var wonItemButton: UIImageView = {
        let v = UIImageView()
        v.image = #imageLiteral(resourceName: "chest")
        v.backgroundColor = UIColor.clear
        v.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(testButtonTapped))
        tap.numberOfTapsRequired = 1
        v.addGestureRecognizer(tap)
        v.isHidden = true
        return v
    }()

    
    @objc func testButtonTapped(){
        print("you have won everything")
        setupchestchild()
        testButton.isHidden = true
        wonItemButton.isHidden = true
        
    }


    let oneview = UIView()
    
    private func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleEnterForeground), name: .UIApplicationWillEnterForeground, object: nil)
    }
    
    @objc private func handleEnterForeground() {
        animatePulsatingLayer()
    }
    
    private func createCircleShapeLayer(strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        
        let circularPath = UIBezierPath(arcCenter: .zero, radius: 20, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = 5
        layer.fillColor = fillColor.cgColor
        layer.lineCap = kCALineCapRound
        
        return layer
    }
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    

    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()

        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        collectionView?.alwaysBounceVertical = true
        collectionView?.delegate = self
        collectionView?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        collectionView?.showsVerticalScrollIndicator = false
//        collectionView?.register(PostPreviewHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        collectionView?.register(PostPreviewNewHeader.self, forCellWithReuseIdentifier: "headerId")
        oneview.backgroundColor = .black
        view.addSubview(dismissButton)
        view.addSubview(oneview)

        dismissButton.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        oneview.anchor(top: view.safeAreaLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

//        setupNavigationButtons()
        
//        self.view.addGestureRecognizer(swipeDown)
    
        view.addSubview(testButton)
        view.addSubview(wonItemButton)
        testButton.anchor(top: nil, left: nil, bottom: dismissButton.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 40, bottomConstant: 25, rightConstant: 25, widthConstant: 30, heightConstant: 31)
        wonItemButton.anchor(top: nil, left: nil, bottom: dismissButton.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 20, rightConstant: 20, widthConstant: 70, heightConstant: 70)

        
//        animatePulsatingLayer()
        

        setupNotificationObservers()
        
        setupCircleLayers()
        
        self.interactionController = LeftEdgeInteractionController(viewController: self)
        
        allthingsItemDisocery()

        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.05
            
            self.collectionView?.contentInset = UIEdgeInsetsMake(0, ph, 50, ph)

            
        default:
            print("i am Everyone except ipad")
            self.collectionView?.contentInset = UIEdgeInsetsMake(0, 15, 50, 15)

            
        }
        
        
        

    }
    
    


    func setupchestchild(){
        
        
        let typecode = self.itemtypecode ?? 0
        
        let chestController = ItemDiscoveredController()
        chestController.typecode = typecode
        chestController.delegate = self
        addChildViewController(chestController)
        view.addSubview(chestController.view)
        chestController.didMove(toParentViewController: self)
        
    }
    

    
    var shapeLayer: CAShapeLayer!
    var pulsatingLayer: CAShapeLayer!
    var trackLayer: CAShapeLayer!
    private func setupCircleLayers() {
        pulsatingLayer = createCircleShapeLayer(strokeColor: .clear, fillColor: UIColor.pulsatingFillColor)

        self.testButton.layer.addSublayer(pulsatingLayer)
        pulsatingLayer.isHidden = true
        animatePulsatingLayer()
        
        trackLayer = createCircleShapeLayer(strokeColor: .trackStrokeColor, fillColor: .backgroundColor)
        trackLayer.isHidden = true
        self.testButton.layer.addSublayer(trackLayer)
        
        shapeLayer = createCircleShapeLayer(strokeColor: .outlineStrokeColor, fillColor: .clear)
        shapeLayer.isHidden = true
        
        shapeLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
        shapeLayer.strokeEnd = 0
        self.testButton.layer.addSublayer(shapeLayer)
        
        myActivityIndicator.stopAnimating()
    }
    
    private func animatePulsatingLayer() {
        let animation = CABasicAnimation(keyPath: "transform.scale")
        
        animation.toValue = 2.0
        animation.duration = 0.8
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        animation.autoreverses = true
        animation.repeatCount = Float.infinity
        pulsatingLayer.add(animation, forKey: "pulsing")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNotificationObservers()
        animatePulsatingLayer()
    }
    
    
    var visitorUid: String?

    fileprivate func allthingsItemDisocery(){

        guard let vUid = visitorUid else { return }
        guard let pid = post?.id else { return }

        let ref = Database.database().reference().child("ItemDiscoveryViewList").child(vUid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.hasChild(pid) {
                print("nothing")
//                self.unhideItemTap()
            } else {
                print("something")
                let values = [pid: true]
            
                if UserDefaults.standard.object(forKey: "currentItemReadCount") != nil {
                    let cc = UserDefaults.standard.integer(forKey: "currentItemReadCount")
                    self.test(curentCount: cc)
                    ref.updateChildValues(values)


                } else {
                    
                }
//                Api.DiscoverItem.discoverItem(curentCount: cc)

            }
            

        }
    }
    
    var itemtypecode: Int?
    
    func test(curentCount: Int){
        print("current count is ", curentCount)
        if curentCount > 50 {
            
            DiscoverItemAPI.discoverItemDefaults.set(0, forKey: "currentItemReadCount")
            
        } else {
            let ncc = curentCount + 1
            print("ncc is", ncc)
            
            switch ncc {
            case 4:
                print("vv")
                self.unhideItemTap()
                self.itemtypecode = 1
                // fetch random item from common list
            case 8:
                print("vv")
                self.unhideItemTap()
                self.itemtypecode = 1

                // fetch random item from common list
            case 15:
                print("vv")
                self.unhideItemTap()
                self.itemtypecode = 1

                // fetch random item from common list
            case 16:
                self.unhideItemTap()
                self.itemtypecode = 1
                
                // fetch random item from uncommon list
            case 23:
                self.unhideItemTap()
                self.itemtypecode = 2
                
                // fetch random item from ucommon list
            case 42:
                self.unhideItemTap()
                self.itemtypecode = 2
                
                // fetch random item from rare list

            default:
                print("nothingasadadasdasdasdasdas")
            }
            
            DiscoverItemAPI.discoverItemDefaults.set(ncc, forKey: "currentItemReadCount")

            
        }
    }
    
    func unhideItemTap(){
        self.wonItemButton.isHidden = false
        self.testButton.isHidden = false
        self.pulsatingLayer.isHidden = false
        self.shapeLayer.isHidden = false
        self.trackLayer.isHidden = false
    }
    
 

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "headerId", for: indexPath) as! PostPreviewNewHeader

        cell.post = self.post
        cell.user = self.user
        cell.delegate = self
        cell.textViewTap = self

        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let storyView:UITextView = UITextView(frame: CGRect(x: 0, y: 0, width: collectionView.frame.width-70, height: CGFloat.greatestFiniteMagnitude))
        storyView.font = UIFont.systemFont(ofSize: 16)
        storyView.text = post?.story
        storyView.sizeToFit()
        storyView.isScrollEnabled = false
        storyView.layoutIfNeeded()
        print(storyView.frame.height)
        
        let width = view.frame.width
        
        let tag = post?.tag ?? "none"
        let title = post?.title ?? ""

        let qs = tag.height(withConstrainedWidth: view.frame.width - 30, font: UIFont.systemFont(ofSize: 11))
        print("qqs is", qs)
        let ts = title.height(withConstrainedWidth: view.frame.width - 30, font: UIFont.boldSystemFont(ofSize: 21))
        
        let hh = (storyView.frame.height) + 5 + 20 + ts + 30 + 35 + 30 + 50
        
        switch UIDevice.current.userInterfaceIdiom {

            case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.10
            
            return CGSize(width: view.frame.width - ph, height: hh)
            
            
            default:
            print("i am Everyone except ipad")
            return CGSize(width: view.frame.width - 30, height: hh)
        
        
        }

    }
    
        @objc func onSwipeDown(_ recognizer: UISwipeGestureRecognizer) {
            dismiss(animated: true, completion: nil)
        }
    
    let currentIndex = 4
            @objc func handleDismiss(){
            self.tabBarController?.selectedIndex = currentIndex
            dismiss(animated: true, completion: nil)

        }
        var newPosts = [PostNew]()
        var posts = [Post]()
    
    
    fileprivate func setupNavigationButtons() {
        navigationController?.navigationBar.tintColor = .black
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(handleCancel))

    }
    @objc func handleCancel() {
        navigationController?.popViewController(animated: true)
    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

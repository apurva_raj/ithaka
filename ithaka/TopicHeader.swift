//
//  TopicHeader.swift
//  ithaka
//
//  Created by Apurva Raj on 24/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit

class TopicHeader: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  Points.swift
//  ithaka
//
//  Created by Apurva Raj on 01/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import Foundation


class Points {
    
    var id: String?
    
    var collectedPoints: Int?
    var likePoints: Int?
    var storyPoints: Int?
    var commentPoints: Int?
    
    
}


extension Points {
    static func transform(dict: [String: Any], key: String) -> Points {
        
        let points = Points()
        points.id = key
        points.collectedPoints = dict["CollectedPoints"] as? Int? ?? 0
        points.storyPoints = dict["storyPoints"] as? Int? ?? 0
        points.commentPoints = dict["commentPoints"] as? Int? ?? 0
        points.likePoints = dict["likePoints"] as? Int? ?? 0

        return points
    }
}





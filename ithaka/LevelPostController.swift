//
//  LevelPostController.swift
//  ithaka
//
//  Created by Apurva Raj on 01/06/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import SwiftMessages

protocol LevelPostDelegate: class {
    
    func onTitleLabelTapped(for cell: LevelPostCell)
    func onStoryViewTapped(post: Post)
    func onNameLabelTapped(for cell: LevelPostCell)
    func onRocketButtonTapped(for cell: LevelPostCell)
    func didTapComment(post: Post)


}


class LevelPostController: UICollectionViewController, UICollectionViewDelegateFlowLayout, LevelPostDelegate,UIViewControllerTransitioningDelegate {
  

    
    func onRocketButtonTapped(for cell: LevelPostCell) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        var post = self.posts[indexPath.item]
        guard let postId = post.id else { return }
        let postUserId = post.user.uid
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Database.database().reference().child("rockets").child(postId)
        
        let ref2 = Database.database().reference().child("users").child(postUserId)
        let ref3 = Database.database().reference().child("posts").child(postUserId).child(postId)
        
        
        let values = [uid : 1]
        
        
        print("Lift off success.")
        
        self.posts[indexPath.item] = post
        
        //            self.finaView?.reloadItems(at: [indexPath])
        
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
            if snapshot.hasChild(uid) {
                print("it has", uid)
                
                SwiftMessages.show {
                    let view = MessageView.viewFromNib(layout: .CardView)
                    view.configureTheme(.warning)
                    view.configureDropShadow()
                    let iconText = "🙄"
                    view.configureContent(title: "Oops", body: "You already sent the rocket. It can't come back now", iconText: iconText)
                    view.button?.isHidden = true
                    
                    return view
                }
                
            } else {
                
                let gifcontroller = GifController()
                self.addChildViewController(gifcontroller)
                self.view.addSubview(gifcontroller.view)
                gifcontroller.didMove(toParentViewController: self)
                
                let when = DispatchTime.now() + 4
                DispatchQueue.main.asyncAfter(deadline: when) {
                    print("Start remove sibview")
                    gifcontroller.willMove(toParentViewController: nil)
                    
                    // Remove the child
                    gifcontroller.removeFromParentViewController()
                    
                    // Remove the child view controller's view from its parent
                    gifcontroller.view.removeFromSuperview()
                    
                }
                ref.updateChildValues(values)
                
                ref3.observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    let value2 = snapshot.value as? NSDictionary
                    
                    let rcount = value2?["rocketCount"] as? Int ?? 0
                    
                    let rfinalcount = rcount+1
                    
                    let values22 = ["rocketCount" : rfinalcount]
                    print(values22)
                    ref3.updateChildValues(values22)
                    
                    ref2.observeSingleEvent(of: .value, with: { (snapshot) in
                        let value3 = snapshot.value as? NSDictionary
                        
                        let rcount3 = value3?["rocketCount"] as? Int ?? 0
                        
                        let totalrocket = rcount3 + 1
                        
                        let totalrocketadd = ["rocketCount": totalrocket]
                        
                        ref2.updateChildValues(totalrocketadd)
                        self.posts[indexPath.item] = post
                        
                        DispatchQueue.main.async {
                            self.collectionView?.reloadItems(at: [indexPath])
                        }
                        
                        
                    })
                    
                })
                
                
                print("succcesssss!")
                
                
            }
        })
        

    }
    
    func didTapComment(post: Post) {
        print("bc")


    }
    
  
    func onNameLabelTapped(for cell: LevelPostCell) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.posts[indexPath.item]
        
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = post.user.uid
        self.navigationController?.pushViewController(profileController,
                                                      animated: true)

    }
    
    
    func onTitleLabelTapped(for cell: LevelPostCell) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        let post = self.posts[indexPath.item]
        
        guard let postId = post.id else { return }
        
        let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
        postController.userId = post.user.uid
        postController.postId = postId
        navigationController?.pushViewController(postController, animated: true)

    }
    
    
    
    func onStoryViewTapped(post: Post) {
        return
    }
    
    
    let headerId = "headerId"
    let cellId = "cellId"
    var level: Int?
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myActivityIndicator.center = view.center
        
        view.addSubview(myActivityIndicator)
        
        myActivityIndicator.startAnimating()

        collectionView?.backgroundColor = .white

        collectionView?.delegate = self
        
        
        collectionView?.register(LevelPostHeaderCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerId)
        collectionView?.register(LevelPostCell.self, forCellWithReuseIdentifier: cellId)

        fetchPosts()
        
        cardView()
        
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        

    }
    
    @objc func handleRefresh() {
        posts.removeAll()
        fetchPosts()
        
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
        }
    }
    
    fileprivate func cardView() {
     
    }
    
    var posts = [Post]()
    var post: Post?

    
   override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId, for: indexPath) as! LevelPostHeaderCell
    let levelN = level ?? 0
    header.level = levelN
    
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! LevelPostCell
        cell.post = posts[indexPath.item]

        cell.delegate = self
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        if (self.posts.count == 0) {
//            self.collectionView?.setEmptyMessage("There is no story writte for this level, yet. Start the quest and be the first one")
//            self.myActivityIndicator.stopAnimating()
//
//        } else {
//            self.collectionView?.restore()
//        }
        return posts.count

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    
        return CGSize(width: view.frame.width, height: 325)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("Aaa")
        let post = self.posts[indexPath.item]
        print(post)
        guard let postId = post.id else { return }
        print(postId)
        let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
        postController.userId = post.user.uid
        postController.postId = postId
        navigationController?.pushViewController(postController, animated: true)
    }

    fileprivate func fetchPosts() {
        
        let levelN = level ?? 0
        let levelString = String(levelN)
        let finalSting = "level"+levelString
        print("bhow bhow is", finalSting)
        let ref = Database.database().reference().child("levelPost").child(finalSting)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            value?.forEach({ (snapshot) in
                let value = snapshot.value as? NSDictionary
                    let uid = value?["uid"] as? String ?? ""
                    let pid = value?["pid"] as? String ?? ""
                Database.fetchUserWithUID(uid: uid, completion: { (user) in
                    self.fetchPostsWithUser(user: user, pid: pid)
                })
            })
        }
        
    }
    
    fileprivate func fetchPostsWithUser(user: UserModel, pid: String){
        let pid = pid
        let ref = Database.database().reference().child("posts").child(user.uid).child(pid)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            self.collectionView?.refreshControl?.endRefreshing()
            
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            
            var post = Post(user: user, dictionary: dictionary)
            post.id = snapshot.key
            print("post is", post.id)
            self.posts.append(post)
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
            self.myActivityIndicator.stopAnimating()
          
        }
    }


}


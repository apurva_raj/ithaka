//
//  QuestCards.swift
//  ithaka
//
//  Created by Apurva Raj on 10/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class QuestCards: UICollectionViewCell {

    var quest: QuestTitleModel? {
        didSet{
            let title = quest?.title
            
            titleLabel.text = title
            
            guard let profileurl = quest?.questImage else { return }
            
            
            emojiLabel.loadImage(urlString: profileurl)

        }
    }
    
    weak var delegate: QuestMainDelegate?
    
    let grayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        
        return view
    }()
    

    lazy var emojiLabel: CustomImageView = {
        let label = CustomImageView()
        label.isUserInteractionEnabled = true
        label.backgroundColor = UIColor.gray
        label.layer.cornerRadius = 3
        label.layer.borderColor  =  UIColor.gray.cgColor
        label.layer.borderWidth = 1
        label.layer.shadowOpacity = 0.5
        label.layer.shadowColor =  UIColor.black.cgColor
        label.layer.shadowRadius = 5.0
        label.layer.shadowOffset = CGSize(width:5, height: 5)
        label.layer.masksToBounds = true

        let tap = UITapGestureRecognizer(target: self, action: #selector(titleTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.textColor = .black
        
        return label
    }()
    
    @objc func titleTapped() {
        print("number 1")
        guard let questTitleID = quest?.id else { return }
        guard let questTitle = quest?.title else { return }
        delegate?.didTapQuestTitle(id: questTitleID, title: questTitle)
    
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        let profileImageUrl = "https://firebasestorage.googleapis.com/v0/b/ithaka-5ec1b.appspot.com/o/quest%2Fdariusz-sankowski-56725-unsplash.jpg?alt=media&token=b275ea30-8ca4-4ea5-8488-cbc084cb95a1"
        
        emojiLabel.loadImage(urlString: profileImageUrl)

        addSubview(emojiLabel)
        addSubview(titleLabel)
        //        view.contentInset = UIEdgeInsetsMake(-30, 0, -30, 0)
        emojiLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        titleLabel.anchor(top: emojiLabel.bottomAnchor, left: emojiLabel.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

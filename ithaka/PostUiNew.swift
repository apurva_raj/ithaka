//
//  PostUiNew.swift
//  ithaka
//
//  Created by Apurva Raj on 15/01/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import KILabel

class PostUiNew: UICollectionViewCell, UITextViewDelegate {
    
    
    weak var delegate: MyCustomDelegate?
    
    weak var textViewTap: textViewTap?


    var user: UserModelNew? {
        didSet{
            guard let profileImageUrl = user?.profileImageUrl else { return }
            
            profileImageView.loadImage(urlString: profileImageUrl)

            setupAttributedDate()

        }
    }

    var post: PostNew? {
        didSet {
            
            guard let post = post else { return }
            
            guard let story = post.story else { return }
            guard let tag = post.tag else { return }
            
            guard let commetsCount = post.commentsCount else { return }

            let commentsCountString = String(commetsCount)
            
            let trailText = "...Read More"
            
            let trailTextTextForQuest = NSMutableAttributedString(string: story)
            
            trailTextTextForQuest.append(NSAttributedString(string: trailText, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
            
            
            storyLabel.text = story
            
            let storyHeight = story.height(withConstrainedWidth: frame.width-40, font: UIFont.systemFont(ofSize: 16))
            print("Str", storyHeight)
            if storyHeight > 160 {
                readMoreLabel.text = "Read more"
            } else {
                readMoreLabel.text = ""
                
            }
            
            
            storyLabel.hashtagLinkTapHandler = { label, string, range in
                print(label)
                
                let tag = String(string.dropFirst()).lowercased()
                self.textViewTap?.hashTagTapped(hash: tag)
            }
            
            storyLabel.userHandleLinkTapHandler = { label, string, range in
                print(string)
                let mention = String(string.dropFirst()).lowercased()
                print(mention)
                self.textViewTap?.usernameTapped(username: mention)
            }
            
            
            
            let attributedTextForComment = NSMutableAttributedString(string: commentsCountString, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)])
            attributedTextForComment.append(NSAttributedString(string: " Comments", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
            
            
            guard let postliked = post.isLiked else { return }
            
            let imageName = !postliked ? "like_unselected" : "like_selected"
            likeImageView.setImage(UIImage(named: imageName), for: .normal)
            let count = post.rocketCount
            
            if post.rocketCount == 1 {
                likeCountLabel.text = "\(count ?? 0) like"
                
            } else {
                likeCountLabel.text = "\(count ?? 0) likes"
            }
            
            commentsCountLabel.attributedText = attributedTextForComment
            
            if tag.count == 0 {
                let attributedTextForQuest = NSMutableAttributedString(string: "Quest: ", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 11)])
                attributedTextForQuest.append(NSAttributedString(string: "None", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                
                questTagLabel.attributedText = attributedTextForQuest
                
                
            } else {
                let attributedTextForQuest = NSMutableAttributedString(string: "Quest: ", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 11)])
                attributedTextForQuest.append(NSAttributedString(string: tag, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                
                questTagLabel.attributedText = attributedTextForQuest
                
            }

            
        }
    }
    
    fileprivate func setupAttributedDate() {
        guard let post = self.post else { return }
        
        guard let username = user?.u_username else { return }
        let attributedText = NSMutableAttributedString(string: username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16)])
        
        let timeAgoDisplay = post.creationDate?.timeAgoDisplay()
        
        timeLabel.text = timeAgoDisplay
        
        nameLabel.attributedText = attributedText
    }

    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 30
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = UIColor.clear
        
        return imageView
    }()
    
    
    let profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "item_6")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 20
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        return imageView
    }()

    lazy var optionButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_expand_more"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOptionButton))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        button.isEnabled = true
        button.isHidden = false
        
        return button
    }()

    
    @objc func handleOptionButton() {
//        self.delegate?.onOptionButtonTapped(for: self)
    }

    lazy var storyLabel: KILabel = {
        let textview = KILabel()
        textview.textColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.85)
        textview.isUserInteractionEnabled = true
        textview.numberOfLines = 0
        textview.font = UIFont(name: "HelveticaNeue", size: CGFloat(16))
        
        return textview
    }()
    
    lazy var readMoreLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .right
        return label
    }()
    
    
    
    @objc func storyViewTapped(_ recognizer: UITapGestureRecognizer) {
        print("Story tv tapped")
        guard let post = post else { return }
        guard let user = user else { return }
        self.delegate?.onStoryViewTapped(user: user, post: post)
        
    }


    lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 11)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .gray
        label.isUserInteractionEnabled = true
        label.text = "1 hour ago"
        return label
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(20)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .black
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        
        return label
    }()
    
    @objc func nameLabelTapped(_ recognizer: UITapGestureRecognizer) {
        guard let uid = user?.uid else { return }
        self.delegate?.onNameIdLabelTapped(userId: uid)
        
    }

    lazy var questTagLabel: UILabel = {
        let textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 11)
        textView.textAlignment = .left
        textView.textColor = UIColor.gray
        textView.isUserInteractionEnabled = true
        return textView
    }()

    
    lazy var likeImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
//        let tap = UITapGestureRecognizer(target: self, action: #selector(rocketButtonTapped))
//        tap.numberOfTapsRequired = 1
//        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    lazy var commentImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "comment"), for: .normal)
        
        return button
    }()
    
    lazy var shareImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "share"), for: .normal)
        
        return button
    }()

    
    lazy var likeCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .left
        label.isUserInteractionEnabled = true
        label.layoutIfNeeded()
        label.layoutSubviews()
        
        return label
    }()
    lazy var commentsCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        
        return label
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 15.0
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 12.0
        self.layer.shadowOpacity = 0.6

        
        addSubview(profileImageView)
        addSubview(profileItemView)
        addSubview(nameLabel)
        addSubview(optionButton)
        
        addSubview(timeLabel)
        addSubview(questTagLabel)
        addSubview(storyLabel)
        addSubview(readMoreLabel)
        
        
        addSubview(likeImageView)
        addSubview(commentImageView)
        addSubview(shareImageView)

        addSubview(likeCountLabel)
        addSubview(commentsCountLabel)

        profileImageView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        
        profileItemView.anchor(top: nil, left: profileImageView.rightAnchor, bottom: profileImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: -10, bottomConstant: 3, rightConstant: 0, widthConstant: 40, heightConstant: 40)

        
        storyLabel.anchor(top: profileImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant:160)
        
        
        nameLabel.anchor(top: self.topAnchor, left: profileImageView.rightAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 35, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        timeLabel.anchor(top: nameLabel.bottomAnchor, left: nameLabel.leftAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 10)

        optionButton.anchor(top: nameLabel.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 30)
        
        questTagLabel.anchor(top: timeLabel.bottomAnchor, left: timeLabel.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant:0, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 20)

    
        
        likeImageView.anchor(top: storyLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        commentImageView.anchor(top: storyLabel.bottomAnchor, left: likeImageView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        shareImageView.anchor(top: storyLabel.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        
        likeCountLabel.anchor(top: likeImageView.bottomAnchor, left: likeImageView.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: -3, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)
        commentsCountLabel.anchor(top: likeCountLabel.bottomAnchor, left: likeImageView.leftAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)
        
        readMoreLabel.anchor(top: nil, left: nil, bottom: storyLabel.bottomAnchor, right: storyLabel.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 85, heightConstant: 25)




    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



//
//  MiniQuestIntroController.swift
//  ithaka
//
//  Created by Apurva Raj on 13/03/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import GhostTypewriter


class MiniQuestIntroController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        return button
    }()

    
    lazy var questLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.tag = 0
        label.layer.cornerRadius = 15
        label.clipsToBounds = true
        
        return label
    }()
    
    
    lazy var storyTimeText: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(17)
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        label.tag = 0
        label.numberOfLines = 0
        label.text = "“Hope the voyage is a long one. May there be many a summer morning when, with what pleasure, what joy, you come into harbors seen for the first time” \n\nIthakians, welcome to your first ever Mini Quest. Choose a character and set sail through waves. There are three levels. Completing all three level  will have you discovered a new place there you shall find a rare and magical item."
        label.textColor = .gray
        
        
        return label
    }()
    
    lazy var chooseLabel: UILabel = {
        let label = UILabel()
        label.font =  UIFont.boldSystemFont(ofSize: 28)
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.text = "Choose A Character"
        return label
    }()
    

    lazy var oneHero: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "boatGirl"), for: .normal)
        button.tag = 1
        button.addTarget(self, action: #selector(heroSelected), for: UIControlEvents.touchUpInside)

        return button
    }()
    
    lazy var twoHero: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "boatMan").withHorizontallyFlippedOrientation(), for: .normal)
        button.tag = 2
        button.addTarget(self, action: #selector(heroSelected), for: UIControlEvents.touchUpInside)

        return button
    }()
    

    
    @objc func heroSelected(sender: UIButton){
        let tag = sender.tag
        
        print("Tag is", tag)
        if tag == 1 {
            print("one selected")
            
            let dailyQuestController = MiniQuestController(collectionViewLayout: UICollectionViewFlowLayout())
            
            dailyQuestController.hidesBottomBarWhenPushed = true
            UserDefaults.standard.set(1, forKey: "HeroSelectionThing")  //Integer

            self.navigationController?.pushViewController(dailyQuestController, animated: true)

            
        }
        
        if tag == 2 {
            print("two selected")
            
            let dailyQuestController = MiniQuestController(collectionViewLayout: UICollectionViewFlowLayout())
            
            UserDefaults.standard.set(2, forKey: "HeroSelectionThing")  //Integer

            
            dailyQuestController.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(dailyQuestController, animated: true)

        }

    }
    
    lazy var lineView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = .white
        
        setupTopBar()
        setupBody()
        setupSelection()
        setGradientBackground()
    }
    
    func setGradientBackground() {
        let colorTop = UIColor.rgb(red: 255, green: 255, blue: 255, alpha: 1).cgColor

        let colorBottom = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1).cgColor
        
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.5, 0.9]
        gradientLayer.frame = self.collectionView?.bounds ?? CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)

        self.collectionView?.layer.insertSublayer(gradientLayer, at: 0)
    }
    

    func setupTopBar() {
        let newview = UIView()
        newview.backgroundColor = .clear
        self.view.addSubview(newview)
        self.view.addSubview(questLabel)
        
        
        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        
        
        newview.addSubview(backButton)
        
        
        backButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        

        self.questLabel.anchor(top: backButton.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 100)
        
    }
    
    @objc func handleBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setupBody() {
        
        self.view.addSubview(storyTimeText)
        
        self.view.addSubview(chooseLabel)
        self.view.addSubview(lineView)
        self.view.addSubview(oneHero)
        self.view.addSubview(twoHero)

        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.15
            
            storyTimeText.font = UIFont.systemFont(ofSize: 20)
            storyTimeText.anchor(top: questLabel.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: ph, bottomConstant: 0, rightConstant: ph, widthConstant: 0, heightConstant: 300)
          
            chooseLabel.anchor(top: nil, left: self.view.leftAnchor, bottom: oneHero.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: ph, bottomConstant: 50, rightConstant: ph, widthConstant: 0, heightConstant: 0)
            
            lineView.anchor(top: chooseLabel.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 10, leftConstant: 25, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 2.5)
            
            
            oneHero.anchor(top: nil, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 0, leftConstant: ph, bottomConstant: 50, rightConstant: 0, widthConstant: 120, heightConstant: 120)
            twoHero.anchor(top: nil, left: nil, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 50, rightConstant: ph, widthConstant: 120, heightConstant: 120)

            let attachment:NSTextAttachment = NSTextAttachment()
            attachment.image = #imageLiteral(resourceName: "decor_4")
            
            attachment.setImageHeight(height: 40)
            
            let attachmentString:NSAttributedString = NSAttributedString(string:" Mini Quest")
            let attributedString:NSMutableAttributedString = NSMutableAttributedString(attachment: attachment)
            
            
            attributedString.append(attachmentString)
            
            self.questLabel.attributedText = attributedString
            self.questLabel.font = UIFont.systemFont(ofSize: 35)

            
        default:
            print("i am Everyone except ipad")
            let mh = UIDevice().type
            let device: String = mh.rawValue
            
            
            switch device {
            case "iPhone 5S":
                storyTimeText.font = UIFont.systemFont(ofSize: 14)
                storyTimeText.anchor(top: questLabel.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: -20, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 250)
                
            default:
                print("as it is")
                storyTimeText.anchor(top: questLabel.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 250)
                
            }
            
            chooseLabel.anchor(top: nil, left: self.view.leftAnchor, bottom: oneHero.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 35, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            
            lineView.anchor(top: chooseLabel.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 10, leftConstant: 25, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 2.5)
            
            
            oneHero.anchor(top: nil, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: nil, topConstant: 0, leftConstant: 55, bottomConstant: 35, rightConstant: 0, widthConstant: 80, heightConstant: 80)
            twoHero.anchor(top: nil, left: nil, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 35, rightConstant: 55, widthConstant: 80, heightConstant: 80)

            let attachment:NSTextAttachment = NSTextAttachment()
            attachment.image = #imageLiteral(resourceName: "decor_4")
            
            attachment.setImageHeight(height: 40)
            
            let attachmentString:NSAttributedString = NSAttributedString(string:" Mini Quest")
            let attributedString:NSMutableAttributedString = NSMutableAttributedString(attachment: attachment)
            
            
            attributedString.append(attachmentString)
            
            self.questLabel.attributedText = attributedString

            self.questLabel.font = UIFont.systemFont(ofSize: 30)


            
        }
        
        
        
        
//        storyTimeText.startTypewritingAnimation(completion: nil)
        
        
    }
    
    func setupSelection() {
        
        
        

        
        
        


    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
        
    }
}

//
//  Welcome1Controller.swift
//  ithaka
//
//  Created by Apurva Raj on 17/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth
import GhostTypewriter
import GoogleSignIn
import FirebaseDatabase


class Welcome1Controller: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate {
   
    var shapeLayer: CAShapeLayer!
    var pulsatingLayer: CAShapeLayer!

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        
        if let error = error {
            print("failed to login", error)
            return

        }
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        Auth.auth().signIn(with: credential) { (user, err) in
            if let error = error {
                print("failed !", error)
                return
            }
            
            guard let useremail = user?.email else { return }
            guard let uid = user?.uid else { return }
            let adminid = "b5MKdFi5mkcJgQC12hEOjJOT3MB3"
            
            Api.MyPosts.REF_MYPOSTS.child(adminid).observeSingleEvent(of: .value, with: { (snapshot) in
                if let dict = snapshot.value as? [String: Any] {
                    
                    dict.forEach({ (snapshot) in
                        if let value = snapshot.value as? NSDictionary {
                            let creationDate = value["creationDate"] as? TimeInterval ?? 0
                            Database.database().reference().child("feed").child(uid).child(snapshot.key).setValue(["uid": adminid, "creationDate":creationDate])
                            
                        }
                    })
                    
                }
            })

            let ref = Database.database().reference().child("users")
            let query =  ref.queryOrdered(byChild: "email").queryEqual(toValue: useremail)
            
            query.observeSingleEvent(of: .value, with: { (snapshot) in
                if snapshot.exists() {
                    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    
                    appDelegate.window?.rootViewController = MainTabBarController()
                    if let tabBarController = appDelegate.window?.rootViewController as? UITabBarController {
                        tabBarController.selectedIndex = 0
                    }

                    self.myActivityIndicator.stopAnimating()

                } else {
                    do
                        {
                            print("bc")
                            try Auth.auth().signOut()
                            self.myActivityIndicator.stopAnimating()
                            self.googleLabel.isEnabled = true

                            
                            SharedClass.sharedInstance.alert(view: self, title: "Tap on - start my journey", message: "No account found with " + useremail)
                            
                    }  catch let signOutErr {
                        print("Failed to sign out:", signOutErr)
                    }
                }
            })
            
            self.googleLabel.isEnabled = true

        }
    }
    

    lazy var storyLabel: UILabel = {
        let label = UILabel()
        label.padding = UIEdgeInsetsMake(10, 15, 10, 15)
        label.text = "Writers. Storytellers. Poets. \nThey all have always played major part in history. Because Words hold the power comparable to sword as well as feather. Words can change and influence people for good. \n\nHere on IthakaApp, your goal is to become a part of future History. Write your heart out. Tell stories. Create a poem. And hope that your journey is long one - full of adventure and surprises."
        label.font = UIFont(name: "HelveticaNeue", size: CGFloat(16))
        label.isUserInteractionEnabled = true
        label.backgroundColor = .white
        label.layer.cornerRadius = 7
        label.numberOfLines = 0
        label.clipsToBounds = true
        return label
    }()
    
    lazy var readLabel: UILabel = {
        let label = UILabel()
        label.text = "Read Ithaka Poem"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.isUserInteractionEnabled = true
        label.padding = UIEdgeInsetsMake(5, 10, 5, 10)
        label.backgroundColor = .white
        label.layer.cornerRadius = 7
        label.numberOfLines = 0
        label.clipsToBounds = true
        label.textAlignment = .left
        label.sizeToFit()
        let tap = UITapGestureRecognizer(target: self, action: #selector(showIthaka))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        return label
    }()
    
    
    
    lazy var BoardImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "bb3")
        iv.tag = 1
        iv.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nextIthaka))
        tap.numberOfTapsRequired = 1
        iv.addGestureRecognizer(tap)
        return iv
    }()


    @objc func nextIthaka() {
        


        let welcome2Controller = WelcomeFeelingController(collectionViewLayout: UICollectionViewFlowLayout())
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        self.navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(welcome2Controller, animated: true)
        
    }

    
    @objc func showIthaka(){
        
        print("Truer")

        let ithakaPoemController = IthakaPoemController()
        let navController = UINavigationController(rootViewController: ithakaPoemController)

        self.present(navController, animated: true, completion: nil)
    }
    
    lazy var googleLabel: UILabel = {
        let label = UILabel()
        label.text = "Tap here to login if you already have an account."
        label.font = UIFont.boldSystemFont(ofSize: 11)
        label.isUserInteractionEnabled = true
        label.textColor = .white
        label.padding = UIEdgeInsetsMake(5, 10, 5, 10)
        label.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.6)
        label.layer.cornerRadius = 7
        label.numberOfLines = 0
        label.textAlignment = .center
        label.sizeToFit()
        let tap = UITapGestureRecognizer(target: self, action: #selector(loginC))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    @objc func loginC() {
        print("Tapped")
        googleLabel.isEnabled = false
        myActivityIndicator.startAnimating()

        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().signIn()

    }
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    
    
    private func createCircleShapeLayer(strokeColor: UIColor, fillColor: UIColor) -> CAShapeLayer {
        let layer = CAShapeLayer()
        let circularPath = UIBezierPath(arcCenter: .zero, radius: 100, startAngle: 0, endAngle: 2 * CGFloat.pi, clockwise: true)
        layer.path = circularPath.cgPath
        layer.strokeColor = strokeColor.cgColor
        layer.lineWidth = 20
        layer.fillColor = fillColor.cgColor
        layer.lineCap = kCALineCapRound
        layer.position = view.center
        return layer
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
       super.viewWillAppear(animated)
    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "DalekPinpointBold", size: 21)!]
        
            navigationItem.title = "Ithaka"

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "DalekPinpointBold", size: 21)!]
        
        navigationItem.title = "Ithaka"

        view.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)
        let mh = UIDevice().type
        let device: String = mh.rawValue
        
        view.addSubview(storyLabel)
        view.addSubview(readLabel)
        view.addSubview(googleLabel)
        view.addSubview(BoardImageView)
        
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            print("I am Iphone")
            storyLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 30, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: (view.frame.height*0.33))
            
            readLabel.anchor(top: storyLabel.bottomAnchor, left: view.leftAnchor , bottom: nil, right: nil, topConstant: 5, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 205, heightConstant: 0)


        case .pad:
            print("I am Ipad")
            let ph = view.frame.width*0.10
            storyLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 30, leftConstant: ph, bottomConstant: 0, rightConstant: ph, widthConstant: 0, heightConstant: view.frame.height*0.40)
            storyLabel.font = UIFont.systemFont(ofSize: 23)
            
            readLabel.anchor(top: storyLabel.bottomAnchor, left: view.leftAnchor , bottom: nil, right: nil, topConstant: 10, leftConstant: ph, bottomConstant: 0, rightConstant: 0, widthConstant: 220, heightConstant: 0)
            readLabel.font = UIFont.boldSystemFont(ofSize: 20)

            googleLabel.font = UIFont.boldSystemFont(ofSize: 14)


        default:
            print("who are you")
            
            storyLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 30, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: (view.frame.height*0.33))
            
            readLabel.anchor(top: storyLabel.bottomAnchor, left: view.leftAnchor , bottom: nil, right: nil, topConstant: 5, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 205, heightConstant: 0)

        }
        

    

        googleLabel.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 150, heightConstant: 0)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: googleLabel.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        
        
        BoardImageView.anchor(top: nil, left: nil, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 150, heightConstant: 250)
        
        view.bringSubview(toFront: googleLabel)
//        storyLabel.startTypewritingAnimation(completion: nil)
        let image = #imageLiteral(resourceName: "book").withRenderingMode(.alwaysOriginal)

        let imageAttachment =  NSTextAttachment()
        imageAttachment.image = image
        
        
        imageAttachment.bounds = CGRect(x: 0, y: -5, width: 20, height: 20)
        let attachmentString = NSAttributedString(attachment: imageAttachment)
        let completeText = NSMutableAttributedString(string: "")
        completeText.append(attachmentString)
        let  textAfterIcon = NSMutableAttributedString(string: " Read Ithaka Poem")
        
        completeText.append(textAfterIcon)
        self.readLabel.attributedText = completeText;
        
        self.readLabel.font = UIFont(name: "HelveticaNeue", size: CGFloat(16))

        

    }
    
}

//
//  ProfileLevelController.swift
//  ithaka
//
//  Created by Apurva Raj on 26/04/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import GTProgressBar

class ProfileLevelController: UIViewController {
    
    
    var user: UserModel? {
        didSet {
            
            guard let username = user?.username else { return }
            
            guard let lcount = user?.profileLevel else { return }
            guard let lprogress = user?.profileProgress else { return }
            guard let postCount = user?.postCount else { return }

            let lcountString = String(lcount)
            let postCountString = String(postCount)

            let pp = CGFloat(lprogress)
            
            let modual = pp.truncatingRemainder(dividingBy: 100)
            let profileLevelCount = pp - modual
            let prfileLeveleCountInt = Int(profileLevelCount)/100
            
            let profilePorgressCount = modual/100
            
            levelProgressBar.progress = profilePorgressCount
            let profileLevelString = String(describing: prfileLeveleCountInt)
            

            
            let levelCountText = NSMutableAttributedString(string: "Level ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: UIColor.white])
            
            
            
            levelCountText.append(NSAttributedString(string: profileLevelString, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)]))
            

            nameLabel.text = username
            levelCount.attributedText = levelCountText
            
            let postCountText = NSMutableAttributedString(string: postCountString, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)])
            postCountText.append(NSAttributedString(string: " stories written", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)]))
            
            totalStoriesCountLabel.attributedText = postCountText
            


        }
    }
    
    lazy var levelCount: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(10)
        label.numberOfLines = 0
        label.textColor = .white
        label.layer.borderWidth = 2
        label.layer.cornerRadius = 15
        label.layer.borderColor = UIColor.gray.cgColor
        
        label.backgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.isUserInteractionEnabled = false
        
        return label
        
    }()

    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(18)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .gray
        label.isUserInteractionEnabled = false
        
        return label
    }()
    
    let waysTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.numberOfLines = 0
        label.sizeToFit()
        label.textAlignment = .center
        label.textColor = .black
        label.isUserInteractionEnabled = false
        label.text = "LEVEL UP BY..."
        
        return label
    }()
    let waysLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .gray
        label.isUserInteractionEnabled = false
        label.text = "-Publishing new stories here. \n-Commenting and giving out rockets. \n-Getting more followers. \n-Taking parts in writing contests."
        
        return label
    }()
    
    let levelProgressBar: GTProgressBar = {
        let bar = GTProgressBar()
        //        bar.progress = 0.3
        bar.barBorderColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        bar.barFillColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        bar.barBackgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 0.2)
        bar.barBorderWidth = 1
        bar.barFillInset = 2
        bar.labelTextColor = UIColor.darkGray
        bar.progressLabelInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        bar.font = UIFont.boldSystemFont(ofSize: 16)
        bar.labelPosition = GTProgressBarLabelPosition.right
        bar.barMaxHeight = 12
        //        bar.direction = GTProgressBarDirection.anticlockwise
        
        return bar
    }()
    
    let boatImageView: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "boatGirl")
        
        return imageview
    }()
    
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    let totalStoriesCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(15)
        label.textAlignment = .center
        label.textColor = UIColor.black
        
        return label
    }()
  


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        myActivityIndicator.center = view.center
        
        view.addSubview(myActivityIndicator)
        
        myActivityIndicator.startAnimating()

        self.view.backgroundColor = UIColor.rgb(red: 240, green: 245, blue: 251, alpha: 1)

        view.addSubview(levelCount)
        view.addSubview(levelProgressBar)
        view.addSubview(boatImageView)
        view.addSubview(totalStoriesCountLabel)

        view.addSubview(waysTitleLabel)
        view.addSubview(waysLabel)
    
        levelCount.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: nil, right: nil, topConstant: 100, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 30)
        
        levelProgressBar.anchor(top: levelCount.bottomAnchor, left: view.self.leftAnchor, bottom: nil, right: view.self.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: view.frame.width, heightConstant: 20)
        
        boatImageView.anchor(top: levelProgressBar.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 50)
        
        totalStoriesCountLabel.anchor(top: boatImageView.topAnchor, left: boatImageView.rightAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        waysTitleLabel.anchor(top: boatImageView.bottomAnchor, left: view.self.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
        waysLabel.anchor(top: waysTitleLabel.bottomAnchor, left: view.self.leftAnchor, bottom: nil, right: view.self.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        myActivityIndicator.stopAnimating()

    }
    
}

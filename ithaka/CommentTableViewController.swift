//
//  CommentTableViewController.swift
//  ithaka
//
//  Created by Apurva Raj on 25/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import SwipeCellKit
import Firebase

class CommentTableViewController: UITableViewController {
   
    
    func collectionView(_ collectionView: UICollectionView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }
        
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            // handle action by updating model with deletion
        }
        
        // customize the action appearance
        deleteAction.image = UIImage(named: "delete")
        
        return [deleteAction]
    }
    
    
    
    let cellId = "cellId"

    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsSelection = true
        tableView.allowsMultipleSelectionDuringEditing = true
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        navigationItem.rightBarButtonItem = editButtonItem
        navigationItem.title = "Comments"
        view.layoutMargins.left = 32
        tableView.register(CommentCell.self, forCellReuseIdentifier: cellId)

    }
       
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//    
////        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CommentCell
////        cell.delegate = self
////        cell.comment = self.comments[indexPath.item]
////        print(cell)
////        return cell
//
//    }
    
    var comments = [Comment]()
    fileprivate func fetchComments() {
         let postId = "-KxHP-aJF7QRqcyjwfvU"
        let ref = Database.database().reference().child("comments").child(postId)
        ref.observe(.childAdded, with: { (snapshot) in
            
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            
            guard let uid = dictionary["uid"] as? String else { return }
            
            Database.fetchUserWithUID(uid: uid, completion: { (user) in
                
                var comment = Comment(user: user, dictionary: dictionary)
                comment.id = snapshot.key
                self.comments.append(comment)
                self.tableView.reloadData()
            })
            
        }) { (err) in
            print("Failed to observe comments")
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        var post: Post?

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
}

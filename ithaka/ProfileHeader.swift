//
//  ProfileHeader.swift
//  ithaka
//
//  Created by Apurva Raj on 17/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import ISEmojiView
import GTProgressBar
import Pulsator

class ProfileHeader: UICollectionViewCell, ISEmojiViewDelegate {
    
    var userNew: UserModelNew? {
        didSet {
            
        }
    }
    
    var user: UserModelNew? {
        didSet {

            setupEditFollowButton()
            blockedCheck()
            
            guard let profileImageUrl = user?.profileImageUrl else { return }
            guard let shortBio = user?.shortBio else { return }
            guard let followers = user?.followers else { return }
            guard let following = user?.following else { return }
            guard let postCount = user?.postCount else { return }
            guard let profileItem = user?.profileSetItem else { return }

            guard let profileProgress = user?.profileProgress else { return }
            
            
            guard let username = user?.username else { return }
            let u_username = user?.u_username ?? username
            
            guard let questCompletedCount = user?.questCompleted else { return }
           
            self.profileItemName = profileItem
            let im = UIImage(named: profileItem + ".png")
            
            profileItemView.image = im
            
            let followersCount = String(followers)
            let followingCount = String(following)
            let postCountString = String(postCount)
            let questComCountString = String(questCompletedCount)
            guard let uid = user?.uid else { return }
            let ref = Database.database().reference().child("users").child(uid)
            
            ref.observeSingleEvent(of: .value) { (snapshot) in
                let value = snapshot.value as? NSDictionary
                
                let avatar = value?["Avatar"] as? Int ?? 0
                
                if avatar == 11 {
                    self.boatImageView.image = #imageLiteral(resourceName: "boatMan")
                } else if avatar == 23 {
                    self.boatImageView.image = #imageLiteral(resourceName: "boatGirl")
                    
                } else {
                    return
                }
                
            }
            
            
            profileImageView.loadImage(urlString: profileImageUrl)
            nameLabel.text = u_username
            shortBioTextView.text =  shortBio
            followersCountLabel.text = followersCount
            followingCountLabel.text = followingCount
            rocketsCountLabel.text = postCountString
            
            let postCountText = NSMutableAttributedString(string: questComCountString, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)])
            postCountText.append(NSAttributedString(string: " Quest completed", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)]))

            totalStoriesCountLabel.attributedText = postCountText
            
            guard let currentLoggedInUserId = Auth.auth().currentUser?.uid else { return }
            
            guard let userId = user?.uid else { return }

            if currentLoggedInUserId == userId {
                editProfileFollowButton.isHidden = true

            }
            

        }
        

    }
    
    
    var collectedPoints: Int? {
        didSet{
            
            guard let profileProgress = collectedPoints else  { return}
            
            let pp = CGFloat(profileProgress)
            
            let approximateCount = pp
            let naturalCount: Int
            let pProgress: CGFloat
            let number = ((4*approximateCount-95).squareRoot() + 15)/10
            print("number is", number)
            
            switch approximateCount {
            case 0...29:
                naturalCount = 1
                pProgress = (approximateCount)/29
                levelProgressBar.progress = pProgress
                
            case 30...105:
                naturalCount = 2
                pProgress = (approximateCount-30)/75
                levelProgressBar.progress = pProgress
                
            case 106...995030:
                
                naturalCount = Int(((4*approximateCount-95).squareRoot() + 15)/10)
                print("numberInt is", naturalCount)
                let newm = CGFloat(naturalCount)
                pProgress = number - newm
                levelProgressBar.progress = pProgress
                
                
            default:
                naturalCount = 1000
            }
            guard let authuid = Auth.auth().currentUser?.uid else { return }
            guard let currentid = user?.uid else { return }
            
            if authuid == currentid {
                
                print("yes its my profile", approximateCount)
                switch approximateCount {
                case 120...2200:
                    print("n my count is", approximateCount)
                    
                    //check from user default
                    
                    if  UserDefaults.standard.object(forKey: "KeyClaimed") == nil {
                        
                        
                        print("yes, it's not stored")
                        
                        let value = ["unlockKey": 1]
                        
                        
                        let ref = Database.database().reference().child("users").child(authuid)
                        
                        ref.updateChildValues(value)
                        
                        UserDefaults.standard.set("stored", forKey: "KeyClaimed")
                        self.profileDelegate?.shwothedamnkeypage()

                    }
                        
                    
                    
                default:
                    print("nothjing")
                }
                
                
            }
            
            // store key value to database user
            //store key value to userdefault
            
            // that's it. next time user opens, she sees it on profile page. maybe another check from same user default. yey? yey.
            //
            //
            
            
            
            print("naturalcount ", naturalCount)
            
            let profileLevelString = String(describing: naturalCount)
            
            
            
            let levelCountText = NSMutableAttributedString(string: "Level ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17), NSAttributedStringKey.foregroundColor: UIColor.white])
            
            
            
            levelCountText.append(NSAttributedString(string: profileLevelString, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)]))
            //            levelCountText.append(attachmentString)
            
            
            
            levelCount.attributedText = levelCountText
            
            
        }
    }

    var profileItemName: String?
    
    var forScroll = SLPagingViewSwift2()

    weak var delegate: MyCustomDelegate?
    weak var profileDelegate: ProfileDelegate?
    
    fileprivate func setupEditFollowButton() {
        guard let currentLoggedInUserId = Auth.auth().currentUser?.uid else { return }
        
        guard let userId = user?.uid else { return }
        

        
        if currentLoggedInUserId == userId {
            //edit profile
        } else {
            
            // check if following
            Database.database().reference().child("following").child(userId).child(currentLoggedInUserId).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.exists() {
                    self.editProfileFollowButton.setTitle("Unfollow", for: .normal)


                } else {
                    self.setupFollowStyle()

                }
                
                print("snapshot value", snapshot.exists())
//                if let isFollowing = snapshot.value as? Int, isFollowing == 1 {
//
//                } else {
//                    self.setupFollowStyle()
//                }
                
            }, withCancel: { (err) in
                print("Failed to check if following:", err)
            })
        }
    }

    // callback when tap a emoji on keyboard
    func emojiViewDidSelectEmoji(emojiView: ISEmojiView, emoji: String) {
        emojitext.insertText(emoji)
        print("hello", emoji)
    }
    
    // callback when tap delete button on keyboard
    func emojiViewDidPressDeleteButton(emojiView: ISEmojiView) {
        emojitext.deleteBackward()
    }
    fileprivate func setupFollowStyle() {
        self.editProfileFollowButton.setTitle("Follow", for: .normal)
        self.editProfileFollowButton.backgroundColor = UIColor.rgb(red: 17, green: 154, blue: 237, alpha: 1)
        self.editProfileFollowButton.setTitleColor(.white, for: .normal)
        self.editProfileFollowButton.layer.borderColor = UIColor(white: 0, alpha: 0.2).cgColor
    }
    
    var shortBioTextView: UILabel = {
        let tv = UILabel()
        tv.font = UIFont.boldSystemFont(ofSize: 16)
        tv.backgroundColor = UIColor.rgb(red: 34, green: 167, blue: 240, alpha: 0)
        tv.numberOfLines = 0
//        tv.layer.cornerRadius = 5
//        tv.layer.borderWidth = 1
//        tv.layer.borderColor = UIColor.rgb(red: 34, green: 167, blue: 240, alpha: 1).cgColor
//        tv.textColor = .white
        tv.textAlignment = .center
        tv.sizeToFit()
        return tv
    }()
    
    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = UIColor.gray.cgColor
        imageView.layer.cornerRadius = 60
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    lazy var levelCount: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(10)
        label.numberOfLines = 0
        label.textColor = .white
        label.layer.borderWidth = 2
        label.layer.cornerRadius = 15
        label.layer.borderColor = UIColor.gray.cgColor
        
        label.backgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(levelCountTapped))
                tap.numberOfTapsRequired = 1
                label.addGestureRecognizer(tap)
        
        return label
        
    }()
    
    @objc func levelCountTapped(sender: UITapGestureRecognizer) {
        guard let user = user else { return }

//        self.delegate?.onLevelCountTapped(user: user)
        print("bhow bhow")
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = UIColor.darkGray
        label.isUserInteractionEnabled = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
//        tap.numberOfTapsRequired = 1
//        label.addGestureRecognizer(tap)
//
        return label
    }()
    
    lazy var firstBackground: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        view.clipsToBounds = true
        view.layer.cornerRadius = 15
        return view
    }()
    

    lazy var progressBackground: UIView = {
        let view = UIView()
//        view.backgroundColor = UIColor.rgb(red: 240, green: 245, blue: 251, alpha: 1)
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 12.0
        view.layer.shadowOpacity = 0.5
        return view
    }()

    lazy var itemsBackground: UIView = {
        let view = UIView()
        //        view.backgroundColor = UIColor.rgb(red: 240, green: 245, blue: 251, alpha: 1)
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 12.0
        view.layer.shadowOpacity = 0.5
        return view
    }()
    

    
    lazy var followersCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(30)
//        label.textColor = .white
        label.textAlignment = .center
        label.isUserInteractionEnabled = true

        let tap = UITapGestureRecognizer(target: self, action: #selector(followersLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
    }()
    
    lazy var followersLabel: UILabel = {
        let label = UILabel()
        label.text = "Followers"
        label.font = label.font.withSize(14)
//        label.textColor = .white
        label.textAlignment = .center
    

        return label
    }()
    
    @objc func followersLabelTapped(sender: UITapGestureRecognizer) {
        guard let user = user else { return }

        print("tapped followers label")
        self.delegate?.onFollowersLabelTapped(user: user)

    }
    
    
    
    lazy var followingCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(30)
//        label.textColor = .white
        label.textAlignment = .center
        label.isUserInteractionEnabled = true

        let tap = UITapGestureRecognizer(target: self, action: #selector(followingLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
    }()
    
    @objc func followingLabelTapped(sender: UITapGestureRecognizer) {
        guard let user = user else { return }
        
        self.delegate?.onFollowingLabelTapped(user: user)
        
    }
    
    let postCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(18)

        return label
    }()
    
    let heartsLabel: UILabel = {
        let label = UILabel()
        label.text = "Stories"
        label.font = label.font.withSize(30)
//        label.textColor = .gray

        return label
    }()
    
    
    let rocketsCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(30)
//        label.textColor = .white
        label.textAlignment = .center

        return label
    }()
    
    
    
    lazy var followingLabel: UILabel = {
        let label = UILabel()
        label.text = "Following"
        label.font = UIFont.systemFont(ofSize: 14)
//        label.textColor = .white
        label.textAlignment = .center

        return label
    }()
    
    lazy var rocketsLabel: UILabel = {
        let label = UILabel()
        label.text = "Stories"
        label.font = UIFont.systemFont(ofSize: 14)
//        label.textColor = .white
        label.textAlignment = .center

        return label
    }()

    
    lazy var editProfileFollowButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Edit Profile", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        button.layer.borderColor = UIColor.gray.cgColor
        button.layer.borderWidth = 2
        button.layer.cornerRadius = 3
        button.titleEdgeInsets = UIEdgeInsetsMake(3, 5, 3, 5)
        button.backgroundColor = UIColor.lightGray
        button.addTarget(self, action: #selector(handleEditProfileOrFollow), for: .touchUpInside)
        return button
    }()
    
    lazy var settingsButton: UIButton = {
        let button = UIButton(type:.system)
        button.setImage(#imageLiteral(resourceName: "ic_settings"), for: .normal)
        button.tintColor = .gray
//        button.backgroundColor = .white
        button.addTarget(self, action: #selector(handleLogOut), for: .touchUpInside)

        return button
        
    }()
    
    @objc func handleLogOut() {
        print("tapped settings")
        self.delegate?.onLogoutTapped(for: self)

    }
    
    let bottomDividerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        return view
    }()
    
    
//
//    guard let uid = Auth.auth().currentUser?.uid else { return }
//    guard let currentUid = user?.uid else { return }
//


    @objc func handleEditProfileOrFollow() {
        print("Execute edit profile / follow / unfollow logic...")
        
        guard let currentLoggedInUserId = Auth.auth().currentUser?.uid else { return }
        
        guard let userId = user?.uid else { return }
        let ref3 = Database.database().reference().child("users").child(userId)
        let ref33 = Database.database().reference().child("users").child(currentLoggedInUserId)


        if editProfileFollowButton.titleLabel?.text == "Unfollow" {
            
            //unfollow
            Api.User.observeCurrentUser(completion: { (currentuser) in
                var fcount = currentuser.following ?? 0
                fcount = fcount - 1
                let addedToUser2 = ["following": fcount]
                ref33.updateChildValues(addedToUser2)
                
                
            })
            
            Api.User.observeUser(withId: userId, completion: { (userMe) in
                var fcount = userMe?.followers ?? 0
                fcount = fcount - 1
                
                var points = userMe?.profileProgress ?? 0
                points = points - 7

                let addedToUser2 = ["followers": fcount, "profileProgress": points]
                

                ref3.updateChildValues(addedToUser2)
                
                let fcountstring = String(fcount)
                self.followersCountLabel.text = fcountstring
                
            })
            
            
            Database.database().reference().child("following").child(userId).child(currentLoggedInUserId).removeValue(completionBlock: { (err, ref) in
                if let err = err {
                    print("Failed to unfollow user:", err)
                    return
                }
                Database.database().reference().child("followers").child(currentLoggedInUserId).child(userId).removeValue()
                
                print("Successfully unfollowed user:", self.user?.username ?? "")
                
                self.setupFollowStyle()
            })
            
            Api.MyPosts.REF_MYPOSTS.child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
                if let dict = snapshot.value as? [String: Any] {
                    for key in dict.keys {
                        Database.database().reference().child("feed").child(currentLoggedInUserId).child(key).removeValue()
                    }
                }
            })
            
        }
       else if editProfileFollowButton.titleLabel?.text == "Follow" {
            //follow
            let ref = Database.database().reference().child("following").child(currentLoggedInUserId)
            let ref2 = Database.database().reference().child("following").child(userId)
            
            let ref00 = Database.database().reference().child("followers").child(currentLoggedInUserId)
            let ref22 = Database.database().reference().child("followers").child(userId)
            
           let values = [currentLoggedInUserId: 1]
            let values00 = [userId: 1]
            ref2.updateChildValues(values) { (err, ref2) in
                if let err = err {
                    print("Failed to  user:", err)
                    return
                }
            ref00.updateChildValues(values00)
                
                Api.MyPosts.REF_MYPOSTS.child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
                    if let dict = snapshot.value as? [String: Any] {
                        
                        dict.forEach({ (snapshot) in
                            if let value = snapshot.value as? NSDictionary {
                                let creationDate = value["creationDate"] as? TimeInterval ?? 0
                                Database.database().reference().child("feed").child(currentLoggedInUserId).child(snapshot.key).setValue(["uid": userId, "creationDate":creationDate])

                            }
                        })
                       
                    }
                })
                
                
                print("Successfully followed user: ", self.user?.username ?? "")
                
                self.editProfileFollowButton.setTitle("Unfollow", for: .normal)
                self.editProfileFollowButton.backgroundColor = .white
                self.editProfileFollowButton.setTitleColor(.black, for: .normal)
                
                Api.User.observeCurrentUser(completion: { (currentuser) in
                    var fcount = currentuser.following ?? 0
                    fcount = fcount + 1
                    let addedToUser2 = ["following": fcount]
                    ref33.updateChildValues(addedToUser2)
                    

                })
                
                Api.User.observeUser(withId: userId, completion: { (userMe) in
                    var fcount = userMe?.followers ?? 0
                    fcount = fcount + 1
                    let addedToUser2 = ["followers": fcount]
                    ref3.updateChildValues(addedToUser2)
                    
                    let fcountstring = String(fcount)
                    self.followersCountLabel.text = fcountstring
                    
                    let badge = userMe?.badge ?? 0
                    
                    let bv = badge + 1
                    let bc = String(bv)
                    

                    guard let username = Auth.auth().currentUser?.displayName else { return }
                    guard let fromUserId = Auth.auth().currentUser?.uid else { return }
                    
                    let notificationMessage = username + " started following you"
                    let ref4 = Database.database().reference().child("notifications").child(userId).childByAutoId()
                    
                    let cd = Date().timeIntervalSince1970

                    
                    let notifValues = ["Message": notificationMessage, "badge":bc, "uid":userId, "fromUserId":fromUserId, "type": "23", "creationDate": cd] as [String : Any]
                    
                    ref4.updateChildValues(notifValues)
                    
                    var points = userMe?.profileProgress ?? 0
                    //suppose point is 5 . then add =
                    points = points + 7
                    let badgevalue = ["badge":bv, "profileProgress": points]


                    Api.User.REF_USERS.child(userId).updateChildValues(badgevalue)
                    
                    

                })
                
                
            

            }
        } else  {
            print("dhoom")
//            editProfileFollowButton.isHidden = true
//            EditProfileTap()
        }
        
    }

    func EditProfileTap(){
        print("osss")

        guard let user = user else { return }
        print("osss")
        self.delegate?.onEditPorfileTap(user: user)
    }
    
     var textView: UITextView!
        
    lazy var emojitext: UITextView = {
        let tv = UITextView()
        tv.text = "😂😎🍕😄"
        tv.isUserInteractionEnabled = true
        tv.font = UIFont.systemFont(ofSize: 15)
        tv.sizeToFit()
        tv.isEditable = false
        tv.isScrollEnabled = false
        tv.layer.borderWidth = 0.5
        tv.layer.borderColor = UIColor.gray.cgColor
        tv.layer.cornerRadius = 5
        
        tv.contentInset = UIEdgeInsetsMake(10, 10, 20, 10)
        tv.textAlignment = .center

        tv.backgroundColor = UIColor.rgb(red: 34, green: 167, blue: 240, alpha: 1)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(emojiTapped))
        tap.numberOfTapsRequired = 1
        tv.addGestureRecognizer(tap)

        return tv
    }()
    
    @objc func emojiTapped(){
        
        self.delegate?.onEmojiTap(for: self)
        print("osss")

    }
    
    let levelBar: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        return view
    }()
    
    let levelProgressBar: GTProgressBar = {
        let bar = GTProgressBar()
//        bar.progress = 0.3
        bar.barBorderColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        bar.barFillColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        bar.barBackgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 0.2)
        bar.barBorderWidth = 1
        bar.barFillInset = 2
        bar.labelTextColor = UIColor.darkGray
        bar.progressLabelInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        bar.font = UIFont.boldSystemFont(ofSize: 16)
        bar.labelPosition = GTProgressBarLabelPosition.right
        bar.barMaxHeight = 12
//        bar.direction = GTProgressBarDirection.anticlockwise

        return bar
    }()
    
    fileprivate func setupLables(){
        let stackView = UIStackView(arrangedSubviews: [followersLabel, rocketsLabel, followingLabel])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        addSubview(stackView)
        
        stackView.anchor(top: levelProgressBar.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 40, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 30)
    }
    fileprivate func setupLableCounts(){
        let stackView = UIStackView(arrangedSubviews: [followersCountLabel, rocketsCountLabel, followingCountLabel])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        addSubview(stackView)
        
        stackView.anchor(top: levelProgressBar.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 13, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 40)
    }
    
    func blockedCheck(){
        guard let currentLoggedInUserId = Auth.auth().currentUser?.uid else { return }
        
        guard let userId = user?.uid else { return }

        print("fuc", userId)
        let ref = Database.database().reference().child("BlockedProfile").child(currentLoggedInUserId).child(userId)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                print("it works")
                self.blockedView()
            } else {
                self.notBlocked()

            }
        }
    }
    let blockMsgLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.numberOfLines = 0
        label.text = "You have blocked this person. You won't be able to see, read, comment on each other's content and profile."
        label.textAlignment = .center
        label.backgroundColor = UIColor.darkGray
        label.textColor = .white
        return label
    }()
    
    lazy var unblockLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(16)
        label.text = "Tap here to unblock."
        label.layer.borderWidth = 0.5
        label.layer.borderColor = UIColor.gray.cgColor
        label.layer.cornerRadius = 5
        
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(unBlockTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    fileprivate func blockedView() {
    
        self.addSubview(blockMsgLabel)
        self.addSubview(unblockLabel)
        blockMsgLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 50, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        unblockLabel.anchor(top: blockMsgLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
    }
    
    @objc func unBlockTapped() {
        print("unblock me")
        guard let currentLoggedInUserId = Auth.auth().currentUser?.uid else { return }
        
        guard let userId = user?.uid else { return }
        
        print("fussssssssssc", userId)
        let ref = Database.database().reference().child("BlockedProfile").child(currentLoggedInUserId).child(userId)
        
        ref.removeValue()
        
        self.blockMsgLabel.isHidden = true
        self.unblockLabel.isHidden = true
        self.delegate?.onUnblock()
        print("unblocked")
    }
    
    let boatImageView: UIImageView = {
        let imageview = UIImageView()
        
        return imageview
    }()
    
    fileprivate func checkAvatar() {
        
//        guard let user1 = user else { return }
//
//        let uid = user1.uid
//        let ref = Database.database().reference().child("users").child(uid)
//
//        ref.observeSingleEvent(of: .value) { (snapshot) in
//            let value = snapshot.value as? NSDictionary
//
//            let avatar = value?["Avatar"] as? Int ?? 0
//
//            if avatar == 11 {
//                print("bc")
//                self.boatImageView.image = #imageLiteral(resourceName: "boatMan")
//            } else if avatar == 23 {
//                print("kar har madan fateh")
//                self.boatImageView.image = #imageLiteral(resourceName: "boatGirl")
//
//            } else {
//                return
//            }
//
//        }
        
    }
    let mainImage: UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        
        return image
        
    }()

    let questCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(15)
        label.textAlignment = .center
        label.textColor = UIColor.black
        label.text = "23 Stories written in quest"
        
        return label
    }()
    
    let totalStoriesCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(15)
        label.textAlignment = .center
        label.textColor = UIColor.black
        
        return label
    }()

    lazy var profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 35
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(profileItemTapped))
        tap.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tap)

        
        return imageView
    }()
    
    @objc func profileItemTapped(){
        guard let ic = self.profileItemName else { return }
        self.profileDelegate?.profileItemTapped(itemcode: ic)
        print("mains")
    }
    

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        checkAvatar()
        blockedCheck()

    }
    
    func notBlocked() {
        let emojiView = ISEmojiView()
        emojiView.translatesAutoresizingMaskIntoConstraints = false
        emojiView.delegate = self
        emojitext.inputView = emojiView
        //        textView.becomeFirstResponder()
        
        backgroundColor = .white
        addSubview(shortBioTextView)
        addSubview(profileImageView)
        addSubview(profileItemView)
        addSubview(nameLabel)
        
        addSubview(firstBackground)
        
        
        addSubview(progressBackground)
        addSubview(itemsBackground)
        addSubview(levelProgressBar)
        addSubview(levelCount)
        
        addSubview(levelBar)
        
        addSubview(boatImageView)
        addSubview(questCountLabel)
        addSubview(totalStoriesCountLabel)
        
        addSubview(followersCountLabel)
        addSubview(followersLabel)
        
        addSubview(followingCountLabel)
        addSubview(followingLabel)
        
        addSubview(postCountLabel)
        addSubview(heartsLabel)
        
        addSubview(rocketsLabel)
        addSubview(followingLabel)
        
        addSubview(rocketsCountLabel)
        addSubview(rocketsLabel)
        addSubview(editProfileFollowButton)
        
        addSubview(settingsButton)
//        self.sendSubview(toBack: levelProgressBar)
        self.sendSubview(toBack: levelBar)
        
        setupLables()
        setupLableCounts()
        
        shortBioTextView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 3, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        
        let dpLeftConstant = (frame.width/2)-60
        profileImageView.anchor(top: shortBioTextView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: dpLeftConstant, bottomConstant: 0, rightConstant: 0, widthConstant: 120, heightConstant: 120)
        
        profileItemView.anchor(top: nil, left: profileImageView.rightAnchor, bottom: profileImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: -25, bottomConstant: 3, rightConstant: 0, widthConstant: 70, heightConstant: 70)

        nameLabel.anchor(top: profileImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        progressBackground.anchor(top: nameLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 15 , widthConstant: 0, heightConstant: 180)
        
        levelCount.anchor(top: progressBackground.topAnchor, left: progressBackground.leftAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 30)
        
        levelProgressBar.anchor(top: levelCount.bottomAnchor, left: progressBackground.leftAnchor, bottom: nil, right: progressBackground.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        

        
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = user?.shortBio
        label.sizeToFit()

        let gg = 230 + label.frame.height
            print("gg is,", gg)
        
        
        
        editProfileFollowButton.anchor(top: progressBackground.bottomAnchor , left: self.leftAnchor, bottom: nil, right: nil, topConstant: -35, leftConstant: 30, bottomConstant: 0, rightConstant: 0, widthConstant: 70, heightConstant: 0)
        settingsButton.anchor(top: progressBackground.topAnchor, left: nil, bottom: nil, right: progressBackground.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 5, widthConstant: 40, heightConstant: 40)

//        setupListViewOfItemsList()
    }
    
    func Blocked() {
        print("you are blocked")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
//    fileprivate func setupStatsBar(){
//        let stackView = UIStackView(arrangedSubviews: [numbersList, namesList])
//        
//        addSubview(stackView)
//        
//        stackView.anchor(top: statusLabel.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
//    }
    
}

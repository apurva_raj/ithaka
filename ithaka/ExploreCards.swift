//
//  ExploreCards.swift
//  ithaka
//
//  Created by Apurva Raj on 11/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class ExploreCards: UICollectionViewCell {
    
    
    weak var delegate: QuestMainDelegate?
    
    var quest: QuestTitleModel? {
        didSet{
            let title = quest?.title
            
//            titleLabel.text = title
            
            guard let profileurl = quest?.questImage else { return }
            
            titleLabel.loadImage(urlString: profileurl)

        }
    }
    
    let grayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        
        return view
    }()
    
    
    lazy var titleLabel: CustomImageView = {
        let label = CustomImageView()
        label.isUserInteractionEnabled = true
        label.backgroundColor = UIColor.random()
        label.layer.cornerRadius = 3
        label.layer.borderColor  =  UIColor.gray.cgColor
        label.layer.borderWidth = 1
        label.layer.shadowOpacity = 0.5
        label.layer.shadowColor =  UIColor.black.cgColor
        label.layer.shadowRadius = 5.0
        label.layer.shadowOffset = CGSize(width:5, height: 5)
        label.layer.masksToBounds = true
    
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    lazy var titleaLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.textColor = .white
        label.text = "Rockstar"
    
        label.backgroundColor = UIColor.random()
        label.layer.cornerRadius = 5.0
        label.layer.borderColor  =  UIColor.gray.cgColor
        label.layer.borderWidth = 1
        label.layer.shadowOpacity = 0.5
        label.layer.shadowColor =  UIColor.black.cgColor
        label.layer.shadowRadius = 5.0
        label.layer.shadowOffset = CGSize(width:5, height: 5)
        label.layer.masksToBounds = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    @objc func titleTapped() {
        print("first one")
        guard let id = quest?.id else { return }
        guard let title = quest?.title else { return }
        delegate?.didTapQuestTitle(id: id, title: title)
        
        //        delegate?.didTapQuestType(for: self)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(titleLabel)
        
        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


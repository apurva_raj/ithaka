//
//  QuestAvatar.swift
//  ithaka
//
//  Created by Apurva Raj on 10/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class QuestAvatar: UICollectionViewCell {
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.text = "Read This Before Starting Quest"
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        label.textColor = .black
        return label
    }()
    
    let grayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        
        return view
    }()

    
    lazy var TextLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        textView.sizeToFit()
        textView.layoutIfNeeded()
        
        textView.text = "As you set out for Ithaka\nhope the voyage is a long one,\nfull of adventure, full of discovery.\nLaistrygonians and Cyclops,\nangry Poseidon—don’t be afraid of them:\nyou’ll never find things like that on your way\nas long as you keep your thoughts raised high,\nas long as a rare excitement\nstirs your spirit and your body.\nLaistrygonians and Cyclops,\nwild Poseidon—you won’t encounter them\nunless you bring them along inside your soul,\nunless your soul sets them up in front of you.\n \nHope the voyage is a long one.\nMay there be many a summer morning when,\nwith what pleasure, what joy,\nyou come into harbors seen for the first time;\nmay you stop at Phoenician trading stations\nto buy fine things,\nmother of pearl and coral, amber and ebony,\nsensual perfume of every kind—\nas many sensual perfumes as you can;\nand may you visit many Egyptian cities\nto gather stores of knowledge from their scholars.\n \nKeep Ithaka always in your mind.\nArriving there is what you are destined for.\nBut do not hurry the journey at all.\nBetter if it lasts for years,\nso you are old by the time you reach the island,\nwealthy with all you have gained on the way,\nnot expecting Ithaka to make you rich.\n \nIthaka gave you the marvelous journey.\nWithout her you would not have set out.\nShe has nothing left to give you now.\n \nAnd if you find her poor, Ithaka won’t have fooled you.\nWise as you will have become, so full of experience,\nyou will have understood by then what these Ithakas mean. \n\n- C.P. Cavafy"
        textView.textAlignment = .center
        textView.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)
        return textView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        self.backgroundColor = .white
        addSubview(titleLabel)
        addSubview(grayLine)
        addSubview(TextLabel)
        
    
        titleLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        grayLine.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 3)
        
        TextLabel.anchor(top: grayLine.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    

}




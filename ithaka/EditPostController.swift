//
//  EditPostController.swift
//  ithaka
//
//  Created by Apurva Raj on 16/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class EditPostController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
   
    var post: PostNew? {
        didSet{
            guard let story = post?.story else { return }
        
            StoryTextView.text = story
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        print("oh bc")
        StoryTextView.becomeFirstResponder()
        
        return true
    }
 

    
    let StoryTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 20)
        tv.backgroundColor = .white
        tv.placeholder = "Start writing"
        tv.isScrollEnabled = true
        tv.tintColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.layoutSubviews()
        tv.sizeToFit()
        tv.layoutIfNeeded()
        tv.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        tv.layoutIfNeeded()
        tv.backgroundColor = .white
        return tv
    }()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        StoryTextView.becomeFirstResponder()
        view.addSubview(StoryTextView)
        StoryTextView.delegate = self
        StoryTextView.scrollsToTop = true
        
        let gg = view.frame.height
        
        let ph = self.view.frame.width*0.05

        print("Gg is", gg)
        var finalHeight: CGFloat?
        
        if  UserDefaults.standard.object(forKey: "keyboardHeight") != nil {
            finalHeight = UserDefaults.standard.object(forKey: "keyboardHeight") as? CGFloat
            
        } else {
            finalHeight = 350
            
        }

        let fh = finalHeight ?? 350

        StoryTextView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 15, leftConstant: ph, bottomConstant: 0, rightConstant: ph, widthConstant: 0, heightConstant: gg - 20 - fh)

        
        view.backgroundColor = .white
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Update", style: .plain, target: self
            , action: #selector(handleUpdate))
        
        



    }
    
    static let updateFeedNotificationName = NSNotification.Name(rawValue: "UpdateFeed")

    var username: String?
    
    @objc func handleUpdate() {
        print("yahoo")
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        myActivityIndicator.center = view.center
        
        view.addSubview(myActivityIndicator)
        
        myActivityIndicator.startAnimating()
        
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard let newPostId = post?.id else { return }

        guard let story = StoryTextView.text, story.count > 0 else { return }
        let cd = Date().timeIntervalSince1970

        let storywords = story.components(separatedBy: CharacterSet(charactersIn: "#@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_").inverted)

        let values = ["story": story, "updated_time":cd] as [String : Any]
        
        let userPostRef = Database.database().reference().child("posts").child(uid)
        let ref = userPostRef.child(newPostId)
        
        ref.updateChildValues(values)

        let hashrefs = Database.database().reference().child("HashtagsSearch")
        let ref_username = Database.database().reference().child("users")
        
    
        
        let username = self.username ?? ""

        for var storyword in storywords {
            if storyword.hasPrefix("#") {
                storyword = storyword.trimmingCharacters(in: CharacterSet.punctuationCharacters).lowercased()
                print(storyword)
                let hashref = Database.database().reference().child("Hashtags").child(storyword).child(newPostId)
                let hashtagvalue = ["uid": uid , "creationDate":cd] as [String : Any]
                let hhs = ["tag": storyword]
                
                hashref.updateChildValues(hashtagvalue)
                hashrefs.child(storyword).updateChildValues(hhs)
                
            }
            
            if storyword.hasPrefix("@") {
                storyword = storyword.trimmingCharacters(in: CharacterSet.punctuationCharacters).lowercased()
                if storyword.count != 0 {
                    ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
                        
                        if snapshot.exists() {
                            
                            Api.User.observeUserByUsername(username: storyword, completion: { (usermodelnew) in
                                
                                guard let uuid = usermodelnew.uid else { return }
                                
                                if uuid == uid {
                                    
                                    print("bye bye")
                                } else {
                                    let notificationMessage = username + " has mentioned you in post: " + story.prefix(40)
                                    
                                    let notifValues = ["Message": notificationMessage, "uid":uuid, "fromUserId":uid, "pid":newPostId, "type":"42", "creationDate": cd] as [String : Any]
                                    
                                    let ref10 = Database.database().reference().child("notifications").child(uuid).childByAutoId()
                                    
                                    ref10.updateChildValues(notifValues)
                                }
                                
                            })
                            
                            
                        } else {
                            print("north remembers")
                            
                        }
                    }
                    
                }
                
            }

        }
        
        NotificationCenter.default.post(name: EditPostController.updateFeedNotificationName, object: nil)

    self.navigationController?.popToRootViewController(animated: true)

    }
    
}



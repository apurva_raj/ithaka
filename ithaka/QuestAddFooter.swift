//
//  QuestAddFooter.swift
//  ithaka
//
//  Created by Apurva Raj on 02/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class QuestAddFooter: BaseCollectionViewCell {
    
    let titleLabel: UITextView = {
       let label = UITextView()
        label.text = "OR"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textColor = UIColor.gray
        label.backgroundColor = .white
        label.textAlignment = .center
        return label
        
    }()
    
    
    override func setupViews() {
        super.setupViews()
        backgroundColor = .white
        addSubview(titleLabel)
        

        titleLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 40)
        
    }
}

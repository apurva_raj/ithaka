//
//  PostPreviewHeader.swift
//  ithaka
//
//  Created by Apurva Raj on 11/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import KILabel

class PostPreviewHeader: UICollectionViewCell {
    
    
    var user: UserModelNew? {
        didSet {
            guard let profileImageUrl = user?.profileImageUrl else { return }
            
            profileImageView.loadImage(urlString: profileImageUrl)
            
            setupAttributedDate()

        }
    }
    weak var textViewTap: textViewTap?

    
    var post: PostNew? {
        didSet {
            
            guard let postn = post else { return }

            guard let title = post?.title else { return }
            guard let story = post?.story else { return }
            guard let tag = post?.tag else { return }
            
            guard let commetsCount = post?.commentsCount else { return }
            guard let rocketCount = post?.rocketCount else { return }
            
            
            guard let postId = post?.id else { return }
            
            guard let currentuid = Auth.auth().currentUser?.uid else { return }
            
            let ref = Database.database().reference().child("rockets").child(postId)
            
            ref.observeSingleEvent(of: .value) { (snapshot) in
                //                let value = snapshot.value as? NSDictionary
                if snapshot.hasChild(currentuid) {
                    self.rocketImageView.setImage(#imageLiteral(resourceName: "icons8-rocket-filled-50").withRenderingMode(.alwaysOriginal), for: .normal)
                } else {
                    self.rocketImageView.setImage(#imageLiteral(resourceName: "rocket_unselected").withRenderingMode(.alwaysOriginal), for: .normal)
                }
            }
            
            let commentsCountString = String(commetsCount)
            let rokcketCountString = String(rocketCount)
            //            let heart = "❤️".stringToImage()
            
            
            titleLabel.text = title
            
            
            titleLabel.numberOfLines = 0
            titleLabel.lineBreakMode = .byWordWrapping
            titleLabel.sizeToFit()
            
            questTagLabel.numberOfLines = 0
            questTagLabel.lineBreakMode = .byWordWrapping
            questTagLabel.sizeToFit()

            let ts = title.height(withConstrainedWidth: frame.width - 30, font: UIFont.boldSystemFont(ofSize: 21))
            
            let qs = tag.height(withConstrainedWidth: frame.width - 30, font: UIFont.systemFont(ofSize: 12))
            print("qs is, ", qs)
            questTagLabel.anchor(top: nameLabel.bottomAnchor, left: self.leftAnchor, bottom: storyLabel.topAnchor, right: self.rightAnchor, topConstant:-5, leftConstant: 150, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 15)
//

            storyLabel.text = story
            
            let sh = self.frame.height - (5 + 20 + ts +  30 + 35 + 30 + 50)
            storyLabel.anchor(top: nameLabel.bottomAnchor, left: profileImageView.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: -5, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: sh )

            titleLabel.hashtagLinkTapHandler = { label, string, range in
                let tag = String(string.dropFirst()).lowercased()
                self.textViewTap?.hashTagTapped(hash: tag)
            }

            titleLabel.userHandleLinkTapHandler = { label, string, range in
                print(string)
                let mention = String(string.dropFirst()).lowercased()
                print(mention)
                self.textViewTap?.usernameTapped(username: mention)
            }

            storyLabel.hashtagLinkTapHandler = { label, string, range in
                let tag = String(string.dropFirst()).lowercased()
                self.textViewTap?.hashTagTapped(hash: tag)
            }
            
            storyLabel.userHandleLinkTapHandler = { label, string, range in
                print(string)
                let mention = String(string.dropFirst()).lowercased()
                print(mention)
                self.textViewTap?.usernameTapped(username: mention)
            }
            
            let attributedTextForComment = NSMutableAttributedString(string: commentsCountString, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
            attributedTextForComment.append(NSAttributedString(string: " Comments", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
            
            
            if post?.rocketCount == 1 {
                rocketCountLabel.text = "\(post?.rocketCount ?? 0) rocket"
                
            } else {
                rocketCountLabel.text = "\(post?.rocketCount ?? 0) rockets"
            }
            
            
            self.updateLike(post: postn)

            CommentLabel.attributedText = attributedTextForComment
            
            if tag.count == 0 {
                
            } else {
                let attributedTextForQuest = NSMutableAttributedString(string: "Quest: ", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 11)])
                attributedTextForQuest.append(NSAttributedString(string: tag, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                
                questTagLabel.attributedText = attributedTextForQuest
                
            }
            
        }
    }
    
    weak var delegate: PostDelegate?
    
    fileprivate func setupAttributedDate() {
        
        guard let username = user?.u_username else { return }

        guard let post = self.post else { return }

        let attributedText = NSMutableAttributedString(string: username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
        
        attributedText.append(NSAttributedString(string: ", ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        
        let timeAgoDisplay = post.creationDate?.timeAgoDisplay()
        attributedText.append(NSAttributedString(string: timeAgoDisplay!, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor.gray]))
        
        nameLabel.attributedText = attributedText
    }
    
    var postId: String?
    
    let titleView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.boldSystemFont(ofSize: 18)
        tv.isEditable = false
        tv.isScrollEnabled = false
        tv.sizeToFit()
        tv.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        tv.textAlignment = .center
        tv.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        return tv
    }()
    
    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 30
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        return imageView
    }()
    
    
    lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        label.textAlignment = .center
        label.textColor = .gray

        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        

        return label
    }()
    
    
    @objc func nameLabelTapped(_ recognizer: UITapGestureRecognizer) {
        print("sadsadasdasdasdasdas")
        guard let uid = user?.uid else { return }
        self.delegate?.onNameIdLabelTapped(userId: uid)

    }
    lazy var emojiLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 18)
        textView.isUserInteractionEnabled = false
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        //        textView.backgroundColor = .red
        textView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        textView.textAlignment = .center
        textView.text = "😂😎🍕😄"
        textView.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        return textView
    }()

    
    let storyView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 17)
        tv.textColor = .black
        tv.isEditable = false
        tv.sizeToFit()
        tv.layoutIfNeeded()
        tv.isScrollEnabled = false
        return tv
    }()
    
    
   
    lazy var rocketImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "rocket_unselected"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(rocketButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    
    lazy var commentImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "comment"), for: .normal)
        button.addTarget(self, action: #selector(handleComment), for: .touchUpInside)
        
        return button
    }()
    @objc func handleComment() {
        print("Trying to show comments...")
        guard let post = post else { return }
        delegate?.didTapComment(post: post)
    }

    
    @objc func rocketButtonTapped(sender: UITapGestureRecognizer){
        print("usrr id is", user?.uid)
        guard let userId = user?.uid else { return }
        let usersRef = Database.database().reference().child("users").child(userId)
        
        
        guard let username = user?.u_username ?? user?.username else { return }
        guard let postTitle = post?.title else { return }
        guard let postUserId = post?.uid else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard let postId = post?.id else { return }
        
        var username1: String?
        Api.User.observeCurrentUser { (usermodelnew) in
            guard let usernamenew = usermodelnew.u_username ?? usermodelnew.username else { return }
            username1 = usernamenew
            
        }
        
        print("user namei s", username1)
        
        let ref10 = Database.database().reference().child("notifications").child(postUserId).childByAutoId()
        
        guard let postliked = post?.isLiked else { return }
        
        if postliked == true {
            self.post?.isLiked = false
            self.post?.rocketCount = (post?.rocketCount)! - 1
            self.delegate?.onRocketButtonTapped(for: self)
            
            Api.NewPost.incrementLikes(userId: userId, postId: (post?.id)!, onSucess: { (post) in
                print(post)
            }, onError: { (err) in
            })
            
            usersRef.observeSingleEvent(of: .value) { (snapshot) in
                let value = snapshot.value as? NSDictionary
                var points = value?["profileProgress"] as? Int ?? 0
                //suppose point is 5 . then add =
                points = points - 1
                let tp = ["profileProgress": points]
                usersRef.updateChildValues(tp)
                
            }
            
            
            //remove notifcs
            
            //nothing
            
        } else {
            self.post?.isLiked = true
            self.post?.rocketCount = (post?.rocketCount)! + 1
            self.delegate?.onRocketButtonTapped(for: self)
            Api.NewPost.incrementLikes(userId: (user?.uid)!, postId: (post?.id)!, onSucess: { (post) in
                print(post)
            }, onError: { (err) in
            })
            
            usersRef.observeSingleEvent(of: .value) { (snapshot) in
                let value = snapshot.value as? NSDictionary
                var points = value?["profileProgress"] as? Int ?? 0
                //suppose point is 5 . then add =
                points = points + 1
                let tp = ["profileProgress": points]
                usersRef.updateChildValues(tp)
                
            }
            
            Api.User.observeUser(withId: postUserId) { (user) in
                let userbadge = user?.badge ?? 0
                let newvalue = userbadge + 1
                let bc = String(newvalue)
                
                let values = ["badge": newvalue]
                Api.User.REF_USERS.child(postUserId).updateChildValues(values)
                
                let username2 = username1 ?? "na"
                if uid != postUserId {
                    let notificationMessage = username2 + " sent a rocket to your story: " + postTitle
                    let notifValues = ["badge": bc, "Message": notificationMessage, "uid":postUserId, "pid":postId, "fromUserId":uid, "type":"11", "creationDate": Date().timeIntervalSince1970] as [String : Any]
                    
                    ref10.updateChildValues(notifValues)
                }
                
            }
            
            
        }
    }
    
    lazy var rocketCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(rocketCountButtonTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
    }()
    
    @objc func rocketCountButtonTapped() {
        print("taho")
        self.delegate?.onRocketCountButtonTapped(for: self)
    }
    
    let CommentLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textAlignment = .center
        label.textColor = .gray
        
        return label
    }()
    let newBack: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return view
    }()
    
    let GrayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        return view
    }()
    lazy var questTagLabel: UILabel = {
        let textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 11)
        textView.textAlignment = .left
        textView.textColor = UIColor.gray
        textView.backgroundColor = .white
        //        textView.contentInset = UIEdgeInsetsMake(2, 5, 5, 2)
        textView.isUserInteractionEnabled = true
        textView.numberOfLines = 0
        textView.lineBreakMode = .byWordWrapping
        textView.sizeToFit()
//        let tap = UITapGestureRecognizer(target: self, action: #selector(tagLabelTapped))
//        tap.numberOfTapsRequired = 1
//        textView.addGestureRecognizer(tap)
        
        return textView
    }()

    lazy var titleLabel: KILabel = {
        let textView = KILabel()
        textView.font = UIFont.boldSystemFont(ofSize: 19)
        textView.isUserInteractionEnabled = true
        textView.backgroundColor = .white
        textView.textAlignment = .left
        textView.numberOfLines = 0
        textView.lineBreakMode = .byWordWrapping
        textView.sizeToFit()

//        let tap = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
//        tap.numberOfTapsRequired = 1
//        textView.addGestureRecognizer(tap)
        textView.layoutIfNeeded()
        
        return textView
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(13)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .gray
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        //        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return label
    }()
    
    lazy var optionButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_expand_more"), for: .normal)
//        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOptionButton))
//        tap.numberOfTapsRequired = 1
//        button.addGestureRecognizer(tap)

        button.isEnabled = true
        button.isHidden = false
        //        button.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return button
    }()
    lazy var storyLabel: KILabel = {
        let textview = KILabel()
        textview.textColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.85)
        //        textview.contentInset = UIEdgeInsetsMake(-5, 0, 0, 0)
        textview.isUserInteractionEnabled = true
        textview.numberOfLines = 0
        //        textview.isEditable = false
        //        textview.isScrollEnabled = false
        textview.lineBreakMode = .byWordWrapping
        textview.font = UIFont(name: "HelveticaNeue", size: CGFloat(16))

                textview.sizeToFit()
        //        textview.translatesAutoresizingMaskIntoConstraints = true
        
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(storyViewTapped))
        //        tap.numberOfTapsRequired = 1
        //        textview.addGestureRecognizer(tap)
        
        return textview
    }()
    let BlackLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        
        return view
    }()
    let BlueLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        
        return view
    }()

    lazy var shareImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "icons8-right-2-50"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(shareButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    @objc func shareButtonTapped(sender: UITapGestureRecognizer){
        self.delegate?.onShareButtonTapped(for: self)
        print("sharing")
    }
    
    let profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 20
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        return imageView
    }()

    
    lazy var swipeImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "icons8-down-arrow-filled-50"), for: .normal)
        button.tintColor = .gray
        let tap = UITapGestureRecognizer(target: self, action: #selector(swipeButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    @objc func swipeButtonTapped() {
        print("swipe")
//        self.delegate?.onswipetButtonTapped(for: self)
    }
    func updateLike(post: PostNew) {
                guard let postliked = post.isLiked else { return }
        let imageName = post.likes == nil || !postliked ? "rocket_unselected" : "rocket_selected"
        rocketImageView.setImage(UIImage(named: imageName), for: .normal)
        let count = post.rocketCount
        
        if post.rocketCount == 1 {
            rocketCountLabel.text = "\(count ?? 0) rocket"
            
        } else {
            rocketCountLabel.text = "\(count ?? 0) rockets"
        }
        
    }
    func tryingnew(){
        Api.NewPost.incrementLikes(userId: (user?.uid)!, postId: (post?.id)!, onSucess: { (post) in
            self.updateLike(post: post)
            self.post?.likes = post.likes
            self.post?.isLiked = post.isLiked
            self.post?.rocketCount = post.rocketCount
            
            if post.uid != Api.User.CURRENT_USER?.uid {
                let timestamp = NSNumber(value: Int(Date().timeIntervalSince1970))
                guard let postliked = post.isLiked else { return }
                
                if postliked {
                    //                    let newNotificationReference = Api.Notification.REF_NOTIFICATION.child(post.uid!).child("\(post.id!)-\(Api.User.CURRENT_USER!.uid)")
                    //                    newNotificationReference.setValue(["from": Api.User.CURRENT_USER!.uid, "objectId": post.id!, "type": "like", "timestamp": timestamp])
                } else {
                    //                    let newNotificationReference = Api.Notification.REF_NOTIFICATION.child(post.uid!).child("\(post.id!)-\(Api.User.CURRENT_USER!.uid)")
                    //                    newNotificationReference.removeValue()
                }
                
            }
            
        }) { (errorMessage) in
            print("error")
        }
        //incrementLikes(forRef: postRef)
        
    }
    override init(frame: CGRect) {
        super.init(frame: frame)

        self.backgroundColor = UIColor.white
        addSubview(questTagLabel)
        self.bringSubview(toFront: questTagLabel)
        addSubview(titleLabel)
        addSubview(profileImageView)
        addSubview(profileItemView)
        addSubview(nameLabel)
        addSubview(optionButton)
        addSubview(emojiLabel)
        addSubview(storyLabel)
        
        addSubview(BlackLine)
        
        addSubview(optionButton)
        
        addSubview(rocketImageView)
        addSubview(commentImageView)
        
        addSubview(rocketCountLabel)
        addSubview(CommentLabel)
        
        addSubview(shareImageView)
        addSubview(swipeImageView)
        addSubview(GrayLine)

        
    
        
        profileImageView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: frame.width/2 - 25, bottomConstant: 10, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        
        profileItemView.anchor(top: nil, left: profileImageView.rightAnchor, bottom: profileImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: -10, bottomConstant: 3, rightConstant: 0, widthConstant: 40, heightConstant: 40)

        
        nameLabel.anchor(top: profileImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 15, bottomConstant: 10, rightConstant: 25, widthConstant: 0, heightConstant: 35)
        
        
        
        optionButton.anchor(top: nameLabel.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 6.5, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        
        BlackLine.anchor(top: storyLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 1)
        
        rocketImageView.anchor(top: BlackLine.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant:5, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        commentImageView.anchor(top: BlackLine.bottomAnchor, left: rocketImageView.rightAnchor, bottom: nil, right: nil, topConstant:5, leftConstant: 30, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        
        shareImageView.anchor(top: BlackLine.bottomAnchor, left: commentImageView.rightAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 35, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        rocketCountLabel.anchor(top: commentImageView.bottomAnchor, left: rocketImageView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        CommentLabel.anchor(top: rocketCountLabel.bottomAnchor, left: rocketImageView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        GrayLine.anchor(top: CommentLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)

   }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//
//  WriteTodaysQuest.swift
//  ithaka
//
//  Created by Apurva Raj on 24/09/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase

protocol QuestDailyDelegate: class {
    func onTappedToQuest(title: String)
    func rightbuttonTapped()
}


class WriteTodaysQuest: UICollectionViewController, UICollectionViewDelegateFlowLayout, QuestDailyDelegate {
    func rightbuttonTapped() {
        print("WW")
    }
    
    
    func onTappedToQuest(title: String) {
        print("yahho ",title)
        let addIthakaController = AddIthakaAPI.addIthakaController
       addIthakaController.questTag = title
        self.navigationController?.pushViewController(addIthakaController, animated: true)

    }
    

    lazy var startwriting: UITextView = {
        let tv = UITextView()
        tv.text = "Start Writing"
        tv.isUserInteractionEnabled = true
        tv.textAlignment = .center
        tv.backgroundColor = UIColor.rgb(red: 240, green: 245, blue: 251, alpha: 1)
        tv.layer.cornerRadius = 7
        tv.textColor = .black
        tv.font = UIFont.systemFont(ofSize: 30)
        let tap = UITapGestureRecognizer(target: self, action: #selector(startwritingtapped))
        tap.numberOfTapsRequired = 1
        tv.addGestureRecognizer(tap)

        return tv
    }()
    
    
    @objc func startwritingtapped() {

        self.willMove(toParentViewController: nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()

    }

    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()

        collectionView?.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)
        collectionView?.register(viewfordailyquest.self, forCellWithReuseIdentifier: "viewfordailyquest")
        
        fetchQuests()
        
    }
    
    var dailyQuest = [DailyQuestModel]()
    var dailyQuest1 = [DailyQuestModel]()

    var todayQuest: String?
    var fquestheight: CGFloat?
    
    func fetchQuests() {
        let ref = Database.database().reference().child("dailyQuest")
        
        var qqs = [DailyQuestModel]()
        
        ref.queryOrderedByKey().queryLimited(toLast: 1).observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            value.forEach({ (snapshot) in
                guard let value5 = snapshot.value as? NSDictionary else { return }
                let qqq = value5["quest"] as? String ?? "asd"
                let quest = DailyQuestModel.init(key: snapshot.key as! String, dictionary: value5 as! [String : Any])
                self.dailyQuest1.append(quest)

                let sizeofq = qqq.height(withConstrainedWidth: self.view.frame.width - 40, font: UIFont.systemFont(ofSize: 24))
                self.fquestheight = sizeofq
                
                
                self.todayQuest = qqq
                
            })
        }

        ref.queryLimited(toLast: 30).observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            

            value.forEach({ (snapshot) in
                let vv = snapshot.value
                print(snapshot.key)
                print("snapshot.key", vv)
                
                let quest = DailyQuestModel.init(key: snapshot.key as! String, dictionary: vv as! [String : Any])
                qqs.append(quest)
                self.dailyQuest.append(quest)


            })
            
            DispatchQueue.main.async {
                qqs.sort(by: {$0.timestamp! > $1.timestamp!})
                qqs.removeFirst()
                self.dailyQuest = qqs
                self.collectionView?.reloadData()
                self.myActivityIndicator.stopAnimating()
            }
            
            
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dailyQuest.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
            let quest = dailyQuest[indexPath.item]
            let qtitle = quest.quest?.height(withConstrainedWidth: view.frame.width - 30, font: UIFont.systemFont(ofSize: 20))
            
            return CGSize(width: view.frame.width, height: qtitle! + 40)

        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "viewfordailyquest", for: indexPath) as! viewfordailyquest
            cell.delegate = self
            cell.dailyQuest = dailyQuest[indexPath.item]
            return cell

        
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        // You need this because this delegate method will run at least
//        // once before the header is available for sizing.
//        // Returning zero will stop the delegate from trying to get a supplementary view
//        return .zero
//
//    }
//
//    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerViewfordailyquest", for: indexPath) as! headerViewfordailyquest
//        header.delegate = self
//        header.dailyQuest = todayQuest
//        return header
//
//    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
    }
    
}

class headerViewfordailyquest: UICollectionViewCell {
    weak var delegate: QuestDailyDelegate?

    var dailyQuest: String?{
        didSet{
            
            let qq = dailyQuest ?? ""
            title1.text = qq
            
        }
    }

    let timestamp: UILabel = {
        let tv = UILabel()
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.numberOfLines = 0
        tv.lineBreakMode = .byWordWrapping
        tv.textAlignment = .center
        tv.textColor = .gray
        return tv
    }()


    lazy var title1: UILabel = {
        let tv = UILabel()
        tv.text = "2018-09-24 11:38:20.773079+0530 ithaka[50123:1708423] TIC Read Status [8:0x0]: 1:57"
        tv.font = UIFont.systemFont(ofSize: 23)
        tv.numberOfLines = 0
        tv.lineBreakMode = .byWordWrapping
        tv.backgroundColor = .white
        tv.layer.cornerRadius = 20
        tv.clipsToBounds = true
        tv.padding = UIEdgeInsetsMake(5, 5, 5, 5)
        tv.sizeToFit()
        tv.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(questButtonTapped))
        tap.numberOfTapsRequired = 1
        tv.addGestureRecognizer(tap)

        return tv
    }()
    
    @objc func questButtonTapped(){
        let tt = title1.text ?? ""
        self.delegate?.onTappedToQuest(title: tt)
    }
    
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(title1)
        addSubview(timestamp)
        
        title1.anchor(top: self.topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 20, rightConstant:15, widthConstant: 0, heightConstant: 0)
        timestamp.anchor(top: title1.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: -20, leftConstant: 15, bottomConstant: 0, rightConstant:15, widthConstant: 0, heightConstant: 20)


    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class viewfordailyquest: UICollectionViewCell {
    
    var dailyQuest: DailyQuestModel?{
        didSet{
            
            
            let qq = dailyQuest?.quest
            
            let attributedText = NSMutableAttributedString(string: qq!)
            attributedText.append(NSAttributedString(string: "\n\n", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))

            
            let qqa = dailyQuest?.id
            let asdx = Int(qqa!)
            let secondsFrom1970 = asdx ?? 0
            let creationDate = Date(timeIntervalSince1970: TimeInterval(secondsFrom1970))

            let timeAgoDisplay = creationDate.timeAgoDisplay()
//            timestamp.text = timeAgoDisplay
//            title2.text = qq
//
            attributedText.append(NSAttributedString(string: timeAgoDisplay, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor.gray]))
            
            title2.attributedText = attributedText
            timestamp.text = timeAgoDisplay

        }
    }
    weak var delegate: QuestDailyDelegate?
    
    lazy var title2: UILabel = {
        let tv = UILabel()
        tv.text = "2018-09-24 11:38:20.773079+0530 ithaka[50123:1708423] TIC Read Status [8:0x0]: 1:57"
        tv.font = UIFont.systemFont(ofSize: 19)
        tv.padding = UIEdgeInsetsMake(15, 15, 15, 15)
        tv.numberOfLines = 0
        tv.lineBreakMode = .byWordWrapping
        tv.layer.cornerRadius = 15
        tv.clipsToBounds = true
        tv.sizeToFit()
        tv.backgroundColor = .white
        tv.isUserInteractionEnabled = true
//        let tap = UITapGestureRecognizer(target: self, action: #selector(questButtonTapped))
//        tap.numberOfTapsRequired = 1
//        tv.addGestureRecognizer(tap)

        return tv
    }()
    
    lazy var timestamp: UILabel = {
        let tv = UILabel()
        tv.text = "not today"
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.numberOfLines = 0
        tv.lineBreakMode = .byWordWrapping
        
        return tv
    }()
    
    lazy var readButton: UIButton = {
        let button = UIButton()
        button.setTitle("Read", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        return button
    }()

    
    @objc func questButtonTapped(){
        let tt = dailyQuest?.quest ?? ""
        self.delegate?.onTappedToQuest(title: tt)
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(title2)

        title2.anchor(top: safeAreaLayoutGuide.topAnchor, left:leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
    
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  QuestIsLockedController.swift
//  ithaka
//
//  Created by Apurva Raj on 10/03/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit

class QuestIsLockedController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    

    lazy var questLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(30)
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.tag = 0
        label.layer.cornerRadius = 15
        label.clipsToBounds = true
        
        return label
    }()
    
    lazy var lockImageView: UIImageView = {
        let imageView = UIImageView()
        
        imageView.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleShare))
        tap.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tap)

        return imageView
    }()
    
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?

    
    @objc func handleShare(){
        print("sharing it")
        
        let newcontroller = UnlockItemSelectionController(collectionViewLayout: UICollectionViewFlowLayout())
        
        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: newcontroller)

        newcontroller.popoverPresentationController?.sourceView = self.view
        newcontroller.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        newcontroller.popoverPresentationController?.permittedArrowDirections = []
        
        newcontroller.transitioningDelegate = self.halfModalTransitioningDelegate
        newcontroller.modalPresentationStyle = .custom
        newcontroller.modalPresentationCapturesStatusBarAppearance = true
        
        
        self.present(newcontroller, animated: true, completion: nil)
        
    }


    
    lazy var lineview: UIView = {
        let view = UIView()
        view.backgroundColor = .gray
        
        return view
    }()
    
    lazy var infoLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        label.layer.cornerRadius = 7
        label.clipsToBounds = true
        label.padding = UIEdgeInsetsMake(10, 10, 10, 10)
        return label
    }()
    lazy var infoLabel1: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        label.layer.cornerRadius = 7
        label.clipsToBounds = true
        label.padding = UIEdgeInsetsMake(10, 10, 10, 10)
        return label
    }()
    lazy var infoLabel2: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        label.layer.cornerRadius = 7
        label.clipsToBounds = true
        label.padding = UIEdgeInsetsMake(10, 10, 10, 10)
        return label
    }()

    
    lazy var footer: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13)
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.7)
        label.numberOfLines = 0
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.layer.cornerRadius = 10
        self.view.clipsToBounds = true
        self.collectionView?.backgroundColor = .white
        
        setupTopBar()
        
        setupBody()
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateData), name: UnlockItemSelectionController.updateUnlockItemNotificationName, object: nil)
    }
    
    @objc func handleUpdateData(){
        print("called for shyre")
        self.dismiss(animated: true, completion: nil)
    }
    
    func setupTopBar() {
        
        let newview = UIView()
        newview.backgroundColor = .clear
        
        self.view.addSubview(newview)
        self.view.addSubview(questLabel)
        
        
        newview.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 110)
        
        let stopButton: UIButton = {
            let button = UIButton()
            button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
            button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
            
            return button
        }()
        
        
        newview.addSubview(stopButton)
    
        
        stopButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 30, heightConstant: 30)
        let ph = self.view.frame.width*0.05

        self.questLabel.anchor(top: stopButton.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 25, leftConstant: ph, bottomConstant: 0, rightConstant: ph, widthConstant: 0, heightConstant: 150)
        
    }

    
    func setupBody(){
        self.view.addSubview(lockImageView)
        self.view.addSubview(infoLabel)
        self.view.addSubview(infoLabel1)
        self.view.addSubview(infoLabel2)

        self.view.addSubview(footer)
        let hh = 100.0
        let ph = self.view.frame.width*0.05

        let ch = view.frame.width/2
        lockImageView.anchor(top: questLabel.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: nil, topConstant: -50, leftConstant: ch - 50.0, bottomConstant: 0, rightConstant: 0, widthConstant: CGFloat(hh), heightConstant: CGFloat(hh))
        
        infoLabel.anchor(top: lockImageView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 20, leftConstant: ph, bottomConstant: 0, rightConstant: ph, widthConstant: 0, heightConstant: 70)
        infoLabel1.anchor(top: infoLabel.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 10, leftConstant: ph, bottomConstant: 0, rightConstant: ph, widthConstant: 0, heightConstant: 70)
        infoLabel2.anchor(top: infoLabel1.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 10, leftConstant: ph, bottomConstant: 0, rightConstant: ph, widthConstant: 0, heightConstant: 70)

        footer.anchor(top: nil, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor , right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)

    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                self.handleDismiss()
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    
    @objc func handleDismiss(){
        dismiss(animated: true, completion: nil)
    }
    

    
}


class QuestIsLockedViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

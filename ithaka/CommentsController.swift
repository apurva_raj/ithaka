//
//  CommentsController.swift
//  ithaka
//
//  Created by Apurva Raj on 31/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//


import UIKit
import Firebase
import SwipeCellKit
import SwiftMessages


protocol commentDelegate: class {
    func onProfileTapped(for cell: CommentCell)
    func onLikeButtonTapped(for cell: CommentCell)
}

protocol CommentInputAccessoryViewDelegate: class {
    func didSubmit(for comment: String)
}

class CommentsController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, commentDelegate, textViewTap, CommentInputAccessoryViewDelegate, SwipeCollectionViewCellDelegate, UITextViewDelegate {
    
    lazy var enterCommentTextVIew: UITextView = {
        let tv = UITextView()
        tv.placeholder = "enter comment"
        tv.font = UIFont.systemFont(ofSize: 25)
        tv.backgroundColor = .yellow
        
        return tv
    }()

    
    func onLikeButtonTapped(for cell: CommentCell) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        DispatchQueue.main.async {
            self.collectionView?.reloadSections([0])

        }
    }
    override var canBecomeFirstResponder: Bool {
        return true
    }

    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        let comment = commentNew[indexPath.item]
        let commentId = comment.id ?? ""
        let commentOwnerId = comment.uid
        guard orientation == .right else { return nil }
        let postId = self.post?.id ?? ""
        let postOwnerId = self.post?.uid ?? ""
        let currentid = Auth.auth().currentUser?.uid ?? ""
        
        func deleteComment() {
            let ref = Database.database().reference().child("comments").child(postId).child(commentId)
            ref.removeValue(completionBlock: { (err, ref) in
                if let err = err {
                    print(err)
                } else {
                }
            })
            
            if postOwnerId != commentOwnerId {
                let usersRef = Database.database().reference().child("users").child(postOwnerId)
                usersRef.observeSingleEvent(of: .value) { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    var points = value?["profileProgress"] as? Int ?? 0
                    //suppose point is 5 . then add =
                    points = points - 3
                    let tp = ["profileProgress": points]
                    usersRef.updateChildValues(tp)
                }

            }
            
            self.commentNew.remove(at: indexPath.row)
            self.collectionView?.deleteItems(at: [indexPath])

        }
        
        func delete() {
            if commentOwnerId == currentid {
                deleteComment()
            }
        }
        func delete2(){
            if currentid == postOwnerId {
                deleteComment()
            }
        }
        let deleteAction = SwipeAction(style: .destructive, title: "Delete") { action, indexPath in
            // handle action by updating model with deletion
            if currentid == postOwnerId {

                deleteComment()
                self.updateCommentsCount()

            } else {
                if currentid == commentOwnerId {
                    deleteComment()
                    self.updateCommentsCount()
                } else {
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙄"
                        view.configureContent(title: "Sorry", body: "You can't delete this comment", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                }
            }
            
        }

        
        // customize the action appearance
        deleteAction.image = UIImage(named: "delete")
        NotificationCenter.default.post(name: CommentsController.deleteCommentsCountName, object: nil)

        return [deleteAction]

    }
    
    fileprivate func updateCommentsCount() {
        guard let toUserId = post?.uid else { return }
        let postId = self.post?.id ?? ""
        
        let ref2 = Database.database().reference().child("comments").child(postId)
        let ref3 = Database.database().reference().child("posts").child(toUserId).child(postId)
        
        ref3.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary

            var cc = value?["commentsCount"] as? Int ?? 0

            cc = cc - 1
            
            let updates = ["commentsCount": cc]
            ref3.updateChildValues(updates)
        }
    }
    
    
    var backbutton: String?{
        didSet{
            if backbutton == "lost" {
                
                let mainview = UIView()
                mainview.backgroundColor = .white
                
                let mainview1 = UIView()
                mainview1.backgroundColor = .white

                
                let nameLabel: UILabel = {
                    let label = UILabel()
                    label.numberOfLines = 0
                    label.textAlignment = .left
                    label.textColor = .black
                    label.isUserInteractionEnabled = true
                    let tap = UITapGestureRecognizer(target: self, action: #selector(goBack))
                    tap.numberOfTapsRequired = 1
                    label.addGestureRecognizer(tap)
                    label.text = "Back"
                    label.font = UIFont.boldSystemFont(ofSize: 20)
                    
                    return label
                }()
                view.addSubview(mainview1)

                view.addSubview(mainview)
                view.addSubview(nameLabel)
                mainview1.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

                mainview.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
                
                nameLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                
                view.bringSubview(toFront: nameLabel)
                
                self.collectionView?.contentInset = UIEdgeInsetsMake(40, 0, 100, 0)

            }
        }
    }
    
    @objc func goBack() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
        func hideKeyboardWhenTappedAround() {
            let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
            tap.cancelsTouchesInView = false
            collectionView?.addGestureRecognizer(tap)
        }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        
    }

    func didSubmit(for comment: String) {
        print("submited")
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard let toUserId = post?.uid else { return }
        
        let postId = self.post?.id ?? ""
        let values = ["text": comment, "creationDate": Date().timeIntervalSince1970, "uid": uid] as [String : Any]
        
       let cd = Date().timeIntervalSince1970
        
        guard  let commentId = Database.database().reference().child("comments").child(postId).childByAutoId().key else { return }
    Database.database().reference().child("comments").child(postId).child(commentId).updateChildValues(values) { (err, ref) in
            
            if let err = err {
                print("Failed to insert comment:", err)
                return
            }
            
            print("Successfully inserted comment.")
                
                Api.User.observeCurrentUser(completion: { (UserModelNew) in
                    
                    
                    let titlewords = comment.components(separatedBy: CharacterSet(charactersIn: "#@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_").inverted)
                    let ref_username = Database.database().reference().child("users")
                    guard let username = UserModelNew.u_username ?? UserModelNew.username else { return }

                    for var titleword in titlewords {
                        if titleword.hasPrefix("@") {
                            titleword = titleword.trimmingCharacters(in: CharacterSet.punctuationCharacters).lowercased()
                            print("the word is,", titleword)
                            if titleword.count != 0 {
                                ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
                                    
                                    if snapshot.exists() {
                                        
                                        Api.User.observeUserByUsername(username: titleword, completion: { (usermodelnew) in
                                            
                                            guard let uuid = usermodelnew.uid else { return }
                                            
                                            if uuid == uid {
                                                print("bye bye")
                                            } else {
                                                
                                                let badge = usermodelnew.badge ?? 0
                                                
                                                let bv = badge + 1
                                                let badgevalue = ["badge":bv]
                                                print("badge value is", bv)
                                                let bc = String(bv)
                                                let notificationMessage = username + " has mentioned you in comment."
                                                
                                                let notifValues = ["Message": notificationMessage, "uid":uuid, "fromUserId":uid, "pid":postId, "badge": bc, "commentid":commentId, "type":"111232", "creationDate": cd] as [String : Any]
                                                
                                                let ref10 = Database.database().reference().child("notifications").child(uuid).childByAutoId()
                                                
                                                ref10.updateChildValues(notifValues)
                                                Api.User.REF_USERS.child(toUserId).updateChildValues(badgevalue)

                                            }
                                            
                                        })
                                        
                                        
                                    } else {
                                        print("north remembers")
                                        
                                    }
                                }
                                
                            }
                            
                        }
                    }
                    


                    
                    if uid == toUserId {
                        return
                    } else {
                        
                        Api.User.observeUser(withId: toUserId, completion: { (usermodelnew) in
                          
                            let badge = usermodelnew?.badge ?? 0
                            
                            let bv = badge + 1
                            let badgevalue = ["badge":bv]
                            let bc = String(bv)
                            
                            
                            let notificationMessage = username + " commented on your post "
                            
                            let ref = Database.database().reference().child("notifications").child(toUserId).childByAutoId()
                            
                            let notifValues = ["Message": notificationMessage, "badge":bc, "uid":toUserId, "pid":postId, "fromUserId":uid, "type":"111", "creationDate": cd] as [String : Any]
                            
                            ref.updateChildValues(notifValues)

                            Api.User.REF_USERS.child(toUserId).updateChildValues(badgevalue)

                        })
                        

                    }
                    

                })

                
            
            //metnion notifcation send
            
            
        }
        
        let commentLikeRef = Database.database().reference().child("comment_likes").child(postId).child(commentId)
        
        let commentLikeCountValue = ["likeCount": 0]
        commentLikeRef.updateChildValues(commentLikeCountValue)

        print("north remembers")
        
    Database.database().reference().child("comments").child(postId).queryLimited(toLast: 1).observe(.childAdded, with: { (snapshot) in
            Api.Comment.observeSingleComment(withPId: postId, withCid: snapshot.key, completion: { (comment) in
                self.fetchUser(uid: comment.uid!, completed: {
                    self.commentNew.append(comment)
                    DispatchQueue.main.async(execute: {

                        self.collectionView?.reloadData()

                        //scroll to the last index
                        let indexPath = IndexPath(item: self.commentNew.count - 1, section: 0)
                        self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)
                        
                       Database.database().reference().child("comments").child(postId).queryLimited(toLast: 1).removeAllObservers()
                    Database.database().reference().child("comments").child(postId).removeAllObservers()
                        
                        


                    })

                })
            })
        })
        
        let ref3 = Database.database().reference().child("posts").child(toUserId).child(postId)
       
        ref3.observeSingleEvent(of: .value) { (snapshot) in
             let value = snapshot.value as? NSDictionary
//            let cc = value?.count ?? 0
            var cc = value?["commentsCount"] as? Int ?? 0
            cc = cc + 1
            let updates = ["commentsCount": cc]
            ref3.updateChildValues(updates)
        }
        
        
        if uid != toUserId {
            
            Api.PointsAPII.increasePoints(n_points: 0, UserId: toUserId, catagory: 1)

//            let usersRef = Database.database().reference().child("users").child(toUserId)
//            usersRef.observeSingleEvent(of: .value) { (snapshot) in
//                let value = snapshot.value as? NSDictionary
//                var points = value?["profileProgress"] as? Int ?? 0
//                //suppose point is 5 . then add =
//                points = points + 3
//                let tp = ["profileProgress": points]
//                usersRef.updateChildValues(tp)
//            }
        }
        
        if commentNew.count != 0 {
            let indexPath = IndexPath(item: commentNew.count - 1, section: 0)
            collectionView?.scrollToItem(at: indexPath, at: .top, animated: true)

        }
        
        
        //Send some points to owner
        

        
        
        NotificationCenter.default.post(name: CommentsController.updateCommentsCountName, object: nil)

        self.view.window?.endEditing(true)
        
        
    }
    
    
        
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func usernameTapped(username: String) {
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_username = Database.database().reference().child("users")
        
        ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.exists() {
                profileController.userName = username
                self.navigationController?.pushViewController(profileController, animated: true)
            } else {
                print("north remembers")
                
            }
        }
        
    }
    
    func hashTagTapped(hash: String) {
        let hh = HashTagPostsController()
        
        let ref_hash = Database.database().reference().child("Hashtags").child(hash)
        
        ref_hash.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                hh.hashtag = hash
                self.navigationController?.pushViewController(hh, animated: true)

            } else {
                print("stupid")
            }
        }
    }

    func onProfileTapped(for cell: CommentCell) {
        
        print("bc")
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        let comment = self.commentNew[indexPath.item]
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = comment.uid
     self.navigationController?.pushViewController(profileController,
                                                      animated: true)


    }
    
    
    
    func onProfileTapped() {
        print("tapped")
    }
    
    var post: PostNew?
    var posterId: String?
    let cellId = "cellId"
    var timer:Timer? = nil
    
    let headerbar: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1).cgColor

        return view
        
    }()
    lazy var arrowIcon: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "down"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleDownArrow))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        button.isEnabled = true
        return button
    }()
    
    @objc func handleDownArrow(){
        print("Damn")
        self.navigationController?.popViewController(animated: true)
    }
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
  
    lazy var containerView: CommentInputAccessoryView = {
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
        let commentInputAccessoryView = CommentInputAccessoryView(frame: frame)
        commentInputAccessoryView.delegate = self
        
        return commentInputAccessoryView
    }()
    
//    lazy var containerView1: CommentInputAccessoryView = {
//        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
//        let commentInputAccessoryView = CommentInputAccessoryView(frame: frame)
//        commentInputAccessoryView.delegate = self
//
//        return commentInputAccessoryView
//    }()
//

    lazy var addCommentLabel: UITextView = {
        let label = UITextView()
        label.text = "Add comment"
        label.font = UIFont.systemFont(ofSize: 20)
        label.textAlignment = .center
        label.textColor = .blue
        label.isUserInteractionEnabled = true
        label.isEditable = true
        
        return label
    }()
    var textHeightConstraint: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 20, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18)]


        collectionView?.backgroundColor = UIColor.rgb(red: 255, green: 255, blue: 255, alpha: 1)
        collectionView?.delegate = self
        collectionView?.isUserInteractionEnabled = true
        collectionView?.register(CommentCell.self, forCellWithReuseIdentifier: cellId)

        fetchNewComments()
        setupKeyboardObservers()
//        fetchComments()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "STHeitiSC-Medium", size: 18) ?? UIFont.systemFont(ofSize: 18)]

       self.navigationItem.title = "Comments"
        
        setupTopBar()
        collectionView?.alwaysBounceVertical = true
        collectionView?.keyboardDismissMode = .interactive
        
        collectionView?.contentInset = UIEdgeInsets(top: 60, left: 0, bottom: 0, right: 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        


    }
    
  
    lazy var backButton1: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        return button
    }()


    func setupTopBar() {
        
        let newview = UIView()
        newview.backgroundColor = .white
        
        let newview1 = UIView()
        newview1.backgroundColor = .white
        

        self.view.addSubview(newview1)
        
        self.view.addSubview(newview)

        
        newview1.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        
        newview.addSubview(backButton1)
        
        backButton1.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
        
        
    }

    static let updateCommentsCountName = NSNotification.Name(rawValue: "UpdateCommentsCount")
    static let deleteCommentsCountName = NSNotification.Name(rawValue: "DeleteCommentsCount")

    func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardDidShow), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
    }
    
    
    @objc func handleKeyboardDidShow() {
        if commentNew.count > 0 {
            let indexPath = IndexPath(item: commentNew.count - 1, section: 0)
            collectionView?.scrollToItem(at: indexPath, at: .top, animated: true)
        }
    }
    
    var containerViewBottomAnchor: NSLayoutConstraint?


    func handleKeyboardWillShow(_ notification: Notification) {
        let keyboardFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        let keyboardDuration = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        
        containerViewBottomAnchor?.constant = -keyboardFrame!.height
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.layoutIfNeeded()
        })
    }
    
    func handleKeyboardWillHide(_ notification: Notification) {
        let keyboardDuration = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue

        containerViewBottomAnchor?.constant = 0
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.view.layoutIfNeeded()
        })
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
        self.navigationController?.navigationBar.isHidden = false


    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }

    var commentNew = [CommentNew]()
    var usersNew = [UserModelNew]()

    
    fileprivate func fetchNewComments() {
        
        if posterId == nil {
            guard let postId = self.post?.id else { return }
            Api.Comment.getRecentComments(withId: postId, limit: 100, completionHandler: { (results) in
                guard let results = results else { self.myActivityIndicator.stopAnimating()
                    return }
                
                if results.count == 0 {
                    self.myActivityIndicator.stopAnimating()

                }
                
                if results.count > 0 {
                    results.forEach({ (result) in
                        self.commentNew.append(result.0)
                        self.usersNew.append(result.1)
                        
                            //scroll to the last index

                    })
                    
                }
                DispatchQueue.main.async {
                    self.myActivityIndicator.stopAnimating()
                    self.collectionView?.reloadSections([0])
                    let indexPath = IndexPath(item: self.commentNew.count - 1, section: 0)
                    self.collectionView?.scrollToItem(at: indexPath, at: .bottom, animated: true)
                    
                }

                print(results.count)
            })
            
            
        } else {
            self.myActivityIndicator.stopAnimating()

        }

    }
    
    
    func fetchUser(uid: String, completed:  @escaping () -> Void ) {
        
        Api.User.observeUser(withId: uid, completion: {
            user in
            self.usersNew.append(user!)
            completed()
        })
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(commentNew.count)

        return commentNew.count
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let comment = commentNew[indexPath.item]
        let user = self.usersNew[indexPath.item]
        
        let name = user.u_username ?? user.username
    
        let text = comment.text
        
        let maintext = name! + text!
        
        let height = maintext.height(withConstrainedWidth: view.frame.width-95, font: UIFont.systemFont(ofSize: 14))

        let finalheight = height + 35
        print("fff is", finalheight)
        return CGSize(width: view.frame.width, height: finalheight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CommentCell
        cell.delegate = self
        cell.textViewTap = self
        cell.cDelegate = self
        cell.comment = self.commentNew[indexPath.item]
        cell.user = self.usersNew[indexPath.item]
        
        let postId = self.post?.id

        cell.postId = postId
        return cell
    }

    lazy var submitButton: UIButton = {
        let button = UIButton(type: .custom)
        button.isEnabled = false
        button.setTitle("Send", for: .normal)
        button.setTitleColor(.gray, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)

        return button
    }()
    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "cancel_shadow"), for: .normal)
        button.backgroundColor = UIColor.darkGray
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        
        return button
    }()
    @objc func handleDismiss(){

        self.navigationController?.popViewController(animated: true)
//        let transition: CATransition = CATransition()
//        transition.duration = 0.5
//        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
//        transition.type = kCATransitionReveal
//        transition.subtype = kCATransitionFromLeft
//        self.view.window!.layer.add(transition, forKey: nil)
//        self.dismiss(animated: false, completion: nil)

//        dismiss(animated: true, completion: nil)
        
    }



    let commentTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Enter Comment"
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        return textField
    }()

    @objc func textFieldDidChange(_ textField: UITextField) {
        let commentCount = commentTextField.text?.count

        if (commentCount == 0){
            submitButton.isEnabled = false
            submitButton.setTitleColor(.gray, for: .normal)


        } else {
            submitButton.isEnabled = true
            submitButton.setTitleColor(.black, for: .normal)
        }
    }

    
    @objc func handleSubmit() {
        print("Really?")
//        guard let uid = Auth.auth().currentUser?.uid else { return }
//        guard let toUserId = post?.uid else { return }
//
//        let postId = self.post?.id ?? ""
//        let values = ["text": commentTextField.text ?? "", "creationDate": Date().timeIntervalSince1970, "uid": uid] as [String : Any]
//
//        Database.database().reference().child("comments").child(postId).childByAutoId().updateChildValues(values) { (err, ref) in
//
//            if let err = err {
//                print("Failed to insert comment:", err)
//                return
//            }
//
//            print("Successfully inserted comment.")
//            self.view.window?.endEditing(true)
//            self.commentTextField.placeholder = " "
//            self.commentTextField.text = " "
//            if(uid == toUserId) {
//                return
//            } else {
//                guard let username = Auth.auth().currentUser?.displayName else { return }
//
//                guard let postname = self.post?.title else { return }
//                let notificationMessage = username + " commented on your post: " + postname
//
//                let ref = Database.database().reference().child("notifications").child(toUserId).childByAutoId()
//
//                let notifValues = ["Message": notificationMessage, "uid":toUserId, "pid":postId, "fromUserId":uid, "type":"11", "creationDate": Date().timeIntervalSince1970] as [String : Any]
//
//                ref.updateChildValues(notifValues)
//            }
//        }
//
//        updateCommentsCount()
    }

    override var inputAccessoryView: UIView? {
        get {
            return containerView
        }
    }

    
}

class CommentInputAccessoryView: UIView {
    
    
    var delegate: CommentInputAccessoryViewDelegate?
    
    func clearCommentTextField() {
        commentTextView.text = nil
    }
    
    fileprivate let commentTextView: UITextView = {
        let tv = UITextView()
        tv.placeholder = "Write comment..."
        tv.isScrollEnabled = false
        //        tv.backgroundColor = .red
        tv.isEditable = true
        tv.font = UIFont.systemFont(ofSize: 18)
        return tv
    }()
    
    fileprivate let submitButton: UIButton = {
        let sb = UIButton(type: .system)
        sb.setTitle("Send", for: .normal)
        sb.setTitleColor(.black, for: .normal)
        sb.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        sb.addTarget(self, action: #selector(handleSubmit), for: .touchUpInside)
        return sb
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        
        // 1
        autoresizingMask = .flexibleHeight
        
        backgroundColor = .white
        
        addSubview(submitButton)
        submitButton.anchor(top: nil, left: nil, bottom: safeAreaLayoutGuide.bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 12, widthConstant: 50, heightConstant: 50)
        
        addSubview(commentTextView)
        // 3
        if #available(iOS 11.0, *) {
            commentTextView.anchor(top: topAnchor, left: leftAnchor, bottom: safeAreaLayoutGuide.bottomAnchor, right: submitButton.leftAnchor, topConstant: 8, leftConstant: 8, bottomConstant: 8, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        } else {
            // Fallback on earlier versions
        }
        self.commentTextView.becomeFirstResponder()

        setupLineSeparatorView()
    }
    
    // 2
    override var intrinsicContentSize: CGSize {
        return .zero
    }
    
    fileprivate func setupLineSeparatorView() {
        let lineSeparatorView = UIView()
        lineSeparatorView.backgroundColor = UIColor.rgb(red: 230, green: 230, blue: 230, alpha: 1)
        addSubview(lineSeparatorView)
        lineSeparatorView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0.5)
    }
    
    @objc func handleSubmit() {
        guard let commentText = commentTextView.text else { return }
        delegate?.didSubmit(for: commentText)
        self.commentTextView.text = ""

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

//
//  Welcome2Controller.swift
//  ithaka
//
//  Created by Apurva Raj on 17/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class Welcome2Controller: UIViewController {
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "How are you feeling right now?"
        label.font = UIFont(name: "STHeitiTC-Medium", size: CGFloat(23))

        return label
    }()

    
    lazy var firstImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "boatMan")
        return iv
    }()
    lazy var secImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "boatGirl").withHorizontallyFlippedOrientation()
        
        return iv
    }()
    
    @objc func selectme(sender: UITapGestureRecognizer){
        guard let tag = sender.view?.tag else { return }
        print("Selected item is", tag)
        
    }
    
    lazy var bg1: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 80, green: 171, blue: 243, alpha: 1)
        view.tag = 1
        view.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nextIthaka))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)

        return view
    }()
    
    lazy var bg2: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 171, green: 112, blue: 212, alpha: 1)
        view.tag = 2
        view.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nextIthaka))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)

        return view
    }()


    @objc func nextIthaka(sender: UITapGestureRecognizer) {
        guard let tag = sender.view?.tag else { return }
        print("Selected item is", tag)
        let welcome3Controller = Welcome3Controller()
        welcome3Controller.tag = tag
        let backItem = UIBarButtonItem()
        backItem.title = "Back"
        self.navigationItem.backBarButtonItem = backItem
     self.navigationController?.pushViewController(welcome3Controller, animated: true)
    
    }
    
    
    lazy var captionLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = UIFont(name: "HelveticaNeue", size: CGFloat(16))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        label.text = "Right now, your avatar is dark figure. In future we will have many cool items - Skins, clothes, accessories, and even pets :) You will be able to have your own unique avatar."

        return label
    }()

    let lineView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    navigationController?.navigationItem.leftBarButtonItem?.title = "Back"
        view.backgroundColor = .white
        
        bg1.layer.cornerRadius = 5
        bg2.layer.cornerRadius = 5
        
        bg1.backgroundColor = UIColor.rgb(red: 80, green: 171, blue: 243, alpha: 1)
        bg2.backgroundColor = UIColor.rgb(red: 171, green: 112, blue: 212, alpha: 1)
        
        view.addSubview(titleLabel)
        
        view.addSubview(bg1)
        view.addSubview(bg2)
        view.addSubview(captionLabel)
        view.addSubview(lineView)
        
        let mh = UIDevice().type
        let device: String = mh.rawValue
        switch device {
        case "iPhone 5", "iPhone 5S":
            titleLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: 0, heightConstant: 0)

        default:
            titleLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 25, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: 0, heightConstant: 0)

        }

        
        lineView.anchor(top: titleLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: 0, heightConstant: 0.5)
        lineView.backgroundColor = .gray
        
        bg1.anchor(top: lineView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 25, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: view.frame.height*0.25)
        
        bg2.anchor(top: bg1.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 25, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: view.frame.height*0.25)

        bg1.addSubview(firstImageView)
        bg2.addSubview(secImageView)
        
        firstImageView.anchor(top: bg1.topAnchor, left: bg1.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: view.frame.width/2, heightConstant: view.frame.height*0.25-20)
        secImageView.anchor(top: bg2.topAnchor, left: nil, bottom: nil, right: bg2.rightAnchor, topConstant: 10, leftConstant: 40, bottomConstant: 0, rightConstant: 40, widthConstant: view.frame.width/2, heightConstant: view.frame.height*0.25-20)
        
        captionLabel.anchor(top: bg2.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 100)
    }
}

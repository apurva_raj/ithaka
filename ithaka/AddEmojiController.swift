//
//  AddEmojiController.swift
//  ithaka
//
//  Created by Apurva Raj on 27/04/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import GTProgressBar
import ISEmojiView
import SwiftMessages

class AddEmojiController: UIViewController, ISEmojiViewDelegate, UITextViewDelegate {
    
    
    var level: Int?
    var checkPoint: Int?


    var questTitle: String? {
        didSet {
            let firstext = "You are writing this as a part of your quest: "
            let stext = questTitle
            
            let ftext = firstext + stext!
            let iconText = "🦄"

            let info = MessageView.viewFromNib(layout: .MessageView)
            info.configureTheme(.info)
            info.button?.isHidden = true
            info.configureContent(title: "Hello", body: ftext, iconText: iconText)
//            info.configureContent(title: "Hello", body: ftext)
            var infoConfig = SwiftMessages.defaultConfig
            infoConfig.presentationStyle = .top
            infoConfig.duration = .seconds(seconds: 3)
           
            SwiftMessages.show(config: infoConfig, view: info)

        }
    }
    let questLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 23)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .white
        label.isUserInteractionEnabled = false
        
        
        return label
    }()
    lazy var levelCount: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .gray
        label.layer.borderWidth = 2
        label.layer.cornerRadius = 50
        label.layer.borderColor = UIColor.gray.cgColor
        label.backgroundColor = .white
        label.textAlignment = .center
        label.layer.masksToBounds = true
        label.isUserInteractionEnabled = false
        label.textAlignment = .center
        
        
        return label
        
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(18)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .gray
        label.isUserInteractionEnabled = false
        
        return label
    }()
    
    let waysTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 17)
        label.numberOfLines = 0
        label.sizeToFit()
        label.textAlignment = .center
        label.textColor = .white
        label.isUserInteractionEnabled = false
        label.text = "LEVEL UP BY..."
        
        return label
    }()
    let emojiLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 23)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .white
        label.isUserInteractionEnabled = false
        label.text = "Add 4 emojis that describes story you're about to tell"
        
        return label
    }()
    
    let addEmoji: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 23)
        tv.layer.borderColor = UIColor.gray.cgColor
        tv.layer.borderWidth = 1
        tv.layer.cornerRadius = 5
        tv.isUserInteractionEnabled = true
        tv.textAlignment = .center
        return tv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.rgb(red: 244, green: 208, blue: 63, alpha: 1)
        view.addSubview(levelCount)
        view.addSubview(nameLabel)
        view.addSubview(waysTitleLabel)
        view.addSubview(emojiLabel)
        view.addSubview(addEmoji)
        
        emojiLabel.anchor(top: view.self.topAnchor, left: view.self.leftAnchor, bottom: nil, right: view.self.rightAnchor, topConstant: 150, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        let ll = view.frame.width*0.20
        
        addEmoji.anchor(top: emojiLabel.bottomAnchor, left: view.self.leftAnchor, bottom: nil, right: view.self.rightAnchor, topConstant: 20, leftConstant: ll, bottomConstant: 0, rightConstant: ll, widthConstant: 0, heightConstant: 30)
        
        
        let emojiView = ISEmojiView()
        emojiView.translatesAutoresizingMaskIntoConstraints = false
        emojiView.delegate = self
        addEmoji.inputView = emojiView
        addEmoji.becomeFirstResponder()

        setupNavigationButton()
        addEmoji.delegate = self
        
        print("quest is", questTitle)

    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        let titleCount = addEmoji.text.count
        
        if (1 ... 4).contains(titleCount){
            navigationItem.rightBarButtonItem?.isEnabled = true
            
        } else if(titleCount == 0) {
            navigationItem.rightBarButtonItem?.isEnabled = false
            SwiftMessages.show {
                let view = MessageView.viewFromNib(layout: .CardView)
                view.configureTheme(.warning)
                view.configureDropShadow()
                let iconText = "🙄"
                view.configureContent(title: "Oops", body: "You must atleast 1 emojis!", iconText: iconText)
                view.button?.isHidden = true
                
                return view
            }
            
        } else {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            SwiftMessages.show {
                let view = MessageView.viewFromNib(layout: .CardView)
                view.configureTheme(.warning)
                view.configureDropShadow()
                let iconText = "🙄"
                view.configureContent(title: "Sorry", body: "Maximum only 4 emojis are allowed", iconText: iconText)
                view.button?.isHidden = true
                
                return view
            }
        }
        
    }

    
    func emojiViewDidSelectEmoji(emojiView: ISEmojiView, emoji: String) {
        addEmoji.insertText(emoji)
        print("hello", emoji)
    }
    func emojiViewDidPressDeleteButton(emojiView: ISEmojiView) {
        addEmoji.deleteBackward()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    }
    
    fileprivate func setupNavigationButton() {
        navigationController?.navigationBar.tintColor = .blue
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self
            , action: #selector(handleCancel))
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self
            , action: #selector(handleNext))
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        
    }
    
    @objc func handleCancel() {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func handleNext() {
        let addIthakaStoryController = AddIthakaStoryController()
        let titleT = String(addEmoji.text)
//        addIthakaStoryController.addedTitle = titleT
        addIthakaStoryController.addedTag = questTitle
        addIthakaStoryController.level = level
        addIthakaStoryController.checkPoint = checkPoint
    
    navigationController?.pushViewController(addIthakaStoryController, animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }



}

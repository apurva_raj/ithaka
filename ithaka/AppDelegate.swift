//
//  AppDelegate.swift
//  ithaka
//
//  Created by Apurva Raj on 16/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import UserNotifications

//import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, MessagingDelegate, UNUserNotificationCenterDelegate {
   
    let gcmMessageIDKey = "gcm.message_id"

    var window: UIWindow?
    //for navbar
    

    var nav: UINavigationController?
    var controller: SLPagingViewSwift2!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        application.registerForRemoteNotifications()

        //replacement code for storyboard
        FirebaseApp.configure()

//        IQKeyboardManager.sharedManager().enable = true

        Messaging.messaging().delegate = self

        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
//        GIDSignIn.sharedInstance().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
    

        self.window = UIWindow(frame: UIScreen.main.bounds)
       

        self.window?.rootViewController = MainTabBarController()
        if let tabBarController = self.window?.rootViewController as? UITabBarController {
            tabBarController.selectedIndex = 0
        }
        self.window?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        self.window?.makeKeyAndVisible()

        UINavigationBar.appearance().barTintColor = UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
        UINavigationBar.appearance().tintColor = UIColor.black
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black]


        return true
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {


        ConnectToFCM()
        let homeController = HomeController()
        guard let fcmToken = Messaging.messaging().fcmToken else { return }
        let token: [String: AnyObject] = [fcmToken: fcmToken as AnyObject]
        homeController.postToken(Token: token)
        
    }
    
    
        func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Print message ID.
            self.window?.rootViewController = MainTabBarController()
            if let tabBarController = self.window?.rootViewController as? UITabBarController {
                tabBarController.selectedIndex = 3
                
                tabBarController.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)
                
                
            }


    }
    private func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // Let FCM know about the message for analytics etc.
        print("213")
        print("mem", MessagingError.self)
        Messaging.messaging().appDidReceiveMessage(userInfo)
        // handle your message
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
    }
    //for google sing up
    
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any])
        -> Bool {
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: [:])
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    } //for ios8 or older
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("21s3")
//        print("mem", MessagingError(rawValue: 0))
//
        completionHandler([.alert, .badge])
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "com.api4223"), object: nil)



        }
    

    func ConnectToFCM() {
        Messaging.messaging().shouldEstablishDirectChannel = true
    }
//    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
//        // Perform any operations when the user disconnects from app here.
//        // ...
//    }


    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
        Messaging.messaging().shouldEstablishDirectChannel = false
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     
        ConnectToFCM()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func viewWithBackground(_ color: UIColor) -> UIView{
        let v = UIView()
        v.backgroundColor = color
        return v
    }

}

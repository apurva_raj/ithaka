//
//  QuestTagController.swift
//  ithaka
//
//  Created by Apurva Raj on 30/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase

class QuestTagController: UICollectionViewController {
    
    let cellId = "cellId"
    
    var questTag: String?

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView?.backgroundColor = .white

        
        collectionView?.register(ExploreBody.self, forCellWithReuseIdentifier: cellId)

        collectionView?.delegate = self
        fetchPosts()

    }
    
    var posts = [Post]()

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return posts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ExploreBody
        
//        cell.post = posts[indexPath.item]

//        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 220)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("Aaa")
        let post = self.posts[indexPath.item]
        print(post)
        guard let postId = post.id else { return }
        print(postId)
        let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
        postController.userId = post.user.uid
        postController.postId = postId
        navigationController?.pushViewController(postController, animated: true)
    }

    fileprivate func fetchPosts(){
        let ref = Database.database().reference().child("explore").child("header")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            value?.forEach({ (snapshot) in
                let value = snapshot.value as? NSDictionary
                let uid = value?["uid"] as? String ?? ""
                let pid = value?["pid"] as? String ?? ""
                Database.fetchUserWithUID(uid: uid) { (user) in
                    self.fetchPostsWithUser(user: user, pid: pid)
                }
            })
        }
        
    }
    
    fileprivate func fetchPostsWithUser(user: UserModel, pid: String){
        let pid = pid
        let ref = Database.database().reference().child("posts").child(user.uid).child(pid)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            self.collectionView?.refreshControl?.endRefreshing()
            
            guard let dictionary = snapshot.value as? [String: Any] else { return }
            
            var post = Post(user: user, dictionary: dictionary)
            post.id = snapshot.key
            self.posts.append(post)
            
            self.collectionView?.reloadData()
//            self.myActivityIndicator.stopAnimating()
        }
    }
    
    
}

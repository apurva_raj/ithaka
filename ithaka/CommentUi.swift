//
//  CommentUi.swift
//  ithaka
//
//  Created by Apurva Raj on 04/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//


import UIKit
import SwipeCellKit
import KILabel
import FirebaseDatabase
import FirebaseAuth

class CommentCell: SwipeCollectionViewCell, UITextViewDelegate {
    
    
    weak var cDelegate: commentDelegate?
    weak var textViewTap: textViewTap?

    var user: UserModelNew? {
        didSet {
            guard let url = user?.profileImageUrl else { return }
            profileImageView.loadImage(urlString: url)
            guard let username = user?.u_username else { return }

            
            let attributedText = NSMutableAttributedString(string: username + " ", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
            attributedText.append(NSAttributedString(string: (comment?.text)!, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14), NSAttributedStringKey.foregroundColor: UIColor.black]))
            commenttextView.attributedText = attributedText

        }
    }
    
    var comment: CommentNew? {
        didSet {

            guard let comment = comment else { return }
            
            
            guard let commentText = comment.text else { return }
            let count = comment.likeCount
            print("commment like cc is ", count ?? "0")
            
            if comment.likeCount == 1 {
                likeCountLabel.text = "\(count ?? 0) like"
                
            } else {
                likeCountLabel.text = "\(count ?? 0) likes"
            }
            
            guard let commentliked = comment.isLiked else { return }

            let imageName = !commentliked ? "like_unselected" : "like_selected"
            
            likeImageView.setImage(UIImage(named: imageName), for: .normal)
        
            
            let timeAgoDisplay = comment.creationDate?.timeAgoDisplay()
            
            
            timeLabel.text = timeAgoDisplay

//            attributedText.append(NSAttributedString(string: " " + commentText, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
            
            print("Ccc is", commentText)
//            profileImageView.loadImage(urlString: comment.user.profileImageUrl)
            
            commenttextView.hashtagLinkTapHandler = { label, string, range in
                print(label)
                
                let tag = String(string.dropFirst()).lowercased()
                self.textViewTap?.hashTagTapped(hash: tag)
            }
            
            commenttextView.userHandleLinkTapHandler = { label, string, range in
                print(string)
                let mention = String(string.dropFirst()).lowercased()
                print(mention)
                self.textViewTap?.usernameTapped(username: mention)
            }
        }
        

    }
    
    
    var postId: String?
    
    lazy var commenttextView: KILabel = {
        let tv = KILabel()
        tv.font = UIFont(name: "HelveticaNeue", size: CGFloat(14))
        tv.numberOfLines = 0
        tv.lineBreakMode = .byWordWrapping
        tv.sizeToFit()
        return tv
    }()
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        
        return label
    }()

    lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 11)
        label.textColor = UIColor.gray
        return label
    }()

    
    lazy var profileImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFill
        iv.backgroundColor = .blue
        iv.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(profileTapped))
        tap.numberOfTapsRequired = 1
        iv.addGestureRecognizer(tap)

        return iv
    }()
    
    @objc func profileTapped(_ recognizer: UITapGestureRecognizer){
        print("fuck")
        self.cDelegate?.onProfileTapped(for: self)
    }
    
    let headerbar: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
        
    }()

    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        switch URL.scheme {
        case "hash"? :
            let hashtag = URL.absoluteString.dropFirst(5)
            
            print("hashtag is ", hashtag)
            self.textViewTap?.hashTagTapped(hash: String(hashtag))
        case "mention"? :
            
            let username = URL.absoluteString.dropFirst(8).lowercased()
            
            print("username is ", username)
            self.textViewTap?.usernameTapped(username: String(username))
        default:
            print("just a regular url")
        }
        
        return true
    }

    lazy var likeImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(likeButtonTapped))
                tap.numberOfTapsRequired = 1
                button.addGestureRecognizer(tap)
        
        return button
    }()
    
    @objc func likeButtonTapped(sender: UITapGestureRecognizer){
        
        print("usrr id is", user?.uid)
        guard let userId = user?.uid else { return }
        let usersRef = Database.database().reference().child("users").child(userId)
        
        
        guard let commentTitle = comment?.text else { return }
        guard let commentUserId = comment?.uid  else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard let commentId = comment?.id else { return }
    
        
        let ref10 = Database.database().reference().child("notifications").child(commentUserId).childByAutoId()
        print("post id is", postId)


        guard let postliked = comment?.isLiked else { return }
        
        if postliked == true {
            self.comment?.isLiked = false
            self.comment?.likeCount  = (comment?.likeCount)! - 1
            self.cDelegate?.onLikeButtonTapped(for: self)
            Api.Comment.incrementLikes(userId: userId, commentId: commentId, postId: postId ?? "", onSucess: { (comment) in
                print(comment)
            }) { (err) in
                print(err)
            }
            
            usersRef.observeSingleEvent(of: .value) { (snapshot) in
                let value = snapshot.value as? NSDictionary
                var points = value?["profileProgress"] as? Int ?? 0
                //suppose point is 5 . then add =
                points = points - 1
                let tp = ["profileProgress": points]
                usersRef.updateChildValues(tp)
                
            }
            
            
            //remove notifcs
            
            //nothing
            
        } else {
            self.comment?.isLiked = true
            self.comment?.likeCount = (comment?.likeCount)! + 1
            self.cDelegate?.onLikeButtonTapped(for: self)
            Api.Comment.incrementLikes(userId: userId, commentId: commentId, postId: postId ?? "", onSucess: { (comment) in
                print(comment)
            }) { (err) in
                print(err)
            }

            usersRef.observeSingleEvent(of: .value) { (snapshot) in
                let value = snapshot.value as? NSDictionary
                var points = value?["profileProgress"] as? Int ?? 0
                //suppose point is 5 . then add =
                points = points + 1
                let tp = ["profileProgress": points]
                usersRef.updateChildValues(tp)
                
            }
            
//            Api.User.observeUser(withId: postUserId) { (user) in
//                let userbadge = user?.badge ?? 0
//                let newvalue = userbadge + 1
//                let bc = String(newvalue)
//
//                let values = ["badge": newvalue]
//                Api.User.REF_USERS.child(postUserId).updateChildValues(values)
//
//                let username2 = username1 ?? "na"
//                if uid != postUserId {
//                    let notificationMessage = username2 + " sent a rocket to your story: " + postTitle
//                    let notifValues = ["badge": bc, "Message": notificationMessage, "uid":postUserId, "pid":postId, "fromUserId":uid, "type":"11", "creationDate": Date().timeIntervalSince1970] as [String : Any]
//
//                    ref10.updateChildValues(notifValues)
//                }
//
//            }
            
            
        }
    }
    
    
    func updateLike(post: PostNew) {
        
        guard let commentLiked = comment?.isLiked else { return }
        
        let imageName = !commentLiked ? "like_unselected" : "like_selected"
        likeImageView.setImage(UIImage(named: imageName), for: .normal)
        let count = post.rocketCount
        
        if post.rocketCount == 1 {
            likeCountLabel.text = "\(count ?? 0) like"
            
        } else {
            likeCountLabel.text = "\(count ?? 0) like"
        }
        
    }


    lazy var likeCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(11)
        label.textColor = .gray
        label.textAlignment = .left
        label.isUserInteractionEnabled = true
        label.layoutIfNeeded()
        label.layoutSubviews()
        
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        addSubview(profileImageView)
        profileImageView.anchor(top:safeAreaLayoutGuide.topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 7, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        profileImageView.layer.cornerRadius = 30 / 2

        addSubview(nameLabel)
        addSubview(commenttextView)
        addSubview(timeLabel)
       
        addSubview(likeImageView)
        addSubview(likeCountLabel)
        commenttextView.anchor(top: self.topAnchor, left: profileImageView.rightAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: -15, leftConstant: 5, bottomConstant: 0, rightConstant: 50, widthConstant: 0, heightConstant: 0)
//        likeImageView.anchor(top: self.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: frame.height/2 - 7.5, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 15, heightConstant: 15)
        commenttextView.numberOfLines = 0
        commenttextView.lineBreakMode = .byWordWrapping
        commenttextView.sizeToFit()
        commenttextView.sendSubview(toBack: self)
        timeLabel.anchor(top: nil, left: profileImageView.rightAnchor, bottom: self.bottomAnchor, right: nil, topConstant: 1, leftConstant: 4, bottomConstant: 0, rightConstant: 4, widthConstant: 0, heightConstant: 0)
        
//        likeCountLabel.anchor(top: timeLabel.topAnchor, left: timeLabel.rightAnchor, bottom: nil, right: nil, topConstant: 1, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        commenttextView.layer.zPosition = -100
//        likeImageView.layer.zPosition = -100

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
    
}


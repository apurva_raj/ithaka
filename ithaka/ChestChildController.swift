//
//  ChestChildController.swift
//  ithaka
//
//  Created by Apurva Raj on 19/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

protocol removeChild: class {
    func removeChild()
    func readbackstory(itemCode: String?)
}

class ChestChildController: UIViewController {
    
    weak var delegate: removeChild?
    
    
    let mainbg: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.alpha = 0.7
        return view
    }()
    
    
    let overlapbg: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        view.alpha = 1
        return view
    }()
    
    let titled: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Here's your chest"
        label.font = UIFont.boldSystemFont(ofSize: 23)
        label.textAlignment = .center
        
        return label
    }()
    
    let titled2: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Woah! You've got"
        label.font = UIFont.boldSystemFont(ofSize: 23)
        label.textAlignment = .center
        label.isHidden = true
        
        return label
    }()


    
    lazy var chestItemView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "chest")
        let tap = UITapGestureRecognizer(target: self, action: #selector(chestTapped))
        tap.numberOfTapsRequired = 1
        iv.addGestureRecognizer(tap)
        iv.isUserInteractionEnabled = true
        return iv
    }()
    
    lazy var recievedItemView: UIImageView = {
        let iv = UIImageView()
        iv.isHidden = true
        return iv
    }()

    let recievedItemname: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Arcane Boots"
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        label.isHidden = true
        
        return label
    }()
    
    let chestaplabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Tap on your chest to open it."
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.textColor = .gray
        label.numberOfLines = 0
        label.sizeToFit()
        
        return label
    }()
    
    lazy var infoItemsLabel: UILabel = {
        let label = UILabel()
        label.text = "You can check out your items from your profile"
        label.font = UIFont.boldSystemFont(ofSize: 10)
        label.isUserInteractionEnabled = true
        label.textColor = .white
        label.padding = UIEdgeInsetsMake(5, 10, 5, 10)
        label.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.8)
        label.layer.cornerRadius = 7
        label.numberOfLines = 0
        label.textAlignment = .center
        label.sizeToFit()
        label.isHidden = true
        
        return label
    }()
    
    lazy var readbutton: UIButton = {
        let button = UIButton()
        button.setTitle("Read backstory", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        button.isHidden = true
        
        button.addTarget(self, action: #selector(readButtonTapped), for: .touchUpInside)
        

        return button
    }()
    
    var ic: String?
    
    @objc func readButtonTapped(){
        print("yes")
        self.delegate?.readbackstory(itemCode: ic)
    
    }

    
    lazy var okaybutton: UIButton = {
        let button = UIButton()
        button.setTitle("Okay! Cool.", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        button.isHidden = true
        
        button.addTarget(self, action: #selector(okayButtonTapped), for: .touchUpInside)

        return button
    }()

    @objc func okayButtonTapped(){
        print("yes")
        self.delegate?.removeChild()
    }

    
    @objc func chestTapped() {
        print("yahho")
        
        UserDefaults.standard.set(1, forKey: "FirstItemRecieved")

        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Database.database().reference().child("users").child(uid)
        
        let value = ["firstItemClaimed": true]
        ref.updateChildValues(value)
        
        //store item into databse
        
        self.titled.isHidden = true
        self.titled2.isHidden = false
        self.chestaplabel.isHidden = true
        self.chestItemView.isHidden = true

        
        UIView.transition(with: recievedItemView, duration: 2, options: .transitionCrossDissolve, animations: {
            self.recievedItemView.isHidden = false
        })
        
        UIView.transition(with: recievedItemname, duration: 2.5, options: .transitionCrossDissolve, animations: {
            self.recievedItemname.isHidden = false
        })
        
        UIView.transition(with: infoItemsLabel, duration: 2.5, options: .transitionCrossDissolve, animations: {
            self.infoItemsLabel.isHidden = false
        })
        
        UIView.transition(with: okaybutton, duration: 2.5, options: .transitionCrossDissolve, animations: {
            self.okaybutton.isHidden = false
        })
        UIView.transition(with: readbutton, duration: 2.5, options: .transitionCrossDissolve, animations: {
            self.readbutton.isHidden = false
        })

        guard let nm = self.ic else  { return }
        
        Api.Items.saveCatchedItemsToDatabase(uid: uid, itemcode: nm)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .clear
        
        
        let randomnumber = Int.random(in: 1...26)
        
        let makename = "ci" + String(randomnumber)
        
        let makeImage = makename + ".png"
        print("makename is ", makeImage)

        let newitemname = Api.Items.commonItemName[makename]
    
        self.ic = makename
        
        recievedItemView.image = UIImage(named: makeImage)
        recievedItemname.text = newitemname
        
        view.addSubview(mainbg)
        view.addSubview(overlapbg)
        
        overlapbg.addSubview(titled)
        overlapbg.addSubview(titled2)
        overlapbg.addSubview(chestItemView)
        overlapbg.addSubview(chestaplabel)
        overlapbg.addSubview(recievedItemView)
        overlapbg.addSubview(recievedItemname)
        
        overlapbg.addSubview(readbutton)
        overlapbg.addSubview(okaybutton)
        
        overlapbg.addSubview(infoItemsLabel)
        
        let mh = UIDevice().type
        let device: String = mh.rawValue
        

        let tc = view.frame.height - view.frame.width - 50
        mainbg.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        switch device {
        case "iPhone 5", "iPhone 5S":
            overlapbg.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: tc/2, leftConstant: 30, bottomConstant: 0, rightConstant: 30, widthConstant: 0, heightConstant: view.frame.width - 50)
            
        default:
            overlapbg.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: tc/2, leftConstant: 50, bottomConstant: 0, rightConstant: 50, widthConstant: 0, heightConstant: view.frame.width - 50)
            
        }

        titled.anchor(top: overlapbg.topAnchor, left: overlapbg.leftAnchor, bottom: nil, right: overlapbg.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        titled2.anchor(top: overlapbg.topAnchor, left: overlapbg.leftAnchor, bottom: nil, right: overlapbg.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        
        chestItemView.anchor(top: titled.bottomAnchor, left: overlapbg.leftAnchor, bottom: nil, right: overlapbg.rightAnchor, topConstant: -20, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: view.frame.width - 140)
        recievedItemView.anchor(top: titled.bottomAnchor, left: overlapbg.leftAnchor, bottom: nil, right: overlapbg.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: view.frame.width - 170)
        recievedItemname.anchor(top: nil, left: overlapbg.leftAnchor, bottom: infoItemsLabel.topAnchor, right: overlapbg.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 40, rightConstant: 20, widthConstant: 0, heightConstant: 0)

        chestaplabel.anchor(top: chestItemView.bottomAnchor, left: overlapbg.leftAnchor, bottom: nil, right: overlapbg.rightAnchor, topConstant: -20, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        readbutton.anchor(top: nil, left: overlapbg.leftAnchor, bottom: infoItemsLabel.topAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: view.frame.width/2 - 20, heightConstant: 20)
        
        okaybutton.anchor(top: nil, left: nil, bottom: infoItemsLabel.topAnchor, right: overlapbg.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: view.frame.width/2 - 40, heightConstant: 20)

        infoItemsLabel.anchor(top: nil, left: overlapbg.leftAnchor, bottom: overlapbg.bottomAnchor, right: overlapbg.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)

        
        
        self.view.sendSubview(toBack: mainbg)
        
        switch UIDevice.current.userInterfaceIdiom {
        
        case .pad:
            self.titled.font = UIFont.boldSystemFont(ofSize: 25)
            self.infoItemsLabel.font = UIFont.boldSystemFont(ofSize: 14)
        default:
            print("ntohing")
        
    }
}

}

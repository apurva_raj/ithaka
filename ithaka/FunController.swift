//
//  FunController.swift
//  ithaka
//
//  Created by Apurva Raj on 19/04/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase


class FunController: UICollectionViewController, UICollectionViewDelegateFlowLayout, QuestDelegate {
    func exit() {
        return
    }
    
    func onTapQuestTitle(id: String, title: String) {
        return
    }
    
    func stopLoadingAnimation() {
        return
    }
    
    func onTypeTapped(for cell: QuestMain) {
        return
    }
    
    func onTtapped(for cell: QuestCards) {
        return
    }
    
    func onTapQuestTitle(id: String) {
        return
    }
    
    
    var questTitleId = "-LHEtPJRa3J9ezajpwVj"
    
    func fetchQuestInfo() {
        
        let ref = Database.database().reference().child("QuestLevels").child(questTitleId)
        
            ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let values = snapshot.value as? [String: Any] else { return }
                
        }

    }
    
    var questmaintitle: String?
    
    func didTapStage(level: Int) {
        print("level id is", level)
        let cv = QuestLevelController(collectionViewLayout: UICollectionViewFlowLayout())
        cv.questId = questTitleId
        cv.level = level
        cv.questMainTitle = questmaintitle
        navigationController?.pushViewController(cv, animated: true)

        print("activited")
    }
    
    func didTapStage1() {
        return
    }
    
    func didTapTest() {
        print("testing")
//        let screenSize: CGRect = UIScreen.main.bounds
//        cv.view.frame = CGRect(x: 50, y: 150, width: screenSize.width - 100, height: screenSize.height - 300)
//        cv.view.layer.cornerRadius = 10
//        cv.view.clipsToBounds = true
////        self.addChildViewController(cv)
////        self.view.addSubview(cv.view)
////        cv.didMove(toParentViewController: self)
//        self.present(cv, animated: true, completion: nil)
        print("activited")
    }
    
    
    let mainbg = UIView()
 
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.isScrollEnabled = false
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        myActivityIndicator.center = view.center
        
        view.addSubview(myActivityIndicator)
        view.addSubview(mainbg)
        mainbg.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)
        
        mainbg.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()

        collectionView?.register(FunHeader.self, forCellWithReuseIdentifier: "cellId")
        collectionView?.isScrollEnabled = false
        collectionView?.backgroundColor = .white
        
        fetchQuestInfo()
        fetchLevel()
        myActivityIndicator.stopAnimating()
        
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        

        fetchUser()
    }
 
    
    @objc func handleRefresh() {
        
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
        }
    }
    
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    var level: Int?
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! FunHeader
        cell.user = self.user
        cell.questLevel = level
        cell.delegate = self
        
        return cell
    }
    
    func fetchLevel() {
        
        let uid = Auth.auth().currentUser?.uid ?? ""
        let questTitleID = questTitleId 
        let ref = Database.database().reference().child("QuestUserData").child(uid).child(questTitleID)
        
            ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let values = snapshot.value as? [String: Any] else { return }
            let levelnumber = values["PresentLevel"] as? Int ?? 0
            self.level = levelnumber
            
            print("levelNumber is", levelnumber)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true

    }
    
    
    var user: UserModel?
    
    fileprivate func fetchUser() {
        
        let uid = Auth.auth().currentUser?.uid ?? ""
        
        Database.fetchUserWithUID(uid: uid) { (user) in
            self.user = user
            self.collectionView?.reloadData()
            
        }
    }
}

//
//  ExploreBody.swift
//  ithaka
//
//  Created by Apurva Raj on 26/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import GTProgressBar


protocol ExploreBodyDelegate: class {
    func openUserProfile(id: String)
}

class ExploreBody: UICollectionViewCell {
    
    weak var delegate: ExploreBodyDelegate?
    
    var user: UserModelNew? {
        didSet {
            
            setupEditFollowButton()
            guard let username = user?.u_username else { return }
            guard let profileImageUrl = user?.profileImageUrl else { return }
            guard let postCount = user?.postCount else { return }

            
            
                guard let profileItem = user?.profileSetItem  else { return }
                profileItemView.image = UIImage(named: profileItem + ".png")

            
            let postCountString = String(postCount)

            let postCountText = NSMutableAttributedString(string: postCountString, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)])
            postCountText.append(NSAttributedString(string: " stories written", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
            
            totalStoriesCountLabel.attributedText = postCountText

            usernameLabel.text = username
            profileImageView.loadImage(urlString: profileImageUrl)
            

        }
    }
   
    lazy var profileImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.layer.cornerRadius = 60 / 2
        iv.isUserInteractionEnabled =  true
        iv.clipsToBounds = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        iv.addGestureRecognizer(tap)

        return iv
    }()
    
    lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        label.isUserInteractionEnabled = true
        
        return label
    }()
    
    @objc func nameLabelTapped(){
        print("Tapped")
        guard let uid = user?.uid else { return }
        self.delegate?.openUserProfile(id: uid)
    }
    
    
    let bgView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15.0
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 12.0
        view.layer.shadowOpacity = 0.5

        return view
    }()

    
    let bottomDividerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return view
    }()
    
    let totalStoriesCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(11)
        label.textAlignment = .center
        label.textColor = UIColor.black
        return label
    }()
    
    
    lazy var editProfileFollowButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("Edit Profile", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 12)
        button.layer.borderColor = UIColor.gray.cgColor
        button.layer.cornerRadius = 5
        button.clipsToBounds = true
        button.titleEdgeInsets = UIEdgeInsetsMake(3, 5, 3, 5)
        button.backgroundColor = UIColor.lightGray
        button.addTarget(self, action: #selector(handleEditProfileOrFollow), for: .touchUpInside)
        return button
    }()
    
    fileprivate func setupFollowStyle() {
        self.editProfileFollowButton.setTitle("Follow", for: .normal)
        self.editProfileFollowButton.backgroundColor = UIColor.rgb(red: 17, green: 154, blue: 237, alpha: 1)
        self.editProfileFollowButton.setTitleColor(.white, for: .normal)
        self.editProfileFollowButton.layer.borderColor = UIColor(white: 0, alpha: 0.2).cgColor
    }

    fileprivate func setupEditFollowButton() {
        guard let currentLoggedInUserId = Auth.auth().currentUser?.uid else { return }
        
        guard let userId = user?.uid else { return }
        
        
        
        if currentLoggedInUserId == userId {
            //edit profile
        } else {
            
            // check if following
            Database.database().reference().child("following").child(userId).child(currentLoggedInUserId).observeSingleEvent(of: .value, with: { (snapshot) in
                
                if snapshot.exists() {
                    self.editProfileFollowButton.setTitle("Unfollow", for: .normal)
                    
                    
                } else {
                    self.setupFollowStyle()
                    
                }
                
                print("snapshot value", snapshot.exists())
                //                if let isFollowing = snapshot.value as? Int, isFollowing == 1 {
                //
                //                } else {
                //                    self.setupFollowStyle()
                //                }
                
            }, withCancel: { (err) in
                print("Failed to check if following:", err)
            })
        }
    }
    

    @objc func handleEditProfileOrFollow() {
        print("Execute edit profile / follow / unfollow logic...")
        
        guard let currentLoggedInUserId = Auth.auth().currentUser?.uid else { return }
        
        guard let userId = user?.uid else { return }
        let ref3 = Database.database().reference().child("users").child(userId)
        let ref33 = Database.database().reference().child("users").child(currentLoggedInUserId)
        
        
        if editProfileFollowButton.titleLabel?.text == "Unfollow" {
            
            //unfollow
            Api.User.observeCurrentUser(completion: { (currentuser) in
                var fcount = currentuser.following ?? 0
                fcount = fcount - 1
                let addedToUser2 = ["following": fcount]
                ref33.updateChildValues(addedToUser2)
                
                
            })
            
            Api.User.observeUser(withId: userId, completion: { (userMe) in
                var fcount = userMe?.followers ?? 0
                fcount = fcount - 1
                
                var points = userMe?.profileProgress ?? 0
                points = points - 7
                
                let addedToUser2 = ["followers": fcount, "profileProgress": points]
                
                
                ref3.updateChildValues(addedToUser2)
                

            })
            
            
            Database.database().reference().child("following").child(userId).child(currentLoggedInUserId).removeValue(completionBlock: { (err, ref) in
                if let err = err {
                    print("Failed to unfollow user:", err)
                    return
                }
                Database.database().reference().child("followers").child(currentLoggedInUserId).child(userId).removeValue()
                
                print("Successfully unfollowed user:", self.user?.username ?? "")
                
                self.setupFollowStyle()
            })
            
            Api.MyPosts.REF_MYPOSTS.child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
                if let dict = snapshot.value as? [String: Any] {
                    for key in dict.keys {
                        Database.database().reference().child("feed").child(currentLoggedInUserId).child(key).removeValue()
                    }
                }
            })
            
        }
        else if editProfileFollowButton.titleLabel?.text == "Follow" {
            //follow
            let ref = Database.database().reference().child("following").child(currentLoggedInUserId)
            let ref2 = Database.database().reference().child("following").child(userId)
            
            let ref00 = Database.database().reference().child("followers").child(currentLoggedInUserId)
            let ref22 = Database.database().reference().child("followers").child(userId)
            
            let values = [currentLoggedInUserId: 1]
            let values00 = [userId: 1]
            ref2.updateChildValues(values) { (err, ref2) in
                if let err = err {
                    print("Failed to  user:", err)
                    return
                }
                ref00.updateChildValues(values00)
                
                Api.MyPosts.REF_MYPOSTS.child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
                    if let dict = snapshot.value as? [String: Any] {
                        
                        dict.forEach({ (snapshot) in
                            if let value = snapshot.value as? NSDictionary {
                                let creationDate = value["creationDate"] as? TimeInterval ?? 0
                                Database.database().reference().child("feed").child(currentLoggedInUserId).child(snapshot.key).setValue(["uid": userId, "creationDate":creationDate])
                                
                            }
                        })
                        
                    }
                })
                
                
                print("Successfully followed user: ", self.user?.username ?? "")
                
                self.editProfileFollowButton.setTitle("Unfollow", for: .normal)
                self.editProfileFollowButton.backgroundColor = .white
                self.editProfileFollowButton.setTitleColor(.black, for: .normal)
                
                Api.User.observeCurrentUser(completion: { (currentuser) in
                    var fcount = currentuser.following ?? 0
                    fcount = fcount + 1
                    let addedToUser2 = ["following": fcount]
                    ref33.updateChildValues(addedToUser2)
                    
                    
                })
                
                Api.User.observeUser(withId: userId, completion: { (userMe) in
                    var fcount = userMe?.followers ?? 0
                    fcount = fcount + 1
                    let addedToUser2 = ["followers": fcount]
                    ref3.updateChildValues(addedToUser2)
                    
                    
                    let badge = userMe?.badge ?? 0
                    
                    let bv = badge + 1
                    let bc = String(bv)
                    
                    
                    guard let username = Auth.auth().currentUser?.displayName else { return }
                    guard let fromUserId = Auth.auth().currentUser?.uid else { return }
                    
                    let notificationMessage = username + " started following you"
                    let ref4 = Database.database().reference().child("notifications").child(userId).childByAutoId()
                    
                    let cd = Date().timeIntervalSince1970
                    
                    
                    let notifValues = ["Message": notificationMessage, "badge":bc, "uid":userId, "fromUserId":fromUserId, "type": "23", "creationDate": cd] as [String : Any]
                    
                    ref4.updateChildValues(notifValues)
                    
                    var points = userMe?.profileProgress ?? 0
                    //suppose point is 5 . then add =
                    points = points + 7
                    let badgevalue = ["badge":bv, "profileProgress": points]
                    
                    
                    Api.User.REF_USERS.child(userId).updateChildValues(badgevalue)
                    
                    
                    
                })
                
                
                
                
            }
        } else  {
            print("dhoom")
            //            editProfileFollowButton.isHidden = true
            //            EditProfileTap()
        }
    }

    
    let profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "ci16")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 20
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        return imageView
    }()
    



    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        
        contentView.layer.cornerRadius = 6.0
        
        addSubview(bgView)
        bgView.addSubview(profileImageView)
        addSubview(profileItemView)
        
        bgView.addSubview(usernameLabel)
        bgView.addSubview(totalStoriesCountLabel)
        
        bgView.addSubview(editProfileFollowButton)
        bgView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)

        
        let mh = UIDevice().type
        let device: String = mh.rawValue
        
        
        switch device {
        case "iPhone 5S":
            
            profileImageView.anchor(top: bgView.topAnchor, left: bgView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 60)
            profileItemView.anchor(top: nil, left: profileImageView.rightAnchor, bottom: profileImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: -10, bottomConstant: 3, rightConstant: 0, widthConstant: 40, heightConstant: 40)
            
            usernameLabel.anchor(top: profileImageView.topAnchor, left: profileItemView.rightAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width*0.25, heightConstant: 20)
            usernameLabel.font = UIFont.boldSystemFont(ofSize: 14)

            totalStoriesCountLabel.anchor(top: usernameLabel.bottomAnchor, left: usernameLabel.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
            
            editProfileFollowButton.anchor(top: bgView.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 80, heightConstant: 30)

        default:
            print("as it is")
            
            profileImageView.anchor(top: bgView.topAnchor, left: bgView.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 60)
            profileItemView.anchor(top: nil, left: profileImageView.rightAnchor, bottom: profileImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: -10, bottomConstant: 3, rightConstant: 0, widthConstant: 40, heightConstant: 40)
            
            usernameLabel.anchor(top: profileImageView.topAnchor, left: profileItemView.rightAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width*0.45, heightConstant: 20)
            
            totalStoriesCountLabel.anchor(top: usernameLabel.bottomAnchor, left: usernameLabel.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
            
            editProfileFollowButton.anchor(top: bgView.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 80, heightConstant: 30)

        }

        

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

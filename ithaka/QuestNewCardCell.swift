//
//  QuestNewCardCell.swift
//  ithaka
//
//  Created by Apurva Raj on 22/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit


class QuestNewCardCell: UICollectionViewCell {
    
    weak var delegate: AddIthakaItemSlect?
    
    var currentLock: Int? {
        didSet{
            let cc = currentLock ?? 23
            
            if cc == 1 {
                questLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 20, rightConstant: 20, widthConstant: 0, heightConstant: 50)

            } else if cc == 23 {
                questLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 20, leftConstant: 10, bottomConstant: 20, rightConstant: 80, widthConstant: 0, heightConstant: 50)
                imageView.anchor(top: self.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: frame.height/2 - 30, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 60, heightConstant: 60)
                lineView.anchor(top: self.topAnchor, left: nil, bottom: self.bottomAnchor, right: imageView.leftAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 20, rightConstant: 5, widthConstant: 2, heightConstant: 1)

            }

        }
        
    }
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    lazy var lineView: UIView = {
        let iv = UIView()
        iv.backgroundColor = .white
        return iv
    }()

    lazy var questLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(30)
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.tag = 0
        return label
    }()
    
    @objc func questLabelTapped(sender:UITapGestureRecognizer){
        print("Send")
        self.delegate?.dailyQuestCardTapped(tag:sender.view?.tag ?? 0)
        
            }

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(questLabelTapped))
        tap.numberOfTapsRequired = 1
        self.addGestureRecognizer(tap)
        
        

        self.layer.cornerRadius = 15
        self.backgroundColor = UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
        self.clipsToBounds = true
    
        
        questLabel.clipsToBounds = true
        addSubview(imageView)

        addSubview(questLabel)
        
        addSubview(lineView)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

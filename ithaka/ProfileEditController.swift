//
//  ProfileEditController.swift
//  ithaka
//
//  Created by Apurva Raj on 14/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import Photos


protocol ProfileEditNewDelegate: class {
    func tapped()
    
    func onProfileImageTapped()
    
    func dismiss()
    
    func onProfileItemImageTapped()
    
    func onProfileEditSaveTapped(username: String, bio: String)
    
}

class ProfileEditController: UICollectionViewController, UIImagePickerControllerDelegate, UICollectionViewDelegateFlowLayout, ProfileEditNewDelegate {
    
    func onProfileEditSaveTapped(username: String, bio: String) {
        print("username is ", username)
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Database.database().reference().child("users").child(uid)
        
        let value = ["profileSetItem":self.currentImageName,"username": username, "shortbio": bio]
        
        ref.updateChildValues(value as [AnyHashable : Any])
        
        self.navigationController?.popViewController(animated: true)
        
        NotificationCenter.default.post(name: ProfileEditController.updateProfileNotificatioName, object: nil)

    }
    
    func dismiss() {
        self.navigationController?.popViewController(animated: true)

    }
    
    
    
    func onProfileItemImageTapped() {
        let newcontroller = AddIthakaItemSelectionController(collectionViewLayout: UICollectionViewFlowLayout())
        newcontroller.modalPresentationStyle = .fullScreen
        newcontroller.popoverPresentationController?.sourceView = self.view
        newcontroller.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        newcontroller.popoverPresentationController?.permittedArrowDirections = []
        
        newcontroller.transitioningDelegate = self
        newcontroller.modalPresentationStyle = .custom
        newcontroller.modalPresentationCapturesStatusBarAppearance = true
        
        newcontroller.wheredoesitcomefrom = "fromEdit"
        
        
        self.present(newcontroller, animated: true, completion: nil)

    }
    
    func tapped() {
        print("not")
    }
    
    func onProfileImageTapped() {
        
        print("tt")
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                    let imagePicker = UIImagePickerController()
                    imagePicker.delegate = self
                    imagePicker.sourceType = .photoLibrary
                    imagePicker.allowsEditing = true
                    self.present(imagePicker, animated: true, completion: nil)
                }

    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        print("should selcted")
        let imageSelected = info[UIImagePickerControllerOriginalImage] as! UIImage
        
        self.profileImage = imageSelected
//        profileImageView.image = image

        
        navigationController?.tabBarController?.selectedIndex = 4
        dismiss(animated:true, completion: nil)
        
        DispatchQueue.main.async {
            self.collectionView?.reloadSections([0])
            
        }

        let image = imageSelected
        
        guard let uploadData = UIImageJPEGRepresentation(image, 0.5) else { return }
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        let filename = NSUUID().uuidString
        let f = "MOU"
        let ff = filename + f + uid
        
        let mainref = Storage.storage().reference().child("Posts").child(ff)
        
        mainref.putData(uploadData, metadata: nil) { (metadata, error) in
            _ =  mainref.downloadURL(completion: { (url, err) in
                guard let downloadURL = url?.absoluteString else { return }
            self.saveToDatabaseWithImageUrl(imageUrl: downloadURL)

            })
            
        }

        
    }

    
    var user: UserModelNew?
    
    var userId: String?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
       collectionView?.register(profileEditCell.self, forCellWithReuseIdentifier: "profileEditCell")
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateData), name: AddIthakaItemSelectionController.updateItemNotificationNameForEdit, object: nil)

        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self
            , action: #selector(handleSave))
        

        checkPermission()
    }
    
    func checkPermission() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({
                (newStatus) in
                print("status is \(newStatus)")
                if newStatus ==  PHAuthorizationStatus.authorized {
                    /* do stuff here */
                    print("success")
                }
            })
            print("It is not determined until now")
        case .restricted:
            // same same
            print("User do not have access to photo album.")
        case .denied:
            // same same
            print("User has denied the permission.")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false

    }
    
    var itemImageData: UIImage?
    var profileImage: UIImage?
    
    var currentImageName: String?
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "profileEditCell", for: indexPath) as! profileEditCell
        cell.user = user
        
        let currentItem = user?.profileSetItem ?? ""
        

        cell.profileItemView.image = itemImageData ?? UIImage(named: currentItem + ".png")
        cell.delegate = self
        
        guard let ppimage = profileImage else { return cell}
        
        cell.profileImageView.image = ppimage
        

        return cell

    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    @objc func handleUpdateData(_ notification: NSNotification) {
        if let itemName = notification.userInfo?["itemName"] as? String {
            
            print("its called called")
            print("oiiasdkasbfsajbf,jsa ", itemName)
            // do something with your image
            let iinm = itemName + ".png"
            self.itemImageData = UIImage(named: iinm)
            self.currentImageName = itemName
            
            DispatchQueue.main.async {
                self.collectionView?.reloadSections([0])
            }
        }

    }
    
    static let updateProfileNotificatioName = NSNotification.Name(rawValue: "UpdateProfile")
    
    static let defaults = UserDefaults.standard

    @objc fileprivate func handleSave() {
        
        print("Called")
    

        
//        guard let image = profileImageView.image else { return }
//        guard let uploadData = UIImageJPEGRepresentation(image, 0.5) else { return }
//
//        guard let uid = Auth.auth().currentUser?.uid else { return }
//
//        navigationItem.rightBarButtonItem?.isEnabled = false
//
//        let filename = NSUUID().uuidString
//        let f = "MOU"
//        let ff = filename + f + uid
//
//        Storage.storage().reference().child("Posts").child(ff).putData(uploadData, metadata: nil) { (metadata,err) in
//            if let err = err {
//                self.navigationItem.rightBarButtonItem?.isEnabled = true
//                print("failed to upload image:", err)
//                return
//            }
//
//            guard let imageURL = metadata?.downloadURL()?.absoluteString else { return }
//            print("Successfully uploaded post image:", imageURL)
//
//            self.saveToDatabaseWithImageUrl(imageUrl: imageURL)
//
//            self.navigationController?.popViewController(animated: true)
//
//        }
        
       
    }
    
    fileprivate func saveToDatabaseWithImageUrl(imageUrl: String) {
        let ref = Database.database().reference()

        guard let uid = Auth.auth().currentUser?.uid else { return }

        let values = ["profileImageUrl": imageUrl]

        ref.child("users").child(uid).updateChildValues(values)
        
        NotificationCenter.default.post(name: ProfileEditController.updateProfileNotificatioName, object: nil)

    }
    
}




extension ProfileEditController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? PreviewPostController {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal,
                               interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard
                let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController,
                interactionController.inProgress
                else {
                    return nil
            }
            return interactionController
    }
}



extension ProfileEditController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return SystemPushAnimator(type: .navigation)
        case .pop:
            return SystemPopAnimator(type: .navigation)
        default:
            return nil
        }
    }
    
}


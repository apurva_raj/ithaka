//
//  UserModelNew.swift
//  ithaka
//
//  Created by Apurva Raj on 05/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
class UserModelNew {

    var isFollowing: Bool?
    
    var uid: String?
    
    var username: String?
    var u_username: String?

    var profileImageUrl: String?
    var shortBio: String?
    var followers: Int?
    var following: Int?
    var postCount: Int?
    var rocketCount: Int?
    
    var profileProgress: Int?
    var profileLevel: Int?
    var questPoint: Int?
    
    var firstItemClaim: Bool?
    var profileSetItem: String?
    var questCompleted: Int?

    var badge: Int?
    
    var avatar: Int?
    
    var allPointsCollcted: Bool?
    
    var questCompletedModel: Bool?
    
}

extension UserModelNew {
    static func transformUser(dictionary: [String: Any], key: String) -> UserModelNew {
        let user = UserModelNew()
        user.uid = key
        
        user.username = dictionary["username"] as? String ?? ""
        user.u_username = dictionary["u_username"] as? String ?? user.username

        user.profileImageUrl = dictionary["profileImageUrl"]  as? String ?? ""
        user.shortBio = dictionary["shortbio"] as? String ?? ""
        user.followers = dictionary["followers"] as? Int ?? 0
        user.following = dictionary["following"] as? Int ?? 0
        user.postCount = dictionary["postCount"] as? Int ?? 0
        user.rocketCount = dictionary["rocketCount"] as? Int ?? 0
        user.questCompleted = dictionary["questCompleted"] as? Int ?? 0

        user.profileProgress = dictionary["profileProgress"] as? Int ?? 0
        user.profileLevel = dictionary["profileLevel"] as? Int ?? 0
        user.questPoint = dictionary["questPoint"] as? Int ?? 0

        user.avatar = dictionary["Avatar"] as? Int ?? 0
        user.badge = dictionary["badge"] as? Int ?? 0

        
        user.firstItemClaim = dictionary["firstItemClaimed"] as? Bool ?? true
        user.profileSetItem = dictionary["profileSetItem"] as? String ?? ""

        user.allPointsCollcted = dictionary["allPointsCollcted"] as? Bool ?? false
        
        user.questCompletedModel = dictionary["questComepleted"] as? Bool ?? false


        return user
    }
}


//
//  MonsterPlaygroundController.swift
//  ithaka
//
//  Created by Apurva Raj on 11/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//


import UIKit

class MonsterPlaygroundController: UICollectionViewController, UICollectionViewDelegateFlowLayout { 
    
    let monsternamelabel: UILabel = {
        let label = UILabel()
        label.text = "Troll Appeared"
        label.font = UIFont.boldSystemFont(ofSize: 21)
        label.textAlignment = .center
        
        return label
    }()
    
    
    let monsterfightlabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = UIColor.black
        label.font = UIFont.systemFont(ofSize: 19)
        label.textAlignment = .center
        label.backgroundColor = UIColor.white
        label.clipsToBounds = true
        label.padding = UIEdgeInsetsMake(10, 10, 10, 10)
        label.layer.cornerRadius = 15
        label.layer.shadowColor = UIColor.gray.cgColor
        label.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        label.layer.shadowRadius = 12.0
        label.layer.shadowOpacity = 0.7

        return label
    }()
    
    
    let monsterfighttaplabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Tap on any of your weapons to start fighting with monster."
        label.font = UIFont.systemFont(ofSize: 11)
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.textColor = .white
        label.padding = UIEdgeInsetsMake(5, 15, 5, 15)
        label.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.6)
        label.layer.cornerRadius = 7
        label.numberOfLines = 0
        label.textAlignment = .center
        label.sizeToFit()

        return label
    }()
    


    
    lazy var trollimageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Troll-Appeared")
        
        return iv
    }()

    let weaponBox: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        
        return view
    }()
    
    let monsterView: UIImageView = {
        let imageview = UIImageView()
        
//        let gif = UIImage(gifName:"ezgif.com-gif-maker")
//        imageview.setGifImage(gif, loopCount: -1)
        return imageview
    }()
    
    lazy var monsterBG: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "snowy02_preview-1")
        
        return iv
    }()

    
    
    lazy var itemView1: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "sword")
        return iv
    }()
    lazy var itemView2: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "sword_2")
        return iv
    }()
    lazy var itemView3: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "daggers (9)")
        return iv
    }()
    lazy var itemView4: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "shield")
        return iv
    }()

    
    
    func setupitems(){
        

    }


    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        view.addSubview(monsterBG)
        view.addSubview(trollimageView)
        view.addSubview(monsterView)
        view.addSubview(weaponBox)
        
        view.addSubview(monsterfightlabel)
        view.addSubview(monsterfighttaplabel)
        monsterBG.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: -UIApplication.shared.statusBarFrame.height, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 355)


        trollimageView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 50)

        self.view.bringSubview(toFront: trollimageView)
        monsterView.anchor(top: trollimageView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: -30, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 250)
        
        
        
        monsterfightlabel.anchor(top: monsterView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 45, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 170)
        
        weaponBox.anchor(top: nil, left: view.leftAnchor, bottom: monsterfighttaplabel.topAnchor, right: view.rightAnchor, topConstant: 30, leftConstant: 15, bottomConstant: 15, rightConstant: 15, widthConstant: 0, heightConstant: 100)
        
        monsterfighttaplabel.anchor(top: nil, left: view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 25, rightConstant: 0, widthConstant: 0, heightConstant: 15)

        
        view.bringSubview(toFront: monsterfightlabel)

        let timeStackView = UIStackView(arrangedSubviews: [itemView1, itemView2, itemView3, itemView4])
        timeStackView.axis = .horizontal
        timeStackView.distribution = .fillEqually
        timeStackView.spacing = 10
        self.view.addSubview(timeStackView)
        
        
        timeStackView.anchor(top: weaponBox.topAnchor, left: weaponBox.leftAnchor, bottom: nil, right: weaponBox.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 70)
    
        collectionView?.contentInset.top = -UIApplication.shared.statusBarFrame.height

        collectionView?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
//        self.view.sendSubview(toBack: monsterBG)
        
        let a1text = NSMutableAttributedString(string: "Write, write and write!", attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 25)])
        
        a1text.append(NSAttributedString(string: "\n\nThat’s the only way to defeat the monster.", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)]))
        
//        a1text.append(NSAttributedString(string: "\n\nTap on your weapon to start writing.", attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)]))
        
        monsterfightlabel.attributedText = a1text

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.navigationBar.isHidden = false

    }

}

//
//  ItemsStoryController.swift
//  
//
//  Created by Apurva Raj on 20/12/18.
//

import UIKit
import FirebaseDatabase


class ItemsStoryController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    
    var itemCode: String?{
        
        didSet{

            guard let ic = itemCode else { return }
            
            let ref = Database.database().reference().child("itemStory").child(ic)
            
            
            ref.observeSingleEvent(of: .value) { (snapshot) in
                guard let value = snapshot.value as? NSDictionary else { return }
                print("snapshot is", snapshot)
                
                let story = value["story"] as? String
                self.storyText = story
            }
            
            
            if ic.hasPrefix("ci"){
                guard let name = Api.Items.commonItemName[ic] else { return }
                self.itemName = name
                self.itemImage = UIImage(named: ic + ".png")
            }
            if ic.hasPrefix("uci"){
                guard let name = Api.Items.unCommonItemsName[ic] else { return }
                self.itemName = name
                self.itemImage = UIImage(named: ic + ".png")
            }
            if ic.hasPrefix("ri"){
                guard let name = Api.Items.rareItemsName[ic] else { return }
                self.itemName = name
                self.itemImage = UIImage(named: ic + ".png")
            }
            
            
        }

        
    }
    
    
    var itemName:  String?
    var itemImage: UIImage?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        collectionView?.register(ItemsStoryViewCell.self, forCellWithReuseIdentifier: "ItemsStoryViewCell")

        collectionView?.contentInset = UIEdgeInsetsMake(20, 0, 20, 0)
        
        
    }
    
    
    var storyText: String?


    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemsStoryViewCell", for: indexPath) as! ItemsStoryViewCell

        guard let im = self.itemName else { return cell }
        
        cell.mainItemName.text = im
        
        guard let ii = self.itemImage else { return cell }
        
        cell.mainItemView.image = ii
        
        if storyText == nil {
            cell.storyView.font = UIFont.boldSystemFont(ofSize: 21)
            cell.storyView.textAlignment = .center
            cell.storyView.text = "Releasing  Soon..."

        }
        
        guard let ss = self.storyText else { return cell }
        cell.storyView.text = ss
        
        return cell

    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let storyHeight = storyText?.height(withConstrainedWidth: view.frame.width - 50, font: UIFont.systemFont(ofSize: 20)) ?? 40
        
        return CGSize(width: view.frame.width, height: 220 + storyHeight)

    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return 1
    }
    
    
}

//
//  WritersMenuBar.swift
//  ithaka
//
//  Created by Apurva Raj on 28/01/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit

class NewMeWritersMenuBar: UIView, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        cv.dataSource = self
        cv.delegate = self

        return cv
    }()
    
    let cellId = "cellId"
    let imageNames = ["book", "shield"]
    let questTitle = ["Daily Quest", "Monster Quest"]
    var homeController: SuggestedPeople?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        collectionView.register(NewMenuWriterCell.self, forCellWithReuseIdentifier: "NewMenuWriterCell")
        
        addSubview(collectionView)
        addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        addConstraintsWithFormat("V:|[v0]|", views: collectionView)
        
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: UICollectionViewScrollPosition())
        
        setupHorizontalBar()
        
        
    }
    lazy var questLabel1: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(24)
        label.textAlignment = .center
        label.textColor = .gray
        return label
    }()
    
    func setupTitleBar() {
        
    }
    
    var horizontalBarLeftAnchorConstraint: NSLayoutConstraint?
    
    func setupHorizontalBar() {
        let horizontalBarView = UIView()
        horizontalBarView.backgroundColor = UIColor.black
        horizontalBarView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(horizontalBarView)
        horizontalBarLeftAnchorConstraint = horizontalBarView.leftAnchor.constraint(equalTo: self.leftAnchor)
        horizontalBarLeftAnchorConstraint?.isActive = true
        
        horizontalBarView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        horizontalBarView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1/2).isActive = true
        horizontalBarView.heightAnchor.constraint(equalToConstant: 4).isActive = true
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        print(indexPath.item)
        //        let x = CGFloat(indexPath.item) * frame.width / 4
        //        horizontalBarLeftAnchorConstraint?.constant = x
        //
        //        UIView.animateWithDuration(0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseOut, animations: {
        //            self.layoutIfNeeded()
        //            }, completion: nil)
        
        homeController?.scrollToMenuIndex(indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewMenuWriterCell", for: indexPath) as! NewMenuWriterCell
        
        
        if indexPath.item == 0 {
            cell.titleBar.text = "Top Writers"
        } else {
            cell.titleBar.text = "New Ithakians"

        }
//        cell.imageView.image = UIImage(named: imageNames[indexPath.item])?.withRenderingMode(.alwaysOriginal)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / 2, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class NewMenuWriterCell: BaseCell {
    
    let titleBar: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.text = "Default"
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    let imageView: UIImageView = {
        let iv = UIImageView()
        iv.tintColor = UIColor.rgb(red: 91, green: 14, blue: 13, alpha: 1)
        return iv
    }()
//
//    override var isHighlighted: Bool {
//        didSet {
//            titleBar.textColor = isHighlighted ? .black : .gray
//        }
//    }
//
//    override var isSelected: Bool {
//        didSet {
//            titleBar.textColor = isHighlighted ? .black : .gray
//        }
//    }

    override func setupViews() {
        super.setupViews()
        
        
        addSubview(titleBar)
        titleBar.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 28)

    }
    
}

//
//  ItemDiscoveredController.swift
//  ithaka
//
//  Created by Apurva Raj on 09/01/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth

class ItemDiscoveredController: UIViewController {
    
    weak var delegate: removeChild?
    
    
    var typecode: Int?{
        didSet{
            guard let uid = Auth.auth().currentUser?.uid else { return }
            
            guard let tt = typecode else { return }
            switch tt {
            case 0:
                print("its zero")

                let randomnumber = Int.random(in: 1...26)
                let makename = "ci" + String(randomnumber)
                
                let makeImage = makename + ".png"
                print("makename is ", makeImage)
                
                let newitemname = Api.Items.commonItemName[makename]
                
                self.recievedItemView.image = UIImage(named: makeImage)
                
                self.recievedItemname.text = newitemname
                
                self.ic = makename

                Api.Items.saveCatchedItemsToDatabase(uid: uid, itemcode: makename)

            case 1:
                print("its one")
                let randomnumber = Int.random(in: 1...26)
                let makename = "ci" + String(randomnumber)
                
                let makeImage = makename + ".png"
                print("makename is ", makeImage)
                
                let newitemname = Api.Items.commonItemName[makename]
                
                self.recievedItemView.image = UIImage(named: makeImage)
                
                self.recievedItemname.text = newitemname
                
                self.ic = makename

                Api.Items.saveCatchedItemsToDatabase(uid: uid, itemcode: makename)


            case 2:
                let randomnumber = Int.random(in: 1...47)
                let makename = "uci" + String(randomnumber)
                
                let makeImage = makename + ".png"
                print("makename is ", makeImage)
                
                let newitemname = Api.Items.unCommonItemsName[makename]
                
                self.recievedItemView.image = UIImage(named: makeImage)
                
                self.recievedItemname.text = newitemname
                
                self.ic = makename

                Api.Items.saveCatchedItemsToDatabase(uid: uid, itemcode: makename)

            case 24:
                print("@3")
                
                Api.Items.saveCatchedItemsToDatabase(uid: uid, itemcode: "uli1")

                self.recievedItemView.image = UIImage(named: "basic_key.png")

                self.recievedItemname.text = "Quest Unlocker Key"
                self.readbutton.isHidden = true
                self.okaybutton.isHidden = true
                self.infoItemsLabel.text = "Unlock mini quest using this key"
                
                
                let tap = UITapGestureRecognizer(target: self, action: #selector(okayButtonTapped))
                tap.numberOfTapsRequired = 1
                self.view.addGestureRecognizer(tap)

            default:
                print("yaala")

            }
        }
    }
    
    let mainbg: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.alpha = 0.7
        return view
    }()
    
    
    let overlapbg: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        view.alpha = 1
        return view
    }()
    
   
    
    let titled2: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Woah! You've got"
        label.font = UIFont.boldSystemFont(ofSize: 23)
        label.textAlignment = .center
        
        return label
    }()
    
    
    
    
    lazy var recievedItemView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    let recievedItemname: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.textAlignment = .center
        
        return label
    }()
    

    lazy var infoItemsLabel: UILabel = {
        let label = UILabel()
        label.text = "You can check out your items from your profile"
        label.font = UIFont.boldSystemFont(ofSize: 10)
        label.isUserInteractionEnabled = true
        label.textColor = .white
        label.padding = UIEdgeInsetsMake(5, 10, 5, 10)
        label.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.8)
        label.layer.cornerRadius = 7
        label.numberOfLines = 0
        label.textAlignment = .center
        label.sizeToFit()
        
        return label
    }()
    
    lazy var readbutton: UIButton = {
        let button = UIButton()
        button.setTitle("Read backstory", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        button.addTarget(self, action: #selector(readButtonTapped), for: .touchUpInside)

        return button
    }()
    
    lazy var okaybutton: UIButton = {
        let button = UIButton()
        button.setTitle("Okay! Cool.", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 16)
        
        button.addTarget(self, action: #selector(okayButtonTapped), for: .touchUpInside)
        
        return button
    }()
    
    var ic: String?

    @objc func readButtonTapped(){
        print("yes")
        self.delegate?.readbackstory(itemCode: ic)
        
    }

    
    @objc func okayButtonTapped(){
        print("yes")
        self.delegate?.removeChild()
    }
    

    //Arcane Boots
    //A poor family - Husband was a shoemaker. Wife was a witch. And their only son knew how to steal things. Son stole something important of king. The king sent his guard to kill the son. The witch did some magic to the last pair or Boots her husband had made. And gave it to her son. Son wore it and got the ability to hide in plain slight and run really fast - he was soon known as the one who never got caught. Later, he never used its power to steal stuffs but used it to travel to world. Even with black magic. Arcane boots always bring good fortune.
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .clear
        
        
        view.addSubview(mainbg)
        view.addSubview(overlapbg)
        
        overlapbg.addSubview(titled2)
        overlapbg.addSubview(recievedItemView)
        overlapbg.addSubview(recievedItemname)
        
        overlapbg.addSubview(readbutton)
        overlapbg.addSubview(okaybutton)
        
        overlapbg.addSubview(infoItemsLabel)
        
        
        let tc = view.frame.height - view.frame.width - 50
        
        let mh = UIDevice().type
        let device: String = mh.rawValue

        mainbg.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        switch device {
        case "iPhone 5", "iPhone 5S":
            overlapbg.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: tc/2, leftConstant: 30, bottomConstant: 0, rightConstant: 30, widthConstant: 0, heightConstant: view.frame.width - 50)
            
        default:
            overlapbg.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: tc/2, leftConstant: 50, bottomConstant: 0, rightConstant: 50, widthConstant: 0, heightConstant: view.frame.width - 50)
            
        }
        

        titled2.anchor(top: overlapbg.topAnchor, left: overlapbg.leftAnchor, bottom: nil, right: overlapbg.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        recievedItemView.anchor(top: titled2.bottomAnchor, left: overlapbg.leftAnchor, bottom: nil, right: overlapbg.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: view.frame.width - 170)
        recievedItemname.anchor(top: nil, left: overlapbg.leftAnchor, bottom: infoItemsLabel.topAnchor, right: overlapbg.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 40, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        
        readbutton.anchor(top: nil, left: overlapbg.leftAnchor, bottom: infoItemsLabel.topAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: view.frame.width/2 - 20, heightConstant: 20)
        
        okaybutton.anchor(top: nil, left: nil, bottom: infoItemsLabel.topAnchor, right: overlapbg.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: view.frame.width/2 - 40, heightConstant: 20)
        
        infoItemsLabel.anchor(top: nil, left: overlapbg.leftAnchor, bottom: overlapbg.bottomAnchor, right: overlapbg.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)
        
        
        
        self.view.sendSubview(toBack: mainbg)
        
    }
}

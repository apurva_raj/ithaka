//
//  PostPreviewNewHeader.swift
//  ithaka
//
//  Created by Apurva Raj on 03/01/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import KILabel

class PostPreviewNewHeader: UICollectionViewCell {
    
    var user: UserModelNew? {
        didSet{
            guard let profileImageUrl = user?.profileImageUrl else { return }
            
            profileImageView.loadImage(urlString: profileImageUrl)
            
            setupAttributedDate()

        }
    }
    
    
    var post: PostNew? {
        didSet{
            
            guard let post = post else { return }

            guard let tag = post.tag else { return }
            guard let story = post.story else { return }
            guard let setItem = post.postSetItem else { return }
            
            self.profileItemView.image = UIImage(named: setItem + ".png" )
            
            storyLabel.text = story
            

            storyLabel.hashtagLinkTapHandler = { label, string, range in
                let tag = String(string.dropFirst()).lowercased()
                self.textViewTap?.hashTagTapped(hash: tag)
            }
            
            storyLabel.userHandleLinkTapHandler = { label, string, range in
                print(string)
                let mention = String(string.dropFirst()).lowercased()
                print(mention)
                self.textViewTap?.usernameTapped(username: mention)
            }
            
            
            if tag.count == 0 {
                
            } else {
                let attributedTextForQuest = NSMutableAttributedString(string: "Quest: ", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor.black])
                attributedTextForQuest.append(NSAttributedString(string: tag, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                

                questTagLabel.attributedText = attributedTextForQuest
                
            }
            
            let qs = tag.height(withConstrainedWidth: frame.width - 30, font: UIFont.systemFont(ofSize: 13))
            
            print("really is, ", qs)
            questTagLabel.anchor(top: nameLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: -10, leftConstant: 15, bottomConstant: 10, rightConstant: 15, widthConstant: 0, heightConstant: 35)
            
            
            let sh = self.frame.height - (5 + 20 +  30 + 35 + 30 + 50)
            
            storyLabel.anchor(top: questTagLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 25, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: sh )

        }
    }

    
    
    fileprivate func setupAttributedDate() {
        
        guard let username = user?.u_username else { return }
        
        guard let post = self.post else { return }
        
        let attributedText = NSMutableAttributedString(string: username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18)])
        
        attributedText.append(NSAttributedString(string: ", ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 18)]))
        
        let timeAgoDisplay = post.creationDate?.timeAgoDisplay()
        attributedText.append(NSAttributedString(string: timeAgoDisplay!, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor.black]))
        
        nameLabel.attributedText = attributedText
    }

    weak var delegate: PostDelegate?
    weak var textViewTap: textViewTap?

    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 30
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return imageView
    }()
    
    
    lazy var likeImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(rocketButtonTapped))
        //                tap.numberOfTapsRequired = 1
        //                button.addGestureRecognizer(tap)
        
        return button
    }()
    
    lazy var likeCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .left
        label.isUserInteractionEnabled = true
        label.layoutIfNeeded()
        label.layoutSubviews()
        
        return label
    }()

    lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 15)
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        label.textAlignment = .center
        label.textColor = .gray
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        
        return label
    }()
    
    
    @objc func nameLabelTapped(_ recognizer: UITapGestureRecognizer) {
        print("sadsadasdasdasdasdas")
        guard let uid = user?.uid else { return }
        self.delegate?.onNameIdLabelTapped(userId: uid)
        
    }
    
    lazy var questTagLabel: UILabel = {
        let textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 11)
        textView.textAlignment = .center
        textView.textColor = UIColor.black
        textView.backgroundColor = .clear
        //        textView.contentInset = UIEdgeInsetsMake(2, 5, 5, 2)
        textView.isUserInteractionEnabled = true
        textView.numberOfLines = 0
        textView.lineBreakMode = .byWordWrapping
        textView.sizeToFit()
        textView.layoutIfNeeded()
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(tagLabelTapped))
        //        tap.numberOfTapsRequired = 1
        //        textView.addGestureRecognizer(tap)
        
        return textView
    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(18)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        //        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return label
    }()
    
    lazy var optionButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_expand_more"), for: .normal)
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOptionButton))
        //        tap.numberOfTapsRequired = 1
        //        button.addGestureRecognizer(tap)
        
        button.isEnabled = true
        button.isHidden = false
        //        button.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return button
    }()
    
    let profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 20
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        return imageView
    }()
    

    
    lazy var storyLabel: KILabel = {
        let textview = KILabel()
        textview.textColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.85)
        //        textview.contentInset = UIEdgeInsetsMake(-5, 0, 0, 0)
        textview.isUserInteractionEnabled = true
        textview.numberOfLines = 0
        //        textview.isEditable = false
        //        textview.isScrollEnabled = false
        textview.lineBreakMode = .byWordWrapping
        textview.font = UIFont(name: "HelveticaNeue", size: CGFloat(16))
        textview.layoutIfNeeded()
        textview.sizeToFit()
        //        textview.translatesAutoresizingMaskIntoConstraints = true
        
        //        let tap = UITapGestureRecognizer(target: self, action: #selector(storyViewTapped))
        //        tap.numberOfTapsRequired = 1
        //        textview.addGestureRecognizer(tap)
        
        return textview
    }()
    
    let bgview: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        
        return view
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        addSubview(profileImageView)
        addSubview(profileItemView)
        
        addSubview(nameLabel)
        addSubview(questTagLabel)
        addSubview(storyLabel)
        
        addSubview(bgview)
        profileImageView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 15, leftConstant: frame.width/2 - 25, bottomConstant: 10, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        
        profileItemView.anchor(top: nil, left: profileImageView.rightAnchor, bottom: profileImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: -10, bottomConstant: 3, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        
        
        nameLabel.anchor(top: profileImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 25, bottomConstant: 10, rightConstant: 25, widthConstant: 0, heightConstant: 35)
        
        bgview.anchor(top: storyLabel.topAnchor, left: self.leftAnchor, bottom: storyLabel.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        self.sendSubview(toBack: bgview)


        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

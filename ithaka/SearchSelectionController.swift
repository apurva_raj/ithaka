//
//  SearchSelectionController.swift
//  ithaka
//
//  Created by Apurva Raj on 21/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase

class SearchSelectionController: UICollectionViewController, UICollectionViewDelegateFlowLayout, hashsearch {
    
    func hashtap(for cell: HashtagList) {
        print("Works")
        let hh = HashTagPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }

        let hashtag = popularHashtag[indexPath.item]
        hh.hashtag = hashtag.tag
        
        self.navigationController?.pushViewController(hh, animated: true)

    }
    
    
    
    lazy var peopleSearchLabel: UILabel = {
        let label = UILabel()
        label.text = "Search People"
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .gray
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(peopleSearchTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
    }()
    
    lazy var hashtagLabel: UILabel = {
        let label = UILabel()
        label.text = "Search Hashtags"
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .gray
        label.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(hashtagSearchTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        
        return label
        
    }()
    
    @objc func peopleSearchTapped() {
        let peopleSearchController = PeopleSearchController(collectionViewLayout: UICollectionViewFlowLayout())
        self.navigationController?.pushViewController(peopleSearchController,
                                                      animated: true)

    }
    @objc func hashtagSearchTapped() {
        let hashtagController = HashtagSearchController(collectionViewLayout: UICollectionViewFlowLayout())
        self.navigationController?.pushViewController(hashtagController,
                                                      animated: true)

    }
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    let redView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        myActivityIndicator.startAnimating()
        redView.backgroundColor = UIColor.white
        view.addSubview(redView)
        
        redView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        redView.addSubview(peopleSearchLabel)
        redView.addSubview(hashtagLabel)
        
        peopleSearchLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 30, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        hashtagLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: peopleSearchLabel.rightAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 30, widthConstant: 0, heightConstant: 0)
        
        collectionView?.register(ExploreBody.self, forCellWithReuseIdentifier: "ExploreBody")
        collectionView?.register(HashtagList.self, forCellWithReuseIdentifier: "HashtagList")
        

        collectionView?.register(TitleHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "TitleHeader")
        collectionView?.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        
        fetchUsers()
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        

    }
    
    @objc func handleRefresh() {
        print("Handling refresh..")
        
        usersNew.removeAll()
        popularHashtag.removeAll()
        
        fetchUsers()
        fetchPopularHashtags()

        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
        }
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let user = self.usersNew[indexPath.item]
            
            let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
            profileController.userId = user.uid
            
        self.navigationController?.pushViewController(profileController, animated: true)
        } else {
            print("ms")
        }
        
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreBody", for: indexPath) as! ExploreBody
            let user = self.usersNew[indexPath.item]

            cell.user = user
            return cell

        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HashtagList", for: indexPath) as! HashtagList
            let tags =  self.popularHashtag[indexPath.item]
            cell.taging = tags.tag
            cell.delegate = self
            cell.hashtagLabel.backgroundColor =  UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

            
            return cell

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if section == 1 {
            return UIEdgeInsetsMake(0, 20, 0, 50)

        }
    
        return UIEdgeInsetsMake(0, 0, 0, 0)

    }
    
 
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 0
        } else {
            return popularHashtag.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.section == 0 {
            return CGSize(width: view.frame.width, height: 0)

        } else {
            let tags =  self.popularHashtag[indexPath.item]
            let size = CGSize(width: 100, height: 30)
            
            let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)]
            
        
            let estimatedFrame = NSString(string: tags.tag!).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)

            let width = estimatedFrame.width + 30
            print("ss is", width)
            return CGSize(width: width, height: 30)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return .zero
        } else {
            return CGSize(width: view.frame.width, height: 60)

        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if indexPath.section == 0 {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TitleHeader", for: indexPath) as! TitleHeader
            header.titleLabel.text = "Amazing writers to follow"
            return header

        } else {
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TitleHeader", for: indexPath) as! TitleHeader
            header.titleLabel.text = "Popular hashtags"
        
            return header

        }
    }
    
    var user: UserModelNew?
    var users = [UserModel]()
    var usersNew = [UserModelNew]()

    var popularHashtag = [PopularHashtag]()
    
    fileprivate func fetchPopularHashtags() {
        let ref = Database.database().reference().child("PopularHashtags")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            value?.forEach({ (snapshot) in
                let tag = snapshot.key as! String
                let poptag = PopularHashtag.init(key: tag)
                self.popularHashtag.append(poptag)

            })
        }
        self.myActivityIndicator.stopAnimating()

        
    }
    fileprivate func fetchUsers() {
        print("Fetching users..")
        
        let ref = Database.database().reference().child("topWriters")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            value?.forEach({ (snapshot) in
                let uid = snapshot.key as! String
                
                Api.User.observeUser(withId: uid, completion: { (userModelNew) in
                    self.user = userModelNew
                    self.usersNew.append(userModelNew!)
                    self.collectionView?.reloadData()

                })
            })
        }
        
        fetchPopularHashtags()
        
    }
    
}


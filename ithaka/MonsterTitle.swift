//
//  MonsterTitle.swift
//  ithaka
//
//  Created by Apurva Raj on 10/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class MonsterTitle: UICollectionViewCell {
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Monster_quest_yellow")
        return iv
    }()
    
    lazy var questLabel1: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(24)
        label.textAlignment = .center
        return label
    }()
    
    lazy var rightView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "right-arrow_color")
        iv.isHidden = true
        
        return iv
    }()
    
    lazy var leftView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "right-arrow_color").withHorizontallyFlippedOrientation()

        return iv
    }()
    
    lazy var lineView: UIView = {
        let iv = UIView()
        iv.backgroundColor = UIColor.black
        return iv
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(imageView)
        imageView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 10, leftConstant: frame.width*0.05, bottomConstant: 10, rightConstant: frame.width*0.05, widthConstant: 0, heightConstant: 0)


//
//        addConstraintsWithFormat("H:[v0(378)]", views: imageView)
//        addConstraintsWithFormat("V:[v0(130)]", views: imageView)
//
//        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
//        addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


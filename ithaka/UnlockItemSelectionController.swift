//
//  UnlockItemSelectionController.swift
//  ithaka
//
//  Created by Apurva Raj on 11/03/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit

class UnlockItemSelectionController: UICollectionViewController, UICollectionViewDelegateFlowLayout, AddIthakaItemSlect, qwer1 {
    func stopAnimation() {
//        self.myActivityIndicator.stopAnimating()

    }
    
    
    
    func onItemimagetapped(story: String) {
        print("bna")

    }
    
    func onShareTapped(storytext: String, itemCode: String) {
        print("no need")
    }
    
    
    
    static let updateUnlockItemNotificationName = NSNotification.Name(rawValue: "UpdateUnlockItem")

    func item_selected(item: String) {
        let dataDict: [String: String] = ["itemName": item]
       
        self.handleDismiss()
        UserDefaults.standard.set("set", forKey: "MiniQuestThings")
        NotificationCenter.default.post(name: UnlockItemSelectionController.updateUnlockItemNotificationName, object: nil, userInfo: dataDict)
        


    }
    func removeItempage() {
        self.dismiss(animated: true, completion: nil)

    }
    
    func dailyQuestCardTapped(tag: Int) {
        print("Ss")

    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        self.collectionView?.register(AddIthakaItemSelectionCellForUnlocking.self, forCellWithReuseIdentifier: "AddIthakaItemSelectionCellForUnlocking")
        
        self.collectionView?.contentInset = UIEdgeInsetsMake(20, 15, 10, 15)
        setupTopBar()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaItemSelectionCellForUnlocking", for: indexPath) as! AddIthakaItemSelectionCellForUnlocking
        cell.delegate = self
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width-30, height: view.frame.height-45)
        
    }
    
    func setupTopBar() {
        
        let newview = UIView()
        newview.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        
        self.view.addSubview(newview)
        
        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        let stopButton: UIButton = {
            let button = UIButton()
            button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
            button.tintColor = UIColor.blue
            button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
            
            return button
        }()
        
        newview.addSubview(stopButton)
        
        stopButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        
        
        
    }
    
    @objc func handleDismiss(){
        self.dismiss(animated: true, completion: nil)
    }

}

import FirebaseAuth
import FirebaseDatabase

class AddIthakaItemSelectionCellForUnlocking: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    weak var delegate: qwer1?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        cv.contentInset = UIEdgeInsetsMake(50, 0, 50, 0)
        cv.contentMode = .scaleAspectFill
        cv.dataSource = self
        cv.delegate = self
        
        return cv
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .green
        
        self.addSubview(collectionView)
        collectionView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        collectionView.register(AddIthakaItemSelectionSingleCell.self, forCellWithReuseIdentifier: "AddIthakaItemSelectionSingleCell")
        collectionView.register(AddIthakaItemSelectionSingleHeaderCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "AddIthakaItemSelectionSingleHeaderCell")
        
        
        fetchMyItems()
        
    }
    
    var unlockItems = [String]()
    var unlockItemsCount = [Int]()
    
    
    fileprivate func fetchMyItems(){
        
        print("fetching start")
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Database.database().reference().child("myItems").child(uid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as! NSDictionary
            
            value.forEach({ (snapshot) in
                let key = snapshot.key as! String
                
                let count = snapshot.value as? Int ?? 0
                
                if key.hasPrefix("uli"){
                    print(key)
                    self.unlockItems.append(key)
                    self.unlockItemsCount.append(count)
                }
                
            })
            
            self.collectionView.reloadData()
        }
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddIthakaItemSelectionSingleHeaderCell", for: indexPath) as! AddIthakaItemSelectionSingleHeaderCell
            header.title.text = "Unlock Items"
            
            return header
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 30, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: frame.width, height: 35)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return unlockItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            print("0")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaItemSelectionSingleCell", for: indexPath) as! AddIthakaItemSelectionSingleCell
            
            
            cell.itemCode = unlockItems[indexPath.item]
            cell.itemCount = unlockItemsCount[indexPath.item]
        
            return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected")
        
            let itemname = unlockItems[indexPath.item]
            self.delegate?.item_selected(item: itemname)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


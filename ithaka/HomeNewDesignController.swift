//
//  HomeNewDesignController.swift
//  ithaka
//
//  Created by Apurva Raj on 29/11/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import KILabel


class HomeNewDesignController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        navigationItem.title = "Ithaka"
        collectionView?.register(NewHome.self, forCellWithReuseIdentifier: "NewHome")
        self.collectionView?.contentInset = UIEdgeInsetsMake(10, 10, 100, 10)
        self.view.backgroundColor = .green
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 400-70)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewHome", for: indexPath) as! NewHome
        //            cell.delegate = self
        //            cell.backgroundColor = .blue
        return cell

    }
    
}

class NewHome: UICollectionViewCell {
    
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "Prisha")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 30
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    let profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "item_6")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 20
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        return imageView
    }()

    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .black
        label.isUserInteractionEnabled = true
        label.text = "Prisha_Writes"
        return label
    }()
    
    lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 11)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .gray
        label.isUserInteractionEnabled = true
        label.text = "1 hour ago"
        return label
    }()

    let GrayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        return view
    }()

    lazy var storyLabel: KILabel = {
        let textview = KILabel()
        textview.textColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.85)
        textview.isUserInteractionEnabled = true
        textview.numberOfLines = 0
        textview.font = UIFont(name: "HelveticaNeue", size: CGFloat(16))
        textview.text = "Welcome to Ithaka Mini Quest 2 Volcano is about to erupt and it may destroy whole island. Your quest is to protect the island by stopping the volcano 🌋 . To do that, you will need to use some magical items. How do you get it? Well, that’s all this quest is about.\nYou write. Unleash your creativity and paint the power or your words.\nCreate a character. It can be for anything. It can be a hero with super powers or just a simple girl with simple dreams or a puppy that loves to cuddle cats or a psychopath who is on mission to create chaos or a bus which abducts people. Anything.\nExplain your character. Make it as real as it can be. You can just describe a small event to showcase the whole persona of your character or write a conversation or just anything. Just create it :)"
        
        return textview
    }()
    lazy var questTagLabel: UILabel = {
        let textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 11)
        textView.textAlignment = .left
        textView.textColor = UIColor.gray
        textView.isUserInteractionEnabled = true
        textView.text = "Monster Quest - Write on yourself. And there there were none"
        return textView
    }()
    lazy var questTagLabelMoreInfo: UILabel = {
        let textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 11)
        textView.textAlignment = .left
        textView.textColor = UIColor.gray
        textView.isUserInteractionEnabled = true
        textView.text = ""
        return textView
    }()

    
    lazy var likeImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
        
        return button
    }()
    
    lazy var commentImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "blank-squared-bubble"), for: .normal)
        
        return button
    }()
    
    lazy var giftImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "gift (3)"), for: .normal)
        
        return button
    }()
    
    lazy var shareImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "share"), for: .normal)
        
        return button
    }()
    

    lazy var likeCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.text = "42 likes"
        
        return label
    }()
    lazy var commentsCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.text = "23 Comments"
        
        return label
    }()
    lazy var giftsCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.text = "7 Gifts"
        
        return label
    }()

    lazy var optionButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_expand_more"), for: .normal)
        
        return button
    }()

    


    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(profileImageView)
        addSubview(profileItemView)
        addSubview(nameLabel)
        addSubview(timeLabel)
        addSubview(optionButton)
        addSubview(questTagLabel)
        addSubview(storyLabel)
        addSubview(likeImageView)
        addSubview(commentImageView)
        addSubview(shareImageView)
        addSubview(giftImageView)
        addSubview(likeCountLabel)
        addSubview(commentsCountLabel)
        addSubview(giftsCountLabel)

        addSubview(GrayLine)
        profileImageView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 6, leftConstant: 15, bottomConstant: 10, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        
        profileItemView.anchor(top: nil, left: profileImageView.rightAnchor, bottom: profileImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: -10, bottomConstant: 3, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        nameLabel.anchor(top: self.topAnchor, left: profileImageView.rightAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 35, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        timeLabel.anchor(top: nameLabel.bottomAnchor, left: nameLabel.leftAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        optionButton.anchor(top: nameLabel.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 30)
        questTagLabel.anchor(top: timeLabel.bottomAnchor, left: timeLabel.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant:-5, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 30)

        storyLabel.anchor(top: profileImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant:160)

        self.backgroundColor = .white
        
        likeImageView.anchor(top: storyLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        commentImageView.anchor(top: storyLabel.bottomAnchor, left: likeImageView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        shareImageView.anchor(top: storyLabel.bottomAnchor, left: commentImageView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        giftImageView.anchor(top: storyLabel.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)


        likeCountLabel.anchor(top: likeImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        commentsCountLabel.anchor(top: likeCountLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        giftsCountLabel.anchor(top: giftImageView.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)


//        buttonStackView.anchor(top: storyLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        GrayLine.anchor(top: nil, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



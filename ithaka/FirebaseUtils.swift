//
//  FirebaseUtils.swift
//  ithaka
//
//  Created by Apurva Raj on 26/09/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import Foundation
import Firebase

extension Database {
    
    static func fetchUserWithUID(uid: String, completion: @escaping (UserModel) -> ()) {
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            guard let userDictionary = snapshot.value as? [String: Any] else { return }
           
            let user = UserModel(uid: uid, dictionary: userDictionary)
            completion(user)
   
            
        }) { (err) in
            print("Failed to fetch user for posts:", err)
        }
    }
//    static func fetchPostWithID(pid: String, completion: @escaping (Post) -> ()) {
//        Database.database().reference().child("posts").child(pid).observeSingleEvent(of: .value, with: { (snapshot) in
//
//            guard let postDictionary = snapshot.value as? [String: Any] else { return }
//
//            let post = Post(user: , dictionary: postDictionary)
//            //            let user = User(uid: uid, dictionary: userDictionary)
//            completion(post)
//
//        }) { (err) in
//            print("Failed to fetch user for posts:", err)
//        }
//    }
    
}


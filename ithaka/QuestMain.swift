//
//  QuestCards.swift
//  ithaka
//
//  Created by Apurva Raj on 09/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase


class QuestMain: UICollectionViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, QuestMainDelegate {
    
    func didTapQuestTitle(id: String, title: String) {
        delegate?.onTapQuestTitle(id: id, title: title)
    }
    
    
    
    func didTapQuestTitle(id: String) {
        print("Second stage")
//        delegate?.onTapQuestTitle(id: id)
    }
    
    
    
    func didTapQuestType(for cell: QuestCards) {
        delegate?.onTtapped(for: cell)
        
        

    }
    
    weak var delegate: QuestDelegate?
    

    let grayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        
        return view
    }()
    
        let scrollCollectionView: UICollectionView = {
            
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
//        layout.scrollDirection = .horizontal
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.decelerationRate = UIScrollViewDecelerationRateFast
//        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
            
        view.contentInset = UIEdgeInsetsMake(0, 20, 0, 20)
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
       scrollCollectionView.backgroundColor = .white
        addSubview(scrollCollectionView)
        
        scrollCollectionView.delegate = self
        scrollCollectionView.dataSource = self
        
        scrollCollectionView.register(QuestCards.self, forCellWithReuseIdentifier: "cards")
        scrollCollectionView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: self.frame.width, heightConstant: self.frame.height)
        
//        scrollCollectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10)
    
    }
    
    var questTitle = [QuestTitleModel]()
   
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return questTitle.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (scrollCollectionView.frame.width-60)/2
        let height = width - 50
        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 50
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cards", for: indexPath) as! QuestCards
        let questTitles = questTitle[indexPath.item]
        cell.quest = questTitles
        cell.delegate = self
        return cell
    }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("name anyone")

        let questTitles = questTitle[indexPath.item]
        
//        delegate?.onTypeTapped(for: QuestMain)
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

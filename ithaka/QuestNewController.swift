//
//  QuestNewController.swift
//  ithaka
//
//  Created by Apurva Raj on 20/11/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class QuestNewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, monstertap, dailyquestTap {
    
    func startWritingTapped(){
        let addIthakaController = AddIthakaAPI.addIthakaController
        addIthakaController.tofix = "a"
        self.present(addIthakaController, animated: true, completion: nil)
//        self.navigationController?.pushViewController(addIthakaController, animated: true)
    }
    

    func rightbuttonTapped() {
        print("Wwww")
        
        let indexPath = IndexPath(item: 1, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition(), animated: true)

    }
    
    func titleTapped(title: String) {
        
        print("yes, its working")
        
        let addIthakaController = AddIthakaAPI.addIthakaController
        addIthakaController.questTag = "Daily Quest: " + title
        
        
        UIView.animate(withDuration: 0.70, animations: { () -> Void in
            UIView.setAnimationCurve(UIViewAnimationCurve.easeInOut)
            self.navigationController!.pushViewController(addIthakaController, animated: false)
            UIView.setAnimationTransition(UIViewAnimationTransition.curlUp, for: self.navigationController!.view!, cache: false)
        })
    }
    
   
    
    func playtapped() {
       print("yes")
        
 
//        UIView.animate(withDuration: 1.5, animations: { () -> Void in
//            UIView.setAnimationCurve(UIViewAnimationCurve.linear)
//            self.navigationController?.pushViewController(monsterplaygroundcontroller, animated: true)
//            UIView.setAnimationTransition(UIViewAnimationTransition.curlDown, for: self.navigationController!.view!, cache: false)
//        })
    }
    

    
    let titles = ["Daily Quest", "Monster Quest", "ada", "me"]
    
    lazy var StoryTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 20)
        tv.backgroundColor = .white
        tv.placeholder = "Start writing..."
        tv.isScrollEnabled = true
        tv.tintColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.layer.cornerRadius = 15
        tv.clipsToBounds = true
        tv.sizeToFit()
        tv.layoutIfNeeded()
        tv.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)
        tv.backgroundColor = .white
        tv.textContainerInset = UIEdgeInsetsMake(5, 5, 5, 5)
        return tv
    }()
    


    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        setupCollectionView()
        setupMenuBar()
        
       
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "DalekPinpointBold", size: 21)!]

        self.navigationItem.title = "Quest"
        


    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.navigationBar.isHidden = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func setupCollectionView() {
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumLineSpacing = 0
        }
        
        collectionView?.delegate = self
        self.collectionView?.showsHorizontalScrollIndicator = false
        collectionView?.backgroundColor = UIColor.white
        collectionView?.register(DailyNewQuestCell.self, forCellWithReuseIdentifier: "cellId")
        collectionView?.register(MonsterQuestCell.self, forCellWithReuseIdentifier: "MonsterQuestCell")
//        collectionView?.contentInset = UIEdgeInsetsMake(0, 0, 30, 0)
//        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.contentInset.top = -(UIApplication.shared.statusBarFrame.height + view.frame.height/2)

        collectionView?.isPagingEnabled = true
        collectionView?.isUserInteractionEnabled = true
    }
    
    
    func setupNavBarButtons() {
        let searchImage = #imageLiteral(resourceName: "gift (2)")
        let searchBarButtonItem = UIBarButtonItem(image: searchImage, style: .plain, target: self, action: #selector(handleSearch))
        
        let moreButton = UIBarButtonItem(image: #imageLiteral(resourceName: "boatMan"), style: .plain, target: self, action: #selector(handleSearch))
        
        navigationItem.rightBarButtonItems = [moreButton, searchBarButtonItem]
    }
    
    @objc func handleSearch() {
        scrollToMenuIndex(2)
    }
    func scrollToMenuIndex(_ menuIndex: Int) {
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: UICollectionViewScrollPosition(), animated: true)
        
        setTitleForIndex(menuIndex)
    }

    
    lazy var menuBar: NewMenuBar = {
        let mb = NewMenuBar()
        mb.homeController = self
        return mb
    }()
    

    fileprivate func setupMenuBar() {
        
        view.addSubview(menuBar)
        view.addConstraintsWithFormat("H:|[v0]|", views: menuBar)
        view.addConstraintsWithFormat("V:[v0(0)]", views: menuBar)
        
        menuBar.topAnchor.constraint(equalTo: view.topAnchor).isActive = true

    }
    
    fileprivate func setTitleForIndex(_ index: Int) {
        if let titleLabel = navigationItem.titleView as? UILabel {
            titleLabel.text = "  \(titles[index])"
        }
        
    }
    

    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! DailyNewQuestCell
            cell.delegate = self
        
            return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }


}

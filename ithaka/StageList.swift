//  StageList.swift
//  ithaka
//
//  Created by Apurva Raj on 29/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//


import UIKit
import Firebase


class stageListTitles: BaseCollectionViewCell {
    
    
    weak var delegate: StageDelegate?
    
    var quest: QuestStageObjectives? {
        didSet{
            guard let title = quest?.title else { return }
            
            q1.text = title
            
            if quest?.isWritten == true {
                q1.backgroundColor = UIColor.rgb(red: 72, green: 207, blue: 250, alpha: 1)
                
                
            }
        }
    }
    
    var questData: QuestUserData? {
        didSet{
            
        }
    }
    
    
    lazy var q1: UILabel = {
        let tv = UILabel()
        tv.text = "2018-09-24 11:38:20.773079+0530 ithaka[50123:1708423] TIC Read Status [8:0x0]: 1:57"
        tv.font = UIFont.systemFont(ofSize: 19)
        tv.padding = UIEdgeInsetsMake(15, 15, 15, 15)
        tv.numberOfLines = 0
        tv.lineBreakMode = .byWordWrapping
        tv.layer.cornerRadius = 15
        tv.clipsToBounds = true
        tv.sizeToFit()
        tv.backgroundColor = .white
        tv.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(q1Tapped))
        tap.numberOfTapsRequired = 1
        tv.addGestureRecognizer(tap)

        return tv
    }()

//    lazy var q1: UITextView = {
//        let textView = UITextView()
//        textView.isUserInteractionEnabled = true
//        textView.sizeToFit()
//        textView.isScrollEnabled = false
//        textView.font = UIFont.systemFont(ofSize: 19)
//        textView.contentInset = UIEdgeInsetsMake(15, 15, 15, 15)
//        textView.isEditable = false
//        textView.layoutIfNeeded()
//        textView.text = "Fuirst quera"
//        textView.layer.cornerRadius = 15
//        textView.clipsToBounds = true
//        textView.adjustsFontForContentSizeCategory = true
//        textView.minimumZoomScale = 10/16
////        textView.contentInset = UIEdgeInsetsMake(3, 5, 3, 5)
//        textView.textAlignment = .left
//        let tap = UITapGestureRecognizer(target: self, action: #selector(q1Tapped))
//        tap.numberOfTapsRequired = 1
//        textView.addGestureRecognizer(tap)
//        textView.textColor = .black
//
//        return textView
//    }()
//
//
    @objc func q1Tapped()  {
        print("fuck me")
        guard let title = quest?.title else { return }
        guard let id = quest?.id else { return }
        self.delegate?.onQ1Tapped(title: title, id: id)
    }
    
    override func setupViews() {
        super.setupViews()

        addSubview(q1)
        
        let mh = UIDevice().type
        let device: String = mh.rawValue
        
        
        switch device {
        case "iPhone 5S":
            q1.font = UIFont.systemFont(ofSize: 17)
            q1.padding = UIEdgeInsetsMake(10, 10, 10, 10)

        default:
            print("as it is")
        }

        q1.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
}

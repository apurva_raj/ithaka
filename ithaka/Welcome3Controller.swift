//
//  Welcome3Controller.swift
//  ithaka
//
//  Created by Apurva Raj on 17/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

import Firebase
import GoogleSignIn

class Welcome3Controller: UIViewController, GIDSignInUIDelegate, GIDSignInDelegate, UITextFieldDelegate, UITextViewDelegate {

    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view is GIDSignInButton {
            return false
        }
        return true
    }

    
    func textFieldDidEndEditing(_ textField: UITextField) {
        MainLabel.resignFirstResponder()
    }
    
    var tag: Int?{
        didSet{
            
        }
    }
    
    var emoji: String?{
        didSet{
            let text1 = "Here’s a chest for you. Sign up to open it and check out what you have got. Hope to see your " + emoji! + " turn into 😍."
            firstLabel.text = text1

        }
    }
    
    var firstLabelText: String? {
        didSet{

        }
    }

    var AvatarC: Int?
    
    let mainView = UIView()
    
    lazy var firstImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "chest")
        return iv
    }()
        
    lazy var emojiLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 40)
        label.text = "👏"
        return label
    }()

    lazy var firstLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "Here’s a chest for you. Sign up to open it and see what you have got. Hope to see 😊 turn into 😍"
        label.font = UIFont(name: "STHeitiTC-Medium", size: CGFloat(16))
        label.numberOfLines = 0
        label.sizeToFit()
        label.padding = UIEdgeInsetsMake(10, 10, 10, 10)
        label.layer.cornerRadius = 7
        label.clipsToBounds = true
        
        return label
    }()
    
    lazy var MainLabel: TextField = {
        let textView = TextField()
        textView.font = UIFont.boldSystemFont(ofSize: 14)
        textView.isUserInteractionEnabled = true
        textView.returnKeyType = UIReturnKeyType.continue
        textView.backgroundColor = .white
        textView.layer.cornerRadius = 5
        textView.placeholder = "Choose your username"
        textView.textAlignment = .center
        textView.clipsToBounds = true
        textView.layer.borderWidth = 0.5
        textView.layer.borderColor = UIColor.gray.cgColor
        return textView
    }()
    
    lazy var btnSignIn : GIDSignInButton = {
        let btnSignIn = GIDSignInButton(frame: CGRect(origin: .zero, size: CGSize(width: 0, height: 48)))
        btnSignIn.style = GIDSignInButtonStyle.standard
        btnSignIn.colorScheme = GIDSignInButtonColorScheme.dark
        btnSignIn.isEnabled = false
        
        
        return btnSignIn
    }()
    
    @objc func loginC() {
        print("Tapped")
        
    }


    lazy var taglineLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(11)
        label.text = "By signing up you indicate that you have read and agree to the Terms of Service and Privacy Policy. Tap here to read it."
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = .gray
        label.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(termsLabelTapped))
        tap.numberOfTapsRequired = 1
        tap.cancelsTouchesInView = false

        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    lazy var submitButton: UILabel = {
        let textView = UILabel()
        textView.font = UIFont.boldSystemFont(ofSize: 10)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.layoutIfNeeded()
        textView.textAlignment = .left

        return textView
    }()
    
    lazy var titleaLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.textColor = .white
        label.text = "Verify"
        
        
        return label
    }()
    
    @objc func termsLabelTapped(_ recognizer: UITapGestureRecognizer){
        
    
        UIApplication.shared.open(URL(string : "https://ithaka.app/termsandprivacy.html")!, options: [:], completionHandler: { (status) in
            
        })
    }
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myActivityIndicator.center = view.center
        myActivityIndicator.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.7)
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.color = .white
        view.addSubview(myActivityIndicator)

        view.backgroundColor = .white
        navigationItem.title = "Let's start"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "STHeitiSC-Medium", size: 20) ?? UIFont.systemFont(ofSize: 20)]

        view.addSubview(mainView)
        view.addSubview(firstImageView)
        
        view.addSubview(MainLabel)
        view.addSubview(submitButton)
        
        let height25 = view.frame.height*0.5 - 170
        
        view.addSubview(firstLabel)
        view.addSubview(emojiLabel)
        
        let mh = UIDevice().type
        let device: String = mh.rawValue
        
        let ph = view.frame.width*0.05

        
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: mainView.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        view.bringSubview(toFront: myActivityIndicator)
      
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("i am ipad")
            
            firstLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 30, leftConstant: ph, bottomConstant: 0, rightConstant: ph, widthConstant: 0, heightConstant: 130)
            
            emojiLabel.anchor(top: firstLabel.topAnchor, left: nil, bottom: nil, right: firstLabel.rightAnchor, topConstant: -20, leftConstant: 0, bottomConstant: 0, rightConstant: -20, widthConstant: 0, heightConstant: 0)
            
            MainLabel.delegate = self
            
            view.addSubview(taglineLabel)
            view.addSubview(btnSignIn)
            
        
                
                
                firstImageView.anchor(top: firstLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: -20, leftConstant: view.frame.width/2 - 90, bottomConstant: 0, rightConstant: 0, widthConstant: 220, heightConstant: 220)
                
                MainLabel.anchor(top: firstImageView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: -20, leftConstant: ph, bottomConstant: 0, rightConstant: ph, widthConstant: 0, heightConstant: 30)
                submitButton.anchor(top: MainLabel.bottomAnchor, left: MainLabel.leftAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                
                btnSignIn.anchor(top: MainLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: ph, bottomConstant: 0, rightConstant: ph, widthConstant: 0, heightConstant: 0)
                taglineLabel.anchor(top: btnSignIn.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: -15, leftConstant: 5, bottomConstant: 40, rightConstant: 5, widthConstant: 0, heightConstant: 60)
                MainLabel.becomeFirstResponder()
            
            
            

        default:
            print("who are you?")
            
            firstLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 130)
            
            emojiLabel.anchor(top: firstLabel.topAnchor, left: nil, bottom: nil, right: firstLabel.rightAnchor, topConstant: -20, leftConstant: 0, bottomConstant: 0, rightConstant: -20, widthConstant: 0, heightConstant: 0)
            
            MainLabel.delegate = self
            
            view.addSubview(taglineLabel)
            view.addSubview(btnSignIn)
            
            
            
            switch device {
                
            case "iPhone 5S":
                print("ipad s selcted ", device)
                taglineLabel.anchor(top: nil, left: view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 5, rightConstant: 5, widthConstant: 0, heightConstant: 60)
                
                btnSignIn.anchor(top: nil, left: view.leftAnchor, bottom: taglineLabel.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                
                MainLabel.anchor(top: nil, left: view.leftAnchor, bottom: btnSignIn.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 35, bottomConstant: 30, rightConstant: 35, widthConstant: 0, heightConstant: 30)
                
                firstImageView.anchor(top: nil, left: view.leftAnchor, bottom: MainLabel.bottomAnchor, right: nil, topConstant: 0, leftConstant: view.frame.width/2 - 90, bottomConstant: 10, rightConstant: 0, widthConstant: 180, heightConstant: 180)
                
                
            default:
                
                firstImageView.anchor(top: firstLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: -20, leftConstant: view.frame.width/2 - 90, bottomConstant: 0, rightConstant: 0, widthConstant: 180, heightConstant: 180)
                
                MainLabel.anchor(top: firstImageView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: -20, leftConstant: 35, bottomConstant: 0, rightConstant: 35, widthConstant: 0, heightConstant: 30)
                submitButton.anchor(top: MainLabel.bottomAnchor, left: MainLabel.leftAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                
                btnSignIn.anchor(top: MainLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
                taglineLabel.anchor(top: btnSignIn.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: -15, leftConstant: 5, bottomConstant: 40, rightConstant: 5, widthConstant: 0, heightConstant: 60)
                MainLabel.becomeFirstResponder()
            }
            
            

        }
        
        myActivityIndicator.stopAnimating()
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        
        NotificationCenter.default.addObserver(self, selector: .keyboardWillShow, name: NSNotification.Name.UIKeyboardWillShow, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        

    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    @objc fileprivate func keyboardWillShow(notification:NSNotification) {
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            print("chilax")
            MainLabel.becomeFirstResponder()

        default:
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
                if self.view.frame.origin.y == 0 {
                    self.view.frame.origin.y -= keyboardSize.height
                }
            }
        }
        
        

        
        if UserDefaults.standard.object(forKey: "keyboardHeight") != nil {
            if let keyboardRectValue = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                let keyboardHeight = keyboardRectValue.height
                
                UserDefaults.standard.set(keyboardHeight, forKey: "keyboardHeight")
                print("freaking height is", keyboardHeight)
            }

        }

    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.btnSignIn.isEnabled = false

        if string.count > 0 {
            let allowedCharacters = CharacterSet.alphanumerics
            
            let unwantedStr = string.trimmingCharacters(in: allowedCharacters)
            return unwantedStr.count == 0
        }
        
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.myActivityIndicator.startAnimating()
        self.titleLabelTapped()
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
//        let tv = self.MainLabel.text?.lowercased()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "STHeitiSC-Medium", size: 20) ?? UIFont.systemFont(ofSize: 20)]

        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "DalekPinpointBold", size: 21)!]
        

    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
    
    
    func checkUserNameAlreadyExist(newUserName: String, completion: @escaping(Bool) -> Void) {
        
        let ref = Database.database().reference()
        ref.child("users").queryOrdered(byChild: "u_username").queryEqual(toValue: newUserName)
            .observeSingleEvent(of: .value, with: {(snapshot) in
                
                if snapshot.exists() {
                    completion(true)
                }
                else {
                    completion(false)
                }
            })
    }
    
    @objc func titleLabelTapped() {
        
        guard let tv = self.MainLabel.text?.lowercased() else { return }
        
        
        switch tv.count {
        case 0 ... 2:
            print("SHOULD BE EITHER 3 OR MORE CHARS. Print warning")
            self.MainLabel.layer.borderColor = UIColor.red.cgColor
            self.btnSignIn.isEnabled = false
            self.submitButton.text = "Minimum 3 characters required."
            self.myActivityIndicator.stopAnimating()
        case 11 ... 1200:
            print("can;t be")
            self.MainLabel.layer.borderColor = UIColor.red.cgColor
            self.btnSignIn.isEnabled = false
            self.submitButton.text = "Maximum 12 characters allowed."
            self.myActivityIndicator.stopAnimating()

        default:
            print("didn;t think about that")
            self.checkUserNameAlreadyExist(newUserName: tv) { isExist in
                if isExist {
                    print("Username exist. Print warning")
                    self.MainLabel.layer.borderColor = UIColor.red.cgColor
                    self.btnSignIn.isEnabled = false
                    self.submitButton.text = "Username already exist."
                    self.myActivityIndicator.stopAnimating()
                    
                }
                else {
                    print("Username is available")
                    self.MainLabel.layer.borderColor = UIColor.green.cgColor
                    self.btnSignIn.isEnabled = true
                    self.submitButton.text = "Username available."
                    self.myActivityIndicator.stopAnimating()
                    
                    
                }
            }
        }
        
   
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        
        print("tapped")
        
        self.myActivityIndicator.startAnimating()
        MainLabel.resignFirstResponder()
        if let error = error {
            print("failed to login", error)
            return
        }
        
        print("sucess", user)
        
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        
        
        Auth.auth().signIn(with: credential) { (user, error) in
            if let error = error {
                print("failed !", error)
                return
            }
            
            guard let useremail = user?.email else { return }
            
            let ref = Database.database().reference().child("users")
            let query =  ref.queryOrdered(byChild: "email").queryEqual(toValue: useremail)
            query.observeSingleEvent(of: .value, with: { (snapshot) in
                if (snapshot.value is NSNull) {
                    
                    // Get a reference to the storage service using the default Firebase App
                    let storage = Storage.storage()
                    
                    // Create a storage reference
                    let storageRef = storage.reference()
                    
                    let filename = NSUUID().uuidString
                    //points to the child directory where the profile picture will be saved on firebase
                    let profilePicRef = storageRef.child("Profile_images").child(filename)
                    GIDSignIn.sharedInstance().shouldFetchBasicProfile = true
                    
                    if (GIDSignIn.sharedInstance().currentUser != nil) {
                        guard let uid = user?.uid else { return }

                        let imageUrl = GIDSignIn.sharedInstance().currentUser.profile.imageURL(withDimension: 400).absoluteString
                        let url  = NSURL(string: imageUrl)! as URL
                        guard let data = NSData(contentsOf: url) else { return }
                        
                        //upload image to storage
                        profilePicRef.putData(data as Data, metadata: nil) { (metadata, error) in
                            guard let metadata = metadata else {
                                print("failed to upload profile_image")
                                return
                            }
                            // Metadata contains file metadata such as size, content-type, and download URL.
                            profilePicRef.downloadURL(completion: { (url, err) in
                                guard let downloadURL = url?.absoluteString else  { return }
                                
                                
                                print("sucess uploaded profile image", downloadURL)
                                guard let email = user?.email else { return }
                                guard let username = user?.displayName else { return }
                                let shortbio = "Keep ithaka always in your mind"
                                
                                let profileLevel = 0
                                let profileProgress = 1
                                let AvatarCode = self.AvatarC ?? 0
                                
                                let emotions = self.emoji ?? "none"
                                
                                let u_username = self.MainLabel.text?.lowercased() ?? ""
                                let dictionaryValues = ["Avatar": AvatarCode,  "u_username": u_username,  "username": username,"email":email,"shortbio":shortbio,"profileImageUrl": downloadURL, "profileLevel":profileLevel, "profileProgress":profileProgress, "selectedEmoji": emotions, "firstItemClaimed": false] as [String : Any]
                                let values = [uid: dictionaryValues]
                                
                                var ref: DatabaseReference!
                                ref = Database.database().reference()
                                ref.child("users").updateChildValues(values, withCompletionBlock: { (err, ref) in
                                    if let err = err {
                                        print("failed to save to database", err)
                                        return
                                    } else {
                                        self.myActivityIndicator.stopAnimating()
                                        let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                                        appDelegate.window?.rootViewController = MainTabBarController()
                                        
                                        print("successfully saved the data")
                                        
                                    }
                                })


                            })
                        }
                        
                        let adminid = "b5MKdFi5mkcJgQC12hEOjJOT3MB3"
                        
                        Api.MyPosts.REF_MYPOSTS.child(adminid).observeSingleEvent(of: .value, with: { (snapshot) in
                            if let dict = snapshot.value as? [String: Any] {
                                
                                dict.forEach({ (snapshot) in
                                    if let value = snapshot.value as? NSDictionary {
                                        let creationDate = value["creationDate"] as? TimeInterval ?? 0
                                        Database.database().reference().child("feed").child(uid).child(snapshot.key).setValue(["uid": adminid, "creationDate":creationDate])
                                        
                                    }
                                })
                                
                            }
                        })

                        
                    }
                } else {
                    print("not updating data again and again")
                    self.resignFirstResponder()
                    let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = MainTabBarController()
                    
                    if let tabBarController = appDelegate.window?.rootViewController as? UITabBarController {
                        tabBarController.selectedIndex = 0
                    }

                }
                
            })
            
            self.myActivityIndicator.stopAnimating()

        }
        
    }
}

extension Selector {
    static let keyboardWillShow = #selector(Welcome3Controller.keyboardWillShow(notification:))
}



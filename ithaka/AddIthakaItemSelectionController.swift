//
//  AddIthakaItemSelectionController.swift
//  ithaka
//
//  Created by Apurva Raj on 20/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class AddIthakaItemSelectionController: UICollectionViewController, UICollectionViewDelegateFlowLayout, AddIthakaItemSlect, qwer1 {
    
    func stopAnimation() {
        self.myActivityIndicator.stopAnimating()
    }
    
    func stopanimation() {
        self.myActivityIndicator.stopAnimating()
    }
    
   
    
    func onItemimagetapped(story: String) {
        print("bna")

    }
    
    func onShareTapped(storytext: String, itemCode: String) {
        print("whyh")
    }
    
    
    static let updateItemNotificationName = NSNotification.Name(rawValue: "UpdateItem")
    
    static let updateItemNotificationNameForEdit = NSNotification.Name(rawValue: "UpdateItemForEdit")

    var wheredoesitcomefrom: String?
    
    func item_selected(item: String) {
        print("selected item", item)
        
        let story = self.preWrittenText ?? ""
        
        print("pre text here is", story)
        let dataDict: [String: Any] = ["itemName": item, "story": story]

        if wheredoesitcomefrom == "fromEdit" {
            print("yes")
            NotificationCenter.default.post(name: AddIthakaItemSelectionController.updateItemNotificationNameForEdit, object: nil, userInfo: dataDict)

        } else {
            NotificationCenter.default.post(name: AddIthakaItemSelectionController.updateItemNotificationName, object: nil, userInfo: dataDict)

        }
        
        
    
        self.handleDismiss()
    }
    
    var preWrittenText: String?
    
    func dailyQuestCardTapped(tag: Int) {
        print("Ss")
    }
    
    func dailyQuestCardTapped() {
        print("none")
    }
    
    func removeItempage() {
        self.dismiss(animated: true, completion: nil)
    }
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.startAnimating()

        self.collectionView?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

    self.collectionView?.register(AddIthakaItemSelectionCell.self, forCellWithReuseIdentifier: "AddIthakaItemSelectionCell")
        
        self.collectionView?.contentInset = UIEdgeInsetsMake(20, 15, 10, 15)
        setupTopBar()
    }
    

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
//    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "headerId", for: indexPath) as! PostPreviewNewHeader
//
//            return cell
//    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaItemSelectionCell", for: indexPath) as! AddIthakaItemSelectionCell
        cell.delegate = self
            return cell


    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//            return .zero
//
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width-30, height: view.frame.height-45)
        
    }
    
    func setupTopBar() {
        
        let newview = UIView()
        newview.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        
        self.view.addSubview(newview)
        
        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        let stopButton: UIButton = {
            let button = UIButton()
            button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
            button.tintColor = UIColor.blue
            button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
            
            return button
        }()
        
        newview.addSubview(stopButton)
        
        stopButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
    
        
        
    }
    
    @objc func handleDismiss(){
        self.dismiss(animated: true, completion: nil)
    }
    
}


protocol qwer1: class {
    func item_selected(item: String)
    func stopAnimation()
}
class AddIthakaItemSelectionCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    weak var delegate: qwer1?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        cv.contentInset = UIEdgeInsetsMake(50, 0, 50, 0)
        cv.contentMode = .scaleAspectFill
        cv.dataSource = self
        cv.delegate = self
        
        return cv
    }()
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .green

            self.addSubview(collectionView)
            collectionView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        collectionView.register(AddIthakaItemSelectionSingleCell.self, forCellWithReuseIdentifier: "AddIthakaItemSelectionSingleCell")
        collectionView.register(AddIthakaItemSelectionSingleHeaderCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "AddIthakaItemSelectionSingleHeaderCell")
        
        
        fetchMyItems()

    }
    
    
    var commonItems = [String]()
    var commonItemsCount = [Int]()
    var commonItemCharLimit = [Int]()
    
    var rareItems = [String]()
    var rareItemsCount = [Int]()
    var rareItemsCharLimit = [Int]()

    var unCommonItems = [String]()
    var unCommonItemsCount = [Int]()
    var unCommonCharLimit = [Int]()

    
    fileprivate func fetchMyItems(){
        
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Database.database().reference().child("myItems").child(uid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as! NSDictionary
            
            value.forEach({ (snapshot) in
                guard let key = snapshot.key as? String else  { return }
                
                guard let count = snapshot.value as? Int else { return }
                
                if key.hasPrefix("ci"){
                    print(key)
                    self.commonItems.append(key)
                    self.commonItemsCount.append(count)
                }
                
                if key.hasPrefix("uci"){
                    print(key)
                    self.unCommonItems.append(key)
                    self.unCommonItemsCount.append(count)

                }

                if key.hasPrefix("ri"){
                    print(key)
                    self.rareItems.append(key)
                    self.rareItemsCount.append(count)
                }

            })
            
            self.collectionView.reloadData()
            self.delegate?.stopAnimation()
        }

    }
    

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 3
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch indexPath.section {
        case 0:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddIthakaItemSelectionSingleHeaderCell", for: indexPath) as! AddIthakaItemSelectionSingleHeaderCell
            header.title.text = "Common Items"
            
            return header
        case 1:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddIthakaItemSelectionSingleHeaderCell", for: indexPath) as! AddIthakaItemSelectionSingleHeaderCell
            header.title.text = "Uncommon Items"

            return header
        case 2:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddIthakaItemSelectionSingleHeaderCell", for: indexPath) as! AddIthakaItemSelectionSingleHeaderCell
            header.title.text = "Rare Items"

            return header


        default:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "AddIthakaItemSelectionSingleHeaderCell", for: indexPath) as! AddIthakaItemSelectionSingleHeaderCell
            
            return header

        }
        

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 0, 30, 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: frame.width, height: 35)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("count is", commonItems.count)
        switch section {
        case 0:
            return commonItems.count
        case 1:
            return unCommonItems.count
        case 2:
            return rareItems.count
            
        default:
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            print("0")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaItemSelectionSingleCell", for: indexPath) as! AddIthakaItemSelectionSingleCell
        

            cell.itemCode = commonItems[indexPath.item]
            cell.itemCount = commonItemsCount[indexPath.item]
            return cell

        case 1:
            print("0")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaItemSelectionSingleCell", for: indexPath) as! AddIthakaItemSelectionSingleCell
            cell.itemCode = unCommonItems[indexPath.item]
            cell.itemCount = unCommonItemsCount[indexPath.item]

            return cell

        case 2:
            print("0")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaItemSelectionSingleCell", for: indexPath) as! AddIthakaItemSelectionSingleCell
            cell.itemCode = rareItems[indexPath.item]
            cell.itemCount = rareItemsCount[indexPath.item]

            print("uuuurri is", indexPath.item)

            return cell


        default:
            print("Default")
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaItemSelectionSingleCell", for: indexPath) as! AddIthakaItemSelectionSingleCell
            
            return cell

        }
        
        

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("selected")
        
        switch indexPath.section {
        case 0:
            let itemname = commonItems[indexPath.item]
            self.delegate?.item_selected(item: itemname)
            print("item name is ", itemname)
        case 1:
            let itemname = unCommonItems[indexPath.item]

            self.delegate?.item_selected(item: itemname)

            print("item name is ", itemname)
        case 2:
            let itemname = rareItems[indexPath.item]

            self.delegate?.item_selected(item: itemname)

            print("item name is ", itemname)

        default:
            print("nothing")
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


class AddIthakaItemSelectionSingleCell: UICollectionViewCell{
    
    var itemCode: String? {
        didSet{
            print("iii is", itemCode)
            let ic = itemCode ?? "ci01"
            let itemname = ic + ".png"
            
            let imagetoset = UIImage(named: itemname)
            
            self.postItemView.image = imagetoset
        }
    }
    
    var itemCount: Int? {
        didSet{
            let ic = itemCount ?? 0
            let icc = String(ic)
            
            countLabel.text = icc
        }
    }
    
    lazy var countLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 11)
        label.text = "123"
        label.textAlignment = .center
        return label
    }()
    
    lazy var postItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 50/2
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(handleShare))
//        tap.numberOfTapsRequired = 1
//        imageView.addGestureRecognizer(tap)
        
        return imageView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(postItemView)
        addSubview(countLabel)
        postItemView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        countLabel.anchor(top: postItemView.bottomAnchor, left: self.leftAnchor, bottom:nil , right: self.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class AddIthakaItemSelectionSingleHeaderCell: UICollectionViewCell{
    
    lazy var title: UILabel = {
       let label = UILabel()
        label.text = "Rare Items"
        label.font = UIFont.boldSystemFont(ofSize: 21)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        addSubview(title)
        
        title.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

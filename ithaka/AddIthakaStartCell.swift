//
//  AddIthakaStartCell.swift
//  ithaka
//
//  Created by Apurva Raj on 29/09/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//


import UIKit
import GTProgressBar
import FirebaseAuth
import FirebaseDatabase

protocol AddIthakaItemSlect: class {
    func onItemimagetapped(story: String)
    func removeItempage()
    func dailyQuestCardTapped(tag: Int)
    func onShareTapped(storytext: String, itemCode: String)
}

class AddIthakaStartCell: UICollectionViewCell, UITextViewDelegate {
    
    
    weak var delegate: AddIthakaItemSlect?
    
    var Questtag: String?{
        didSet{

        }
    }
    
    var postItem: UIImage? {
        didSet{
        }
    }
    
    var requireChars: Int?{
        didSet{
            let rc = requireChars ?? 40
            
            minimumWordsRequired.text = "\(rc)"
        }
    }

    var imageisEmpty: Bool?{
        didSet{
            print("please choose an item to write", imageisEmpty)
            if imageisEmpty == false{
                self.StoryTextView.becomeFirstResponder()
            }
        }
    }
    
    
    
    lazy var StoryTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 21)
        tv.backgroundColor = .white
        tv.placeholder = "Tell stories, write experiences, create poems...."
        tv.isScrollEnabled = true
        tv.tintColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.layer.cornerRadius = 15
        tv.clipsToBounds = true
        tv.sizeToFit()
        tv.layoutIfNeeded()
        tv.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)
        tv.backgroundColor = .white
        tv.textContainerInset = UIEdgeInsetsMake(10, 5, 5, 5)
        
        return tv
    }()
    
    let wordsProgressBar: GTProgressBar = {
        let bar = GTProgressBar()
        bar.progress = 0
        bar.barBorderColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        bar.barFillColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        bar.barBackgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 0.2)
        bar.barBorderWidth = 1
        bar.barFillInset = 2
        bar.labelTextColor = UIColor.darkGray
        bar.progressLabelInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        bar.font = UIFont.boldSystemFont(ofSize: 16)
        bar.labelPosition = GTProgressBarLabelPosition.right
        bar.barMaxHeight = 12
        bar.displayLabel = false
        bar.layer.cornerRadius = 15
        bar.clipsToBounds = true
        
        
        return bar
    }()


    
    lazy var shareButton: UIButton = {
        let button = UIButton()
        button.setTitle("Share", for: .normal)
        button.addTarget(self, action: #selector(handleShare), for: UIControlEvents.touchUpInside)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(UIColor.gray, for: .normal)
        button.isEnabled = true
        
        return button
    }()
    
    var currentItemCode: String?
    
    @objc func handleShare(){
        print("sharing it")
        //Things that will go through funx
        
        // StoryText
        // ItemCode
        
        guard let storyText = StoryTextView.text else { return }
        self.shareButton.isEnabled = false
        let ItemCode = currentItemCode ?? ""
        self.delegate?.onShareTapped(storytext: storyText, itemCode: ItemCode)
    }
    
    lazy var lineView: UIView = {
        let iv = UIView()
        iv.backgroundColor = UIColor.white
        iv.layer.cornerRadius = 15
        iv.clipsToBounds = true
        return iv
    }()
    
    lazy var minimumWordsRequiredLabel: UILabel = {
        let label = UILabel()
        label.text = "Minimum Characters Required"
        label.font = UIFont.systemFont(ofSize: 9)
        label.textColor = UIColor.gray
        
        return label
        
    }()
    lazy var minimumWordsRequired: UILabel = {
        let label = UILabel()
        label.text = "40"
        label.font = UIFont.boldSystemFont(ofSize: 12)
        
        return label
        
    }()

    lazy var youWillEarnLabel: UILabel = {
        let label = UILabel()
        label.text = "You will earn "
        label.font = UIFont.systemFont(ofSize: 9)
        label.textColor = UIColor.gray
        

        return label
        
    }()
    lazy var IWillEarnLabel: UILabel = {
        let label = UILabel()
        label.text = "10 Points & An item "
        label.font = UIFont.boldSystemFont(ofSize: 12)
        return label
        
    }()
    
    

    
    func textViewDidChange(_ textView: UITextView) {
        
        
        if self.imageisEmpty == true {
            self.delegate?.onItemimagetapped(story: StoryTextView.text)
        }
        
        let r = requireChars ?? 40
        let c = StoryTextView.text.count
        let pn = CGFloat(r)
        
        let cc = CGFloat(c)
        
        let mc = cc/pn
        
        if r > c {
            let rc = r - c
            self.minimumWordsRequired.text = "\(rc)"

        } else if r == c {
            self.minimumWordsRequired.text = "0"
        }
        
        wordsProgressBar.progress = mc

        if c > (r - 1) {
            print("bingo")
            self.shareButton.setTitleColor(UIColor.blue, for: .normal)
            self.shareButton.isEnabled = true
        } else {
            self.shareButton.setTitleColor(UIColor.gray, for: .normal)
            self.shareButton.isEnabled = false

        }
    }

    lazy var profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "basic_lock")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 50/2
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(itemTapped))
        tap.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tap)

        return imageView
    }()
    
    var newText: String?

    @objc func itemTapped(){
        
        let storytext = self.StoryTextView.text ?? ""
        print("Ssss is ", storytext)

        self.delegate?.onItemimagetapped(story: storytext)
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        StoryTextView.delegate = self
        
        addSubview(wordsProgressBar)

        addSubview(StoryTextView)
        addSubview(shareButton)
        addSubview(profileItemView)
        addSubview(minimumWordsRequired)
        addSubview(youWillEarnLabel)
        addSubview(IWillEarnLabel)
        addSubview(minimumWordsRequiredLabel)
        
        StoryTextView.becomeFirstResponder()
        
        
        addSubview(lineView)
        
        
        wordsProgressBar.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 10)
        
        StoryTextView.anchor(top: wordsProgressBar.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: self.frame.height - 100)
        
        
        profileItemView.anchor(top: StoryTextView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)

        shareButton.anchor(top: StoryTextView.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        minimumWordsRequiredLabel.anchor(top: StoryTextView.bottomAnchor, left: profileItemView.rightAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)

        minimumWordsRequired.anchor(top: minimumWordsRequiredLabel.bottomAnchor, left: profileItemView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 15)

        youWillEarnLabel.anchor(top: minimumWordsRequired.bottomAnchor, left: profileItemView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        IWillEarnLabel.anchor(top: youWillEarnLabel.bottomAnchor, left: profileItemView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)


        lineView.anchor(top: nil, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 85, bottomConstant: 0, rightConstant: 85, widthConstant: 0, heightConstant: 2.5)


    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}



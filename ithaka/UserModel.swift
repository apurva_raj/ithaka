//
//  User.swift
//  ithaka
//
//  Created by Apurva Raj on 25/09/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import Foundation

struct UserModel {
    
    var uid: String
    
    var username: String
    var profileImageUrl: String
    var shortBio: String
    var followers: Int
    var following: Int
    var postCount: Int
    var rocketCount: Int
    
    var profileProgress: Int
    var profileLevel: Int
    var questPoint: Int
    
    var u_username: String
    
    init(uid: String, dictionary: [String: Any]) {
        self.uid = uid
        self.username = dictionary["username"] as? String ?? ""
        self.u_username = dictionary["u_username"] as? String ?? ""

        self.profileImageUrl = dictionary["profileImageUrl"]  as? String ?? ""
        self.shortBio = dictionary["shortbio"] as? String ?? ""
        self.followers = dictionary["followers"] as? Int ?? 0
        self.following = dictionary["following"] as? Int ?? 0
        self.postCount = dictionary["postCount"] as? Int ?? 0
        self.rocketCount = dictionary["rocketCount"] as? Int ?? 0
        
        self.profileProgress = dictionary["profileProgress"] as? Int ?? 0
        self.profileLevel = dictionary["profileLevel"] as? Int ?? 0
        self.questPoint = dictionary["questPoint"] as? Int ?? 0
        
    }

}



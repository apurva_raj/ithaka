//
//  API.swift
//  ithaka
//
//  Created by Apurva Raj on 04/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
struct Api {
    static var User = UserApi()
    static var NewPost = PostApi()
    static var Comment = CommentAPI()
//    static var Post_Comment = Post_CommentApi()
    static var MyPosts = MyPostAPI()
    static var FollowApi = FollowAPI()
    static var Feed = FeedAPI()
//    static var HashTag = HashTagApi()
    static var Notification = NotificationAPI()
    static var Profile = ProfileAPI()
    
    static var Quest = QuestAPI()
    static var AddIthaka = AddIthakaAPI()
    
    static var Utility = UtilityAPI()
    
    static var Hashtag = HashtagAPI()
    
    static var DiscoverItem = DiscoverItemAPI()
    
    static var PointsAPII = PointsAPI()
    
    static var Items = ItemsAPI()

}


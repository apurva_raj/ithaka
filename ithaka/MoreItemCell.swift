//
//  MoreItemCell.swift
//  ithaka
//
//  Created by Apurva Raj on 20/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit


class MoreItemCell: UICollectionViewCell {
    

    lazy var itemView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "red_crystals")
        iv.contentMode = .scaleAspectFill

        return iv
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(itemView)
        itemView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 60)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//.swift
//  ithaka
//
//  Created by Apurva Raj on 21/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import AudioToolbox


class CollectPointsViewCellController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    
    let threeTitles = ["Daily Points", "Recieved Likes Points", "Recieved Comments Points" ]
    let threeTitlesCount = ["10", "23", "42" ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = .white
        
        collectionView?.register(CollectPointsSimpleCell.self, forCellWithReuseIdentifier: "CollectPointsSimpleCell")
        self.collectionView?.contentInset = UIEdgeInsetsMake(120, 0, 0, 0)
        self.view.layer.cornerRadius = 10
        self.view.clipsToBounds = true

        setupTopBar()
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        
        fetchPoints()
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.05
            
            self.collectionView?.contentInset = UIEdgeInsetsMake(20, ph, 0, ph)
            
            
        default:
            print("i am Everyone except ipad")
            
            
        }
        
    }
    
    
    var pointsModel = [Points]()
    
    fileprivate func fetchPoints() {
        print("fetching it")
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        Api.PointsAPII.fetchUserPoints(uid: uid) { (pointsnew) in
         
            self.pointsModel.append(pointsnew)
            print("psss is ", pointsnew)
            
            DispatchQueue.main.async {
                self.collectionView?.reloadData()
            }

        }
        
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                self.handleDismiss()
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("ssssss is ", pointsModel.count)

        return pointsModel.count
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectPointsSimpleCell", for: indexPath) as! CollectPointsSimpleCell
        cell.points = pointsModel[indexPath.item]
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.1
            
            return CGSize(width: view.frame.width - ph, height: view.frame.height)

            
        default:
            print("i am Everyone except ipad")
            return CGSize(width: view.frame.width, height: view.frame.height)

            
        }
    }
    
    let wonItemButton: UIImageView = {
        let v = UIImageView()
        v.image = #imageLiteral(resourceName: "Red Bag")

        v.backgroundColor = UIColor.clear
        
        return v
    }()

    func setupTopBar() {
        
        let newview = UIView()
        newview.backgroundColor = .white
        
        
        

        
        
        self.view.addSubview(newview)

        
        newview.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 110)

        let stopButton: UIButton = {
            let button = UIButton()
            button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
//            button.tintColor = UIColor.white
            button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
//            button.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.8)
//            button.layer.cornerRadius = 15
//            button.clipsToBounds = true
            
            return button
        }()
        
        
        newview.addSubview(wonItemButton)
        newview.addSubview(stopButton)
        
        
        wonItemButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: view.frame.width/2 - 50, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 100)
        
        stopButton.anchor(top: newview.topAnchor, left: nil, bottom: nil, right: newview.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 30, heightConstant: 30)
        


    }
    
    @objc func handleDismiss(){
        dismiss(animated: true, completion: nil)
    }
    

    
}



class CollectPointsSimpleCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.item {
            

        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectPointsSingleCell", for: indexPath) as! CollectPointsSingleCell
            cell.number = self.points?.storyPoints ?? 0
            cell.pointsLabel.text = "Story Points"
            cell.pointsCollectbutton.tag = 0
            return cell

        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectPointsSingleCell", for: indexPath) as! CollectPointsSingleCell
            cell.number = self.points?.likePoints ?? 0
            cell.pointsLabel.text = "Recieved Like Points"
            cell.pointsCollectbutton.tag = 1

            return cell

        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectPointsSingleCell", for: indexPath) as! CollectPointsSingleCell
            cell.number = self.points?.commentPoints ?? 0
            cell.pointsLabel.text = "Recieved Comment Points"
            cell.pointsCollectbutton.tag = 2

            return cell

        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectPointsSingleCell", for: indexPath) as! CollectPointsSingleCell
            
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: 90)
    }
    

    
    var points: Points?

    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
//        cv.layer.cornerRadius = 15
//        cv.clipsToBounds = true
        cv.backgroundColor = UIColor.white
        cv.dataSource = self
        cv.delegate = self
        cv.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        return cv
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(collectionView)
        
        collectionView.register(CollectPointsSingleCell.self, forCellWithReuseIdentifier: "CollectPointsSingleCell")

        collectionView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: self.frame.width, heightConstant: self.frame.height)

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    

}


class CollectPointsSingleCell: UICollectionViewCell {
    
    var number: Int?{
        didSet{
            guard let n = number else { return }
            self.pointsCountLabel.text = String(n)
            
            if n == 0 {
                self.pointsCollectbutton.isEnabled = false
                self.pointsCollectbutton.setTitle("Collected", for: .disabled)
                self.pointsCollectbutton.setTitleColor(.gray, for: .disabled)

            }
        }
    }

    lazy var totalsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .gray
        label.textAlignment = .center
        
        return label
    }()
    
    lazy var pointsLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = .gray
        label.textAlignment = .center
        
        return label
    }()
    
    lazy var pointsCountLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 21)
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    
    lazy var pointsCollectbutton: UIButton = {
        let button = UIButton()
        button.setTitle("Collect", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 19)
        button.setTitle("Collected", for: .selected)
        button.setTitleColor(UIColor.gray, for: .selected)
        button.addTarget(self, action: #selector(buttonClicked), for: .touchUpInside)
        return button
    }()
    
    @objc func buttonClicked(sender:UIButton){
        print("tapped")
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))

        let uid = Auth.auth().currentUser?.uid
        
        Api.PointsAPII.collectPoints(uid: uid ?? "00", catagory: sender.tag)
        
        self.pointsCollectbutton.isEnabled = false
        self.pointsCountLabel.text = "0"
        self.pointsCollectbutton.setTitle("Collected", for: .disabled)
        self.pointsCollectbutton.setTitleColor(.gray, for: .disabled)
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PointsCollected"), object: nil)

        
    }
    
    
    let mainbg: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        
        return view
    }()

   
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.white
        addSubview(mainbg)
        addSubview(pointsLabel)
        addSubview(pointsCountLabel)
        addSubview(pointsCollectbutton)
        
        mainbg.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        pointsLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 20)
        
        pointsCountLabel.anchor(top: pointsLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 35, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        pointsCollectbutton.anchor(top: self.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 35, widthConstant: 0, heightConstant: 0)
        
        
        self.sendSubview(toBack: mainbg)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

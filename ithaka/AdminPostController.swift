//
//  AdminPostController.swift
//  ithaka
//
//  Created by Apurva Raj on 29/03/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth

class AdminPostController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()
        
        collectionView?.register(PostUi.self, forCellWithReuseIdentifier: "PostUi")
        
        navigationItem.title = "Daily Quest Stories"
        
        collectionView?.backgroundColor = UIColor.white
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        self.navigationController?.delegate = self
        
        setupTopBar()
        fetchPosts()
        
        collectionView?.contentInset = UIEdgeInsetsMake(55, 0, 70, 0)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    
    let newview = UIView()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        return button
    }()
    
    func setupTopBar() {
        
        newview.backgroundColor = .white
        
        let newview1 = UIView()
        newview1.backgroundColor = .white
        self.view.addSubview(newview1)
        newview1.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        view.addSubview(newview)
        
        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        
        newview.addSubview(backButton)
        backButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        view.bringSubview(toFront: backButton)
        
        
    }
    
    @objc func handleBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    
    @objc func handleRefresh() {
        postNew.removeAll()
        userModelNew.removeAll()
        fetchPosts()
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("yaho")
        
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        
        let user = userModelNew[indexPath.item]
        let post = postNew[indexPath.item]
        previewpostController.post = post
        previewpostController.user = user
        previewpostController.visitorUid = uid
        previewpostController.modalPresentationStyle = .fullScreen
        previewpostController.popoverPresentationController?.sourceView = self.view
        previewpostController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        previewpostController.popoverPresentationController?.permittedArrowDirections = []
        
        previewpostController.transitioningDelegate = self
        previewpostController.modalPresentationStyle = .custom
        previewpostController.modalPresentationCapturesStatusBarAppearance = true
        self.present(previewpostController, animated: true, completion: nil)
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postNew.count
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostUi", for: indexPath) as! PostUi
        let post = postNew[indexPath.item]
        cell.post = post
        
        let user = userModelNew[indexPath.item]
        cell.user = user
        
        //        cell.textViewTap = self
        
        print("Vasavasvsavsdavdsvsdvsvsadv", post)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width - 30, height: 330)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 15, 0, 15)
        
    }
    
    fileprivate var isLoadingPost = false
    
    var postNew = [PostNew]()
    var userModelNew = [UserModelNew]()
    
    fileprivate func fetchPosts()  {
        
        isLoadingPost = true
        
        Api.Feed.getRecentExploreFeedForAdmin(start: postNew.first?.timestamp, limit: 3) { (results) in
            
            guard let results = results else { self.myActivityIndicator.stopAnimating()
                return }
            
            if results.count > 0 {
                
                results.forEach({ (result) in
                    self.postNew.append(result.0)
                    self.userModelNew.append(result.1)
                    
                })
                
            }
            self.isLoadingPost = false
            self.myActivityIndicator.stopAnimating()
            self.collectionView?.reloadSections([0])
        }
        
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
            
            guard !isLoadingPost else {
                return
            }
            isLoadingPost = true
            guard let lastPostTimestamp = self.postNew.last?.timestamp else {
                isLoadingPost = false
                return
            }
            self.myActivityIndicator.startAnimating()
            
            Api.Feed.getOldExploreFeedForAdmin(start: (postNew.last?.timestamp)!, limit: 3, completionHandler: { (results) in
                guard let results = results else { self.myActivityIndicator.stopAnimating()
                    return }
                
                if results.count == 0 {
                    self.myActivityIndicator.stopAnimating()
                    return
                }
                for result in results {
                    self.postNew.append(result.0)
                    self.userModelNew.append(result.1)
                }
                self.myActivityIndicator.stopAnimating()
                self.collectionView?.reloadSections([0])
                self.isLoadingPost = false
                
            })
            
        }
    }
    
    
}


extension AdminPostController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? PreviewPostController {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal,
                               interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard
                let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController,
                interactionController.inProgress
                else {
                    return nil
            }
            return interactionController
    }
}



extension AdminPostController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return SystemPushAnimator(type: .navigation)
        case .pop:
            return SystemPopAnimator(type: .navigation)
        default:
            return nil
        }
    }
    
}

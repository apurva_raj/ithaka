////
////  StoryCards.swift
////  ithaka
////
////  Created by Apurva Raj on 06/12/17.
////  Copyright © 2017 Apurva Raj. All rights reserved.
////
//
//
//import UIKit
//
//class StoryCards: UICollectionViewLayout {
//    private var contentWidth:CGFloat!
//    private var contentHeight:CGFloat!
//    private var yOffset:CGFloat = 0
//    var maxAlpha:CGFloat = 1
//    var minAlpha:CGFloat = 0
//    var widthOffset:CGFloat = 35
//    var heightOffset:CGFloat = 35
//    var currentIndex=0
//    var scrollToItem=false
//    public weak var articleViewController:HomeController!
//    private var cache = [UICollectionViewLayoutAttributes]()
//
//    private var itemWidth:CGFloat{
//        return (collectionView?.bounds.width)!
//    }
//
//    private var itemHeight:CGFloat{
//        return (collectionView?.bounds.height)!
//    }
//
//    private var collectionViewHeight:CGFloat{
//        return (collectionView?.bounds.height)!
//    }
//
//    private var numberOfItems:Int{
//        return (collectionView?.numberOfItems(inSection: 0))!
//    }
//
//    private var dragOffset:CGFloat{
//        return (collectionView?.bounds.height)!
//    }
//    private var currentItemIndex:Int{
//        return max(0, Int(collectionView!.contentOffset.y / collectionViewHeight))
//    }
//
//    var nextItemBecomeCurrentPercentage:CGFloat{
//        return (collectionView!.contentOffset.y / (collectionViewHeight)) - CGFloat(currentItemIndex)
//    }
//
//    override func prepare() {
//        cache.removeAll(keepingCapacity: false)
//        yOffset = 0
//        for item in 0 ..< numberOfItems{
//            let indexPath=NSIndexPath(item: item, section: 0)
//            let attribute = UICollectionViewLayoutAttributes(forCellWith: indexPath as IndexPath)
//            attribute.zIndex = -indexPath.row
//            if (indexPath.item == currentItemIndex+1) && (indexPath.item < numberOfItems){
//                attribute.alpha = minAlpha + max((maxAlpha-minAlpha) * nextItemBecomeCurrentPercentage, 0)
//                let width = itemWidth - widthOffset + (widthOffset * nextItemBecomeCurrentPercentage)
//                let height = itemHeight - heightOffset + (heightOffset * nextItemBecomeCurrentPercentage)
//                let deltaWidth =  width/itemWidth
//                let deltaHeight = height/itemHeight
//                attribute.frame = CGRect(x: 0, y: yOffset, width: itemWidth, height: itemHeight)
//                attribute.transform = CGAffineTransform(scaleX: deltaWidth, y: deltaHeight)
//                attribute.center.y = (collectionView?.center.y)! +  (collectionView?.contentOffset.y)!
//                attribute.center.x = (collectionView?.center.x)! + (collectionView?.contentOffset.x)!
//                yOffset += collectionViewHeight
//            }else{
//                attribute.frame=CGRect(x: 0, y: yOffset, width: itemWidth, height: itemHeight)
//                attribute.center.y = (collectionView?.center.y)! + yOffset
//                attribute.center.x = (collectionView?.center.x)!
//                yOffset += collectionViewHeight
//            }
//            cache.append(attribute)
//        }
//    }
//
//    override var collectionViewContentSize: CGSize {
//        contentWidth = (collectionView?.bounds.width)!
//        contentHeight = CGFloat(numberOfItems) * (collectionView?.bounds.height)!
//        return CGSize(width: contentWidth, height: contentHeight)
//    }
//
//    //Return Attributes  whose frame lies in the Visible Rect
//    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
//        var layoutAttributes = [UICollectionViewLayoutAttributes]()
//        for attribute in cache{
//            if attribute.frame.intersects(rect){
//                layoutAttributes.append(attribute)
//            }
//        }
//        return layoutAttributes
//    }
//
//    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
//        return true
//    }
//    
//    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
//        let presentOffset = CGFloat(currentIndex) * itemHeight
//        var yOffset=CGFloat(0)
//        let count=collectionView?.numberOfItems(inSection: 0)
//        if velocity.y > 0  /* Swiped up */{
//            if currentIndex < count!-1 {
//                currentIndex=currentIndex+1
//            }
//        }else if(velocity.y < 0){
//            if currentIndex > 0 {
//                currentIndex=currentIndex-1
//            }
//        }else{
//            if presentOffset > proposedContentOffset.y {
//                let diff=presentOffset-proposedContentOffset.y
//                if diff > (itemHeight/4){
//                    currentIndex=currentIndex-1
//                }
//            }else{
//                let diff=proposedContentOffset.y - presentOffset
//                if diff > (itemHeight/4){
//                    currentIndex=currentIndex+1
//                }
//            }
//        }
//        articleViewController.updateCurrentIndex(IndexPath(item: currentIndex, section: 0))
//        yOffset=CGFloat(currentIndex)*itemHeight
//        return CGPoint(x: 0, y: yOffset)
//    }
//
//    override open func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes?{
//        return UICollectionViewLayoutAttributes(forCellWith: indexPath as IndexPath)
//    }
//
//}


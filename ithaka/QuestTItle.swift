//
//  QuestTItle.swift
//  ithaka
//
//  Created by Apurva Raj on 02/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class QuestTitle: UICollectionViewCell {
   
    weak var delegate: QuestDailyDelegate?
    
    lazy var imageView: UIImageView = {
        let iv = UIImageView()
        return iv
    }()
    
    lazy var questLabel1: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(24)
        label.textAlignment = .center
        return label
    }()
    
    lazy var rightView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "right-arrow_color")
        iv.isHidden = true
        iv.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(rightViewTapped))
        tap.numberOfTapsRequired = 1
        iv.addGestureRecognizer(tap)

        return iv
    }()
    
    lazy var rightButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "right-arrow_color"), for: .normal)
        button.addTarget(self, action: #selector(rightViewTapped), for: .touchDown)
        return button
    }()
    
    @objc func rightViewTapped() {
        print("mm")
        self.delegate?.rightbuttonTapped()
    }
    
    lazy var leftView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "right-arrow_color").withHorizontallyFlippedOrientation()
        iv.isHidden = true
        return iv
    }()
    
    lazy var lineView: UIView = {
        let iv = UIView()
        iv.backgroundColor = UIColor.black
        return iv
    }()


    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        let attachment:NSTextAttachment = NSTextAttachment()
        attachment.image =  #imageLiteral(resourceName: "book")
        
        attachment.setImageHeight(height: 28)
        let attachmentString:NSAttributedString = NSAttributedString(string:"Daily Quest")
        let attributedString:NSMutableAttributedString = NSMutableAttributedString(attachment: attachment)
        
        
        attributedString.append(attachmentString)
        
        questLabel1.attributedText = attributedString
        
        addSubview(rightButton)
        addSubview(leftView)
        addSubview(questLabel1)
        addSubview(lineView)
        questLabel1.clipsToBounds = true
        questLabel1.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  ExploreSearch.swift
//  ithaka
//
//  Created by Apurva Raj on 11/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class ExploreSearch: UICollectionViewCell {
    
    
    weak var delegate: exploreDelegate?
    
    lazy var searchBar: UILabel = {
        let sb = UILabel()
        sb.text = "Search"
        sb.textAlignment = .center
        sb.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(searchBarTapped))
        tap.numberOfTapsRequired = 1
        sb.addGestureRecognizer(tap)
        sb.textColor = .gray
        sb.font = UIFont.systemFont(ofSize: 16)
        
        
        //        sb.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.2)
        sb.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        return sb
    }()
    
    @objc func searchBarTapped() {
        self.delegate?.onSearchTapped()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(searchBar)
        self.backgroundColor = .red
        searchBar.anchor(top: safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


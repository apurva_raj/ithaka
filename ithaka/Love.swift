//
//  Love.swift
//  ithaka
//
//  Created by Apurva Raj on 23/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import Foundation

struct Love {
    let user: UserModel
    let taps: Int
    
    init(user: UserModel, dictionary: [String: Any]) {
        self.user = user
        self.taps = dictionary["tap"] as? Int ?? 0
    }
}


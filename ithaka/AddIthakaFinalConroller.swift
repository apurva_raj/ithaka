//
//  AddIthakaFinalConroller.swift
//  ithaka
//
//  Created by Apurva Raj on 01/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import Foundation
import Firebase

class AddIthakaFinalController: UIViewController {
    
    var addedStory: String? {
        didSet {
            self.storyView.text = addedStory
        }
    }
    var addedTitle: String? {
        didSet {
            self.titleView.text = addedTitle
        }
    }
    
    var selectedImage: UIImage? {
        didSet {
            self.photoImageView.image = selectedImage
        }
    }
    
    let titleView: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .center
        label.numberOfLines = 0
        
        return label
    }()
    
    
    let storyView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 14)
        tv.textColor = .gray
        tv.isEditable = false
        return tv
    }()
    
    
    let photoImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        iv.backgroundColor = .lightGray
        return iv
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Share", style: .plain, target: self
            , action: #selector(handleShare))
        
        setupMainView()
    }
    
    fileprivate func setupMainView() {
        
        let th = (view.frame.height)*0.1
        let sh = (view.frame.height)*0.4
        let ih = (view.frame.height)*0.42
        
        view.addSubview(titleView)
        if #available(iOS 11.0, *) {
            titleView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: th)
        } else {
            titleView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: th)
        }
        
        view.addSubview(storyView)
        storyView.anchor(top: titleView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: sh)
        
        view.addSubview(photoImageView)
        photoImageView.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: -10, leftConstant: 5, bottomConstant: 5, rightConstant: 5, widthConstant: 0, heightConstant: ih)
    }
    
    @objc func handleShare(){
        guard let title = titleView.text, title.count > 0 else { return }
        guard let story = storyView.text, story.count > 0 else { return }
        guard let image = selectedImage else { return }
        guard let uploadData = UIImageJPEGRepresentation(image, 0.5) else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        let filename = NSUUID().uuidString
        let f = "MOU"
        let ff = filename + f + uid
        
        let storageRef = Storage.storage().reference().child("Posts").child(ff)
        
            storageRef.putData(uploadData, metadata: nil) { (metadata,err) in
            if let err = err {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                print("failed to upload image:", err)
                return
            }
            
                storageRef.downloadURL(completion: { (url, err) in
                    if let err = err {
                        // Handle any errors
                        print("ERr is ", err)
                    } else {
                        // Get the download URL for 'images/stars.jpg'
                        print("Successfully uploaded post image:", url)
                        guard let Urlpath = url?.absoluteString else  { return }
                        self.saveToDatabaseWithImageUrl(imageUrl: Urlpath)

                    }

                })

    }
}
    static let updateFeedNotificationName = NSNotification.Name(rawValue: "UpdateFeed")

    fileprivate func saveToDatabaseWithImageUrl(imageUrl: String) {
        guard let title = titleView.text else { return }
        guard let story = storyView.text else { return }
        guard let postImage = selectedImage else { return }
        
        let star = 0

        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let userPostRef = Database.database().reference().child("posts").child(uid)
        let ref = userPostRef.childByAutoId()
        let ref3 = Database.database().reference().child("users").child(uid)

        let values = ["title": title, "story": story, "imageUrl": imageUrl, "imageWidth": postImage.size.width, "imageHeight": postImage.size.height, "star":star, "creationDate": Date().timeIntervalSince1970] as [String : Any]

        ref.updateChildValues(values) { (err, ref) in
            if let err = err {
                self.navigationItem.rightBarButtonItem?.isEnabled = true
                print("Failed to save post to DB", err)
                return
            }
            userPostRef.observeSingleEvent(of: .value, with: { (snapshot) in
                let value = snapshot.value as? NSDictionary
                
                let cc = value?.count ?? 0
                
                let postCount = ["postCount": cc]
                ref3.updateChildValues(postCount)
                
            })
            print("Successfully saved post to DB")
            self.navigationController?.popToRootViewController(animated: true)
            NotificationCenter.default.post(name: AddIthakaFinalController.updateFeedNotificationName, object: nil)

        }

    }
    override var prefersStatusBarHidden: Bool {
        return true
    }
}


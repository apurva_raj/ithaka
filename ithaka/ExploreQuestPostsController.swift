//
//  ExploreQuestPostsController
//  ithaka
//
//  Created by Apurva Raj on 15/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ExploreQuestPostsController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    
    var questId: String?
    var questTitle: String?
    var questmodel: QuestTitleModel?
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)


    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant:0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()


    collectionView?.register(ExploreQuestPostHeaderViewCell.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        collectionView?.register(PostUi.self, forCellWithReuseIdentifier: "PostUi")
        
        collectionView?.delegate = self
        collectionView?.dataSource = self

        navigationItem.title = questTitle
        fetchPosts()
        fetchQuest()
    }
    
    fileprivate func fetchQuest() {
        guard let questId = questId else { return }
        let ref = Database.database().reference().child("QuestTitles").child(questId)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            
            let quest = QuestTitleModel.init(id: snapshot.key, dictionary: value as! [String : Any])
            self.questmodel = quest
            self.collectionView?.reloadData()
        }
    }
    
    
func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 70)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! ExploreQuestPostHeaderViewCell
        header.backgroundColor = .white
        
        let qqwe = questmodel?.questImage ?? ""
        header.titleLabel.loadImage(urlString: qqwe)
////        header.titleLabel.text = questTitle
        return header
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return postNew.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostUi", for: indexPath) as! PostUi

        
        let post = postNew[indexPath.item]
        cell.post = post
        
        let user = userModelNew[indexPath.item]
        cell.user = user
        
        
        print("Vasavasvsavsdavdsvsdvsvsadv", post)
        
        return cell


    }
    var tagheight = 0

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: 350)
    }
    
    fileprivate var isLoadingPost = false
    
    
    var postNew = [PostNew]()
    var userModelNew = [UserModelNew]()
    
    fileprivate func fetchPosts()  {

        isLoadingPost = true
        
        guard let questId = questId else { return }
        Api.Feed.getExploreQuestFeed(qid: questId, start: postNew.first?.timestamp, limit: 3) { (results) in
            guard let results = results else { return }

            if results.count > 0 {
                
                results.forEach({ (result) in
                    self.postNew.append(result.0)
                    self.userModelNew.append(result.1)
                    
                })
                
            }
            self.isLoadingPost = false
            self.myActivityIndicator.stopAnimating()

            self.collectionView?.reloadData()
            
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
            
            guard !isLoadingPost else {
                return
            }
            isLoadingPost = true
            guard (self.postNew.last?.timestamp) != nil else {
                isLoadingPost = false
                self.myActivityIndicator.stopAnimating()

                return
            }
            guard let questId = questId else { return }

            Api.Feed.getOldExploreQuestFeed(qid: questId, start: (postNew.last?.timestamp)!, limit: 3, completionHandler: { (results) in
                
                guard let results = results else { return }

                if results.count == 0 {
                    return
                }
                for result in results {
                    self.postNew.append(result.0)
                    self.userModelNew.append(result.1)
                }
                self.myActivityIndicator.stopAnimating()
                self.collectionView?.reloadData()
                self.isLoadingPost = false
                
            })
            
        }
    }
    
}


class ExploreQuestPostHeaderViewCell: BaseCollectionViewCell {
    
    lazy var titleLabel: CustomImageView = {
        let label = CustomImageView()
        label.isUserInteractionEnabled = true
        label.backgroundColor = UIColor.random()
        label.layer.cornerRadius = 3
        label.layer.borderColor  =  UIColor.gray.cgColor
        label.layer.borderWidth = 1
        label.layer.shadowOpacity = 0.5
        label.layer.shadowColor =  UIColor.black.cgColor
        label.layer.shadowRadius = 5.0
        label.layer.shadowOffset = CGSize(width:5, height: 5)
        label.layer.masksToBounds = true
        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(titleTapped))
//        tap.numberOfTapsRequired = 1
//        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    override func setupViews() {
        self.backgroundColor = .white
        
        addSubview(titleLabel)
        
        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

    }
}


class ExploreQuestPostViewCell: BaseCollectionViewCell {
    
    override func setupViews() {
        self.backgroundColor = .white
    }
}


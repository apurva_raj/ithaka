//
//  ItemsStoryViewCell.swift
//  ithaka
//
//  Created by Apurva Raj on 20/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class ItemsStoryViewCell: UICollectionViewCell {
    
    lazy var mainItemView: UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "yellow_rune")
        iv.contentMode = .scaleAspectFill
//        let tap = UITapGestureRecognizer(target: self, action: #selector(chestTapped))
//        tap.numberOfTapsRequired = 1
//        iv.addGestureRecognizer(tap)
//        iv.isUserInteractionEnabled = true
        
        return iv
    }()
    
    lazy var mainItemName: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 27)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = UIColor.black
        
        return label
    }()

    let frontbg: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        view.alpha = 1
        return view
    }()

    
    lazy var storyView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 20)
        textView.textColor = UIColor.gray
        textView.isUserInteractionEnabled = true
        textView.textAlignment = .left
//        let tap = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
//        tap.numberOfTapsRequired = 1
//        textView.addGestureRecognizer(tap)
        
        textView.sizeToFit()
        textView.isScrollEnabled = true
        textView.layoutIfNeeded()
        textView.backgroundColor = .white
        textView.contentInset = UIEdgeInsetsMake(10, 10, 10, 10)
        textView.layer.cornerRadius = 15
        textView.clipsToBounds = true
        
        return textView
    }()
    


    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(frontbg)
        
        addSubview(mainItemView)
        addSubview(mainItemName)
        addSubview(storyView)
        
        
        mainItemView.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant:frame.width/2 - 70, bottomConstant: 0, rightConstant: 0, widthConstant: 140, heightConstant: 140)
        
        mainItemName.anchor(top: mainItemView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        frontbg.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: mainItemName.bottomAnchor, right: self.rightAnchor, topConstant: 25, leftConstant: 15, bottomConstant: -10, rightConstant: 15, widthConstant: 0, heightConstant: 0)

        self.sendSubview(toBack: frontbg)
        storyView.anchor(top: mainItemName.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 30, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: frame.height - 220)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


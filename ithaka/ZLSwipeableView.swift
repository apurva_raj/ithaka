//
//  ZLSwipeableView.swift
//  ZLSwipeableViewSwiftDemo
//
//  Created by Zhixuan Lai on 4/27/15.
//  Copyright (c) 2015 Zhixuan Lai. All rights reserved.
//

import UIKit
import FirebaseAuth

// data source
public typealias NextViewHandler = () -> UIView?
public typealias PreviousViewHandler = () -> UIView?

// customization
public typealias AnimateViewHandler = (_ view: UIView, _ index: Int, _ views: [UIView], _ swipeableView: ZLSwipeableView) -> ()
public typealias InterpretDirectionHandler = (_ topView: UIView, _ direction: Direction1, _ views: [UIView], _ swipeableView: ZLSwipeableView) -> (CGPoint, CGVector)
public typealias ShouldSwipeHandler = (_ view: UIView, _ movement: Movement, _ swipeableView: ZLSwipeableView) -> Bool

// delegates
public typealias DidStartHandler = (_ view: UIView, _ atLocation: CGPoint) -> ()
public typealias SwipingHandler = (_ view: UIView, _ atLocation: CGPoint, _ translation: CGPoint) -> ()
public typealias DidEndHandler = (_ view: UIView, _ atLocation: CGPoint) -> ()
public typealias DidSwipeHandler = (_ view: UIView, _ inDirection: Direction1, _ directionVector: CGVector) -> ()
public typealias DidCancelHandler = (_ view: UIView) -> ()
public typealias DidTap = (_ view: UIView, _ atLocation: CGPoint) -> ()
public typealias DidDisappear = (_ view: UIView) -> ()

public struct Movement {
    public let location: CGPoint
    public let translation: CGPoint
    public let velocity: CGPoint
}

// MARK: - Main
open class ZLSwipeableView: UIView {
    
    // MARK: Data Source
    open var numberOfActiveView = UInt(4)
    open var nextView: NextViewHandler? {
        didSet {
            loadViews()
        }
    }
    open var previousView: PreviousViewHandler?
    // Rewinding
    open var history = [UIView]()
    open var numberOfHistoryItem = UInt(10)
    
    // MARK: Customizable behavior
    open var animateView = ZLSwipeableView.defaultAnimateViewHandler()
    open var interpretDirection = ZLSwipeableView.defaultInterpretDirectionHandler()
    open var shouldSwipeView = ZLSwipeableView.defaultShouldSwipeViewHandler()
    open var minTranslationInPercent = CGFloat(0.25)
    open var minVelocityInPointPerSecond = CGFloat(750)
    open var allowedDirection = Direction1.Horizontal
    open var onlySwipeTopCard = false
    
    // MARK: Delegate
    open var didStart: DidStartHandler?
    open var swiping: SwipingHandler?
    open var didEnd: DidEndHandler?
    open var didSwipe: DidSwipeHandler?
    open var didCancel: DidCancelHandler?
    open var didTap: DidTap? {
        didSet {
            guard didTap != nil else { return }
            // Update all viewManagers to listen for taps
            viewManagers.forEach { view, viewManager in
                viewManager.addTapRecognizer()
            }
        }
    }
    open var didDisappear: DidDisappear?
    
    // MARK: Private properties
    /// Contains subviews added by the user.
    fileprivate var containerView = UIView()
    
    /// Contains auxiliary subviews.
    fileprivate var miscContainerView = UIView()
    
    fileprivate var animator: UIDynamicAnimator!
    
    fileprivate var viewManagers = [UIView: ViewManager]()
    
    fileprivate var scheduler = Scheduler()
    
    
    ///collection view cell testing. Hope this stupid idea of mine works.
//    weak open var delegate: MyCustomDelegate?

    var post: Post? {
        didSet {
            guard let postImageUrl = post?.imageUrl else { return }
            guard let title = post?.title else { return }
            guard let story = post?.story else { return }
            
            guard let profileImageUrl = post?.user.profileImageUrl else { return }
            guard let username = post?.user.username else { return }
            
            guard let allLove = post?.loveCount else { return }
            guard let commetsCount = post?.commentsCount else { return }
            guard let rocketCount = post?.rocketCount else { return }
            
            
            let allLoveString = String(allLove)
            let commentsCountString = String(commetsCount)
            let rokcketCountString = String(rocketCount)
            let heart = "❤️".stringToImage()
            loveImageView.setImage(post?.hasLiked == true ? heart : #imageLiteral(resourceName: "like_unselected").withRenderingMode(.alwaysOriginal), for: .normal)
            
            if post?.hasLiked == true {
                loveImageView.isUserInteractionEnabled = false
            }
            let postImageCount = postImageUrl.count
            
            mainpicView.loadImage(urlString: postImageUrl)
            titleLabel.text = title
            storyLabel.text = story
            loveCountLabel.text = allLoveString
            CommentLabel.text = commentsCountString
            rocketCountLabel.text = rokcketCountString
            
            nameLabel.text = username
            profileImageView.loadImage(urlString: profileImageUrl)
            
            
            guard let uid = post?.user.uid else { return }
            guard let cuid = Auth.auth().currentUser?.uid else { return }
            
            if(uid != cuid) {
                optionButton.isEnabled = false
                optionButton.isHidden = true
            } else {
                optionButton.isEnabled = true
                optionButton.isHidden = false
            }
            
            if(postImageCount == 0) {
                mainpicView.anchor(top: nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
            }
            
            setupAttributedDate()
            
        }
    }
    
    fileprivate func setupAttributedDate() {
        guard let post = self.post else { return }
        
        let attributedText = NSMutableAttributedString(string: post.user.username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
        
        attributedText.append(NSAttributedString(string: "  ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        
        let timeAgoDisplay = post.creationDate.timeAgoDisplay()
        attributedText.append(NSAttributedString(string: timeAgoDisplay, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor.gray]))
        
        nameLabel.attributedText = attributedText
    }
    
    
    lazy var titleLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 16)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        
        textView.layoutIfNeeded()
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
        tap.numberOfTapsRequired = 1
        textView.addGestureRecognizer(tap)
        
        
        return textView
    }()
    
    
    @objc func titleLabelTapped() {
//        self.delegate?.onTitleLabelTapped(for: self)
    }
    
    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 12.5
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(13)
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    @objc func nameLabelTapped(_ recognizer: UITapGestureRecognizer) {
//        self.delegate?.onNameLabelTapped(for: self)
    }
    
    lazy var optionButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_expand_more"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOptionButton))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        button.isEnabled = false
        button.isHidden = true
        return button
    }()
    
    @objc func handleOptionButton() {
//        self.delegate?.onOptionButtonTapped(for: self)
    }
    
    lazy var storyLabel: UITextView = {
        let textview = UITextView()
        textview.font = UIFont.systemFont(ofSize: 14)
        textview.textColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.85)
        
        textview.isUserInteractionEnabled = true
        textview.isEditable = false
        textview.isScrollEnabled = false
        textview.sizeToFit()
        textview.translatesAutoresizingMaskIntoConstraints = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(storyViewTapped))
        tap.numberOfTapsRequired = 1
        textview.addGestureRecognizer(tap)
        
        return textview
    }()
    
    @objc func storyViewTapped(_ recognizer: UITapGestureRecognizer) {
        print("Story tv tapped")
        guard let post = post else { return }
        
//        self.delegate?.onStoryViewTapped(post: post)
        
    }
    
    let mainpicView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .random()
        return imageView
    }()
    
    
    
    
    
    @objc func handleComment() {
        print("Trying to show comments...")
        guard let post = post else { return }
        
//        delegate?.didTapComment(post: post)
    }
    
    
    
    
    
    
    // MARK: Life cycle
    public override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(titleLabel)
        
        addSubview(profileImageView)
        addSubview(nameLabel)
        addSubview(optionButton)
        addSubview(storyLabel)
        addSubview(mainpicView)
        
        
        
        titleLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: -3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        profileImageView.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 25, heightConstant: 25)
        nameLabel.anchor(top: titleLabel.bottomAnchor, left: profileImageView.rightAnchor, bottom: nil, right: nil, topConstant: 1, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 200, heightConstant: 20)
        optionButton.anchor(top: nameLabel.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        storyLabel.anchor(top: profileImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 223)
        
        
        mainpicView.anchor(top: storyLabel.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 8, widthConstant: 5, heightConstant: 220)
        

        setupPostReactionBar()
        setupReactionCounts()
        
        setup()
    }
    
   
    func LogoutbuttonTapped() {
        print(42)
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
    
    lazy var loveImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "like_unselected"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(loveButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    lazy var rokcetImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "rocket_selected"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(rocketButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    
    lazy var commentImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "comment"), for: .normal)
        button.addTarget(self, action: #selector(handleComment), for: .touchUpInside)
        
        return button
    }()
    
    fileprivate func setupPostReactionBar(){
        
        let stackView = UIStackView(arrangedSubviews: [loveImageView, commentImageView, rokcetImageView])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        addSubview(stackView)
        
        stackView.anchor(top: storyLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
    }
    
    
    @objc func rocketButtonTapped(sender: UITapGestureRecognizer){
//        self.delegate?.onRocketButtonTapped(for: self)
        print("flyyy")
        
    }
    
    @objc func loveButtonTapped(sender: UITapGestureRecognizer){
        //        var loc : CGPoint = sender.location(in: sender.view)
        guard let location : CGPoint = sender.view?.superview?.superview?.frame.origin else { return }
        let loc = convert(location, to: nil)
        
        print("there", loc)
        
        
//        self.delegate?.onloveButtonTapped(loc: loc, for: self)
    }
    
    
    func sampleTap(){
        print(123)
    }
    
    let loveCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(12)
        label.textColor = .gray
        label.textAlignment = .center
        
        return label
    }()
    let rocketCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(12)
        label.textColor = .gray
        label.textAlignment = .center
        
        return label
    }()
    let CommentLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.textColor = .gray
        
        return label
    }()
    
    fileprivate func setupReactionCounts() {
        
        
        let bottomDividerView = UIView()
        bottomDividerView.backgroundColor = UIColor.lightGray
        
        let stackView = UIStackView(arrangedSubviews: [loveCountLabel, CommentLabel, rocketCountLabel])
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        stackView.backgroundColor = .green
        addSubview(stackView)
        addSubview(bottomDividerView)
        
        stackView.anchor(top: storyLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 30, leftConstant: 0, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 30)
        
        bottomDividerView.anchor(top: stackView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0.5)
        
        
    }
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addSubview(titleLabel)
        
        addSubview(profileImageView)
        addSubview(nameLabel)
        addSubview(optionButton)
        addSubview(storyLabel)
        addSubview(mainpicView)
        
        
        
        titleLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: -3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        profileImageView.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 25, heightConstant: 25)
        nameLabel.anchor(top: titleLabel.bottomAnchor, left: profileImageView.rightAnchor, bottom: nil, right: nil, topConstant: 1, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 200, heightConstant: 20)
        optionButton.anchor(top: nameLabel.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        storyLabel.anchor(top: profileImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 223)
        
        
        mainpicView.anchor(top: storyLabel.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 8, widthConstant: 5, heightConstant: 220)
        
        
        setupPostReactionBar()
        setupReactionCounts()
        setup()
    }
    
    fileprivate func setup() {
        addSubview(containerView)
        addSubview(miscContainerView)
        animator = UIDynamicAnimator(referenceView: self)
    }
    
    deinit {
        nextView = nil
        
        didStart = nil
        swiping = nil
        didEnd = nil
        didSwipe = nil
        didCancel = nil
        didDisappear = nil
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        containerView.frame = bounds
    }
    
    // MARK: Public APIs
    open func topView() -> UIView? {
        return activeViews().first
    }
    
    // top view first
    open func activeViews() -> [UIView] {
        return allViews().filter() {
            view in
            guard let viewManager = viewManagers[view] else { return false }
            if case .swiping(_) = viewManager.state {
                return false
            }
            return true
            }.reversed()
    }
    
    open func loadViews() {
        for _ in UInt(activeViews().count) ..< numberOfActiveView {
            if let nextView = nextView?() {
                insert(nextView, atIndex: 0)
            }
        }
        updateViews()
    }
    
    open func rewind() {
        var viewToBeRewinded: UIView?
        if let lastSwipedView = history.popLast() {
            viewToBeRewinded = lastSwipedView
        } else if let view = previousView?() {
            viewToBeRewinded = view
        }
        
        guard let view = viewToBeRewinded else { return }
        
        if UInt(activeViews().count) == numberOfActiveView && activeViews().first != nil {
            remove(activeViews().last!)
        }
        insert(view, atIndex: allViews().count)
        updateViews()
    }
    
    open func discardTopCard() {
        guard let topView = topView() else { return }
        
        remove(topView)
        loadViews()
    }
    
    open func discardViews() {
        for view in allViews() {
            remove(view)
        }
    }
    
    open func swipeTopView(inDirection direction: Direction1) {
        guard let topView = topView() else { return }
        let (location, directionVector) = interpretDirection(topView, direction, activeViews(), self)
        swipeTopView(fromPoint: location, inDirection: directionVector)
    }
    
    open func swipeTopView(fromPoint location: CGPoint, inDirection directionVector: CGVector) {
        guard let topView = topView(), let topViewManager = viewManagers[topView] else { return }
        topViewManager.state = .swiping(location, directionVector)
        swipeView(topView, location: location, directionVector: directionVector)
    }
    
    // MARK: Private APIs
    fileprivate func allViews() -> [UIView] {
        return containerView.subviews
    }
    
    fileprivate func insert(_ view: UIView, atIndex index: Int) {
        guard !allViews().contains(view) else {
            // this view has been schedule to be removed
            guard let viewManager = viewManagers[view] else { return }
            viewManager.state = viewManager.snappingStateAtContainerCenter()
            return
        }
        
        let viewManager = ViewManager(view: view, containerView: containerView, index: index, miscContainerView: miscContainerView, animator: animator, swipeableView: self)
        viewManagers[view] = viewManager
    }
    
    fileprivate func remove(_ view: UIView) {
        guard allViews().contains(view) else { return }
        
        viewManagers.removeValue(forKey: view)
        self.didDisappear?(view)
    }
    
    open func updateViews() {
        let activeViews = self.activeViews()
        let inactiveViews = allViews().arrayByRemoveObjectsInArray(activeViews)
        
        for view in inactiveViews {
            view.isUserInteractionEnabled = false
        }
        
        guard let gestureRecognizers = activeViews.first?.gestureRecognizers, gestureRecognizers.filter({ gestureRecognizer in gestureRecognizer.state != .possible }).count == 0 else { return }
        
        for i in 0 ..< activeViews.count {
            let view = activeViews[i]
            view.isUserInteractionEnabled = onlySwipeTopCard ? i == 0 : true
            let shouldBeHidden = i >= Int(numberOfActiveView)
            view.isHidden = shouldBeHidden
            guard !shouldBeHidden else { continue }
            animateView(view, i, activeViews, self)
        }
    }
    
    func swipeView(_ view: UIView, location: CGPoint, directionVector: CGVector) {
        let direction = Direction1.fromPoint(point: CGPoint(x: directionVector.dx, y: directionVector.dy))
        
        scheduleToBeRemoved(view) { aView in
            !self.containerView.convert(aView.frame, to: nil).intersects(UIScreen.main.bounds)
        }
        didSwipe?(view, direction, directionVector)
        loadViews()
    }
    
    func scheduleToBeRemoved(_ view: UIView, withPredicate predicate: @escaping (UIView) -> Bool) {
        guard allViews().contains(view) else { return }
        
        history.append(view)
        if UInt(history.count) > numberOfHistoryItem {
            history.removeFirst()
        }
        scheduler.scheduleRepeatedly({ () -> Void in
            self.allViews().arrayByRemoveObjectsInArray(self.activeViews()).filter({ view in predicate(view) }).forEach({ view in self.remove(view) })
        }, interval: 0.3) { () -> Bool in
            return self.activeViews().count == self.allViews().count
        }
    }
    
}

// MARK: - Default behaviors
extension ZLSwipeableView {
    
    static func defaultAnimateViewHandler() -> AnimateViewHandler {
        func toRadian(_ degree: CGFloat) -> CGFloat {
            return degree * CGFloat(Double.pi / 180)
        }
        
        func rotateView(_ view: UIView, forDegree degree: CGFloat, duration: TimeInterval, offsetFromCenter offset: CGPoint, swipeableView: ZLSwipeableView,  completion: ((Bool) -> Void)? = nil) {
            UIView.animate(withDuration: duration, delay: 0, options: .allowUserInteraction, animations: {
                view.center = swipeableView.convert(swipeableView.center, from: swipeableView.superview)
                var transform = CGAffineTransform(translationX: offset.x, y: offset.y)
                transform = transform.rotated(by: toRadian(degree))
                transform = transform.translatedBy(x: -offset.x, y: -offset.y)
                view.transform = transform
            },
                           completion: completion)
        }
        
        return { (view: UIView, index: Int, views: [UIView], swipeableView: ZLSwipeableView) in
            let degree = CGFloat(1)
            let duration = 0.4
            let offset = CGPoint(x: 0, y: swipeableView.bounds.height * 0.3)
            switch index {
            case 0:
                rotateView(view, forDegree: 0, duration: duration, offsetFromCenter: offset, swipeableView: swipeableView)
            case 1:
                rotateView(view, forDegree: degree, duration: duration, offsetFromCenter: offset, swipeableView: swipeableView)
            case 2:
                rotateView(view, forDegree: -degree, duration: duration, offsetFromCenter: offset, swipeableView: swipeableView)
            default:
                rotateView(view, forDegree: 0, duration: duration, offsetFromCenter: offset, swipeableView: swipeableView)
            }
        }
    }
    
    static func defaultInterpretDirectionHandler() -> InterpretDirectionHandler {
        return { (topView: UIView, direction: Direction1, views: [UIView], swipeableView: ZLSwipeableView) in
            let programmaticSwipeVelocity = CGFloat(1000)
            let location = CGPoint(x: topView.center.x, y: topView.center.y*0.7)
            var directionVector: CGVector!
            
            switch direction {
            case Direction1.Left:
                directionVector = CGVector(dx: -programmaticSwipeVelocity, dy: 0)
            case Direction1.Right:
                directionVector = CGVector(dx: programmaticSwipeVelocity, dy: 0)
            case Direction1.Up:
                directionVector = CGVector(dx: 0, dy: -programmaticSwipeVelocity)
            case Direction1.Down:
                directionVector = CGVector(dx: 0, dy: programmaticSwipeVelocity)
            default:
                directionVector = CGVector(dx: 0, dy: 0)
            }
            
            return (location, directionVector)
        }
    }
    
    static func defaultShouldSwipeViewHandler() -> ShouldSwipeHandler {
        return { (view: UIView, movement: Movement, swipeableView: ZLSwipeableView) -> Bool in
            let translation = movement.translation
            let velocity = movement.velocity
            let bounds = swipeableView.bounds
            let minTranslationInPercent = swipeableView.minTranslationInPercent
            let minVelocityInPointPerSecond = swipeableView.minVelocityInPointPerSecond
            let allowedDirection = swipeableView.allowedDirection
            
            func areTranslationAndVelocityInTheSameDirection() -> Bool {
                return CGPoint.areInSameTheDirection(translation, p2: velocity)
            }
            
            func isDirectionAllowed() -> Bool {
                return Direction1.fromPoint(point: translation).intersection(allowedDirection) != .None
            }
            
            func isTranslationLargeEnough() -> Bool {
                return abs(translation.x) > minTranslationInPercent * bounds.width || abs(translation.y) > minTranslationInPercent * bounds.height
            }
            
            func isVelocityLargeEnough() -> Bool {
                return velocity.magnitude > minVelocityInPointPerSecond
            }
            
            return isDirectionAllowed() && areTranslationAndVelocityInTheSameDirection() && (isTranslationLargeEnough() || isVelocityLargeEnough())
        }
    }
    
}

// MARK: - Deprecated APIs
extension ZLSwipeableView {
    
    @available(*, deprecated: 1, message: "Use numberOfActiveView")
    public var numPrefetchedViews: UInt {
        get {
            return numberOfActiveView
        }
        set(newValue){
            numberOfActiveView = newValue
        }
    }
    
    @available(*, deprecated: 1, message: "Use allowedDirection")
    public var direction: Direction1 {
        get {
            return allowedDirection
        }
        set(newValue){
            allowedDirection = newValue
        }
    }
    
    @available(*, deprecated: 1, message: "Use minTranslationInPercent")
    public var translationThreshold: CGFloat {
        get {
            return minTranslationInPercent
        }
        set(newValue){
            minTranslationInPercent = newValue
        }
    }
    
    @available(*, deprecated: 1, message: "Use minVelocityInPointPerSecond")
    public var velocityThreshold: CGFloat {
        get {
            return minVelocityInPointPerSecond
        }
        set(newValue){
            minVelocityInPointPerSecond = newValue
        }
    }
    
}

// MARK: - Helper extensions
public func *(lhs: CGPoint, rhs: CGFloat) -> CGPoint {
    return CGPoint(x: lhs.x * rhs, y: lhs.y * rhs)
}

extension CGPoint {
    
    init(vector: CGVector) {
        self.init(x: vector.dx, y: vector.dy)
    }
    
    var normalized: CGPoint {
        return CGPoint(x: x / magnitude, y: y / magnitude)
    }
    
    var magnitude: CGFloat {
        return CGFloat(sqrtf(powf(Float(x), 2) + powf(Float(y), 2)))
    }
    
    static func areInSameTheDirection(_ p1: CGPoint, p2: CGPoint) -> Bool {
        
        func signNum(_ n: CGFloat) -> Int {
            return (n < 0.0) ? -1 : (n > 0.0) ? +1 : 0
        }
        
        return signNum(p1.x) == signNum(p2.x) && signNum(p1.y) == signNum(p2.y)
    }
    
}

extension CGVector {
    
    init(point: CGPoint) {
        self.init(dx: point.x, dy: point.y)
    }
    
}

extension Array where Element: Equatable {
    
    func arrayByRemoveObjectsInArray(_ array: [Element]) -> [Element] {
        return Array(self).filter() { element in !array.contains(element) }
    }
    
}

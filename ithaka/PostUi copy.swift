//
//  PostUiController.swift
//  ithaka
//
//  Created by Apurva Raj on 17/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import ActiveLabel
import KILabel

class PostUi: UICollectionViewCell, UITextViewDelegate {
    
    var forScroll = SLPagingViewSwift2()

//    func nextCardView() -> UIView? {
//        return UIView
//    }
    
    weak var delegate: MyCustomDelegate?


    var user: UserModelNew? {
        didSet{
            guard let profileImageUrl = user?.profileImageUrl else { return }
            
            profileImageView.loadImage(urlString: profileImageUrl)

            setupAttributedDate()

        }
    }
    
    var post: PostNew? {
        didSet {
            
            guard let post = post else { return }

            guard let title = post.title else { return }
            guard let story = post.story else { return }
            guard let tag = post.tag else { return }
            
            guard let commetsCount = post.commentsCount else { return }
            guard let rocketCount = post.rocketCount else { return }

            
            

            let commentsCountString = String(commetsCount)
            
            let trailText = "...Read More"
            
            let trailTextTextForQuest = NSMutableAttributedString(string: story)
            
            trailTextTextForQuest.append(NSAttributedString(string: trailText, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))

            
            titleLabel.text = title
            storyLabel.text = story
            
            let storyHeight = story.height(withConstrainedWidth: frame.width-40, font: UIFont.systemFont(ofSize: 16))
            print("Str", storyHeight)
            if storyHeight > 160 {
                readMoreLabel.text = "Read more"
            } else {
                readMoreLabel.text = ""

            }
            
            
            storyLabel.hashtagLinkTapHandler = { label, string, range in
                print(label)

                let tag = String(string.dropFirst()).lowercased()
                self.textViewTap?.hashTagTapped(hash: tag)
            }
            
            storyLabel.userHandleLinkTapHandler = { label, string, range in
                print(string)
                let mention = String(string.dropFirst()).lowercased()
                print(mention)
                self.textViewTap?.usernameTapped(username: mention)
            }
            

            
            let attributedTextForComment = NSMutableAttributedString(string: commentsCountString, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)])
        attributedTextForComment.append(NSAttributedString(string: " Comments", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
            
            
            guard let postliked = post.isLiked else { return }

            let imageName = !postliked ? "like_unselected" : "like_selected"
            likeImageView.setImage(UIImage(named: imageName), for: .normal)
            let count = post.rocketCount
            
            if post.rocketCount == 1 {
                likeCountLabel.text = "\(count ?? 0) like"
                
            } else {
                likeCountLabel.text = "\(count ?? 0) likes"
            }
            
            commentsCountLabel.attributedText = attributedTextForComment
            
            if tag.count == 0 {
                let attributedTextForQuest = NSMutableAttributedString(string: "Quest: ", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 11)])
                attributedTextForQuest.append(NSAttributedString(string: "None", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                
                questTagLabel.attributedText = attributedTextForQuest
                

            } else {
                let attributedTextForQuest = NSMutableAttributedString(string: "Quest: ", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 11)])
                attributedTextForQuest.append(NSAttributedString(string: tag, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                
                questTagLabel.attributedText = attributedTextForQuest

            }

        }
    }
    
   
    
    weak var textViewTap: textViewTap?
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        switch URL.scheme {
        case "hash"? :
            let hashtag = URL.absoluteString.dropFirst(5)

            print("hashtag is ", hashtag)
            self.textViewTap?.hashTagTapped(hash: String(hashtag))
        case "mention"? :
            
            let username = URL.absoluteString.dropFirst(8).lowercased()

            print("username is ", username)
            self.textViewTap?.usernameTapped(username: String(username))
        default:
            print("just a regular url")
        }
        
        return true
    }
    
    fileprivate func setupAttributedDate() {
        guard let post = self.post else { return }
        
        guard let username = user?.u_username else { return }
//        guard let u_username = user?.u_username else { return }
        
//        let fusername = username ?? u_username
        
        let attributedText = NSMutableAttributedString(string: username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 16)])

//        attributedText.append(NSAttributedString(string: ", ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
//
        let timeAgoDisplay = post.creationDate?.timeAgoDisplay()
        
        timeLabel.text = timeAgoDisplay
//        attributedText.append(NSAttributedString(string: timeAgoDisplay!, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor.gray]))

        nameLabel.attributedText = attributedText
    }
  
    lazy var titleLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 19)
        textView.isUserInteractionEnabled = true
        textView.textAlignment = .left
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
        tap.numberOfTapsRequired = 1
        textView.addGestureRecognizer(tap)
        textView.sizeToFit()
        textView.isScrollEnabled = true

        return textView
    }()
    
    let newBack: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        return view
    }()
    
    @objc func titleLabelTapped() {
   //     nothing today
//        self.delegate?.onTitleLabelTapped(for: self)
    }
    
    lazy var emojiLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 17)
        textView.isUserInteractionEnabled = false
        
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        //        textView.backgroundColor = .red
        textView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        textView.textAlignment = .center
        textView.text = "😂😎🍕😄"
//        textView.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return textView
    }()

    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 30
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = UIColor.clear
        
        return imageView
    }()
    
    let profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "item_6")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 20
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        return imageView
    }()

    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(20)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .black
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        

        return label
    }()
    
    @objc func nameLabelTapped(_ recognizer: UITapGestureRecognizer) {
        guard let uid = user?.uid else { return }
        self.delegate?.onNameIdLabelTapped(userId: uid)

    }
    
    lazy var optionButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_expand_more"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOptionButton))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        button.isEnabled = true
        button.isHidden = false
        
        return button
    }()

    @objc func handleOptionButton() {
        self.delegate?.onOptionButtonTapped(for: self)
    }
    
    lazy var storyLabel: KILabel = {
        let textview = KILabel()
        textview.textColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.85)
        textview.isUserInteractionEnabled = true
        textview.numberOfLines = 0
        textview.font = UIFont(name: "HelveticaNeue", size: CGFloat(16))

        return textview
    }()
    
    lazy var readMoreLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.gray
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .right
        return label
    }()
    
    
    
    @objc func storyViewTapped(_ recognizer: UITapGestureRecognizer) {
       print("Story tv tapped")
        guard let post = post else { return }
        guard let user = user else { return }
        self.delegate?.onStoryViewTapped(user: user, post: post)
        
    }
    
    
    lazy var rocketImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "rocket_unselected"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(rocketButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)

        return button
    }()
    
//    lazy var shareImageView: UIButton = {
//        let button = UIButton(type: .custom)
//        button.setImage(#imageLiteral(resourceName: "icons8-right-2-50"), for: .normal)
//        let tap = UITapGestureRecognizer(target: self, action: #selector(shareButtonTapped))
//        tap.numberOfTapsRequired = 1
//        button.addGestureRecognizer(tap)
//
//        return button
//    }()
    
    @objc func shareButtonTapped(sender: UITapGestureRecognizer){
        self.delegate?.onShareButtonTapped(for: self)
        print("sharing")
    }

    
    lazy var swipeImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "icons8-down-arrow-filled-50"), for: .normal)
        button.tintColor = .gray
        let tap = UITapGestureRecognizer(target: self, action: #selector(swipeButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    @objc func swipeButtonTapped() {
        print("swipe")
        self.delegate?.onswipetButtonTapped(for: self)
    }
    
//
//    lazy var commentImageView: UIButton = {
//        let button = UIButton(type: .custom)
//        button.setImage(#imageLiteral(resourceName: "comment"), for: .normal)
//        button.addTarget(self, action: #selector(tapHandleComment), for: .touchUpInside)
//
//        return button
//    }()
//
    @objc func tapHandleComment() {
        print(" show comments...")
        guard let post = post else { return }
        delegate?.didTapNewComment(post: post)
    }


    
    @objc func rocketButtonTapped(sender: UITapGestureRecognizer){
        
        print("usrr id is", user?.uid)
        guard let userId = user?.uid else { return }
        let usersRef = Database.database().reference().child("users").child(userId)
        
    
        guard let username = user?.u_username ?? user?.username else { return }
        guard let postTitle = post?.title else { return }
        guard let postUserId = post?.uid else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        guard let postId = post?.id else { return }
        
        var username1: String?
        Api.User.observeCurrentUser { (usermodelnew) in
            guard let usernamenew = usermodelnew.u_username ?? usermodelnew.username else { return }
            username1 = usernamenew

        }
        
        print("user namei s", username1)
        
        let ref10 = Database.database().reference().child("notifications").child(postUserId).childByAutoId()

        guard let postliked = post?.isLiked else { return }

        if postliked == true {
            self.post?.isLiked = false
            self.post?.rocketCount = (post?.rocketCount)! - 1
            self.delegate?.onRocketButtonTapped(for: self)

            Api.NewPost.incrementLikes(userId: userId, postId: (post?.id)!, onSucess: { (post) in
                print(post)
            }, onError: { (err) in
            })
            
            usersRef.observeSingleEvent(of: .value) { (snapshot) in
                let value = snapshot.value as? NSDictionary
                var points = value?["profileProgress"] as? Int ?? 0
                //suppose point is 5 . then add =
                points = points - 1
                let tp = ["profileProgress": points]
                usersRef.updateChildValues(tp)
                
            }
            

            //remove notifcs
            
            //nothing

        } else {
            self.post?.isLiked = true
            self.post?.rocketCount = (post?.rocketCount)! + 1
            self.delegate?.onRocketButtonTapped(for: self)
            Api.NewPost.incrementLikes(userId: (user?.uid)!, postId: (post?.id)!, onSucess: { (post) in
                print(post)
            }, onError: { (err) in
            })
            
            usersRef.observeSingleEvent(of: .value) { (snapshot) in
                let value = snapshot.value as? NSDictionary
                var points = value?["profileProgress"] as? Int ?? 0
                //suppose point is 5 . then add =
                points = points + 1
                let tp = ["profileProgress": points]
                usersRef.updateChildValues(tp)
                
            }
            
            Api.User.observeUser(withId: postUserId) { (user) in
                let userbadge = user?.badge ?? 0
                let newvalue = userbadge + 1
                let bc = String(newvalue)

                let values = ["badge": newvalue]
                Api.User.REF_USERS.child(postUserId).updateChildValues(values)
                
                let username2 = username1 ?? "na"
                if uid != postUserId {
                    let notificationMessage = username2 + " sent a rocket to your story: " + postTitle
                    let notifValues = ["badge": bc, "Message": notificationMessage, "uid":postUserId, "pid":postId, "fromUserId":uid, "type":"11", "creationDate": Date().timeIntervalSince1970] as [String : Any]
                    
                    ref10.updateChildValues(notifValues)
                }

            }


        }
    }
    
    func updateLike(post: PostNew) {
        
        guard let postliked = post.isLiked else { return }
        
        let imageName = !postliked ? "rocket_unselected" : "rocket_selected"
        rocketImageView.setImage(UIImage(named: imageName), for: .normal)
        let count = post.rocketCount
        
        if post.rocketCount == 1 {
            rocketCountLabel.text = "\(count ?? 0) rocket"
            
        } else {
            rocketCountLabel.text = "\(count ?? 0) rockets"
        }

    }
    
    func tryingnew(){
        Api.NewPost.incrementLikes(userId: (user?.uid)!, postId: (post?.id)!, onSucess: { (post) in
    
            self.updateLike(post: post)
            self.post?.likes = post.likes
            self.post?.isLiked = post.isLiked
            self.post?.rocketCount = post.rocketCount
            if post.uid != Api.User.CURRENT_USER?.uid {
                let timestamp = NSNumber(value: Int(Date().timeIntervalSince1970))
                guard let postliked = post.isLiked else { return }

                if postliked {
//                    let newNotificationReference = Api.Notification.REF_NOTIFICATION.child(post.uid!).child("\(post.id!)-\(Api.User.CURRENT_USER!.uid)")
//                    newNotificationReference.setValue(["from": Api.User.CURRENT_USER!.uid, "objectId": post.id!, "type": "like", "timestamp": timestamp])
                } else {
//                    let newNotificationReference = Api.Notification.REF_NOTIFICATION.child(post.uid!).child("\(post.id!)-\(Api.User.CURRENT_USER!.uid)")
//                    newNotificationReference.removeValue()
                }
                
            }
            
        }) { (errorMessage) in
            print("error")
        }
        //incrementLikes(forRef: postRef)

    }
    lazy var rocketCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(rocketCountButtonTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)


        return label
    }()
    
    @objc func rocketCountButtonTapped() {
        print("taho")
        self.delegate?.onRocketCountButtonTapped(for: self)
    }
    
    let CommentLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textAlignment = .center
        label.textColor = .gray

        return label
    }()
    
    lazy var questTagLabel: UILabel = {
        let textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 11)
        textView.textAlignment = .left
        textView.textColor = UIColor.gray
        textView.isUserInteractionEnabled = true
        return textView
        }()
  
    let BlackLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        
        return view
    }()
    let BlueLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray

        return view
    }()
    let GrayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        return view
    }()
    
    let deepBg: UIView = {
        let view = UIView()
        return view
    }()

    

    @objc func handleCommentUpdate() {
        self.post?.commentsCount = (post?.commentsCount)! + 1
        
    }
    @objc func handleCommentDeleteUpdate() {
        self.post?.commentsCount = (post?.commentsCount)! - 1
        
    }
    lazy var timeLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 11)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .gray
        label.isUserInteractionEnabled = true
        label.text = "1 hour ago"
        return label
    }()
    
    lazy var likeImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(rocketButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)

        return button
    }()
    
    lazy var commentImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "comment"), for: .normal)
        
        return button
    }()
    
    lazy var giftImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "gift (3)"), for: .normal)
        
        return button
    }()
    
    lazy var shareImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "share"), for: .normal)
        
        return button
    }()
    
    lazy var likeCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .left
        label.isUserInteractionEnabled = true
        label.layoutIfNeeded()
        label.layoutSubviews()
        
        return label
    }()
    lazy var commentsCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        
        return label
    }()
    lazy var giftsCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.text = "7 Gifts"
        
        return label
    }()



    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.layer.cornerRadius = 15
        self.clipsToBounds = true
        
    
        NotificationCenter.default.addObserver(self, selector: #selector(handleCommentUpdate), name: CommentsController.updateCommentsCountName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleCommentDeleteUpdate), name: CommentsController.deleteCommentsCountName, object: nil)

        self.translatesAutoresizingMaskIntoConstraints = true
        self.backgroundColor = UIColor.white
        addSubview(GrayLine)
        addSubview(questTagLabel)
        self.bringSubview(toFront: questTagLabel)
        addSubview(newBack)
        addSubview(titleLabel)
        self.sendSubview(toBack: titleLabel)
        addSubview(profileImageView)
        addSubview(profileItemView)
        addSubview(nameLabel)
        addSubview(optionButton)
        addSubview(emojiLabel)
        addSubview(storyLabel)
        addSubview(readMoreLabel)
        addSubview(timeLabel)
        addSubview(BlackLine)
        
        addSubview(optionButton)
        
        addSubview(likeImageView)
        addSubview(commentImageView)
        addSubview(shareImageView)
        addSubview(giftImageView)

        addSubview(rocketCountLabel)
        addSubview(CommentLabel)
        
        addSubview(likeCountLabel)
        addSubview(commentsCountLabel)
        addSubview(giftsCountLabel)

        addSubview(shareImageView)

        
        addSubview(BlueLine)
        
        let mh = UIDevice().type
        
        print(mh.rawValue)
        
        readMoreLabel.anchor(top: nil, left: nil, bottom: storyLabel.bottomAnchor, right: storyLabel.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 85, heightConstant: 25)
        
        profileImageView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 15, bottomConstant: 10, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        
        profileItemView.anchor(top: nil, left: profileImageView.rightAnchor, bottom: profileImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: -10, bottomConstant: 3, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        
        storyLabel.anchor(top: profileImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant:160)

        
        nameLabel.anchor(top: self.topAnchor, left: profileImageView.rightAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 35, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)


        optionButton.anchor(top: nameLabel.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 30)
        timeLabel.anchor(top: nameLabel.bottomAnchor, left: nameLabel.leftAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        
        questTagLabel.anchor(top: timeLabel.bottomAnchor, left: timeLabel.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant:-5, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 30)


        likeImageView.anchor(top: storyLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        commentImageView.anchor(top: storyLabel.bottomAnchor, left: likeImageView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        shareImageView.anchor(top: storyLabel.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        
        likeCountLabel.anchor(top: likeImageView.bottomAnchor, left: likeImageView.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: -3, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)
        commentsCountLabel.anchor(top: likeCountLabel.bottomAnchor, left: likeImageView.leftAnchor, bottom: nil, right: nil, topConstant: 3, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)

        
}
   
    
    
    func LogoutbuttonTapped() {
        print(42)
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()

        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
   
}

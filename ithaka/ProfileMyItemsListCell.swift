//
//  ProfileMyItemsListCell.swift
//  ithaka
//
//  Created by Apurva Raj on 24/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

protocol profileItem2ndStageTapped: class {
    func itemTapped(itemCode: String)
}

class ProfileMyItemsListCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, profileItemList {
    func itemTapped(itemCode: String) {
        print("mc")
    }
    
    
    let itemListNames = ["name1":"boots", "name2":"key", "name3":
        "shiled", "name0":"sword"]

    
    weak var delegate: profileItem2ndStageTapped?
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = itemNameAndCount[indexPath.item]
        
        guard let name = cell.itemName else { return }
        self.delegate?.itemTapped(itemCode: name)

    }
    func itemTapped() {
        print("first stage working")
    }
    
    var itemNameAndCount = [ItemNameAndCount]()
    
    var user: UserModelNew?{
        didSet{
            guard let uid = user?.uid else { return }
            let ref = Database.database().reference().child("myItems").child(uid)
            
            ref.observeSingleEvent(of: .value) { (snapshot) in
                
                if snapshot.exists() {
                    let value = snapshot.value as! NSDictionary
                    
                    value.forEach({ (snapshot) in
                        guard let key = snapshot.key as? String else  { return }
                        
                        guard let count = snapshot.value as? Int else { return }
                        
                        let inc = ItemNameAndCount.transfrom(key: key, count: count)
                        
                        self.itemNameAndCount.append(inc)
                    })
                    
                }
                
                self.collectionView.reloadData()
            }

        }
    }
    
    var homeController: ProfilePreviewController?

    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        cv.layer.cornerRadius = 15.0
        cv.layer.shadowColor = UIColor.gray.cgColor
        cv.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        cv.layer.shadowRadius = 12.0
        cv.layer.shadowOpacity = 0.5

        cv.dataSource = self
        cv.delegate = self
        cv.layer.zPosition = 100
        cv.contentInset = UIEdgeInsetsMake(0, 15, 0, 15)
        return cv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.clear
        self.layer.cornerRadius = 15.0
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 12.0
        self.layer.shadowOpacity = 0.5

        addSubview(collectionView)
    
      collectionView.register(ProfileMyItemsIndividualCell.self, forCellWithReuseIdentifier: "ProfileMyItemsIndividualCell")
        
        collectionView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 15, leftConstant: 0, bottomConstant: 15, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        
    }
    
    var namelist = [String]()
    var countlist = [Int]()

    func fetchMyItemList(){
        print("fetch it...")
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemNameAndCount.count
    
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileMyItemsIndividualCell", for: indexPath) as! ProfileMyItemsIndividualCell
        cell.delegate = self
        cell.itemNameAndCount = itemNameAndCount[indexPath.item]

        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60, height: 80)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

protocol profileItemList: class {
    func itemTapped(itemCode: String)
}

class ProfileMyItemsIndividualCell: UICollectionViewCell {
    
    var item: UIImage? {
        didSet{
            itemView.image = item
        }
    }
    
    var items: MyItems? {
        didSet{
            
        }
    }
    
    var itemNameAndCount: ItemNameAndCount?{
        didSet{
            guard let iac = itemNameAndCount else { return }
            
            guard let name = iac.itemName else { return }
            let count = iac.itemCount
            
            let cc = count ?? 0
            let cctext = String(cc)
            
            let countText = NSMutableAttributedString(string: cctext + " ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)])

            if name.hasPrefix("ci") {
                
                let nameV = Api.Items.commonItemName[name] ?? "NA"
                
                countText.append(NSAttributedString(string: nameV, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                
                tempName.attributedText = countText
                
            }
            if name.hasPrefix("uci"){
                let nameV = Api.Items.unCommonItemsName[name] ?? "NA"
                
                countText.append(NSAttributedString(string: nameV, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                
                
                tempName.attributedText = countText
                
            }
            
            if name.hasPrefix("ri"){
                
                let nameV = Api.Items.rareItemsName[name] ?? "NA"
                
                countText.append(NSAttributedString(string: nameV, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                
                
                tempName.attributedText = countText
                
            }
            
            itemView.image = UIImage(named: name + ".png")

        }
    }
    
    
    var name: String?{
        didSet{
            
            guard let ic = name else { return }
            
            print("ic is stron", ic)
            let countText = NSMutableAttributedString(string: " ")
            

            if ic.hasPrefix("ci") {
                
                let nameV = Api.Items.commonItemName[ic] ?? "NA"
                
                countText.append(NSAttributedString(string: nameV, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
 
                tempName.attributedText = countText

            }
            if ic.hasPrefix("uci"){
                let nameV = Api.Items.unCommonItemsName[ic] ?? "NA"
                
                countText.append(NSAttributedString(string: nameV, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                

                tempName.attributedText = countText

            }
            
            if ic.hasPrefix("ri"){
                
                let nameV = Api.Items.rareItemsName[ic] ?? "NA"
                
                countText.append(NSAttributedString(string: nameV, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)]))
                

                tempName.attributedText = countText

            }
            
            itemView.image = UIImage(named: ic + ".png")

            

        }
    }
    
    var count: Int?{
        didSet{
            let cc = count ?? 0
            let cctext = String(cc)
            
            self.countLabel.text = cctext
        }
    }
    
    weak var delegate: profileItemList?
    
    
    lazy var itemView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "yellow_rune")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 30
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
//
//        let tap = UITapGestureRecognizer(target: self, action: #selector(itemTapped))
//        tap.numberOfTapsRequired = 1
//        imageView.addGestureRecognizer(tap)
//
//
        return imageView
    }()
    
    lazy var tempName: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 11)
        label.text = "None Yet"
        label.textAlignment = .center
        label.textColor = UIColor.gray
        return label
    }()
    
    lazy var countLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 11)
        label.text = "0"
        label.textColor = UIColor.gray
        return label
    }()

    
    @objc func itemTapped() {
        
//        self.delegate?.itemTapped()
    }
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(itemView)
        
        addSubview(tempName)
        itemView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        
        
        tempName.anchor(top: itemView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)

        
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

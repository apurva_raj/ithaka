//
//  HashtagList.swift
//  ithaka
//
//  Created by Apurva Raj on 23/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class HashtagList: BaseCollectionViewCell {
    
    var HashSearch: HashTagSearch? {
        didSet{
            let ns = NSMutableString(string:"#")
            ns.append((HashSearch?.tag)!)
            hashtagLabel.text = ns as String
        }
    }
    var taging: String? {
        didSet{
            
            let ns = NSMutableString(string:"#")
            ns.append(taging!)
            hashtagLabel.text = ns as String!
        }
    }

    lazy var hashtagLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.text = "#Mylife"
        textView.textAlignment = .left
        textView.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        textView.addGestureRecognizer(tap)
        

        return textView
    }()
    
    weak var delegate: hashsearch?
    
    @objc func nameLabelTapped() {
        self.delegate?.hashtap(for: self)
        
    }
    
   
    
    override func setupViews() {
        
        super.setupViews()
        self.backgroundColor = UIColor.white
        self.addSubview(hashtagLabel)
        hashtagLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 10, bottomConstant: 5, rightConstant: 0, widthConstant: 0, heightConstant: 0)

    }
    

}

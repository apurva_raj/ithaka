//
//  HomeHeader.swift
//  ithaka
//
//  Created by Apurva Raj on 07/10/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class HomeHeader: UICollectionViewCell {
    
    lazy var TitleTextView: UITextView = {
        let tv = UITextView()
        tv.textAlignment = .center
        tv.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)
        tv.textColor = .black
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.sizeToFit()
        tv.text = "Tap on the boat to start writing"
        tv.font = UIFont(name: "Helvetica", size: CGFloat(21))
        tv.isEditable = false
        tv.isScrollEnabled = false
//        tv.isScrollEnabled = false
//        tv.isHidden = true
        return tv
    }()
    
    lazy var boatImageView: UIImageView = {
        let imageview = UIImageView()
        imageview.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)
        imageview.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(starWriting))
        tap.numberOfTapsRequired = 1
        imageview.addGestureRecognizer(tap)

        return imageview
    }()
    weak var delegate: IthakaAddDelegate?

    @objc func starWriting() {
        print("cxz")
        self.delegate?.onShareTapped()
    }
    
    let GrayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        return view
    }()
    

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(boatImageView)
        addSubview(TitleTextView)
        addSubview(GrayLine)
        
        boatImageView.anchor(top: self.topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 30, rightConstant: 0, widthConstant: frame.width, heightConstant: 0)
        
        TitleTextView.anchor(top: nil, left: leftAnchor, bottom: self.bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 15, rightConstant: 0, widthConstant: 0, heightConstant: 37)
        

        
        setBoatImage()

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setBoatImage() {
        
        let avatar = ProfileEditController.defaults.integer(forKey: "Avatar")
        
        if avatar == 0 {
            Api.User.observeCurrentUser { (user) in
                guard let avatar2 = user.avatar else { return }
                
                ProfileEditController.defaults.set(avatar2, forKey: "Avatar")
                
                self.checkAvatar()
                
            }
            
        } else {
            self.checkAvatar()
        }
        
    }
    
    func checkAvatar() {
        let avatar = ProfileEditController.defaults.integer(forKey: "Avatar")
        
        
//        self.TitleTextView.isHidden = false
//        self.grayLine.isHidden = false
        
    }
}

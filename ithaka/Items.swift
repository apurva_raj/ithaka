//
//  Items.swift
//  ithaka
//
//  Created by Apurva Raj on 27/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit


class Items {
   
    var id: String?
    var name: String?
    var code: Int?
}

extension Items {
    static func transformItems(dictionary: [String: Any], key: String) -> Items {
        
        let items = Items()
        items.id = key
        items.name = dictionary["itemListNames"] as? String ?? ""
        items.code = dictionary["itemListCodes"] as? Int ?? 0
        
        return items
    }
    
}


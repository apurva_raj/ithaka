//
//  WelcomeFeelingController.swift
//  ithaka
//
//  Created by Apurva Raj on 12/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit


class WelcomeFeelingController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    let emoji = ["😊", "😞", "😨", "😡", "😯", "🤮"]
    
    let titleEmoji = ["Happy", "Sad", "Fear", "Anger", "Surprise", "Disgust"]
    
    lazy var footerLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.text = "Based on your current mood, you will be given a virtual item to get started your journey on Ithaka."
        label.font = UIFont(name: "STHeitiTC-Medium", size: CGFloat(11))
        label.sizeToFit()
        label.numberOfLines = 0
        label.padding = UIEdgeInsetsMake(15, 15, 15, 15)
        label.clipsToBounds = true
        label.layer.cornerRadius = 7
        
        return label
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationItem.leftBarButtonItem?.title = "Back"

        collectionView?.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)



        
        collectionView?.register(FeelingHeaderFile.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "FeelingHeaderFile")

        
        collectionView?.register(FeelingFile.self, forCellWithReuseIdentifier: "FeelingFile")
        collectionView?.register(FeelingFile2.self, forCellWithReuseIdentifier: "FeelingFile2")


        collectionView?.contentInset = UIEdgeInsetsMake(5, 20, 0, 20)
        collectionView?.layer.cornerRadius = 10
        
        
        view.addSubview(footerLabel)
        footerLabel.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 80)
        
        
        switch UIDevice.current.userInterfaceIdiom {
        case .pad:
            footerLabel.font = UIFont(name: "STHeitiTC-Medium", size: CGFloat(14))
        default:
            print("who are you?")
        }
        
        
        
        collectionView?.delegate = self
    }
    
    
    @objc func emojiTapped(){
        
        print("osss")
        
    }

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        switch section {
        case 0:
            return CGSize(width: view.frame.width, height: 150)

        default:
            return .zero
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        switch section {
        case 0:
            return UIEdgeInsetsMake(0, 0, 0, 0)
        default:
            return UIEdgeInsetsMake(0, 0, 0, 10)
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "FeelingHeaderFile", for: indexPath) as! FeelingHeaderFile
        
        
        return header
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeelingFile", for: indexPath) as! FeelingFile
            cell.emoji = emoji[indexPath.item]
            cell.title = titleEmoji[indexPath.item]
            
            return cell

        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeelingFile2", for: indexPath) as! FeelingFile2
            cell.emojiLabel.text = "😶"
            cell.titleLabel.text = "I don't know!"
            
            return cell

        }
    }
    
    

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0:
            return CGSize(width: view.frame.width/2 - 30, height: 100)

        default:
            return CGSize(width: view.frame.width, height: 200)

        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 6

        default:
            return 1

        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("yahho")
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeelingFile", for: indexPath) as! FeelingFile
            
            cell.whiteview.backgroundColor = .green
            cell.titleLabel.backgroundColor = .yellow
            cell.emojiLabel.backgroundColor = .purple
            
            let emojititle = emoji[indexPath.item]
            
            let welcome3Controller = Welcome3Controller()
            welcome3Controller.emoji = emojititle
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            self.navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(welcome3Controller, animated: true)
            

        default:
            let welcome3Controller = Welcome3Controller()
            welcome3Controller.emoji = "😶"
            let backItem = UIBarButtonItem()
            backItem.title = "Back"
            self.navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(welcome3Controller, animated: true)
            

        }
        
    }
    
    
}

class FeelingFile2: UICollectionViewCell {
    
    
    lazy var emojiLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 40)
        
        return label
    }()
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = UIColor.white
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.font = UIFont(name: "STHeitiTC-Medium", size: CGFloat(14))
        
        return label
    }()
    
    let whiteview: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 7
        view.clipsToBounds = true
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(emojiLabel)
        addSubview(titleLabel)
        
        addSubview(whiteview)
        
        let h = frame.width/3
        
        emojiLabel.anchor(top: safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: frame.width/2 - h/2, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width/3, heightConstant: 0)
        titleLabel.anchor(top: emojiLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: frame.width/2 - h/2, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width/3, heightConstant: 0)
        
        whiteview.anchor(top: emojiLabel.topAnchor, left: titleLabel.leftAnchor, bottom: titleLabel.bottomAnchor, right: titleLabel.rightAnchor, topConstant: -10, leftConstant: 0, bottomConstant: -10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.sendSubview(toBack: whiteview)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

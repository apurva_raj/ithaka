//
//  Notification.swift
//  ithaka
//
//  Created by Apurva Raj on 20/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import Foundation

struct Notification1 {
    
    var id: String?
    
    let user: UserModel

    let message: String
    let uid: String
    let pid: String
    let type: String
    let fromUserId: String
    let creationDate: Date

    init(user: UserModel, dictionary: [String: Any]) {
        self.user = user
        self.message = dictionary["Message"] as? String ?? ""
        self.uid = dictionary["uid"] as? String ?? ""
        self.pid = dictionary["pid"] as? String ?? ""
        self.type = dictionary["type"] as? String ?? ""
        self.fromUserId = dictionary["fromUserId"] as? String ?? ""
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)

    }
}

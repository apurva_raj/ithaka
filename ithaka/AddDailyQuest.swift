//
//  AddDailyQuest.swift
//  ithaka
//
//  Created by Apurva Raj on 10/10/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class AddDailyQuestController: UIViewController {
    
    let addnew: UITextView = {
        let tv = UITextView()
        tv.placeholder = "Believe it! Apurva"
        tv.layer.backgroundColor = UIColor.gray.cgColor
        
        return tv
    }()
    
    lazy var submit: UILabel = {
        let label = UILabel()
        label.text = "submit"
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(submitTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
    }()
    
    @objc func submitTapped(){
        
        print("osss")
        
        let quest = addnew.text ?? ""
        //add it on the data
        
        let ref = Database.database().reference().child("dailyQuest")
        let refkey = Date().timeIntervalSince1970
        let refk = Int(refkey)
        let reff = String(refk)
        let values = ["quest": quest, "creationDate": refkey] as [String : Any]
        
        ref.child(reff).updateChildValues(values)

        
        let ref23 = Database.database().reference().child("users")
        
        ref23.observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            let notificationMessage = "Today's Quest: " + quest
            print(notificationMessage)
            let cd = Date().timeIntervalSince1970

            value.forEach({ (snapshot) in
                let uid = snapshot.key
                let value = snapshot.value as? NSDictionary

                print("key is", uid)
                var badge = value?["badge"] as? Int ?? 0
                badge = badge + 1
                let bv = String(badge)
                
                let ref4 = Database.database().reference().child("notifications").child(uid as! String).childByAutoId()
                
                let notifValues = ["Message": notificationMessage, "badge":bv, "uid":uid, "fromUserId":"b5MKdFi5mkcJgQC12hEOjJOT3MB3", "type": "424223", "creationDate": cd] as [String : Any]
                ref4.updateChildValues(notifValues)

            })
        }
//        guard let postUserId = Auth.auth().currentUser?.uid else { return }
//
//        let ref4 = Database.database().reference().child("notifications").child(postUserId).childByAutoId()
//
//        //sending the notification
//
//        let notificationMessage = "Today's Quest: " + quest
//print(notificationMessage)
//        let cd = Date().timeIntervalSince1970
//
//        print(reff)
//
//        let notifValues = ["Message": notificationMessage, "badge":"4", "uid":postUserId, "fromUserId":"b5MKdFi5mkcJgQC12hEOjJOT3MB3", "type": "424223", "creationDate": cd] as [String : Any]

//    ref4.updateChildValues(notifValues)
        self.navigationController?.popToRootViewController(animated: true)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        view.addSubview(addnew)
        view.addSubview(submit)
        
        addnew.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: view.frame.height/4)
        
        submit.anchor(top: addnew.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 50, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 50)
    }
}

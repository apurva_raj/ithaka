//
//  ItemsApi.swift
//  ithaka
//
//  Created by Apurva Raj on 28/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class ItemsAPI {
    let ref = Database.database().reference().child("ItemList")
    
    
    // COMMON ITEMS DATA STARTS
    
    
    let commonItemName = ["ci1": "First Man's Bow L2",
                           "ci2": "First Man's rope",
                           "ci3": "Bronze Hook",
                           "ci4": "Mountain's Boots",
                           "ci5": "Mountain's helm",
                           "ci6": "Broom",
                           "ci7": "Jack's Gloves",
                           "ci8": "Jack's Loot",
                           "ci9": "Jack's Snake Rope",
                           "ci10": "First Man's Bow",
                           "ci11": "First Man's Golden Belt",
                           "ci12": "First Man's Spear",
                           "ci13": "First Man's Knife",
                           "ci14": "Salazar's Sword L1",
                           "ci15": "First Man's Armour",
                           "ci16": "First Man's Golden Belt",
                           "ci17": "First Man's Arrows",
                           "ci18": "Jack's Sword",
                           "ci19": "Jack's Shield",
                           "ci20": "Hakuna's Axe",
                           "ci21": "Claudia's Golden Ring",
                           "ci22": "Claudia's Silver Ring",
                           "ci23": "Pine woods",
                           "ci24": "Jack's Hammer",
                           "ci25": "The Blue Book",
                           "ci26": "The Healing Herb"]
    
    
    let commonItemsRequiredChar = ["ci1": 60,
                                           "ci2": 45,
                                           "ci3": 40,
                                           "ci4": 45,
                                           "ci5": 55,
                                           "ci6": 40,
                                           "ci7": 40,
                                           "ci8": 65,
                                           "ci9": 55,
                                           "ci10": 40,
                                           "ci11": 60,
                                           "ci12": 50,
                                           "ci13": 65,
                                           "ci14": 50,
                                           "ci15": 50,
                                           "ci16": 40,
                                           "ci17": 50,
                                           "ci18": 60,
                                           "ci19": 55,
                                           "ci20": 60,
                                           "ci21": 80,
                                           "ci22": 50,
                                           "ci23": 30,
                                           "ci24": 45,
                                           "ci25": 40,
                                           "ci26": 40]
    
    //  COMMON ITEMS DATA ENDS
    
    
    
    
    
    
    
    // UNCOMMON ITEMS DTA STARTS
    
    let unCommonItemsName = ["uci1":"Dark Moon L1",
                             "uci2":"Salazar's Sword L2",
                             "uci3":"Quelling Blade",
                             "uci4":"Silver Coins",
                             "uci5":"Salazar's Sword L3",
                             "uci6":"Ogre Axe",
                             "uci7":"Zandra's Ring",
                             "uci8":"Green Venom Staff",
                             "uci9":"Skywrath's Arrow L1",
                             "uci10":"Soulless Skeletal Chopper",
                             "uci11":"Death's Fang",
                             "uci12":"Skywrath's Arrow L2",
                             "uci13":"Skywrath's Arrow L3",
                             "uci14":"Dark Moon L2",
                             "uci15":"Ghoc's band",
                             "uci16":"Blight - Dawn of the Oracle",
                             "uci17":"Krykra's Scroll",
                             "uci18":"Aahnath Stone",
                             "uci19":"Fire",
                             "uci20":"First Man's Gold Armor",
                             "uci21":"First Man's Gold Gauntlet",
                             "uci22":"First Man's Gold Boots",
                             "uci23":"The Golden Pride",
                             "uci24":"Stout Shield",
                             "uci25":"Krykra's Diamond",
                             "uci26":"Nandaka",
                             "uci27":"Poor Man's Shield",
                             "uci28":"Chaos Knight",
                             "uci29":"Wood Guard",
                             "uci30":"Arcane Aura",
                             "uci31":"The Knowledge Project",
                             "uci32":"Book of Dragons",
                             "uci33":"Nirvana",
                             "uci34":"Unholy kris",
                             "uci35":"5 Diamond Doomblade",
                             "uci36":"Dragon's Gold Shortblade",
                             "uci37":"Shadowbite",
                             "uci38":"Blink Dagger L1",
                             "uci39":"Blink Dagger L2",
                             "uci40":"Arcane Boots",
                             "uci41":"Booster Drink L1",
                             "uci42":"Booster Drink L2",
                             "uci43":"Booster Drink L3",
                             "uci44":"Booster Drink L4",
                             "uci45":"Booster Drink L5",
                             "uci46":"Boar's Poison",
                             "uci47":"Treasure Key"]
    
    let unCommonItemsRequiredChar = ["uci1":50,
                                     "uci2":150,
                                     "uci3":100,
                                     "uci4":50,
                                     "uci5":300,
                                     "uci6":80,
                                     "uci7":100,
                                     "uci8":100,
                                     "uci9":80,
                                     "uci10":120,
                                     "uci11":85,
                                     "uci12":120,
                                     "uci13":160,
                                     "uci14":80,
                                     "uci15":60,
                                     "uci16":80,
                                     "uci17":350,
                                     "uci18":80,
                                     "uci19":60,
                                     "uci20":105,
                                     "uci21":120,
                                     "uci22":115,
                                     "uci23":240,
                                     "uci24":90,
                                     "uci25":290,
                                     "uci26":142,
                                     "uci27":60,
                                     "uci28":300,
                                     "uci29":80,
                                     "uci30":350,
                                     "uci31":300,
                                     "uci32":150,
                                     "uci33":100,
                                     "uci34":150,
                                     "uci35":130,
                                     "uci36":220,
                                     "uci37":130,
                                     "uci38":150,
                                     "uci39":180,
                                     "uci40":250,
                                     "uci41":60,
                                     "uci42":70,
                                     "uci43":90,
                                     "uci44":110,
                                     "uci45":140,
                                     "uci46":160,
                                     "uci47":142
    ]
    
    // UNCOMMON ITEMS DATA ENDS
    
    
    
    
    
    // RARE ITEMS DATA STARTS
    
    let rareItemsName = ["ri1": "The cloak of Ei'lor",
                        "ri2": "The Time Potion",
                        "ri3": "The perfume of Queen Elissa",
                        "ri4": "The True Nature Potion",
                        "ri5": "The Basatan killing hammer",
                        "ri6": "Prashu",
                        "ri7": "Cheese",
                        "ri8": "Meat",
                        "ri9": "Red Crystals of Old Island",
                        "ri10": "Cyan Crystals of Old Island",
                        "ri11":"Anorak's Scroll"]
    
    
    let rareItemsRequiredChar = ["ri1": 2000,
                                 "ri2": 2300,
                                 "ri3": 2100,
                                 "ri4": 2200,
                                 "ri5": 2800,
                                 "ri6": 3000,
                                 "ri7": 1500,
                                 "ri8": 1800,
                                 "ri9": 2100,
                                 "ri10": 1800,
                                 "ri11": 3500]

    // RARE ITEMS DATA ENDS
    
    
    
    // ULTRARARE ITEMS DATA STARTS

    let ultraRareItemsName = ["uri01": "name"]
    
    let ultraRareItemsRequiredChar = ["uri01": 100]
    
    
    //  ULTRARARE ITEMS DATA ENDS

    func catchDiscoverdItem(uid: String){
        
    }
    
    func grantItemForPostWriting(uid: String){
        
    }
    
    func saveCatchedItemsToDatabase(uid: String, itemcode: String){
        
        let ref = Database.database().reference().child("myItems").child(uid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            

            if snapshot.exists() {
                guard let value = snapshot.value as? NSDictionary else { return }

                if snapshot.hasChild(itemcode){
                    print("yes it has")
                    guard let grabThatItemCount = value[itemcode] as? Int else { return }
                    
                    let newCount = grabThatItemCount + 1
                    
                    let value = [itemcode: newCount]
                    ref.updateChildValues(value)
                    
                    print("i grabbed it, ", grabThatItemCount)
                } else  {
                    print("iashq")
                    
                    let value = [itemcode: 1]
                    ref.updateChildValues(value)
                     
                    let valueforprofile = ["profileSetItem": itemcode]
                    
                    let userref = Database.database().reference().child("users").child(uid)
                    userref.updateChildValues(valueforprofile)
                }

            } else {
                let value = [itemcode: 1]
                ref.updateChildValues(value)
                let userref = Database.database().reference().child("users").child(uid)
                let valueforprofile = ["profileSetItem": itemcode]

                userref.updateChildValues(valueforprofile)

            }
        }

    }
    
    
    func fetchItemsForAddIthaka(){
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Database.database().reference().child("myItems").child(uid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as! NSDictionary
        
            value.forEach({ (snapshot) in
                let key = snapshot.key as! String
                
                let count = snapshot.value
                
                print("key is", key)
                print("count is", count)

                if key.hasPrefix("name"){
                        print(key)
                }
            })
        }
        
    }
}

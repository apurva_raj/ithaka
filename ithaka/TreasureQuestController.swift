//
//  TreasureQuestController.swift
//  ithaka
//
//  Created by Apurva Raj on 24/09/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class TreasureQuestController: UIViewController {
    
    let addTitleLabel: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = false
        label.text = "Treasure Hunt"
        label.textAlignment = .center
        label.textColor = .gray
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        return label
        
    }()
    
    let addTitleLabel2: UILabel = {
        let label = UILabel()
        label.isUserInteractionEnabled = false
        label.text = "Coming Soon"
        label.textAlignment = .center
        label.textColor = .gray
        label.font = UIFont.systemFont(ofSize: 23)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        return label
        
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        view.addSubview(addTitleLabel)
        view.addSubview(addTitleLabel2)

        addTitleLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: (view.frame.height/2) - 150, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        addTitleLabel2.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 30, rightConstant: 20, widthConstant: 0, heightConstant: 0)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true

    }
}

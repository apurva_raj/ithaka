//
//  IthakaPoemController.swift
//  ithaka
//
//  Created by Apurva Raj on 17/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class IthakaPoemController: UIViewController {
   
    let ithakaTitle: UILabel = {
       let label = UILabel()
        label.text = "Ithaka"
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.textAlignment = .center
        label.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)

        return label
    }()
    let poemLabel: UITextView = {
        let label = UITextView()
        label.text = "\n\nAs you set out for Ithaka\nhope the voyage is a long one,\nfull of adventure, full of discovery.\nLaistrygonians and Cyclops,\nangry Poseidon—don’t be afraid of them:\nyou’ll never find things like that on your way\nas long as you keep your thoughts raised high,\nas long as a rare excitement\nstirs your spirit and your body.\nLaistrygonians and Cyclops,\nwild Poseidon—you won’t encounter them\nunless you bring them along inside your soul,\nunless your soul sets them up in front of you.\n \nHope the voyage is a long one.\nMay there be many a summer morning when,\nwith what pleasure, what joy,\nyou come into harbors seen for the first time;\nmay you stop at Phoenician trading stations\nto buy fine things,\nmother of pearl and coral, amber and ebony,\nsensual perfume of every kind—\nas many sensual perfumes as you can;\nand may you visit many Egyptian cities\nto gather stores of knowledge from their scholars.\n \nKeep Ithaka always in your mind.\nArriving there is what you are destined for.\nBut do not hurry the journey at all.\nBetter if it lasts for years,\nso you are old by the time you reach the island,\nwealthy with all you have gained on the way,\nnot expecting Ithaka to make you rich.\n \nIthaka gave you the marvelous journey.\nWithout her you would not have set out.\nShe has nothing left to give you now.\n \nAnd if you find her poor, Ithaka won’t have fooled you.\nWise as you will have become, so full of experience,\nyou will have understood by then what these Ithakas mean. \n\n -A poem by C.P. Cavafy \n\n\n\n\n\n\n\n\n\n"
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .center
        label.isEditable = false
        label.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)

        return label
    }()
    
    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        
        return button
    }()
    let oneview = UIView()

    @objc func handleDismiss(){
        dismiss(animated: true, completion: nil)
        oneview.backgroundColor = .white
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = false

    }
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)

        view.addSubview(ithakaTitle)
        view.addSubview(poemLabel)
        
        ithakaTitle.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        poemLabel.anchor(top: ithakaTitle.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: view.frame.width, heightConstant: view.frame.height)
        
        oneview.backgroundColor = .black
        
        view.addSubview(dismissButton)
        view.addSubview(oneview)
        
        dismissButton.anchor(top: nil, left: view.self.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        oneview.anchor(top: view.safeAreaLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

    }
}

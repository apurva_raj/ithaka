//
//  ProfileAPI.swift
//  ithaka
//
//  Created by Apurva Raj on 07/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseDatabase

class ProfileAPI {
    var REF_USER_POSTS = Database.database().reference().child("posts")
    
    func getRecentProfileFeed(withId uid: String, start timestamp: TimeInterval? = nil, limit: UInt, completionHandler: @escaping ([PostNew]?) -> Void) {
        
        var feedQuery = REF_USER_POSTS.child(uid).queryOrdered(byChild: "creationDate")
        
        if let latestPostTimestamp = timestamp, latestPostTimestamp > 0 {
            feedQuery = feedQuery.queryStarting(atValue: latestPostTimestamp + 1 , childKey: "creationDate").queryLimited(toLast: limit)
        } else {
            feedQuery = feedQuery.queryLimited(toLast: limit)
        }
        
        feedQuery.observeSingleEvent(of: .value) { (snapshot) in
            let items = snapshot.children.allObjects
            let myGroup = DispatchGroup()
            
            var result = [PostNew]()
            
            for (_, item) in (items as! [DataSnapshot]).enumerated() {
                myGroup.enter()
                let value = item.value as? NSDictionary
                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    
                    result.append(PostNew!)
                    
                    myGroup.leave()
                        
                })
            }
            
            myGroup.notify(queue: .main) {
                result.sort(by: {$0.timestamp! > $1.timestamp! })
                completionHandler(result)
            }
        }

    
    }

    
    func getOldFeed(withId id: String, start timestamp: TimeInterval, limit: UInt, completionHandler: @escaping ([PostNew]?) -> Void) {
        
        let feedOrderQuery = REF_USER_POSTS.child(id).queryOrdered(byChild: "creationDate")
        
        let feedLimitedQuery = feedOrderQuery.queryEnding(atValue: timestamp - 1, childKey: "creationDate").queryLimited(toLast: limit)
        
        
        
        feedLimitedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects as! [DataSnapshot]
            
            let myGroup = DispatchGroup()
            
            var result = [PostNew]()
            
            for (_, item) in items.enumerated() {
                print(item)
                
                myGroup.enter()
                let value = item.value as? NSDictionary
                Api.NewPost.observeSinglePost(withUId: id, withPid: item.key, completion: { (PostNew) in
                    
                    guard let pp = PostNew else { return }
                    print("pp is", pp.title)
                    result.append(pp)
                    
                    myGroup.leave()

                })
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
                result.sort(by: {$0.timestamp! > $1.timestamp! })
                completionHandler(result)
            })
            
        })
        
    }

}


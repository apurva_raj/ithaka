//
//  CommnetNew.swift
//  ithaka
//
//  Created by Apurva Raj on 03/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
class CommentNew {
    
    var id: String?
    var timestamp: TimeInterval?
    var text: String?
    var uid: String?
    var creationDate: Date?
    
    
    var likeCount: Int?
    var likes: Dictionary<String, Any>?
    var isLiked: Bool?

}

extension CommentNew {
    static func transformComment(dict: [String: Any], key: String) -> CommentNew {
        let comment = CommentNew()
        comment.id = key
        comment.text = dict["text"] as? String
        comment.uid = dict["uid"] as? String
        comment.timestamp = dict["creationDate"] as? TimeInterval? ?? 0
       
        let secondsFrom1970 = dict["creationDate"] as? Double ?? 0
        comment.creationDate = Date(timeIntervalSince1970: secondsFrom1970)
        comment.likeCount = dict["likeCount"] as? Int
        
        if comment.likeCount == nil {
            comment.likeCount = 0
        }
        comment.likes = dict["likes"] as? Dictionary<String, Any>
    
        
        return comment
    }
}


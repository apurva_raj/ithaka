//
//  LevelPostHeaderCell.swift
//  ithaka
//
//  Created by Apurva Raj on 02/06/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class LevelPostHeaderCell: UICollectionViewCell {
    
    
    var level: Int? {
        didSet {
            guard let levelN = level else { return }
            
            let levelString = String(levelN)
            
            let finalString = "Level " + levelString + " stories"
            
            titleLable.text = finalString
        }
    }
    
//    let levelN = level ?? 0
    
    let titleLable: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 27)
        
        return label
    }()
    
    let captionLable: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(16)
        label.text = "Following are the stories people wrote during their quest"
        label.numberOfLines = 0
        
        
        return label
    }()
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        addSubview(titleLable)
        addSubview(captionLable)
        titleLable.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        captionLable.anchor(top: titleLable.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

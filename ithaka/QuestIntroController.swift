//
//  QuestIntroController.swift
//  ithaka
//
//  Created by Apurva Raj on 23/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import SwiftMessages
import Firebase

class QuestIntroController: UIViewController {

    lazy var MainLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 26)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        textView.layoutIfNeeded()
        
        textView.text = "Ithaka Writing Quest"
        textView.textAlignment = .center
        
        return textView
    }()
    
    lazy var SecLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 18)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        textView.layoutIfNeeded()
        textView.text = "Ready to set sail? Choose your character"
        textView.textAlignment = .center
        
        
        return textView
    }()
    
    lazy var GirlImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "girl_sailerr").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .gray

        let tap = UITapGestureRecognizer(target: self, action: #selector(GirlSailerTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    @objc func GirlSailerTapped() {
        print("Woman avatar selcted")
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let ref = Database.database().reference().child("users").child(uid)
        
        let value = ["Avatar":23]
        ref.updateChildValues(value)
        
        SwiftMessages.hide()
        self.dismiss(animated: true, completion: nil)

//        self.willMove(toParentViewController: FunController())
//        self.removeFromParentViewController()
//        self.view.removeFromSuperview()
        
    }
    
    lazy var BoyImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "boy_sailer").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .gray
        let tap = UITapGestureRecognizer(target: self, action: #selector(BoySailerTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    @objc func BoySailerTapped(){
        print("Man avatar selcted")
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let ref = Database.database().reference().child("users").child(uid)
        
        let value = ["Avatar":11]
        ref.updateChildValues(value)
        SwiftMessages.hide()

        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func templateController(title: String, unselectedImage: UIImage, selectedImage: UIImage,
                                        rootViewController: UIViewController = UIViewController()) -> UINavigationController {
        let viewController = rootViewController
        let navController = UINavigationController(rootViewController: viewController)
        navController.tabBarItem.image = unselectedImage
        navController.tabBarItem.selectedImage = selectedImage
        navController.tabBarItem.title = title
        return navController
    }
    let WaterImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "water1"), for: .normal)

        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
    
        view.addSubview(MainLabel)
        view.addSubview(SecLabel)
        view.addSubview(GirlImageView)
        view.addSubview(BoyImageView)
        view.addSubview(WaterImageView)
        
        MainLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 300, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        
        SecLabel.anchor(top: nil, left: view.leftAnchor, bottom: WaterImageView.topAnchor, right: view.rightAnchor, topConstant: 10, leftConstant: 30, bottomConstant: 100, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        BoyImageView.anchor(top: nil, left: view.leftAnchor, bottom: WaterImageView.topAnchor, right: nil, topConstant: 0, leftConstant: 40, bottomConstant: -30, rightConstant: 10, widthConstant: 80, heightConstant: 100)
        
        GirlImageView.anchor(top: nil, left: nil, bottom: WaterImageView.topAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: -30, rightConstant: 35, widthConstant: 80, heightConstant: 100)
        
        WaterImageView.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: -10, bottomConstant: -15, rightConstant: -20, widthConstant: 0, heightConstant: 100)
        
        introCards()

    }
    
    fileprivate func introCards() {
        let iconText = "😍"
        let ftext = "You need to choose a character to enter the quest."
        let info = MessageView.viewFromNib(layout: .MessageView)
        info.configureTheme(.info)
        info.button?.isHidden = true
        info.configureContent(title: "Welcome to Ithaka Writing Quest", body: ftext, iconText: iconText)
        var infoConfig = SwiftMessages.defaultConfig
        infoConfig.presentationStyle = .top
        infoConfig.duration = .forever
        infoConfig.preferredStatusBarStyle = .lightContent
        
        SwiftMessages.show(config: infoConfig, view: info)


    }

    
}

//
//  MySinglePostDelegate.swift
//  ithaka
//
//  Created by Apurva Raj on 13/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit

protocol MySinglePostDelegate: class {
    func onStoryViewTapped(post: Post)
    func onloveButtonTapped(loc: CGPoint, for cell: PostSingle)
    func onTopicButtonTapped()
    func onTitleLabelTapped(for cell: PostSingle)
    func onNameLabelTapped(for cell: PostSingle)
    func didTapComment(post: Post)
    func onOptionButtonTapped(for cell: PostSingle)
    
}

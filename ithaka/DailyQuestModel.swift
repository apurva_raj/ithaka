//
//  DailyQuestModel.swift
//  ithaka
//
//  Created by Apurva Raj on 04/10/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation

class DailyQuestModel {
    
    var id: String?
    var quest: String?
    var timestamp: TimeInterval?

    init(key: String, dictionary: [String: Any]) {
        self.id = key
        self.quest = dictionary["quest"] as? String ?? ""
        self.timestamp = dictionary["creationDate"] as? TimeInterval

    }
    
}



//
//  Rocket.swift
//  ithaka
//
//  Created by Apurva Raj on 09/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation

struct Rocket {
    var pid: String?
    
    init(pid: String) {
        self.pid = pid
    }
}

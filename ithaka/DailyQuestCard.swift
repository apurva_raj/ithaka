//
//  DailyQuestCard.swift
//  ithaka
//
//  Created by Apurva Raj on 19/09/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit


protocol DailyQuestDelegate: class {
    
    func ontodaytapped()
    func onTresureTapped()
    func onTrophyTapped()
}

class DailyQuestCard: UICollectionViewCell {
    
    weak var delegate: DailyQuestDelegate?
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "Helvetica", size: CGFloat(23))
        label.numberOfLines = 0
        label.textAlignment = .left
        label.isUserInteractionEnabled = false
        label.textColor = .black
        return label
    }()
    
    lazy var TitleTextView: UITextView = {
        let tv = UITextView()
        tv.textAlignment = .center
        tv.layer.cornerRadius = 5
        tv.textColor = .black
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.sizeToFit()
        tv.text = "Or Choose A Quest"
        tv.font = UIFont(name: "Helvetica", size: CGFloat(23))
        tv.isEditable = false
        tv.isScrollEnabled = false
        tv.isHidden = true
        return tv
    }()

    
    lazy var portalImageView: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "porthole").withRenderingMode(.alwaysOriginal)
        imageview.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(portalopen))
        tap.numberOfTapsRequired = 1
        imageview.addGestureRecognizer(tap)
        
        
        return imageview
    }()
    
    
    lazy var treasureImageView: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "treasure (1)-1").withRenderingMode(.alwaysOriginal)
        imageview.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(treasureOpen))
        tap.numberOfTapsRequired = 1
        imageview.addGestureRecognizer(tap)
        
        
        return imageview
    }()
    
    @objc func treasureOpen() {
        print("ASd")
        self.delegate?.onTresureTapped()
    }
    

    
    lazy var trophyImageView: UIImageView = {
        let imageview = UIImageView()
        imageview.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(trophyOpen))
        tap.numberOfTapsRequired = 1
        imageview.addGestureRecognizer(tap)
        
        return imageview
    }()
    
    @objc func trophyOpen() {
        print("ASd")
        self.delegate?.onTrophyTapped()
    }

    
    @objc func portalopen() {
        print("ASd")
        self.delegate?.ontodaytapped()
    }
    
    let stackvv: UIStackView = {
        let view = UIStackView()
        
        return view
    }()

    let bgview: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        view.layer.borderColor  =  UIColor.black.cgColor
        view.layer.borderWidth = 1
        view.layer.shadowOpacity = 0.5
        view.layer.shadowColor =  UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1).cgColor
        view.layer.shadowRadius = 5.0
        view.layer.shadowOffset = CGSize(width:5, height: 5)
        view.layer.cornerRadius = 5

        view.layer.masksToBounds = true

        return view
    }()
    
    let latestTitle: UILabel = {
        let label = UILabel()
        label.text = "Thank you for reporting this post. We will check and will send you update within 24 hours.Thank you for reporting this post. We will check and will send you update within 24 hours"
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0

        
        return label
    }()
    
    let badgeCount: UILabel = {
        let label = UILabel()
        label.text = "12"
        label.font = UIFont.systemFont(ofSize: 14)
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.red
        label.textAlignment = .center
        label.numberOfLines = 0
        label.layer.cornerRadius = 15
        label.clipsToBounds = true
        return label
    }()
    
    let grayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.isHidden = true
        return view
    }()


    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(TitleTextView)
        
        TitleTextView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 20, rightConstant: 15, widthConstant: 0, heightConstant: 40)
        TitleTextView.textColor = .black


        
        addSubview(portalImageView)
        addSubview(trophyImageView)
        addSubview(treasureImageView)

        addSubview(grayLine)
        
        grayLine.anchor(top: TitleTextView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0.5)
        grayLine.backgroundColor = .black

        trophyImageView.anchor(top: TitleTextView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: (frame.width/2)-20, bottomConstant: 0, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        
        portalImageView.anchor(top: TitleTextView.bottomAnchor, left: nil, bottom: nil, right: trophyImageView.leftAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 42, heightConstant: 42)

        treasureImageView.anchor(top: TitleTextView.bottomAnchor, left: trophyImageView.rightAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 40, heightConstant: 40)
        


//        addSubview(bgview)
//        bgview.addSubview(latestTitle)
//        addSubview(badgeCount)
//
//        bgview.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 100)
//
//        latestTitle.anchor(top: bgview.topAnchor, left: bgview.leftAnchor, bottom: nil, right: bgview.rightAnchor, topConstant: 10, leftConstant: 20, bottomConstant: 10, rightConstant: 20, widthConstant: 0, heightConstant: 80)
//
//        badgeCount.anchor(top: topAnchor, left: nil, bottom: nil, right: bgview.rightAnchor, topConstant: 5, leftConstant: 0, bottomConstant: 0, rightConstant: 3, widthConstant: 30, heightConstant: 30)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

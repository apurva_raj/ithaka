//
//  QuestInfo.swift
//  ithaka
//
//  Created by Apurva Raj on 09/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class QuestInfo: UICollectionViewCell {
    lazy var MainLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 26)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        
        textView.layoutIfNeeded()
        
        textView.text = "Ithaka Writing Quest"
        textView.textAlignment = .center
        
        return textView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(MainLabel)
        MainLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

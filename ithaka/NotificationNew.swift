//
//  NotificationNew.swift
//  ithaka
//
//  Created by Apurva Raj on 06/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseAuth

class NotificationNew {
    
    var id: String?
    
    var message: String?
    var uid: String?
    var pid: String?
    var type: String?
    var fromUserId: String?
    var timestamp: TimeInterval?

}
extension NotificationNew {
    static func transform(dict: [String: Any], key: String) -> NotificationNew {
        let notification = NotificationNew()
        notification.id = key
        notification.message = dict["Message"] as? String ?? ""
        notification.uid = dict["uid"] as? String ?? ""
        notification.pid = dict["pid"] as? String ?? ""
        notification.type = dict["type"] as? String ?? ""
        notification.fromUserId = dict["fromUserId"] as? String ?? "b5MKdFi5mkcJgQC12hEOjJOT3MB3"
        notification.timestamp = dict["creationDate"]  as? TimeInterval? ?? 0
        
        return notification
        
    }
}



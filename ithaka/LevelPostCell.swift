//
//  LevelPostCell.swift
//  ithaka
//
//  Created by Apurva Raj on 02/06/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import SwiftMessages

class LevelPostCell: UICollectionViewCell {
    

    //who wrote it.
    //title
    //story
    //reactions comments
    //tag

    
    weak var delegate: LevelPostDelegate?
    
    var post: Post? {
        didSet {
        
            guard let title = post?.title else { return }
            guard let story = post?.story else { return }
            
           
            guard let profileImageUrl = post?.user.profileImageUrl else { return }
            guard let username = post?.user.username else { return }
            guard let tag = post?.tag else { return }

            guard let emoji = post?.emoji else { return }
            
            guard let commetsCount = post?.commentsCount else { return }
            guard let rocketCount = post?.rocketCount else { return }
            
            guard let postId = post?.id else { return }
            
            guard let currentuid = Auth.auth().currentUser?.uid else { return }
          
            questTagLabel.text = tag
            nameLabel.text = username


            let ref = Database.database().reference().child("rockets").child(postId)
            
            ref.observeSingleEvent(of: .value) { (snapshot) in
                //                let value = snapshot.value as? NSDictionary
                if snapshot.hasChild(currentuid) {
                    self.rocketImageView.setImage(#imageLiteral(resourceName: "icons8-rocket-filled-50").withRenderingMode(.alwaysOriginal), for: .normal)
                } else {
                    self.rocketImageView.setImage(#imageLiteral(resourceName: "rocket_unselected").withRenderingMode(.alwaysOriginal), for: .normal)
                }
            }
            
            let commentsCountString = String(commetsCount)
            let rokcketCountString = String(rocketCount)
            
            
            //            mainpicView.loadImage(urlString: postImageUrl)
            titleLabel.text = title
            storyLabel.text = story
            
            emojiLabel.text = emoji
            
            CommentLabel.text = commentsCountString
            rocketCountLabel.text = rokcketCountString
            
            //            nameLabel.text = username
            profileImageView.loadImage(urlString: profileImageUrl)
            
            setupAttributedDate()
            
        }

    }
    
    fileprivate func setupAttributedDate() {
        guard let post = self.post else { return }
        
        let attributedText = NSMutableAttributedString(string: post.user.username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
        
        attributedText.append(NSAttributedString(string: "  ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        
        let timeAgoDisplay = post.creationDate.timeAgoDisplay()
        attributedText.append(NSAttributedString(string: timeAgoDisplay, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor.gray]))
        
        //        nameLabel.attributedText = attributedText
    }
    
    //    lazy var ss: UIScrollView = {
    //        let ss = UIScrollView()
    //        ss.isScrollEnabled
    //    }()
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 19)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .black
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        label.text = "Apurva Raj"
        
        return label
    }()
    
    lazy var questTagLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textAlignment = .center
        label.textColor = .white
        label.text = "here is the task given pleas edoasdasdasdasdasdasdsadsadasdasdasdasdasdaads sadajskdjasnda"
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0

        label.isUserInteractionEnabled = false
        label.backgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        
        return label
    }()
    
    @objc func nameLabelTapped(_ recognizer: UITapGestureRecognizer) {
                self.delegate?.onNameLabelTapped(for: self)
    }
    lazy var titleLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 18)
        textView.isUserInteractionEnabled = true
        //        textView.sizeToFit()
        textView.isScrollEnabled = true
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        //        textView.backgroundColor = .red
        textView.contentInset = UIEdgeInsets(top: 5, left: 20, bottom: 5, right: 5)
        //        textView.textAlignment = .center
        
        textView.text = "here is the longest text possible. is it but reallY? you are fucked up here"
        textView.layoutIfNeeded()
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
        tap.numberOfTapsRequired = 1
        textView.addGestureRecognizer(tap)
        
        
        return textView
    }()
    
    
    @objc func titleLabelTapped() {
        self.delegate?.onTitleLabelTapped(for: self)
    }
    
    lazy var emojiLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 18)
        textView.isUserInteractionEnabled = false
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        //        textView.backgroundColor = .red
        textView.contentInset = UIEdgeInsets(top: 0, left: 5, bottom: 5, right: 5)
        textView.textAlignment = .center
        textView.text = "😂😎🍕😄"
        
        return textView
    }()
    
    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
  
    lazy var storyLabel: UITextView = {
        let textview = UITextView()
        textview.textColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.85)
        textview.font = UIFont.systemFont(ofSize: 17)
        textview.isUserInteractionEnabled = true
        textview.isEditable = false
        textview.isScrollEnabled = false
        textview.sizeToFit()
        textview.translatesAutoresizingMaskIntoConstraints = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
        tap.numberOfTapsRequired = 1
        textview.addGestureRecognizer(tap)
        return textview
    }()
    
    
    
    @objc func storyViewTapped(_ recognizer: UITapGestureRecognizer) {
        print("Story tv tapped")
        guard let post = post else { return }
        
//        self.delegate?.onStoryViewTapped(post: post)
        
    }
    
    
    
    
    
    @objc func handleComment() {
        print("Trying to show comments...")
        guard let post = post else { return }
        
        self.delegate?.didTapComment(post: post)
    }
    
    lazy var rocketImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "rocket_unselected"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(rocketButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    
    lazy var commentImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "comment"), for: .normal)
        button.addTarget(self, action: #selector(handleComment), for: .touchUpInside)
        
        return button
    }()
    
    
    @objc func rocketButtonTapped(sender: UITapGestureRecognizer){
        self.delegate?.onRocketButtonTapped(for: self)
        print("flyyy")
        
    }
    
    let rocketCountLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textColor = .gray
        label.textAlignment = .center
        label.text = "23"

        return label
    }()
    let CommentLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(14)
        label.textAlignment = .center
        label.textColor = .gray
        label.text = "12"
        return label
    }()
    
    let bottomDividerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return view
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //        backgroundColor = UIColor.rgb(red: 232, green: 126, blue: 4, alpha: 0.5)
        addSubview(questTagLabel)
        addSubview(titleLabel)
        
        addSubview(profileImageView)
        addSubview(emojiLabel)
        addSubview(storyLabel)
        
        
        addSubview(rocketImageView)
        addSubview(commentImageView)
        
        addSubview(rocketCountLabel)
        addSubview(CommentLabel)
        
        
        addSubview(bottomDividerView)
        //        let dpl = (frame.width/2)-12.5
        self.bringSubview(toFront: bottomDividerView)
        
        
        questTagLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant:0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        profileImageView.anchor(top: questTagLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 20, heightConstant: 20)

        titleLabel.anchor(top: questTagLabel.bottomAnchor, left: profileImageView.rightAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: -15, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 40)

        
        emojiLabel.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: -10, leftConstant: 10, bottomConstant: 5, rightConstant: 10, widthConstant: 0, heightConstant: 30)
        
        storyLabel.anchor(top: emojiLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 190)
        
        let wh = (frame.width*0.20)
        commentImageView.anchor(top: storyLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant:-10, leftConstant: wh, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        rocketImageView.anchor(top: storyLabel.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant:-10, leftConstant: 0, bottomConstant: 0, rightConstant: wh, widthConstant: 0, heightConstant: 30)
        
        CommentLabel.anchor(top: rocketImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: wh+5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        rocketCountLabel.anchor(top: rocketImageView.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: wh+10, widthConstant: 0, heightConstant: 0)
        
        bottomDividerView.anchor(top: CommentLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 3, leftConstant: 0, bottomConstant: -20, rightConstant: 0, widthConstant: 0, heightConstant: 20)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

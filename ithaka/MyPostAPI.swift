//
//  MyPostAPI.swift
//  ithaka
//
//  Created by Apurva Raj on 05/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class MyPostAPI {
    var REF_MYPOSTS = Database.database().reference().child("posts")
   
    func fetchMyPosts(userId: String, completion: @escaping (String) -> Void) {
        REF_MYPOSTS.child(userId).observe(.childAdded, with: {
            snapshot in
            completion(snapshot.key)
        })
    }
    
    func fetchCountMyPosts(userId: String, completion: @escaping (Int) -> Void) {
        REF_MYPOSTS.child(userId).observe(.value, with: {
            snapshot in
            let count = Int(snapshot.childrenCount)
            completion(count)
        })
    }
    
    var completedVar: String?
    
    func publishMyStory(storytext: String, itemcode: String?, wonItemCode: String, questTag: String?, questId: String?, questObjId: String?, level: Int?, username: String?, completionHandler: @escaping (String?) -> Void ){
        
        print("won item is", wonItemCode)
        let itemcode = itemcode ?? "na"
        
        guard let newPostId = Api.NewPost.REF_POSTS.childByAutoId().key else { return }
        let cd = Date().timeIntervalSince1970
        
        guard let uid = Auth.auth().currentUser?.uid else { return }

        let story = storytext
       
        let storywords = story.components(separatedBy: CharacterSet(charactersIn: "#@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_").inverted)
        
        
        // adding to rocket tabel
        
        let rocketValue = ["rocketCount": 0]
        Database.database().reference().child("rockets").child(newPostId).updateChildValues(rocketValue)
        
        // adding post data to post tabel
        let userPostRef = Database.database().reference().child("posts").child(uid)
        let ref = userPostRef.child(newPostId)
        let ref3 = Database.database().reference().child("users").child(uid)
        
        
        let values = ["story": story,"itemcode": itemcode, "wonItemCode": wonItemCode, "rocketCount": 0, "creationDate": cd, "questTag": questTag ?? "", "questId": questId ?? "", "uid": uid] as [String : Any]
        
        ref.updateChildValues(values)
        
        
        // add it to admin table
        
        let adminref = Database.database().reference().child("AllPostsForAdmin")
        
        let valueForAdminRef = ["creationDate": cd, "uid": uid] as [String : Any]
    adminref.child(newPostId).updateChildValues(valueForAdminRef)

        // Remove one item form the list

        let myItemRef = Database.database().reference().child("myItems").child(uid)
        myItemRef.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists(){
                guard let value = snapshot.value as? NSDictionary else { return }
                if snapshot.hasChild(itemcode){
                    guard let grabThatItemCount = value[itemcode] as? Int else { return }
                    
                    let newCount = grabThatItemCount - 1
                    
                    if newCount == 0 {
                        print("itemcode is" , itemcode)
                        myItemRef.child(itemcode).removeValue(completionBlock: { (err, reff) in
                            
                            print("Err is ", err)
                            print("reff is ", reff)
                        })

                    } else {
                        let value = [itemcode: newCount]
                        myItemRef.updateChildValues(value)

                    }


                }
            }
        }

        
        // add it to quest table
        print("questag is ", questTag)
        if questTag != nil {
            if questTag != "" {
                let questfeedref = Database.database().reference().child("AllPostsViaQuests")
                questfeedref.child(newPostId).updateChildValues(valueForAdminRef)


            }
        }
        
        // Adding hashtag entry to database
        
        let hashrefs = Database.database().reference().child("HashtagsSearch")
        
        for var storyword in storywords {
            if storyword.hasPrefix("#") {
                storyword = storyword.trimmingCharacters(in: CharacterSet.punctuationCharacters).lowercased()
                print(storyword)
                let hashref = Database.database().reference().child("Hashtags").child(storyword).child(newPostId)
                let hashtagvalue = ["uid": uid , "creationDate":cd] as [String : Any]
                let hhs = ["tag": storyword]
                
                hashref.updateChildValues(hashtagvalue)
                hashrefs.child(storyword).updateChildValues(hhs)
                
            }
        }
        
        // adding post in auth users feed
        Api.Feed.REF_FEED.child(uid).child(newPostId).setValue(["uid":uid, "creationDate": cd])


        // adding post in every followers feed
        let followref = Database.database().reference().child("following").child(uid)
        followref.observeSingleEvent(of: .value, with: { (snapshot) in
            let arraySnapshot = snapshot.children.allObjects as! [DataSnapshot]
            arraySnapshot.forEach({ (child) in
                Api.Feed.REF_FEED.child(child.key).child(newPostId).setValue(["uid":uid, "creationDate": cd])
            })
        })
        
        // adding post to quest feed
        let questObjId = questObjId ?? ""
        
        let questTitleId = questId ?? ""
        
        if questTitleId.count == 0 {
            print("bye bye")
            
        } else {
            let QuestTagValue = ["uid": uid, "creationDate":cd] as [String : Any]
            let questFeedREF = Database.database().reference().child("questFeed").child(questTitleId).child(newPostId)
            
            questFeedREF.updateChildValues(QuestTagValue)
            
        }

        
        //adding quest data to questuserdata
        
        //        levelNumber
        //        questobjid
        //        questiD
        var mainp = 0
        
        let questLevel = level ?? 0
        
        //        let qlevelstring = String(questLevel)
        
        print(questTitleId,questObjId,questLevel)
        if questTitleId.count == 0 {
            print("waay good")
            
        } else {
            let value_quest = [questObjId:true]
            let levelnumber = "level\(questLevel)"
            
            let refff = Database.database().reference().child("QuestUserData").child(uid).child(questTitleId)
            
            let ref_quest = Database.database().reference().child("QuestUserData").child(uid).child(questTitleId).child(levelnumber)
            
            refff.observeSingleEvent(of: .value) { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                var presentLevel = 1
                if value == nil {
                    presentLevel =  1
                } else {
                    presentLevel = value!["PresentLevel"] as? Int ?? 1
                    
                }
                
                
                //okay this should work
                ref_quest.observeSingleEvent(of: .value, with: { (snapshot) in
                    if snapshot.exists() {
                        guard let refvalue = snapshot.value as? NSDictionary else { return }
                        
                        print("count is sdsa, ", refvalue.count)
                        if refvalue.count == 2 {
                            
                            if presentLevel == 3 {
                                mainp = 23
                                ref_quest.updateChildValues(value_quest)
//                                self.congretsCard1()
                                
                                let userref = Database.database().reference().child("users").child(uid)
                                
                                let vvmm = ["questComepleted": true]
                                
                                userref.updateChildValues(vvmm)
                                
                                UserDefaults.standard.set(23, forKey: "questCompleted")  //Integer
                                
                                print("dabbu print qiuest set doner")

                            } else {
                                let valuefornumber = ["PresentLevel": presentLevel + 1]
                                refff.updateChildValues(valuefornumber)
                                ref_quest.updateChildValues(value_quest)
//                                self.congretsCard()
                                
                            }
                            
                        } else {
                            
                            let valuefornumber = ["PresentLevel": presentLevel]
                            refff.updateChildValues(valuefornumber)
                            ref_quest.updateChildValues(value_quest)
                            
                        }
                        
                    } else {
                        let valuefornumber = ["PresentLevel": presentLevel]
                        refff.updateChildValues(valuefornumber)
                        ref_quest.updateChildValues(value_quest)
                        
                    }
                })
                
                
                
            }
            
        }

        
        //updating total post count
        
        let refcountall = Database.database().reference().child("allpostcount")
        
        refcountall.observeSingleEvent(of: .value) { (snapshot) in
            
            guard let value = snapshot.value as? NSDictionary else { return }
            
            var allcount = value["count"] as? Int ?? 0
            
            allcount = allcount + 1
            
            let ccvalue = ["count": allcount]
            refcountall.updateChildValues(ccvalue)
            
            
        }
        
        
        //adding total post count to auth user table
        
        ref3.observeSingleEvent(of: .value, with: { (snapshot) in
            let value = snapshot.value as? NSDictionary
            
            let cc = value?["postCount"] as? Int ?? 0
            let count = cc + 1
            
            
            let tp = ["postCount": count]
            
            ref3.updateChildValues(tp)

            
            // adding points to my profile

            Api.PointsAPII.increasePoints(n_points: 0, UserId: uid, catagory: 3)

            
            
//            let badge = value?["badge"] as? Int ?? 0
//            let bv = badge + 1
//
//            let bcc = String(bv)
//            if mainp == 23 {
//                var questcount = value?["questCompleted"] as? Int ?? 0
//
//                questcount = questcount + 1
//                points = points + 110
//
//            } else {
//                points = points + 10
//                let tp = ["profileProgress": points, "postCount": count, "badge":bv]
//                ref3.updateChildValues(tp)
//
//            }
            
            
            //         Sending user self notification - dont do that. its stupid
            
            
            
        })
        
        // sending mention notification
        
        let username = username ?? ""
        
        let ref_username = Database.database().reference().child("users")
        
        for var storyword in storywords {
            if storyword.hasPrefix("@") {
                storyword = storyword.trimmingCharacters(in: CharacterSet.punctuationCharacters).lowercased()
                print("the word is,", storyword)
                if storyword.count != 0 {
                    ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
                        
                        if snapshot.exists() {
                            
                            Api.User.observeUserByUsername(username: storyword, completion: { (usermodelnew) in
                                
                                guard let uuid = usermodelnew.uid else { return }
                                
                                if uuid == uid {
                                    
                                    print("bye bye")
                                } else {
                                    let notificationMessage = username + " has mentioned you in post: " + story.prefix(40)
                                    
                                    let notifValues = ["Message": notificationMessage, "uid":uuid, "fromUserId":uid, "pid":newPostId, "type":"42", "creationDate": cd] as [String : Any]
                                    
                                    let ref10 = Database.database().reference().child("notifications").child(uuid).childByAutoId()
                                    
                                    ref10.updateChildValues(notifValues)
                                }
                                
                            })
                            
                            
                        } else {
                            print("north remembers")
                            
                        }
                    }
                    
                }
                
            }
        }
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                completionHandler("uploaded")

            } else {
                completionHandler("not_uploaded")

            }
        }


    }
}

//
//  AddIthakaStartCell.swift
//  ithaka
//
//  Created by Apurva Raj on 29/09/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//


import UIKit

class AddIthakaStartCell: UICollectionViewCell, UITextViewDelegate {
    
    lazy var emoView: UILabel = {
        let tv = UILabel()
        tv.font = UIFont.systemFont(ofSize: 23)
        tv.layer.borderColor = UIColor.gray.cgColor
        tv.layer.borderWidth = 1
        tv.layer.cornerRadius = 5
        tv.isUserInteractionEnabled = true
        tv.backgroundColor = .white
        tv.text = "masclasknfkjdasnfkjdsaf"
        
        return tv
    }()
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        addSubview(emoView)
        
        emoView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 40)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


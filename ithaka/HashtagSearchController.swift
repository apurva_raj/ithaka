//
//  HashtagSearchController.swift
//  ithaka
//
//  Created by Apurva Raj on 21/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase


protocol hashsearch: class {
    func hashtap(for cell: HashtagList)
}

class HashtagSearchController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, hashsearch {
    func hashtap(for cell: HashtagList) {
        let hh = HashTagPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }

        let hash = hashtags[indexPath.item]
        hh.hashtag = hash.tag
        
        self.navigationController?.pushViewController(hh, animated: true)
        
    }
    
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    lazy var searchBar: UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "Search"
        sb.sizeToFit()
        sb.delegate = self
        return sb
    }()
    let cellId = "cellId"

    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: view.frame.height/2 - 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        collectionView?.backgroundColor = .white
        
        navigationItem.titleView = searchBar
        collectionView?.register(HashtagList.self, forCellWithReuseIdentifier: cellId)
        
        searchBar.delegate = self
        searchBar.becomeFirstResponder()
        collectionView?.isUserInteractionEnabled = true
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Wwow")
        if searchText.isEmpty {
            
        } else {
            let text = searchText.lowercased()
            if text.hasPrefix("#") {
                let text1 = text.dropFirst()
                startSearch(text: String(text1))

            } else {
                startSearch(text: text)

            }
            
        }
        
        self.collectionView?.reloadData()
        
    }
    
    
    func startSearch(text: String) {
        self.myActivityIndicator.startAnimating()
        let ref = Database.database().reference().child("HashtagsSearch").queryOrdered(byChild: "tag").queryStarting(atValue: text).queryEnding(atValue: text+"\u{f8ff}")

        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            self.hashtags.removeAll()

            ref.observeSingleEvent(of: .value) { (snapshot) in
                
                
                guard let dictionaries = snapshot.value as? [String: Any] else { return }
                
                dictionaries.forEach({ (key, value) in
                    guard let userDictionary = value as? [String: Any] else { return }
                    
                    print("me hu na", userDictionary)
                    
                    let supertags = HashTagSearch(key: key)
                    self.hashtags.append(supertags)
                })
                self.myActivityIndicator.stopAnimating()
                self.collectionView?.reloadData()

            }

        }
        
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return hashtags.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! HashtagList
        cell.HashSearch = hashtags[indexPath.item]
        cell.backgroundColor = .white
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    var hashtags = [HashTagSearch]()
    


}

class hashTagSearchList: UICollectionViewCell {
    
    
    var HashSearch: HashTagSearch? {
        didSet{
            let ns = NSMutableString(string:"#")
            guard let tag = HashSearch?.tag else { return }
            ns.append(tag)
            hashtagLabel.text = ns as String
        }
    }
    
    lazy var hashtagLabel: UILabel = {
        let textView = UILabel()
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.isUserInteractionEnabled = true
        //        textView.isScrollEnabled = false
        //        textView.isEditable = false
        textView.text = "#Mylife"
        textView.textAlignment = .left
        //        textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5)
        return textView
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.addSubview(hashtagLabel)
        hashtagLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


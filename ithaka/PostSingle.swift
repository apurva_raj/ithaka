//
//  PostSingle.swift
//  ithaka
//
//  Created by Apurva Raj on 13/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//


import UIKit
import FirebaseAuth

class PostSingle: UICollectionViewCell {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: self)
            print(currentPoint)
            // do something with your currentPoint
        }
        
    }
    weak var delegate: MySinglePostDelegate?
    var post: Post? {
        didSet {
            guard let postImageUrl = post?.imageUrl else { return }
            guard let title = post?.title else { return }
            guard let story = post?.story else { return }
            
            guard let profileImageUrl = post?.user.profileImageUrl else { return }
            guard let username = post?.user.username else { return }
            
            
            mainpicView.loadImage(urlString: postImageUrl)
            titleLabel.text = title
            storyLabel.text = story
            
            nameLabel.text = username
            profileImageView.loadImage(urlString: profileImageUrl)
            let ptag = postImageUrl.characters.count
            mainpicView.tag = ptag
            print("why",ptag)
            setupAttributedDate()
            
        }
    }
    
    fileprivate func setupAttributedDate() {
        guard let post = self.post else { return }
        
        let attributedText = NSMutableAttributedString(string: post.user.username, attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 14)])
        
        attributedText.append(NSAttributedString(string: "  ", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        
        let timeAgoDisplay = post.creationDate.timeAgoDisplay()
        attributedText.append(NSAttributedString(string: timeAgoDisplay, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13), NSAttributedStringKey.foregroundColor: UIColor.gray]))
        
        nameLabel.attributedText = attributedText
    }
    
    var cHeight: CGFloat?


    lazy var titleLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 16)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        
        textView.layoutIfNeeded()
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
        tap.numberOfTapsRequired = 1
        textView.addGestureRecognizer(tap)
        
        
        return textView
    }()
    
    
    @objc func titleLabelTapped() {
        self.delegate?.onTitleLabelTapped(for: self)
    }
    
    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 12.5
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    
    lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(13)
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(nameLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        return label
    }()
  

    
    @objc func nameLabelTapped(_ recognizer: UITapGestureRecognizer) {
        print("DSaasdasd")
        self.delegate?.onNameLabelTapped(for: self)
    }
    
    lazy var optionButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_expand_more"), for: .normal)
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleOptionButton))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    @objc func handleOptionButton() {
        self.delegate?.onOptionButtonTapped(for: self)
    }
    
    lazy var storyLabel: UITextView = {
        let textview = UITextView()
        textview.font = UIFont.systemFont(ofSize: 17)
        textview.textColor = .white
        
        textview.isUserInteractionEnabled = true
        textview.isEditable = false
        textview.isScrollEnabled = false
        textview.sizeToFit()
        textview.translatesAutoresizingMaskIntoConstraints = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(storyViewTapped))
        tap.numberOfTapsRequired = 1
        textview.addGestureRecognizer(tap)
        
        return textview
    }()
    
    @objc func storyViewTapped(_ recognizer: UITapGestureRecognizer) {
        print("Story tv tapped")
        guard let post = post else { return }
        
        self.delegate?.onStoryViewTapped(post: post)
        
    }
    
    let mainpicView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    
    @objc func handleComment() {
        print("Trying to show comments...")
        guard let post = post else { return }
        
        delegate?.didTapComment(post: post)
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(titleLabel)
        
        addSubview(profileImageView)
        addSubview(nameLabel)
        addSubview(optionButton)
        addSubview(storyLabel)
        
        addSubview(mainpicView)
        
        
        titleLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: -3, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        profileImageView.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 25, heightConstant: 25)
        nameLabel.anchor(top: titleLabel.bottomAnchor, left: profileImageView.rightAnchor, bottom: nil, right: nil, topConstant: 1, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 200, heightConstant: 20)
        optionButton.anchor(top: nameLabel.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        storyLabel.anchor(top: profileImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)

        
        mainpicView.anchor(top: storyLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width, heightConstant: frame.width)
        
        
    }
    
    func LogoutbuttonTapped() {
        print(42)
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
    }
    
 
    
    
    @objc func rocketButtonTapped(sender: UITapGestureRecognizer){
        print("flyyy")
        
    }
    
    @objc func loveButtonTapped(sender: UITapGestureRecognizer){
        //        var loc : CGPoint = sender.location(in: sender.view)
        guard let location : CGPoint = sender.view?.superview?.superview?.frame.origin else { return }
        let loc = convert(location, to: nil)
        
        print("there", loc)
        
        
        self.delegate?.onloveButtonTapped(loc: loc, for: self)
    }
    
    
    func sampleTap(){
        print(123)
    }
   
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}


//
//  MainTabBarController.swift
//  ithaka
//
//  Created by Apurva Raj on 17/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import VegaScrollFlowLayout


class MainTabBarController: UITabBarController, UITabBarControllerDelegate{
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.tabBar.isTranslucent = false
    
        if Auth.auth().currentUser != nil {
            
            guard let uid = Auth.auth().currentUser?.uid else { return }
            let ref = Database.database().reference().child("users").child(uid)

            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                
                let value = snapshot.value as? NSDictionary
                
                let avatar = value?["Avatar"] as? Int ?? 0
                let username = value?["u_username"] as? String ?? ""

                if avatar == 0 {
                    let questIntroController = QuestIntroController()
                    questIntroController.popoverPresentationController?.sourceView = self.view
                    questIntroController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                    
                    questIntroController.popoverPresentationController?.permittedArrowDirections = []

                    self.present(questIntroController, animated: true, completion: nil)
                }
                
                if username == "" {
                    let userselectController = UserNameSelectionController()
                    userselectController.popoverPresentationController?.sourceView = self.view
                    userselectController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                    
                    userselectController.popoverPresentationController?.permittedArrowDirections = []
                    
                    self.present(userselectController, animated: true, completion: nil)

                }
                

            })
            
            setupViewControllers()
            return
        } else {
            //show if not logged in
            DispatchQueue.main.async {
                let welcomeController = Welcome1Controller()
                
                let navController = UINavigationController(rootViewController: welcomeController)
                self.selectedIndex = 0
                self.present(navController, animated: true, completion: nil)
            }
        }
        self.tabBarController?.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
       
        
        guard let tabViewControllers = tabBarController.viewControllers, let toIndex = tabViewControllers.index(of: viewController) else {
            return false
        }
        animateToTab(toIndex: toIndex)
        return true
    }
    
    func animateToTab(toIndex: Int) {
        guard let tabViewControllers = viewControllers,
            let selectedVC = selectedViewController else { return }
        
        guard let fromView = selectedVC.view,
            let toView = tabViewControllers[toIndex].view,
            let fromIndex = tabViewControllers.index(of: selectedVC),
            fromIndex != toIndex else { return }
        
        
        // Add the toView to the tab bar view
        fromView.superview?.addSubview(toView)
        
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width
        let scrollRight = toIndex > fromIndex
        let offset = (scrollRight ? screenWidth : -screenWidth)
        toView.center = CGPoint(x: fromView.center.x + offset, y: toView.center.y)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: 0.3,
                       delay: 0.0,
                       usingSpringWithDamping: 1,
                       initialSpringVelocity: 0,
                       options: .curveEaseOut,
                       animations: {
                        // Slide the views by -offset
                        fromView.center = CGPoint(x: fromView.center.x - offset, y: fromView.center.y)
                        toView.center = CGPoint(x: toView.center.x - offset, y: toView.center.y)
                        
        }, completion: { finished in
            // Remove the old view from the tabbar view.
            fromView.removeFromSuperview()
            self.selectedIndex = toIndex
            self.view.isUserInteractionEnabled = true
        })
    }

    
    @objc func homeTapped() {
        print("my my my")
    }
    
    
    var tapCounter : Int = 0
    weak var myDelegate: MyCustomDelegate?

    
    
func setupViewControllers() {

    let homeController = templateController(title: "Home", unselectedImage: #imageLiteral(resourceName: "icons8-home-50"), selectedImage: #imageLiteral(resourceName: "icons8-home-filled-50"),rootViewController: HomeController(collectionViewLayout: UICollectionViewFlowLayout()))
    
    let tap = UITapGestureRecognizer(target: self, action: #selector(homeTapped))
    tap.numberOfTapsRequired = 2
    

    let exploreController = templateController(title: "Explore", unselectedImage: #imageLiteral(resourceName: "icons8-search-filled-50"), selectedImage: #imageLiteral(resourceName: "search_selected"),rootViewController: ExploreController(collectionViewLayout: UICollectionViewFlowLayout()))
    
    
    
    let notificationController = templateController(title: "Notification", unselectedImage: #imageLiteral(resourceName: "icons8-rocket-50 (5)"), selectedImage: #imageLiteral(resourceName: "icons8-rocket-filled-50"),rootViewController: NotificationController(collectionViewLayout: UICollectionViewFlowLayout()))
    
    notificationController.tabBarItem.badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)

    
    let profileController = templateController(title: "Profile", unselectedImage: #imageLiteral(resourceName: "icons8-happy-50"), selectedImage: #imageLiteral(resourceName: "icons8-happy-filled-50"),rootViewController: ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout()))
  
    let questController = templateController(title: "Quest", unselectedImage: #imageLiteral(resourceName: "sailing-boat"), selectedImage: #imageLiteral(resourceName: "sailing-boat").withRenderingMode(.alwaysOriginal), rootViewController: IthakaQuestAddFInalController(collectionViewLayout: UICollectionViewFlowLayout()))

    viewControllers = [homeController, exploreController, questController, notificationController, profileController]
    tabBar.tintColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
    
    
    guard let items = tabBar.items else { return }
    
    for item in items {
        item.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
     
        }
    }
    
    
    fileprivate func templateController(title: String, unselectedImage: UIImage, selectedImage: UIImage,
                                        rootViewController: UIViewController = UIViewController()) -> UINavigationController {
        let viewController = rootViewController
        let navController = UINavigationController(rootViewController: viewController)
        navController.tabBarItem.image = unselectedImage
        navController.tabBarItem.selectedImage = selectedImage
        navController.tabBarItem.title = title
        
        return navController
    }
    
}

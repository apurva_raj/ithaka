//
//  MonsterBattleFeedController.swift
//  ithaka
//
//  Created by Apurva Raj on 28/02/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit

class MonsterBattleFeedController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.view.backgroundColor = UIColor.yellow
        
        navigationItem.title = "Monster Battle Feed"
    }
    
}

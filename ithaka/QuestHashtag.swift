//
//  QuestHashtag.swift
//  ithaka
//
//  Created by Apurva Raj on 10/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase

class QuestHashtag: UICollectionViewCell, UITextViewDelegate {
    
    
    weak var textViewTap: textViewTap?
    
    func fetchline() {
        let refs = Database.database().reference().child("Hashtagoftheday").observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            let fline = value?["fline"] as? String ?? ""
            let sline = value?["sline"] as? String ?? ""
            let tline = value?["tline"] as? String ?? ""
            
            print("First ", fline)
            print("First ", sline)
            print("First ", tline)
            
            let attributedstring = NSMutableString(string: fline)
            attributedstring.append(" #")
            attributedstring.append(sline)
            attributedstring.append(" ")
            attributedstring.append(tline)
            
            self.TextLabel.text = attributedstring as String!
            
            self.TextLabel.resolveHashTags()
        }
    }
    
    lazy var TextLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 16)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        textView.layoutIfNeeded()
        
        textView.textAlignment = .left
        textView.backgroundColor = UIColor.white
        return textView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.red
        
        TextLabel.delegate = self
        addSubview(TextLabel)
        TextLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
        
        fetchline()
    }
    
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        switch URL.scheme {
        case "hash"? :
            let hashtag = URL.absoluteString.dropFirst(5)
            
            print("hashtag is ", hashtag)
            self.textViewTap?.hashTagTapped(hash: String(hashtag))
      
        default:
            print("just a regular url")
        }
        
        return true
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//
//  TopicController.swift
//  ithaka
//
//  Created by Apurva Raj on 19/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit

class TopicController: UICollectionViewController, UICollectionViewDelegateFlowLayout  {
    
    let postCellId = "postCellId"

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white
        navigationItem.title = "TopicController"
        
        collectionView?.register(FunHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        
        collectionView?.register(PostUi.self, forCellWithReuseIdentifier: postCellId)

    }
    
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath)
        return header
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let height = (view.frame.height)/2

        return CGSize(width: view.frame.width, height:height)
    }

    //showing top posts
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postCellId, for: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let width = view.frame.width
//        let height = view.frame.height
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    
}

//
//  AddIthakaNewCell.swift
//  ithaka
//
//  Created by Apurva Raj on 30/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class AddIthakaNewCell: BaseCollectionViewCell {
    
    weak var delegate: IthakaAddDelegate?
    
    let image = #imageLiteral(resourceName: "write1").withRenderingMode(.alwaysOriginal)
    
    let portalimage = #imageLiteral(resourceName: "porthole").withRenderingMode(.alwaysOriginal)
    
    lazy var TitleTextView: UITextView = {
        let tv = UITextView()
        tv.textAlignment = .center
        tv.layer.cornerRadius = 5
        tv.textColor = .black
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.sizeToFit()
        tv.text = "Start Writing"
        tv.font = UIFont(name: "Helvetica", size: CGFloat(23))
        tv.isEditable = false
        tv.isScrollEnabled = false
        tv.isHidden = true
        return tv
    }()
    
    @objc func starWriting() {
        
        print("cxz")
        self.delegate?.onShareTapped()

    }
    
    let grayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black
        view.isHidden = true
        return view
    }()
    
    lazy var waterView: UIView = {
       let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 240, green: 245, blue: 251, alpha: 1)
        view.isUserInteractionEnabled = true

       return view
    }()
    
    override func setupViews() {
        super.setupViews()
        self.TitleTextView.backgroundColor = UIColor.clear

        self.TitleTextView.textAlignment = .center
        addSubview(bg)
        bg.addSubview(boatImageView)
        bg.addSubview(TitleTextView)
        bg.addSubview(portalImageView)
        bg.addSubview(treasureImageView)
        bg.addSubview(trophyImageView)

        bg.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 40, rightConstant: 0, widthConstant: frame.width, heightConstant: self.frame.height)
        boatImageView.anchor(top: nil, left: bg.leftAnchor, bottom: bg.bottomAnchor, right: bg.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width, heightConstant: self.frame.height-50)
        TitleTextView.anchor(top: bg.topAnchor, left: bg.leftAnchor, bottom: nil, right: bg.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 20, rightConstant: 15, widthConstant: 0, heightConstant: 40)
        bg.addSubview(grayLine)
        grayLine.anchor(top: TitleTextView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0.5)

//        portalImageView.anchor(top: TitleTextView.topAnchor, left: bg.leftAnchor, bottom: nil, right: nil, topConstant: 7.5, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 35, heightConstant: 35)
//        
//
//        treasureImageView.anchor(top: TitleTextView.topAnchor, left: nil, bottom: nil, right: bg.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 30, heightConstant: 30)
//        trophyImageView.anchor(top: TitleTextView.topAnchor, left: nil, bottom: nil, right: treasureImageView.leftAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 30, heightConstant: 30)
//
//        bringSubview(toFront: portalImageView)
        setBoatImage()
        
    }
    

    lazy var bg: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = true
        view.layer.cornerRadius = 5
        view.backgroundColor = UIColor.rgb(red: 217, green: 235, blue: 250, alpha: 1)
        let tap = UITapGestureRecognizer(target: self, action: #selector(starWriting))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)
        return view
    }()
    
    
    let boatImageView: UIImageView = {
        let imageview = UIImageView()
        
        return imageview
    }()
    
    lazy var portalImageView: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "porthole").withRenderingMode(.alwaysOriginal)
        imageview.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(portalopen))
        tap.numberOfTapsRequired = 1
        imageview.addGestureRecognizer(tap)
        

        return imageview
    }()
    
    lazy var treasureImageView: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "treasure (1)-1").withRenderingMode(.alwaysOriginal)
        imageview.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(portalopen))
        tap.numberOfTapsRequired = 1
        imageview.addGestureRecognizer(tap)
        
        
        return imageview
    }()
    
    lazy var trophyImageView: UIImageView = {
        let imageview = UIImageView()
//        imageview.image = #imageLiteral(resourceName: "trophy (2)").withRenderingMode(.alwaysOriginal)
        imageview.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(portalopen))
        tap.numberOfTapsRequired = 1
        imageview.addGestureRecognizer(tap)
        
        
        return imageview
    }()

    
    @objc func portalopen() {
        print("nc")
    }

    
    func setBoatImage() {
        
        let avatar = ProfileEditController.defaults.integer(forKey: "Avatar")
        
        if avatar == 0 {
            Api.User.observeCurrentUser { (user) in
                guard let avatar2 = user.avatar else { return }
                
                ProfileEditController.defaults.set(avatar2, forKey: "Avatar")
                
                self.checkAvatar()
                
            }

        } else {
            self.checkAvatar()
        }

    }
    
    func checkAvatar() {
        let avatar = ProfileEditController.defaults.integer(forKey: "Avatar")

        if avatar == 11 {
            
            
        } else if avatar == 23 {
        } else {
            return
        }
        
        self.TitleTextView.isHidden = false
        self.grayLine.isHidden = false

    }
}

//
//  FeedbackController.swift
//  ithaka
//
//  Created by Apurva Raj on 10/06/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class FeedbackController: UIViewController, UIWebViewDelegate {
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
   
    lazy var containerview = UIView()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        return button
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        myActivityIndicator.backgroundColor = UIColor.white
        myActivityIndicator.color = .black
        setupTopBar()

        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        myActivityIndicator.startAnimating()

        
        
        let myWebView:UIWebView = UIWebView(frame: CGRect(x:0, y:100, width: UIScreen.main.bounds.width, height:UIScreen.main.bounds.height))
        
        myWebView.delegate = self
        myWebView.scrollView.isScrollEnabled = true
        myWebView.isUserInteractionEnabled = true
    
        self.view.addSubview(myWebView)

        //1. Load web site into my web view
        let myURL = URL(string: "https://docs.google.com/forms/d/e/1FAIpQLSdKmJY5ijrtCL3RNHcg19eZ_l2SX-mCnYAWkv4ofNbh3SWqlQ/viewform?usp=sf_link")
        let myURLRequest:URLRequest = URLRequest(url: myURL!)
        myWebView.loadRequest(myURLRequest)
        
    }
    func setupTopBar() {
        let newview = UIView()
        newview.backgroundColor = .clear
        
        self.view.addSubview(newview)
        
        
        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        
        
        newview.addSubview(backButton)
        
        
        backButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        
    }
    
    @objc func handleBack(){
        self.navigationController?.popViewController(animated: true)
    }

    func webViewDidFinishLoad(_ webView: UIWebView) {
        print("done")
        self.myActivityIndicator.stopAnimating()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false

    }
}

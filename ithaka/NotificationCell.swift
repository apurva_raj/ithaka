//
//  NotificationCell.swift
//  ithaka
//
//  Created by Apurva Raj on 20/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit

class NotificationCell: UICollectionViewCell {
    
    var user: UserModelNew? {
        didSet {
            
            guard let imgURL = user?.profileImageUrl else { return }
            profileImageView.loadImage(urlString: imgURL)
            
        }
    }
    var notification: NotificationNew? {
        didSet {
            guard let notification = notification else { return }
            guard let date = notification.timestamp else { return }
            guard let message = notification.message else { return }

            
            let time = Date(timeIntervalSince1970: date)
            let timeago = time.timeAgoDisplay()
            
            
            DateLabel.text = timeago
            textView.text = message
            
//            profileImageView.loadImage(urlString: notification.user.profileImageUrl)
        }
    }
    
    let textView: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.systemFont(ofSize: 14)
        //        label.numberOfLines = 0
        //        label.backgroundColor = .lightGray
        textView.isScrollEnabled = false
        textView.isUserInteractionEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = false
        return textView
    }()
    
    
    let DateLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(13)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .gray
        label.text = "EAsy"
        //        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return label
    }()
    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 15.0
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowRadius = 12.0
        self.layer.shadowOpacity = 0.5

        
        addSubview(profileImageView)
        profileImageView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        profileImageView.layer.cornerRadius = 60 / 2

        addSubview(textView)
        textView.anchor(top: topAnchor, left: profileImageView.rightAnchor, bottom: nil, right: rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 4, rightConstant: 10, widthConstant: 0, heightConstant: 50)
        
        addSubview(DateLabel)
        DateLabel.anchor(top: textView.bottomAnchor, left: textView.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


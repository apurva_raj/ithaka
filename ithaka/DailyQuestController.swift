//
//  DailyQuestController.swift
//  ithaka
//
//  Created by Apurva Raj on 19/09/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SwiftMessages


class DailyQuestController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    lazy var unsubscribe: UILabel = {
        let label = UILabel()
        label.text = "Unsubscribe"
        label.textColor = UIColor.gray
        label.font = UIFont.boldSystemFont(ofSize: 13)
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(unsubscribeME))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
    }()
    
    
    @objc func unsubscribeME() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        alertController.popoverPresentationController?.permittedArrowDirections = []
        
        alertController.addAction(UIAlertAction(title: "Unsubscribe", style: .destructive, handler: { (_) in
            
            do {
                SwiftMessages.show {
                    let view = MessageView.viewFromNib(layout: .MessageView)
                    view.configureTheme(.warning)
                    view.configureDropShadow()
                    let iconText = "🙂"
                    view.configureContent(title: "Thank You", body: "You will not recieve notifications anymore.", iconText: iconText)
                    view.button?.isHidden = true
                    
                    return view
                }
                
            }
            
        }))
        

        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)


    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.backgroundColor = .white

        view.addSubview(unsubscribe)
        unsubscribe.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        collectionView?.register(DailyQuestCell.self, forCellWithReuseIdentifier: "DailyQuestCell")

        collectionView?.contentInset = UIEdgeInsetsMake(50, 10, 10, 0)
        navigationController?.navigationBar.isTranslucent = false

    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DailyQuestCell", for: indexPath) as! DailyQuestCell
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 30)
    }
    
    
    
}

class DailyQuestCell: UICollectionViewCell {
    
    lazy var title: UILabel = {
        let label = UILabel()
        label.text = "My daily quest is here"
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(title)
        title.anchor(top: safeAreaLayoutGuide.topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

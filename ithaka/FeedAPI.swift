//
//  FeedAPI.swift
//  ithaka
//
//  Created by Apurva Raj on 05/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseDatabase


class FeedAPI {
    
    var REF_FEED = Database.database().reference().child("feed")
    
    var REF_EXPLORE_POSTS = Database.database().reference().child("explore")
    

//    func observeFeed(withId id: String, completion: @escaping (Post) -> Void) {
//        REF_FEED.child(id).queryOrdered(byChild: "timestamp").observe(.childAdded, with: {
//            snapshot in
//            let key = snapshot.key
//            Api.Post.observePost(withId: key, completion: { (post) in
//                completion(post)
//            })
//        })
//    }
    
    func getRecentFeed(withId id: String, start timestamp: TimeInterval? = nil, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]?) -> Void) {
        
        var feedQuery = REF_FEED.child(id).queryOrdered(byChild: "creationDate")
        if let latestPostTimestamp = timestamp, latestPostTimestamp > 0 {
            feedQuery = feedQuery.queryStarting(atValue: latestPostTimestamp + 1 , childKey: "creationDate").queryLimited(toLast: limit)
        } else {
            feedQuery = feedQuery.queryLimited(toLast: limit)
        }
        
        // Call Firebase API to retrieve the latest records
        feedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects
            let myGroup = DispatchGroup()
            
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in (items as! [DataSnapshot]).enumerated() {
                myGroup.enter()
                let value = item.value as? NSDictionary
                let uid = value!["uid"] as? String ?? ""
                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                        if PostNew?.uid == UserModelNew?.uid {
                        
                            results.append((PostNew!, UserModelNew!))

                        }
                    myGroup.leave()

                    })
                })
            }
            myGroup.notify(queue: .main) {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            }

            
        })
    }
    
    func getOldFeed(withId id: String, start timestamp: TimeInterval, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]?) -> Void) {
        
        let feedOrderQuery = REF_FEED.child(id).queryOrdered(byChild: "creationDate")

        let feedLimitedQuery = feedOrderQuery.queryEnding(atValue: timestamp - 1, childKey: "creationDate").queryLimited(toLast: limit)


        
        feedLimitedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects as! [DataSnapshot]
            
            let myGroup = DispatchGroup()
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in items.enumerated() {
                print(item)
                myGroup.enter()
                let value = item.value as? NSDictionary
                let uid = value!["uid"] as? String ?? ""
                
                print("uid is", uid)
                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    
                            print("post id is", item.key)
                    
                    Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                        
                        if UserModelNew?.uid == nil {
                            print("bhow bhow")
                        } else {
                            results.append((PostNew!, UserModelNew!))
                            
                            myGroup.leave()
                            
                        }
                        
                    })
                })
            }
        
            myGroup.notify(queue: DispatchQueue.main, execute: {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            })


        })
        
    }
    
    func getRecentExploreFeed(start timestamp: TimeInterval? = nil, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]?) -> Void) {
        
        var feedQuery = REF_EXPLORE_POSTS.queryOrdered(byChild: "creationDate")
        
        if let latestPostTimestamp = timestamp, latestPostTimestamp > 0 {
            feedQuery = feedQuery.queryStarting(atValue: latestPostTimestamp + 1 , childKey: "creationDate").queryLimited(toLast: limit)
        } else {
            feedQuery = feedQuery.queryLimited(toLast: limit)
        }
        
        feedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects
            let myGroup = DispatchGroup()
            
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in (items as! [DataSnapshot]).enumerated() {
                myGroup.enter()
                let value = item.value as? NSDictionary
                
                let uid = value!["uid"] as? String ?? ""

                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    
                    if PostNew?.uid != "" {
                        Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                            if PostNew?.uid == UserModelNew?.uid {
                                results.append((PostNew!, UserModelNew!))
                                
                            }
                            myGroup.leave()
                            
                        })
                    }
                })
            }
            myGroup.notify(queue: .main) {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            }
            
            
        })

    }
    func getOldExploreFeed(start timestamp: TimeInterval, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]?) -> Void) {
        
        let feedOrderQuery = REF_EXPLORE_POSTS.queryOrdered(byChild: "creationDate")

        let feedLimitedQuery = feedOrderQuery.queryEnding(atValue: timestamp - 1, childKey: "creationDate").queryLimited(toLast: limit)

        
        
        feedLimitedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects as! [DataSnapshot]
            
            let myGroup = DispatchGroup()
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in items.enumerated() {
                print(item)
                myGroup.enter()
                let value = item.value as? NSDictionary
                let uid = value!["uid"] as? String ?? ""
            
                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    
//                    print(PostNew.uid)
                    Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                        
                        if UserModelNew?.uid == nil {
                            print("bhow bhow")
                        } else {
                            results.append((PostNew!, UserModelNew!))
                            
                            myGroup.leave()

                        }
                        
                    })
                })
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            })
            
        })
        
    }
    
    
    
    
    func getExploreQuestFeed(qid: String, start timestamp: TimeInterval? = nil, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]?) -> Void) {
        
        var feedQuery = Database.database().reference().child("questFeed").child(qid).queryOrdered(byChild: "creationDate")
        
        if let latestPostTimestamp = timestamp, latestPostTimestamp > 0 {
            feedQuery = feedQuery.queryStarting(atValue: latestPostTimestamp + 1 , childKey: "creationDate").queryLimited(toLast: limit)
        } else {
            feedQuery = feedQuery.queryLimited(toLast: limit)
        }
        
        feedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects
            let myGroup = DispatchGroup()
            
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in (items as! [DataSnapshot]).enumerated() {
                myGroup.enter()
                let value = item.value as? NSDictionary
                
                let uid = value!["uid"] as? String ?? ""
                
                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    
                    if PostNew?.uid != "" {
                        Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                            if PostNew?.uid == UserModelNew?.uid {
                                results.append((PostNew!, UserModelNew!))
                                
                            }
                            myGroup.leave()
                            
                        })
                    }
                })
            }
            myGroup.notify(queue: .main) {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            }
            
            
        })
        
    }
    
    func getOldExploreQuestFeed(qid: String, start timestamp: TimeInterval, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]?) -> Void) {
        
        let feedOrderQuery = Database.database().reference().child("questFeed").child(qid).queryOrdered(byChild: "creationDate")

        let feedLimitedQuery = feedOrderQuery.queryEnding(atValue: timestamp - 1, childKey: "creationDate").queryLimited(toLast: limit)
        
        
        
        feedLimitedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects as! [DataSnapshot]
            
            let myGroup = DispatchGroup()
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in items.enumerated() {
                print(item)
                myGroup.enter()
                let value = item.value as? NSDictionary
                let uid = value!["uid"] as? String ?? ""
                
                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    
                    //                    print(PostNew.uid)
                    Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                        
                        if UserModelNew?.uid == nil {
                            print("bhow bhow")
                        } else {
                            results.append((PostNew!, UserModelNew!))
                            
                            myGroup.leave()
                            
                        }
                        
                    })
                })
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            })
            
        })
        
    }
    
    
    let forquestref = Database.database().reference().child("AllPostsViaQuests")

    
    func getRecentExploreFeedForQuests(start timestamp: TimeInterval? = nil, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]?) -> Void) {
        
        var feedQuery = forquestref.queryOrdered(byChild: "creationDate")
        
        if let latestPostTimestamp = timestamp, latestPostTimestamp > 0 {
            feedQuery = feedQuery.queryStarting(atValue: latestPostTimestamp + 1 , childKey: "creationDate").queryLimited(toLast: limit)
        } else {
            feedQuery = feedQuery.queryLimited(toLast: limit)
        }
        
        feedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects
            let myGroup = DispatchGroup()
            
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in (items as! [DataSnapshot]).enumerated() {
                myGroup.enter()
                let value = item.value as? NSDictionary
                
                let uid = value!["uid"] as? String ?? ""
                
                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    
                    if PostNew?.uid != "" {
                        Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                            if PostNew?.uid == UserModelNew?.uid {
                                results.append((PostNew!, UserModelNew!))
                                
                            }
                            myGroup.leave()
                            
                        })
                    }
                })
            }
            myGroup.notify(queue: .main) {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            }
            
            
        })
        
    }
    func getOldExploreFeedForQuests(start timestamp: TimeInterval, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]?) -> Void) {
        
        let feedOrderQuery = forquestref.queryOrdered(byChild: "creationDate")
        
        let feedLimitedQuery = feedOrderQuery.queryEnding(atValue: timestamp - 1, childKey: "creationDate").queryLimited(toLast: limit)
        
        
        
        feedLimitedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects as! [DataSnapshot]
            
            let myGroup = DispatchGroup()
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in items.enumerated() {
                print(item)
                myGroup.enter()
                let value = item.value as? NSDictionary
                let uid = value!["uid"] as? String ?? ""
                
                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    
                    //                    print(PostNew.uid)
                    Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                        
                        if UserModelNew?.uid == nil {
                            print("bhow bhow")
                        } else {
                            results.append((PostNew!, UserModelNew!))
                            
                            myGroup.leave()
                            
                        }
                        
                    })
                })
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            })
            
        })
        
    }
    

    let foradminref = Database.database().reference().child("AllPostsForAdmin")


    func getRecentExploreFeedForAdmin(start timestamp: TimeInterval? = nil, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]?) -> Void) {
        
        var feedQuery = foradminref.queryOrdered(byChild: "creationDate")
        
        if let latestPostTimestamp = timestamp, latestPostTimestamp > 0 {
            feedQuery = feedQuery.queryStarting(atValue: latestPostTimestamp + 1 , childKey: "creationDate").queryLimited(toLast: limit)
        } else {
            feedQuery = feedQuery.queryLimited(toLast: limit)
        }
        
        feedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects
            let myGroup = DispatchGroup()
            
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in (items as! [DataSnapshot]).enumerated() {
                myGroup.enter()
                let value = item.value as? NSDictionary
                
                let uid = value!["uid"] as? String ?? ""
                
                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    
                    if PostNew?.uid != "" {
                        Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                            if PostNew?.uid == UserModelNew?.uid {
                                results.append((PostNew!, UserModelNew!))
                                
                            }
                            myGroup.leave()
                            
                        })
                    }
                })
            }
            myGroup.notify(queue: .main) {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            }
            
            
        })
        
    }
    func getOldExploreFeedForAdmin(start timestamp: TimeInterval, limit: UInt, completionHandler: @escaping ([(PostNew, UserModelNew)]?) -> Void) {
        
        let feedOrderQuery = foradminref.queryOrdered(byChild: "creationDate")
        
        let feedLimitedQuery = feedOrderQuery.queryEnding(atValue: timestamp - 1, childKey: "creationDate").queryLimited(toLast: limit)
        
        
        
        feedLimitedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects as! [DataSnapshot]
            
            let myGroup = DispatchGroup()
            
            var results: [(post: PostNew, user: UserModelNew)] = []
            
            for (_, item) in items.enumerated() {
                print(item)
                myGroup.enter()
                let value = item.value as? NSDictionary
                let uid = value!["uid"] as? String ?? ""
                
                Api.NewPost.observeSinglePost(withUId: uid, withPid: item.key, completion: { (PostNew) in
                    
                    //                    print(PostNew.uid)
                    Api.User.observeUser(withId: (PostNew?.uid!)!, completion: { (UserModelNew) in
                        
                        if UserModelNew?.uid == nil {
                            print("bhow bhow")
                        } else {
                            results.append((PostNew!, UserModelNew!))
                            
                            myGroup.leave()
                            
                        }
                        
                    })
                })
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {
                results.sort(by: {$0.0.timestamp! > $1.0.timestamp! })
                completionHandler(results)
            })
            
        })
        
    }

    
    
}

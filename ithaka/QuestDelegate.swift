//
//  QuestDelegate.swift
//  ithaka
//
//  Created by Apurva Raj on 23/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

protocol QuestDelegate: class {
    
    func onTypeTapped(for cell: QuestMain)
    
    func onTtapped(for cell: QuestCards)
    
    func onTapQuestTitle(id: String)
    func onTapQuestTitle(id: String, title: String)

    func didTapStage(level: Int)
    
    func stopLoadingAnimation()
    
    func exit()
}

//
//  AmazingWritersCell.swift
//  ithaka
//
//  Created by Apurva Raj on 28/01/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase


protocol test1023: class {
    func tap12()
}
class AmazingWritersCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ExploreBodyDelegate {
    
    
    func openUserProfile(id: String) {
        print("Working")
        self.delegate?.openUserProfile(id: id)
    }
    
    
    weak var delegate: ExploreBodyDelegate?
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        cv.dataSource = self
        cv.delegate = self
        cv.contentInset = UIEdgeInsetsMake(40, 0, 0, 0)
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.frame.width*0.05
            
            cv.contentInset = UIEdgeInsetsMake(0, ph, 0, ph)
            
            
        default:
            print("i am Everyone except ipad")
            cv.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
            
            
        }

        return cv
    }()

    var usersNew1 = [UserModelNew]()

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        print("count is ", usersNew1.count)
        return usersNew1.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreBody", for: indexPath) as! ExploreBody
        let user = self.usersNew1[indexPath.item]
        cell.delegate = self
        cell.user = user
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.frame.width*0.1
            
            return CGSize(width: self.frame.width - ph, height: 70)

            
        default:
            print("i am Everyone except ipad")
            return CGSize(width: self.frame.width, height: 70)

            
        }


    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        addSubview(collectionView)
        collectionView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: self.frame.width, heightConstant: self.frame.height)

        collectionView.register(ExploreBody.self, forCellWithReuseIdentifier: "ExploreBody")

    }
    
    var tableTitle: String? {
        didSet{
            fetchUsers()
        }
    }

    
    fileprivate func fetchUsers() {
        print("Fetching users..")
        
        let tt = self.tableTitle ?? "topWriters"
        let ref = Database.database().reference().child(tt)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            value?.forEach({ (snapshot) in
                let uid = snapshot.key as! String
                
                Api.User.observeUser(withId: uid, completion: { (userModelNew) in
                    self.usersNew1.append(userModelNew!)
                    self.collectionView.reloadData()
                })

            })
        }
        
        
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

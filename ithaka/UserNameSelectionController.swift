//
//  UserNameSelectionController.swift
//  ithaka
//
//  Created by Apurva Raj on 19/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase

class UserNameSelectionController: UIViewController, UITextViewDelegate {
    
    lazy var MainLabel: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 20)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = true
        textView.layoutIfNeeded()
        textView.layer.borderWidth = 1
        textView.placeholder = "Choose your username"
        textView.textAlignment = .center
        
        return textView
    }()
    
    lazy var submitButton: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 19)
        textView.isUserInteractionEnabled = true
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        textView.layoutIfNeeded()
        
        textView.text = "submit"
        textView.textAlignment = .center
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleLabelTapped))
        tap.numberOfTapsRequired = 1
        textView.addGestureRecognizer(tap)

        return textView
    }()
    
    lazy var doneButton: UITextView = {
        let textView = UITextView()
        textView.font = UIFont.boldSystemFont(ofSize: 17.5)
        textView.isUserInteractionEnabled = false
        textView.sizeToFit()
        textView.isScrollEnabled = false
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.isEditable = false
        textView.layoutIfNeeded()
        textView.textColor = .gray
        textView.text = "Done"
        textView.textAlignment = .center
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doneTapped))
        tap.numberOfTapsRequired = 1
        textView.addGestureRecognizer(tap)
        
        return textView
    }()

    @objc func doneTapped() {
        print("closing down")
        self.dismiss(animated: true, completion: nil)
    }
    
    func textViewDidChange(_ textView: UITextView) {
//        MainLabel.text
    }
    
    
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(MainLabel)
        view.addSubview(submitButton)
        view.addSubview(doneButton)
        MainLabel.delegate = self
        
        doneButton.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: nil, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)

        MainLabel.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 200, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        submitButton.anchor(top: MainLabel.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
    }
    func checkUserNameAlreadyExist(newUserName: String, completion: @escaping(Bool) -> Void) {
        
        let ref = Database.database().reference()
        ref.child("users").queryOrdered(byChild: "u_username").queryEqual(toValue: newUserName)
            .observeSingleEvent(of: .value, with: {(snapshot) in
                
                if snapshot.exists() {
                    completion(true)
                }
                else {
                    completion(false)
                }
            })
    }
   @objc func titleLabelTapped() {
    let tv = self.MainLabel.text

    self.checkUserNameAlreadyExist(newUserName: tv!) { isExist in
        if isExist {
            print("Username exist. Print warning")
            self.doneButton.textColor = .gray

            
        }
        else {
            print("Username is available")
            guard let currentuid = Auth.auth().currentUser?.uid else { return }

            let ref = Database.database().reference().child("users").child(currentuid)
            
            let value = ["u_username": tv]
            
            ref.updateChildValues(value as Any as! [AnyHashable : Any])
            
            self.doneButton.isUserInteractionEnabled = true
            self.doneButton.textColor = .black
        }
    }
    
    }

}


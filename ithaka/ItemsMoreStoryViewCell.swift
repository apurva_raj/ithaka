//
//  ItemsMoreStoryViewCell.swift
//  ithaka
//
//  Created by Apurva Raj on 20/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class ItemsMoreStoryViewCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    lazy var title: UILabel = {
        let label = UILabel()
        label.text = "Backstories from similar items"
        label.font = UIFont.systemFont(ofSize: 19)
        label.textAlignment = .left
        label.numberOfLines = 0
        label.textColor = UIColor.black
        
        return label
    }()

    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.layer.cornerRadius = 15
        cv.clipsToBounds = true
        cv.backgroundColor = UIColor.white
        cv.dataSource = self
        cv.delegate = self
        cv.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        return cv
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        self.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        addSubview(title)
        title.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 20)
        

        addSubview(collectionView)
        
        collectionView.register(MoreItemCell.self, forCellWithReuseIdentifier: "MoreItemCell")
        collectionView.anchor(top: title.bottomAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: self.frame.width, heightConstant: self.frame.height - 20)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoreItemCell", for: indexPath) as! MoreItemCell
        
        return cell

    }
    

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//
//  ExploreHashtags.swift
//  ithaka
//
//  Created by Apurva Raj on 11/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class ExploreHashtags: UICollectionViewCell {
    
    
    weak var delegate: exploreDelegate?
    
 
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = true
        label.textColor = .black
        label.text = "#myname #crazy #wow #many #names"
        
        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
//        label.layer.cornerRadius = 5.0
//        label.layer.borderColor  =  UIColor.gray.cgColor
//        label.layer.borderWidth = 1
//        label.layer.shadowOpacity = 0.5
//        label.layer.shadowColor =  UIColor.black.cgColor
//        label.layer.shadowRadius = 5.0
//        label.layer.shadowOffset = CGSize(width:5, height: 5)
//        label.layer.masksToBounds = true
//
        let tap = UITapGestureRecognizer(target: self, action: #selector(titleTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    @objc func titleTapped() {
        print("first one")
        let hash = titleLabel.text ?? ""
        self.delegate?.onHashtagtapped(hash: hash)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
//        addSubview(titleLabel)
//        //        view.contentInset = UIEdgeInsetsMake(-30, 0, -30, 0)
//        
//        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


//
//  ExploreController.swift
//  ithaka
//
//  Created by Apurva Raj on 26/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//


import UIKit
import Firebase

protocol exploreDelegate: class {
  
    func onTitleLabelTapped(for cell: ExploreBody)
    func onSearchTapped()
    func onStoryViewTapped(post: Post)

    func questCardTapped(id: String)
    
    func onHashtagtapped(hash: String)
    
    func onDailyQuestFeed()
    
    func onMonsterBattleFeed()
}

class ExploreController: UICollectionViewController, UICollectionViewDelegateFlowLayout, exploreDelegate, UISearchBarDelegate, MyCustomDelegate, QuestDelegate, UIViewControllerTransitioningDelegate, textViewTap, ExploreBodyDelegate {


    func openUserProfile(id: String) {
        print("tapped")
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = id
    self.navigationController?.pushViewController(profileController, animated: true)
    }

    
    func exit() {
        return
    }
    
    func onMonsterBattleFeed() {
        let monsterFeedController = MonsterBattleFeedController()
        
        self.navigationController?.pushViewController(monsterFeedController, animated: true)
    }
    
    
    func onDailyQuestFeed(){
        let dailyFeedController = DailyQuestFeedController(collectionViewLayout: UICollectionViewFlowLayout())
        
        self.navigationController?.pushViewController(dailyFeedController, animated: true)
    }
    
    func onHashtagtapped(hash: String) {
        let hh = HashTagPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        hh.hashtag = hash
        self.navigationController?.pushViewController(hh, animated: true)

    }
    
    func onRocketCountButtonTapped(for cell: PostUi) {
        let rocketListController = RocketListController(collectionViewLayout: UICollectionViewFlowLayout())
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        
        let post = self.postNew[indexPath.item]
        rocketListController.post = post
        navigationController?.pushViewController(rocketListController, animated: false)

    }
    
    func onFollowersLabelTapped(user: UserModelNew) {
        return
    }
    
    func onFollowingLabelTapped(user: UserModelNew) {
        return
    }
    
    func didTapNewComment(post: PostNew) {
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.post = post
        
        navigationController?.pushViewController(commentsController, animated: false)

    }
    
    func didTapComment(post: PostNew) {
        return
    }
    
    func onEditPorfileTap(user: UserModelNew) {
        return
    }
    
    func onNameIdLabelTapped(userId: String) {
        return
    }
    
    func onTapQuestTitle(id: String, title: String) {
        
        let questPostController = ExploreQuestPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        questPostController.questId = id
        questPostController.questTitle = title
        
        navigationController?.pushViewController(questPostController, animated: true)

    }
    
    func usernameTapped(username: String) {
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_username = Database.database().reference().child("users")
        
        ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.exists() {
                profileController.userName = username
                self.navigationController?.pushViewController(profileController, animated: true)
            } else {
                print("north remembers")
                
            }
        }
        
    }
    
    func hashTagTapped(hash: String) {
        let hh = HashTagPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        hh.hashtag = hash
        self.navigationController?.pushViewController(hh, animated: true)
    }
    
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?

    
    func onShareButtonTapped(for cell: PostUi) {
        let downloadImgaesController = DownloadImagesController()
        
        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: downloadImgaesController)
        
        
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        let post = self.postNew[indexPath.item]
        let user = self.userModelNew[indexPath.item]
        
        downloadImgaesController.post = post
        downloadImgaesController.user = user
        
        downloadImgaesController.popoverPresentationController?.sourceView = self.view
        downloadImgaesController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        downloadImgaesController.popoverPresentationController?.permittedArrowDirections = []
        
        
        downloadImgaesController.modalPresentationStyle = .custom
        downloadImgaesController.transitioningDelegate = self.halfModalTransitioningDelegate
        self.present(downloadImgaesController, animated: true, completion: nil)

    }
    
    func stopLoadingAnimation() {
        return
    }
    
    func onTypeTapped(for cell: QuestMain) {
        return
    }
    
    func onTtapped(for cell: QuestCards) {
        return
    }
    
    func onTapQuestTitle(id: String) {
        print("finally working")
        

    }
    
    func didTapStage(level: Int) {
        return
    }
    
    func questCardTapped(id: String) {
        print("Working")
    }
    
    func onStoryViewTapped(user: UserModelNew, post: PostNew) {
        return
    }
    
    func onloveButtonTapped(loc: CGPoint, for cell: PostUi) {
        return
    }
    
    func onTitleLabelTapped(for cell: PostUi) {
        return
    }
    
    func onNameLabelTapped(for cell: PostUi) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let user = self.userModelNew[indexPath.item]
        print("user is", user)
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.user = user
        self.navigationController?.pushViewController(profileController,
                                                      animated: true)

    }
    
    func didTapComment(post: Post) {
        return
    }
    
    func onOptionButtonTapped(for cell: PostUi) {
        return
    }
    
    func onEditPorfileTap(user: UserModel) {
        return
    }
    
    func onRocketButtonTapped(for cell: PostUi) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        DispatchQueue.main.async {
            self.collectionView?.reloadItems(at: [indexPath])
        }

    }
    
    func onEmojiTap(for cell: ProfileHeader) {
        return
    }
    
    func onLogoutTapped(for cell: ProfileHeader) {
        return
    }
    
    func onLevelCountTapped(user: UserModel) {
        return
    }
    
    func onswipetButtonTapped(for cell: PostUi) {
        return
    }
    
    func swipetoTop() {
        return
    }
    
    func onUnblock() {
        return
    }
    
    func onFollowersLabelTapped(user: UserModel) {
        return
    }
    
    func onFollowingLabelTapped(user: UserModel) {
        return
    }
    
    func onTagLabelTapped(post: Post) {
        return
    }
    
    func onOptionButtonTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    func onTitleLabelTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    func onRocketButtonTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    func level1Tapped(level: Int) {
        
        let levelPostController = LevelPostController(collectionViewLayout: UICollectionViewFlowLayout())
        levelPostController.level = level
        self.navigationController?.pushViewController(levelPostController, animated: true)

    }
    
    
    func level1Tapped() {
        
        print("heyaa")
        
    }
    
    func onStoryViewTapped(post: Post) {
        print("bc")
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
//        previewpostController.post = post
        previewpostController.modalPresentationStyle = .popover
        
        navigationController?.pushViewController(previewpostController, animated: true)
//        present(previewpostController, animated: true, completion: nil)

    }
    
    
    @objc func onSearchTapped() {
        print("bhow bhow bc")
        let searchController = SearchController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(searchController, animated: true)

    }
    
    @objc func onSearchTappeds() {
        let searchController = SearchController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(searchController, animated: true)

    }
    func onTitleLabelTapped(for cell: ExploreBody) {
        
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
//
//
//        let post = self.posts[indexPath.item]
//
//        guard let postId = post.id else { return }
//
//        let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
//        postController.userId = post.user.uid
//        postController.postId = postId
//        navigationController?.pushViewController(postController, animated: true)
    }

   
    @objc func searchBarTapped(){
        print("tapped")
        let selectionController = SearchSelectionController(collectionViewLayout: UICollectionViewFlowLayout())
                navigationController?.pushViewController(selectionController, animated: true)

    }
    
    
    let cellId = "cellId"
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()

        collectionView?.register(ExploreBody.self, forCellWithReuseIdentifier: cellId)
        collectionView?.register(TitleHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "TitleHeader")
        collectionView?.register(ExploreTwoTitles.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "ExploreTwoTitles")

        collectionView?.register(ExploreSearch.self, forCellWithReuseIdentifier: "ExploreSearch")
        collectionView?.register(PostUi.self, forCellWithReuseIdentifier: "PostUi")

        collectionView?.register(ExploreCardContainer.self, forCellWithReuseIdentifier: "ExploreCardContainer")


        collectionView?.register(ExploreBody.self, forCellWithReuseIdentifier: "ExploreBody")
        
        
        collectionView?.register(TitleHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "TitleHeader1")
        collectionView?.register(SeeMoreLabel.self, forCellWithReuseIdentifier: "SeeMoreLabel")

        
 collectionView?.register(ExploreHashtags.self, forCellWithReuseIdentifier: "ExploreHashtags")

        collectionView?.register(c1.self, forCellWithReuseIdentifier: "cellIdc1")

        collectionView?.delegate = self
    
        collectionView?.contentInset = UIEdgeInsetsMake(0, 0, 100, 0)

        fetchPosts()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        setupAddIthaka()
        navigationItem.title = "Explore"
        
        collectionView?.backgroundColor = UIColor.white
        
        navigationController?.delegate = self
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleReadBackStory), name: PreviewPostController.updateReadStoryNotificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hashtagTappedFromPost), name: PreviewPostController.PushHashtagPage, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(usernameTappedFromPost), name: PreviewPostController.PushProfilePage, object: nil)

        
    }
    
    @objc func hashtagTappedFromPost(_ notification: NSNotification){
        if let hash = notification.userInfo?["hashtag"] as? String {
            self.hashTagTapped(hash: hash)
        }
    }
    
    @objc func usernameTappedFromPost(_ notification: NSNotification){
        if let uname = notification.userInfo?["username"] as? String {
            
            self.usernameTapped(username: uname)
            
        }
    }

    
    @objc func handleReadBackStory(_ notification: NSNotification){
        
        if let itemName = notification.userInfo?["itemName"] as? String {
            
            let itemsStoryController = ItemsStoryController(collectionViewLayout: UICollectionViewFlowLayout())
            itemsStoryController.itemCode = itemName
            self.navigationController?.pushViewController(itemsStoryController, animated: true)
            
        }
        
        
    }
    
    
    fileprivate func setupAddIthaka(){
        
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8-search-50").withRenderingMode(.alwaysTemplate) , style: .plain, target: self, action: #selector(searchBarTapped))
        navigationItem.rightBarButtonItem?.tintColor = .black

    }
    
    @objc func handleRefresh() {
        users.removeAll()
        usersNew1.removeAll()
        postNew.removeAll()
        userModelNew.removeAll()
        fetchPosts()
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
        }
    }

    // SECTION

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }

    
    // HEADER
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch indexPath.section {
        case 0:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ExploreTwoTitles", for: indexPath) as! ExploreTwoTitles
            header.delegate = self
            header.layer.zPosition = 0.0
            return header

        case 1:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TitleHeader1", for: indexPath) as! TitleHeader
            header.titleLabel.textAlignment = .center
            header.layer.zPosition = 0.0
            
            switch UIDevice.current.userInterfaceIdiom {
                
            case .pad:
                header.titleLabel.text = "       Amazing writers"

                
            default:
                header.titleLabel.text = "Amazing writers"

                
            }

            return header
            
        case 2:
            return UICollectionViewCell()


        case 3:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "TitleHeader1", for: indexPath) as! TitleHeader
            header.titleLabel.textAlignment = .center
            header.layer.zPosition = 0.0

            switch UIDevice.current.userInterfaceIdiom {
                
            case .pad:
                header.titleLabel.text = "       Best stories"
                
                
            default:
                header.titleLabel.text = "Best stories"

                
            }

            return header


        default:
            return UICollectionViewCell()
        }

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.1
            
            switch section {
            case 0:
                return CGSize(width: view.frame.width - ph, height: 100)
            case 1:
                return CGSize(width: view.frame.width - ph, height: 80)
            case 2:
                return .zero //add for that
            case 3:
                return CGSize(width: view.frame.width - ph, height: 80)
                
                
            default:
                return .zero
            }

            
        default:
            switch section {
            case 0:
                return CGSize(width: view.frame.width, height: 100)
            case 1:
                return CGSize(width: view.frame.width, height: 80)
            case 2:
                return .zero //add for that
            case 3:
                return CGSize(width: view.frame.width, height: 80)
                
                
            default:
                return .zero
            }
        }
        
     
    }

    var user1: UserModelNew?
    var users1 = [UserModel]()
    var usersNew1 = [UserModelNew]()

    fileprivate func fetchUsers() {
        print("Fetching users..")
        
        let ref = Database.database().reference().child("topWriters")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            value?.forEach({ (snapshot) in
                let uid = snapshot.key as! String
                
                Api.User.observeUser(withId: uid, completion: { (userModelNew) in
                    self.user1 = userModelNew
                    self.usersNew1.append(userModelNew!)
                    
                })
            })
        }
        
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
        }

        
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 0:
            print("yaho")
        case 1:
            print("yahosaddaaaaaaaa")
            
            


        case 2:
            print("yahasdasdo")
            
            

            let suggestedPeopleController = SuggestedPeople(collectionViewLayout: UICollectionViewFlowLayout())
            self.navigationController?.pushViewController(suggestedPeopleController, animated: true)

        case 3:
            print("yaho")
            
            let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
            
            guard let uid = Auth.auth().currentUser?.uid else { return }
            
            
            let user = userModelNew[indexPath.item]
            let post = postNew[indexPath.item]
            previewpostController.post = post
            previewpostController.user = user
            previewpostController.visitorUid = uid
            previewpostController.modalPresentationStyle = .fullScreen
            previewpostController.popoverPresentationController?.sourceView = self.view
            previewpostController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            
            previewpostController.popoverPresentationController?.permittedArrowDirections = []
            
            previewpostController.transitioningDelegate = self
            previewpostController.modalPresentationStyle = .custom
            previewpostController.modalPresentationCapturesStatusBarAppearance = true
            self.present(previewpostController, animated: true, completion: nil)
            


        default:
            print("yaho")
        }
    }
    
    var exploreH: ExploreBody?

  
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        switch section {
        case 0:
            return 1
        case 1:
            return usersNew1.count
        case 2:
            return 1
        case 3:
            return postNew.count

        default:
            return 1
        }
    
       
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let section = indexPath.section
        switch (section) {
        case 0:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreHashtags", for: indexPath) as! ExploreHashtags
//            cell.delegate = self
//            cell.backgroundColor = .blue
            return cell
        
        case 1:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreBody", for: indexPath) as! ExploreBody
            let user = self.usersNew1[indexPath.item]
            cell.delegate = self
            cell.user = user
            
            return cell

            
        case 2:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SeeMoreLabel", for: indexPath) as! SeeMoreLabel

            return cell
            
            //add for that

            
        case 3:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostUi", for: indexPath) as! PostUi
            cell.delegate = self
            let post = postNew[indexPath.item]
            cell.post = post
            
            let user = userModelNew[indexPath.item]
            cell.user = user
            
            cell.textViewTap = self
            
            print("Vasavasvsavsdavdsvsdvsvsadv", post)
            
            return cell

        default:
            print("section is 1 sadasdasdasdas", indexPath.section)
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ExploreBody
            
            return cell
        }

    
    }
    var tagheight = 0

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.10
            
            switch indexPath.section {
            case 0:
                return CGSize(width: view.frame.width - ph, height: 1)
            case 1:
                return CGSize(width: view.frame.width - ph, height: 70)
            case 2:
                return CGSize(width: view.frame.width - ph, height: 60)
                
            case 3:
                return CGSize(width: view.frame.width - ph - 30, height: 330)
                
                
            default:
                return .zero
            }

        default:
            switch indexPath.section {
            case 0:
                return CGSize(width: view.frame.width, height: 1)
            case 1:
                return CGSize(width: view.frame.width, height: 70)
            case 2:
                return CGSize(width: view.frame.width, height: 60)
                
            case 3:
                return CGSize(width: view.frame.width - 30, height: 330)
                
                
            default:
                return .zero
            }

            
        }

    
      

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.05
            
            switch section {
            case 0:
                return UIEdgeInsetsMake(0, ph, 0, ph)
            case 1:
                return UIEdgeInsetsMake(0, ph, 0, ph)
            case 2:
                return UIEdgeInsetsMake(0, ph, 0, ph)
                
            case 3:
                return UIEdgeInsetsMake(10, ph + 15, 0, ph + 15)
                
            default:
                return UIEdgeInsetsMake(0, ph, 0, ph)
                
            }

            
        default:
            print("i am Everyone except ipad")
            switch section {
            case 0:
                return UIEdgeInsetsMake(0, 0, 0, 0)
            case 1:
                return UIEdgeInsetsMake(0, 0, 0, 0)
            case 2:
                return UIEdgeInsetsMake(0, 0, 0, 0)
                
            case 3:
                return UIEdgeInsetsMake(10, 15, 0, 15)
                
            default:
                return UIEdgeInsetsMake(0, 0, 0, 0)
                
            }

        }

       
        
        
    }

    
    fileprivate var isLoadingPost = false

    
    var postNew = [PostNew]()
    var userModelNew = [UserModelNew]()

    
    fileprivate func fetchPosts()  {
        
        isLoadingPost = true

        Api.Feed.getRecentExploreFeed(start: postNew.first?.timestamp, limit: 3) { (results) in
            
            guard let results = results else { self.myActivityIndicator.stopAnimating()
                return }

            if results.count > 0 {

                results.forEach({ (result) in
                    self.postNew.append(result.0)
                    self.userModelNew.append(result.1)
                    
                })
                
            }
            self.isLoadingPost = false
            self.myActivityIndicator.stopAnimating()
            self.collectionView?.reloadData()
        }
        fetchUsers()

        
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
            
            guard !isLoadingPost else {
                return
            }
            isLoadingPost = true
            guard let lastPostTimestamp = self.postNew.last?.timestamp else {
                isLoadingPost = false
                return
            }
            self.myActivityIndicator.startAnimating()
    
            Api.Feed.getOldExploreFeed(start: (postNew.last?.timestamp)!, limit: 3, completionHandler: { (results) in
                guard let results = results else { self.myActivityIndicator.stopAnimating()
                    return }

                if results.count == 0 {
                    self.myActivityIndicator.stopAnimating()
                    return
                }
                for result in results {
                    self.postNew.append(result.0)
                    self.userModelNew.append(result.1)
                }
                self.myActivityIndicator.stopAnimating()
                self.collectionView?.reloadData()
                self.isLoadingPost = false

            })
            
        }
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsetsMake(60, 0, 0, 0)
//    }
//    
    var filteredUsers = [UserModel]()
    var users = [UserModel]()
    var user: UserModel?



}



extension ExploreController {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? PreviewPostController {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal,
                               interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard
                let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController,
                interactionController.inProgress
                else {
                    return nil
            }
            return interactionController
    }
}



extension ExploreController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return SystemPushAnimator(type: .navigation)
        case .pop:
            return SystemPopAnimator(type: .navigation)
        default:
            return nil
        }
    }
    
}

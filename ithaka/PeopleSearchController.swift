//
//  PeopleSearchController.swift
//  ithaka
//
//  Created by Apurva Raj on 21/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class PeopleSearchController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    lazy var searchBar: UISearchBar = {
        let sb = UISearchBar()
        sb.placeholder = "Search"
        sb.sizeToFit()
        sb.delegate = self
        return sb
    }()
    let cellId = "cellId"

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: view.frame.height/2 - 50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        collectionView?.backgroundColor = .white
        navigationItem.titleView = searchBar
        collectionView?.register(UserSearchCell.self, forCellWithReuseIdentifier: cellId)

        searchBar.delegate = self
        searchBar.becomeFirstResponder()
//        fetchUsers()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Wwow")
        if searchText.isEmpty {
            
//            filteredUsers = users
        } else {
            let text = searchText.lowercased()
        print(text)
            startSearch(text: text)
//            filteredUsers = self.users.filter { (user) -> Bool in
//                return user.username.lowercased().contains(searchText.lowercased())
//            }
        }
        
        self.collectionView?.reloadData()

    }
    
    func startSearch(text: String) {
        self.users.removeAll()
        self.myActivityIndicator.startAnimating()

        let ref = Database.database().reference().child("users")
        ref.queryOrdered(byChild: "u_username").queryStarting(atValue: text).queryEnding(atValue: text+"\u{f8ff}").observeSingleEvent(of: .value) { (snapshot) in
            guard let dictionaries = snapshot.value as? [String: Any] else { return }
            
            dictionaries.forEach({ (key, value) in
                self.puser.removeAll()
                guard let userDictionary = value as? [String: Any] else { return }
                
                let puser = UserModelNew.transformUser(dictionary: userDictionary, key: key)
                self.puser.append(puser)

            })
            self.myActivityIndicator.stopAnimating()
            self.collectionView?.reloadData()

            
        }

    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        
        let user = puser[indexPath.item]
        profileController.userId = user.uid
        self.navigationController?.pushViewController(profileController, animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return puser.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! UserSearchCell
        cell.user = puser[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    var filteredUsers = [UserModel]()
    var users = [UserModel]()
    var puser = [UserModelNew]()
    fileprivate func fetchUsers() {
        
        let ref = Database.database().reference().child("users")
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            guard let dictionaries = snapshot.value as? [String: Any] else { return }
            
            dictionaries.forEach({ (key, value) in
                
                if key == Auth.auth().currentUser?.uid {
                    return
                }
                
                guard let userDictionary = value as? [String: Any] else { return }
                
                let user = UserModel(uid: key, dictionary: userDictionary)
                let puser = UserModelNew.transformUser(dictionary: userDictionary, key: key)
                self.puser.append(puser)
                self.users.append(user)
            })
            
            self.collectionView?.reloadData()
            
        }) { (err) in
            print("Failed to fetch users for search:", err)
        }
    }

}

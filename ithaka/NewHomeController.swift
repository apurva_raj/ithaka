//
//  NewHomeController.swift
//  ithaka
//
//  Created by Apurva Raj on 28/01/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//
import UIKit
import Firebase
import SwiftMessages
import UIColor_FlatColors


class NewHomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, MyCustomDelegate, UIViewControllerTransitioningDelegate, UITabBarControllerDelegate, textViewTap, IthakaAddDelegate, removeChild {
    func readbackstory(itemCode: String?) {
        print("poor controlller")
    }
    
    
    
    func removeChild() {
        print("working??")
        
        if self.childViewControllers.count > 0{
            let viewControllers:[UIViewController] = self.childViewControllers
            for viewContoller in viewControllers{
                viewContoller.willMove(toParentViewController: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParentViewController()
            }
        }
    }
    
    
    
    func onShareTapped() {
        print("tas")
        let addIthakaController = AddIthakaAPI.addIthakaController
        self.navigationController?.pushViewController(addIthakaController, animated: true)
        
    }
    
    func onDimissTapped() {
        return
    }
    
    func onRocketCountButtonTapped(for cell: PostUi) {
        let rocketListController = RocketListController(collectionViewLayout: UICollectionViewFlowLayout())
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        let post = self.newPosts[indexPath.item]
        rocketListController.post = post
        navigationController?.pushViewController(rocketListController, animated: false)
        
    }
    
    
    
    func onFollowersLabelTapped(user: UserModelNew) {
        return
    }
    
    func onFollowingLabelTapped(user: UserModelNew) {
        return
    }
    
    func didTapNewComment(post: PostNew) {
        
        print("yahho")
        
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.post = post
        
        navigationController?.pushViewController(commentsController, animated: false)
        
    }
    
    
    
    func onEditPorfileTap(user: UserModelNew) {
        return
    }
    
    func onNameIdLabelTapped(userId: String) {
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        
        print("bc na", userId)
        
        profileController.modalPresentationStyle = .custom
        profileController.transitioningDelegate = self
        profileController.userId = userId
        self.navigationController?.pushViewController(profileController, animated: true)
        
    }
    
    
    func usernameTapped(username: String) {
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_username = Database.database().reference().child("users")
        
        ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.exists() {
                profileController.userName = username
                self.navigationController?.pushViewController(profileController, animated: true)
            } else {
                print("north remembers")
                
            }
        }
    }
    
    func hashTagTapped(hash: String) {
        let hh = HashTagPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_hash = Database.database().reference().child("Hashtags").child(hash)
        
        ref_hash.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                hh.hashtag = hash
                self.navigationController?.pushViewController(hh, animated: true)
                
            } else {
                print("stupid")
            }
        }
        
    }
    
    
    func onShareButtonTapped(for cell: PostUi) {
        print("yaho")
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        let post = self.newPosts[indexPath.item]
        let user = self.userModelNew[indexPath.item]
        
        let downloadImgaesController = DownloadImagesController()
        downloadImgaesController.post = post
        downloadImgaesController.user = user
        
        downloadImgaesController.popoverPresentationController?.sourceView = self.view
        downloadImgaesController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        downloadImgaesController.popoverPresentationController?.permittedArrowDirections = []
        self.present(downloadImgaesController, animated: true, completion: nil)
    }
    
    var tapCounter : Int = 0
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if isLoadingPost == false {
            self.tapCounter = tapCounter + 1
            
            print("lets see", tapCounter)
            let tabBarIndex = tabBarController.selectedIndex
            
            if tabBarIndex == 0 && tapCounter == 2{
                let nextIndexPath = IndexPath(row: 0, section: 0)
                self.collectionView?.scrollToItem(at: nextIndexPath, at: .top, animated: true)
                
                self.tapCounter = 0
                
            }
            
            if self.tapCounter == 1 {
                let time = DispatchTime.now() + 0.35
                DispatchQueue.main.asyncAfter(deadline: time, execute: {
                    self.tapCounter = 0
                })
            }
            
        }
        
    }
    
    func swipetoTop() {
        print("its fucking works")
    }
    
    
    func onswipetButtonTapped(for cell: PostUi) {
        print("Awesome mausam")
        
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let nextIndexPath = IndexPath(row: indexPath.row + 1, section: indexPath.section)
        
        self.collectionView?.scrollToItem(at: nextIndexPath, at: .top, animated: true)
        
    }
    
    func onTagLabelTapped(post: Post) {
        return
    }
    
    func onFollowingLabelTapped(user: UserModel) {
        return
    }
    
    func onFollowersLabelTapped(user: UserModel) {
        return
    }
    
    
    func onUnblock() {
        handleRefresh()
    }
    
    func onLogoutTapped(for cell: ProfileHeader) {
        return
    }
    
    func onLevelCountTapped(user: UserModel) {
        return
    }
    
    
    
    func onRocketButtonTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    
    func onOptionButtonTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    func onTitleLabelTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    func onEmojiTap(for cell: ProfileHeader) {
        return
    }
    //for depthcontroller
    
    var currentIndexpath:IndexPath!
    public weak var depthPageTransformer:DepthPageTransformer!
    
    
    func onRocketButtonTapped(for cell: PostUi) {
        print("pritn cbc")
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        DispatchQueue.main.async {
            self.collectionView?.reloadItems(at: [indexPath])
        }
    }
    
    
    func onLogoutTapped() {
        return
    }
    
    func onEditPorfileTap(user: UserModel) {
        return
    }
    
    func onEditPorfileTap() {
        return
    }
    
    func onOptionButtonTapped(for cell: PostUi) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        alertController.popoverPresentationController?.permittedArrowDirections = []
        
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let AdminId = "FaKyjBJXnzeZAeSYe13XV2WIrp23"
        
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        
        let post = self.newPosts[indexPath.item]
        let postUid = post.uid ?? ""
        guard let pid = post.id else { return }
        
        if uid == AdminId {
            alertController.addAction(UIAlertAction(title: "Add to explore", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference().child("explore")
                    
                    let cd = Date().timeIntervalSince1970
                    
                    let values = ["uid": postUid, "creationDate": cd] as [String : Any]
                    ref.child(pid).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Done", body: "Keep going Apurva", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                    let notificationMessage = "We have featured your post on our explore page. This will give your more reach. Happy writing :)"
                    guard let toUserId = post.uid else { return }
                    guard let postId = post.id else { return }
                    
                    let ref54 = Database.database().reference().child("notifications").child(toUserId).childByAutoId()
                    
                    
                    let notifValues = ["Message": notificationMessage, "uid":toUserId, "pid":postId, "fromUserId":"b5MKdFi5mkcJgQC12hEOjJOT3MB3", "type":"2312", "creationDate": cd] as [String : Any]
                    
                    ref54.updateChildValues(notifValues)
                    
                    
                }
                
            }))
        }
        
        
        if uid == postUid {
            alertController.addAction(UIAlertAction(title: "Edit post", style: .default, handler: { (_) in
                
                do {
                    
                    let editPostController = EditPostController()
                    editPostController.post = post
                    self.navigationController?.pushViewController(editPostController,
                                                                  animated: true)
                }
                
            }))
            
            alertController.addAction(UIAlertAction(title: "Delete Post", style: .destructive, handler: { (_) in
                do {
                    self.myActivityIndicator.startAnimating()
                    
                    UtilityAPI.deletePost(post: post)
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.success)
                        view.configureDropShadow()
                        let iconText = "👻"
                        view.configureContent(title: "Done", body: "Your post is deleted.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                    DispatchQueue.main.async {
                        self.newPosts.remove(at: indexPath.item)
                        self.userModelNew.remove(at: indexPath.item)
                        self.collectionView?.deleteItems(at: [indexPath])
                    }
                    
                    self.myActivityIndicator.stopAnimating()
                }
                
            }))
            
        } else {
            alertController.addAction(UIAlertAction(title: "Report", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference()
                    
                    guard let postId = post.id else { return }
                    let values = [postUid:true] as [String : Any]
                    ref.child("report").child(postId).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Thank You", body: "Thank you for reporting this post. We will check and will send you update within 24 hours.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                }
                
            }))
            
            
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }
    
    
    func checkIfKeyExists(){
        
        if UserDefaults.standard.object(forKey: "currentItemReadCount") == nil {
            UserDefaults.standard.set(0, forKey: "currentItemReadCount")
        }
        
    }
    
    
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("just to keep an eye")
        
        
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        
        let user = userModelNew[indexPath.item]
        let post = newPosts[indexPath.item]
        previewpostController.post = post
        previewpostController.user = user
        previewpostController.visitorUid = uid
        previewpostController.popoverPresentationController?.sourceView = self.view
        previewpostController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        previewpostController.popoverPresentationController?.permittedArrowDirections = []
        
        present(previewpostController, animated: true, completion: nil)
        
    }
    
    func onStoryViewTapped(user:UserModelNew, post: PostNew) {
        print("bc")
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
        
        previewpostController.post = post
        previewpostController.user = user
        previewpostController.popoverPresentationController?.sourceView = self.view
        previewpostController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        previewpostController.popoverPresentationController?.permittedArrowDirections = []
        
        present(previewpostController, animated: true, completion: nil)
    }
    
    func onTopicButtonTapped() {
        print("topic button tapped")
    }
    
    var postUi: PostUi?
    
    let postCellId = "postCellId"
    
    func onTitleLabelTapped(for cell: PostUi){
        print("title button tapped")
        
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.newPosts[indexPath.item]
        
        guard let postId = post.id else { return }
        
        let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
        postController.userId = post.uid
        postController.postId = postId
        navigationController?.pushViewController(postController, animated: true)
        
        
    }
    
    func onNameLabelTapped(for cell: PostUi){
        print("rocket test")
        
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let user = self.userModelNew[indexPath.item]
        print("user is", user)
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.user = user
        self.navigationController?.pushViewController(profileController,
                                                      animated: true)
        
    }
    
    
    func onloveButtonTapped(loc: CGPoint, for cell: PostUi){
        print("love button tapped")
        
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        var post = self.posts[indexPath.item]
        let postUserId = post.user.uid
        guard let postId = post.id else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Database.database().reference().child("love").child(postId)
        let ref3 = Database.database().reference().child("posts").child(postUserId).child(postId)
        
        let values = [uid: post.hasLiked == true ? 0 : 1]
        
        ref.updateChildValues(values) { (err, _) in
            
            if let err = err {
                print("Failed to like post:", err)
                return
            }
            
            print("Successfully liked post.")
            
            post.hasLiked = true
            
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                let value = snapshot.value as? NSDictionary
                let cc = value?.count ?? 0
                
                let loveCount = ["loveCount":cc]
                let taps = 1
                let updates = [uid: taps]
                
                ref.updateChildValues(updates)
                
                ref3.updateChildValues(loveCount)
                post.loveCount = cc
                self.posts[indexPath.item] = post
                
                self.collectionView?.reloadItems(at: [indexPath])
            })
            
        }
        
    }
    
    
    
    var scene: AnimationScene!
    var size: CGSize!
    
    lazy var rocketView: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "ithaka_rocket")
        
        return image
    }()
    
    lazy var disButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ithaka_rocket"), for: .normal)
        
        button.tag = 111
        return button
    }()
    
    
    
    lazy var finaView: UICollectionView? = {
        return UICollectionView(frame: self.view.bounds, collectionViewLayout: DepthPageTransformer())
    }()
    //
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.backgroundColor = .white
        tabBarController?.delegate = self
        
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant:0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        myActivityIndicator.startAnimating()
        
        
        collectionView?.register(HomeHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        
        
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "DalekPinpointBold", size: 21)!]
        
        navigationItem.title = "Ithaka"
        collectionView?.delegate = self
        
        self.navigationController?.navigationBar.isTranslucent = false
        
        collectionView?.register(PostUi.self, forCellWithReuseIdentifier: postCellId)
        //        addPosts()
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: AddIthakaFinalController.updateFeedNotificationName, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: EditPostController.updateFeedNotificationName, object: nil)
        
        
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        
        collectionView?.refreshControl = refreshControl
        
        collectionView?.contentInset = UIEdgeInsetsMake(10, 0, 100, 0)
        
        
        
        loadPosts()
        guard let fcmToken = Messaging.messaging().fcmToken else { return }
        let token: [String: AnyObject] = [fcmToken: fcmToken as AnyObject]
        self.postToken(Token: token)
        
        ///for swipe
        self.navigationController?.navigationBar.backgroundColor = UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
        
        //        upd()
        NotificationCenter.default.addObserver(self, selector: #selector(self.BadgeWasUpdated), name: NSNotification.Name(rawValue: "com.api4242.ithaka"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleCommentUpdate), name: CommentsController.updateCommentsCountName, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: AddIthakaFinalController.updateFeedNotificationName, object: nil)
        
        collectionView?.setNeedsLayout()
        collectionView?.layoutIfNeeded()
        
        checkIfKeyExists()
        //        setupchestchild()
    }
    
    func setupchestchild(){
        
        let chestController = ChestChildController()
        chestController.delegate = self
        addChildViewController(chestController)
        view.addSubview(chestController.view)
        chestController.didMove(toParentViewController: self)
        
    }
    
    @objc func BadgeWasUpdated() {
        self.tabBarController?.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)
    }
    
    
    @objc func handleCommentUpdate() {
        collectionView?.reloadData()
    }
    
    
    func upd() {
        let ref = Database.database().reference().child("users")
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let ld = snapshot.value as? NSDictionary else { return }
            
            ld.forEach({ (snapshot) in
                let uidkey = snapshot.key
                
                let vv = ["uid":uidkey]
                
                let ref1 = Database.database().reference().child("posts").child(uidkey as! String)
                
                ref1.observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    guard let lds = snapshot.value as? NSDictionary else { return }
                    
                    lds.forEach({ (snapshot) in
                        ref1.child(snapshot.key as! String).updateChildValues(vv)
                        
                    })
                    
                })
            })
        }
    }
    
    func doit(){
        let ref1 = Database.database().reference().child("users")
        let ref2 = Database.database().reference().child("posts")
        let ref3 = Database.database().reference().child("rockets")
        
        ref1.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            value?.forEach({ (snapshot) in
                print("first all the user ids are", snapshot.key)
                
                ref2.child(snapshot.key as! String).observeSingleEvent(of: .value, with: { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    
                    value?.forEach({ (snapshot) in
                        print("first all the post ids are", snapshot.key)
                        let value5 = snapshot.value as? NSDictionary
                        let rc = value5!["rocketCount"] ?? 0
                        
                        let valueRock = ["rocketCount": rc]
                        
                        ref3.child(snapshot.key as! String).updateChildValues(valueRock)
                        
                    })
                })
                
            })
        }
    }
    fileprivate var isLoadingPost = false
    
    func loadPosts() {
        isLoadingPost = true
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        Api.Feed.getRecentFeed(withId: uid, start: newPosts.first?.timestamp, limit: 3) { (results) in
            guard let results = results else { return }
            if results.count > 0 {
                results.forEach({ (result) in
                    if result.0.title == nil {
                        self.myActivityIndicator.stopAnimating()
                        
                        self.collectionView?.reloadData()
                        
                        return
                    } else {
                        self.newPosts.append(result.0)
                        self.userModelNew.append(result.1)
                    }
                    
                })
            }
            self.isLoadingPost = false
            
            DispatchQueue.main.async {
                self.myActivityIndicator.stopAnimating()
                self.collectionView?.reloadData()
            }
            
            
        }
    }
    
    
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
            guard let uid = Auth.auth().currentUser?.uid else { return }
            
            guard !isLoadingPost else {
                return
            }
            isLoadingPost = true
            
            guard (self.newPosts.last?.timestamp) != nil else {
                isLoadingPost = false
                return
            }
            self.myActivityIndicator.startAnimating()
            
            Api.Feed.getOldFeed(withId: uid, start: (newPosts.last?.timestamp)!, limit: 3, completionHandler: { (results) in
                
                
                guard let results = results else { return }
                if results.count == 0 {
                    self.myActivityIndicator.stopAnimating()
                    self.collectionView?.reloadData()
                    self.isLoadingPost = false
                    
                    return
                }
                for result in results {
                    if result.0.id == nil {
                        return
                    } else {
                        self.newPosts.append(result.0)
                        self.userModelNew.append(result.1)
                    }
                }
                
                self.myActivityIndicator.stopAnimating()
                
                self.collectionView?.reloadData()
            })
            
            
        }
    }
    
    
    let BlackLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        
        return view
    }()
    
    
    
    func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
    }
    
    func postToken(Token: [String: AnyObject]) {
        let ref = Database.database().reference()
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        guard let fcmToken = Messaging.messaging().fcmToken else { return }
        
        ref.child("fcmToken").child(uid).child(fcmToken).setValue(Token)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "DalekPinpointBold", size: 21)!]
        self.tabBarController?.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    @objc func handleUpdateFeed() {
        handleRefresh()
    }
    
    @objc func handleRefresh() {
        newPosts.removeAll()
        userModelNew.removeAll()
        loadPosts()
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
            self.myActivityIndicator.stopAnimating()
            
        }
    }
    
    func fetchAllPosts() {
        //        fetchFollowingUserIds()
    }
    
    var isFinishedPaging = false
    var posts = [Post]()
    var newPosts = [PostNew]()
    var userModelNew = [UserModelNew]()
    var user: UserModel?
    
    
    var users = [UserModel]()
    
    
    fileprivate func setupAddIthaka(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8-plus_math_filled").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleAddIthaka))
        
    }
    
    @objc func handleAddIthaka() {
        let addIthakaController = AddIthakaAPI.addIthakaController
        addIthakaController.modalPresentationStyle = .custom
        addIthakaController.transitioningDelegate = self
        present(addIthakaController, animated: true, completion: nil)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 100)
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! HomeHeader
        
        
        header.delegate = self
        return header
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("new posts count are", newPosts.count)
        
        return newPosts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return view.frame.height-100
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postCellId, for: indexPath) as! PostUi
        
        let post = newPosts[indexPath.item]
        
        let user = userModelNew[indexPath.item]
        
        cell.post = post
        cell.user = user
        cell.textViewTap = self
        cell.delegate = self
        collectionView.collectionViewLayout.invalidateLayout()
        return cell
    }
    
    var tagheight = 0
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: view.frame.width, height: CGFloat(330))
        
    }
    
    func showProfile() {
        let profileController = ProfileController()
        navigationController?.pushViewController(profileController, animated: true)
    }
    
    
}


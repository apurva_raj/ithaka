//
//  PostPreview.swift
//  ithaka
//
//  Created by Apurva Raj on 11/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit

class PostPreview: UICollectionViewCell {
    
    var post: Post? {
        didSet {
            guard let story = post?.story else { return }
            
                        storyTextView.text = story
            print(story)
        }
    }
    
    var postId: String?
    
    lazy var storyTextView: UITextView = {
        let textview = UITextView()
        textview.font = UIFont.systemFont(ofSize: 17)
        textview.isEditable = false
        textview.sizeToFit()
        textview.textColor = .white
        
        return textview
    }()
    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "cancel_shadow"), for: .normal)
        button.backgroundColor = .gray
        
        return button
    }()

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = .white
        
        addSubview(storyTextView)
        storyTextView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 100)
        
        addSubview(dismissButton)
        dismissButton.anchor(top: storyTextView.bottomAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

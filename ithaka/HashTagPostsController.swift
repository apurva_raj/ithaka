//
//  HashTagPostsController.swift
//  ithaka
//
//  Created by Apurva Raj on 21/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase

class HashTagPostsController: UICollectionViewController, UICollectionViewDelegateFlowLayout, MyCustomDelegate, textViewTap {
   
    var hashdimiss: String?{
        didSet{
            if hashdimiss == "lost"{
                let dismissButton: UIButton = {
                    let button = UIButton()
                    button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
                    button.tintColor = .white
                    button.backgroundColor = .black
                    button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
                    
                    return button
                }()
                
                let oneview = UIView()
                
                oneview.backgroundColor = .black
                
                view.addSubview(dismissButton)
                view.addSubview(oneview)
                
                dismissButton.anchor(top: nil, left: view.self.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
                oneview.anchor(top: view.safeAreaLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)


            }
        }
    }
    
    @objc func handleDismiss(){
        dismiss(animated: true, completion: nil)
    }

    func onRocketCountButtonTapped(for cell: PostUi) {
        let rocketListController = RocketListController(collectionViewLayout: UICollectionViewFlowLayout())
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        let post = self.newPosts[indexPath.item]
        rocketListController.post = post
        navigationController?.pushViewController(rocketListController, animated: false)

    }
    
    func onFollowersLabelTapped(user: UserModelNew) {
        return
    }
    
    func onFollowingLabelTapped(user: UserModelNew) {
        return
    }
    
    func didTapNewComment(post: PostNew) {
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.post = post
        
        navigationController?.pushViewController(commentsController, animated: false)

    }
    
    func didTapComment(post: PostNew) {
        return
    }
    
    func onEditPorfileTap(user: UserModelNew) {
        return
    }
    
    func onNameIdLabelTapped(userId: String) {
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        
        print("bc na", userId)
        
        profileController.userId = userId
        self.navigationController?.pushViewController(profileController, animated: true)

    }
    
    func hashTagTapped(hash: String) {
        let hh = HashTagPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_hash = Database.database().reference().child("Hashtags").child(hash)
        
        ref_hash.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                hh.hashtag = hash
                self.navigationController?.pushViewController(hh, animated: true)
                
            } else {
                print("stupid")
            }
        }

    }
    
    func usernameTapped(username: String) {
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_username = Database.database().reference().child("users")
        
        ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.exists() {
                profileController.userName = username
                self.navigationController?.pushViewController(profileController, animated: true)
            } else {
                print("north remembers")
                
            }
        }

    }
    
    func onStoryViewTapped(user: UserModelNew, post: PostNew) {
        return
    }
    
    func onloveButtonTapped(loc: CGPoint, for cell: PostUi) {
        return
    }
    
    func onTitleLabelTapped(for cell: PostUi) {
        return
    }
    
    func onNameLabelTapped(for cell: PostUi) {
        return
    }
    
    func didTapComment(post: Post) {
        return
    }
    
    func onOptionButtonTapped(for cell: PostUi) {
        return
    }
    
    func onEditPorfileTap(user: UserModel) {
        return
    }
    
    func onRocketButtonTapped(for cell: PostUi) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        DispatchQueue.main.async {
            self.collectionView?.reloadItems(at: [indexPath])
        }

    }
    
    func onShareButtonTapped(for cell: PostUi) {
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        let post = self.newPosts[indexPath.item]
        let user = self.userModelNew[indexPath.item]
        
        let downloadImgaesController = DownloadImagesController()
        downloadImgaesController.post = post
        downloadImgaesController.user = user
        
        downloadImgaesController.popoverPresentationController?.sourceView = self.view
        downloadImgaesController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        downloadImgaesController.popoverPresentationController?.permittedArrowDirections = []
        self.present(downloadImgaesController, animated: true, completion: nil)

    }
    
    func onEmojiTap(for cell: ProfileHeader) {
        return
    }
    
    func onLogoutTapped(for cell: ProfileHeader) {
        return
    }
    
    func onLevelCountTapped(user: UserModel) {
        return
    }
    
    func onswipetButtonTapped(for cell: PostUi) {
        return
    }
    
    func swipetoTop() {
        return
    }
    
    func onUnblock() {
        return
    }
    
    func onFollowersLabelTapped(user: UserModel) {
        return
    }
    
    func onFollowingLabelTapped(user: UserModel) {
        return
    }
    
    func onTagLabelTapped(post: Post) {
        return
    }
    
    func onOptionButtonTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    func onTitleLabelTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    func onRocketButtonTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
   
    
    var hashtag: String? {
        didSet{
            let hash = NSMutableAttributedString(string: "#", attributes: nil)
            hash.append(NSAttributedString(string: hashtag!))
            
            navigationItem.title = hash.string
        }
    }
    
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 20, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        myActivityIndicator.startAnimating()
        collectionView?.register(PostUi.self, forCellWithReuseIdentifier: "postCellId")
        collectionView?.backgroundColor = .white
        setupTopBar()

        loadPosts()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleCommentUpdate), name: CommentsController.updateCommentsCountName, object: nil)

        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.05
            
            collectionView?.contentInset = UIEdgeInsetsMake(55, ph, 70, ph)
            
        default:
            print("i am Everyone except ipad")
            collectionView?.contentInset = UIEdgeInsetsMake(55, 0, 70, 0)
            
        }


    }
    
    @objc func handleCommentUpdate() {
        collectionView?.reloadData()
    }
    
    let newview = UIView()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        return button
    }()
    

    
    func setupTopBar() {
        
        newview.backgroundColor = .white
        
        let newview1 = UIView()
        newview1.backgroundColor = .white
        self.view.addSubview(newview1)
        newview1.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        view.addSubview(newview)
        
        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        
        newview.addSubview(backButton)
        backButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        view.bringSubview(toFront: backButton)
        
        
    }
    
    @objc func handleBack(){
        self.navigationController?.popViewController(animated: true)
    }
    

    var newPosts = [PostNew]()
    var userModelNew = [UserModelNew]()

    var user: UserModelNew?
    var tagheight = 0
    fileprivate var isLoadingPost = false

    
    func loadPosts() {
        isLoadingPost = true
        print("loading started")
        guard let tag = hashtag else { return }
        
        Api.Hashtag.getRecentHashtagfeed(withTag: tag, start: newPosts.first?.timestamp, limit: 3) { (results) in
            if results.count > 0 {
                results.forEach({ (result) in
                    self.newPosts.append(result.0)
                    self.userModelNew.append(result.1)

                })
            }
            self.isLoadingPost = false
            
            self.myActivityIndicator.stopAnimating()
            
            self.collectionView?.reloadData()

        }
        
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
            guard let tag = hashtag else { return }

            guard !isLoadingPost else {
                return
            }
            isLoadingPost = true
            guard (self.newPosts.last?.timestamp) != nil else {
                self.myActivityIndicator.stopAnimating()

                isLoadingPost = false
                return
            }
            Api.Hashtag.getOldHashtagFeed(withId: tag, start: (newPosts.last?.timestamp)!, limit: 3, completionHandler: { (results) in
                if results.count == 0 {
                    self.myActivityIndicator.stopAnimating()

                    return
                }
                for result in results {
                    self.newPosts.append(result.0)
                    self.userModelNew.append(result.1)
                }
                
                self.myActivityIndicator.stopAnimating()

                self.collectionView?.reloadData()
                self.isLoadingPost = false

            })
            
        }

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.1
            
            return CGSize(width: view.frame.width - ph, height: 330)
            
            
        default:
            print("i am Everyone except ipad")
            return CGSize(width: view.frame.width - 30, height: 330)
            
        }
        
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "postCellId", for: indexPath) as! PostUi
        
        
        let post = newPosts[indexPath.item]
        let user = userModelNew[indexPath.item]

        
        cell.post = post
        cell.user = user
        
        cell.delegate = self
        cell.textViewTap = self
        return cell

    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newPosts.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 15, 0, 15)
        
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    

    
}

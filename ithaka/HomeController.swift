//  HomeController.swift
//  ithaka
//
//  Created by Apurva Raj on 17/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import SwiftMessages
import UIColor_FlatColors
import VegaScrollFlowLayout


protocol doubleTap: class {
    func swipetoTop()
}

class HomeController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate, MyCustomDelegate, UITabBarControllerDelegate, textViewTap, IthakaAddDelegate, removeChild {
   
    
    func readbackstory(itemCode: String?) {
        //first remove child
        
        removeChild()
        
        
        //forward user to itemsory page
        
        let itemsStoryController = ItemsStoryController(collectionViewLayout: UICollectionViewFlowLayout())
       itemsStoryController.itemCode = itemCode
    self.navigationController?.pushViewController(itemsStoryController, animated: true)

    }
    
   
    
    
    func removeChild() {
        print("working??")
        
        if self.childViewControllers.count > 0{
            let viewControllers:[UIViewController] = self.childViewControllers
            for viewContoller in viewControllers{
                viewContoller.willMove(toParentViewController: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParentViewController()
            }
        }
    }
    
   
    
    func onShareTapped() {
        print("tas")
        let addIthakaController = AddIthakaAPI.addIthakaController
        self.navigationController?.pushViewController(addIthakaController, animated: true)

    }
    
    func onDimissTapped() {
        return
    }
    
    func onRocketCountButtonTapped(for cell: PostUi) {
        let rocketListController = RocketListController(collectionViewLayout: UICollectionViewFlowLayout())
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        let post = self.newPosts[indexPath.item]
        rocketListController.post = post
        navigationController?.pushViewController(rocketListController, animated: false)

    }
    
    

    func onFollowersLabelTapped(user: UserModelNew) {
        return
    }
    
    func onFollowingLabelTapped(user: UserModelNew) {
        return
    }
    
    func didTapNewComment(post: PostNew) {
        
        print("yahho")

        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.post = post
    self.navigationController?.pushViewController(commentsController, animated: false)
        

    }
    
    func onEditPorfileTap(user: UserModelNew) {
        return
    }
    
    func onNameIdLabelTapped(userId: String) {
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        
        print("bc na", userId)
        
        profileController.modalPresentationStyle = .custom
        profileController.transitioningDelegate = self
        profileController.userId = userId
    self.navigationController?.pushViewController(profileController, animated: true)

    }
    
    
    func usernameTapped(username: String) {
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_username = Database.database().reference().child("users")
        
        ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.exists() {
                profileController.userName = username
            self.navigationController?.pushViewController(profileController, animated: true)
            } else {
                print("north remembers")
                
            }
        }
    }
    
    func hashTagTapped(hash: String) {
        let hh = HashTagPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_hash = Database.database().reference().child("Hashtags").child(hash)
        
        ref_hash.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                hh.hashtag = hash
                self.navigationController?.pushViewController(hh, animated: true)
                
            } else {
                print("stupid")
            }
        }
        
    }
    
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?


    func onShareButtonTapped(for cell: PostUi) {
        print("yaho")
        
        
        
    let downloadImgaesController = DownloadImagesController()
        
        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: downloadImgaesController)

        
                        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
                        let post = self.newPosts[indexPath.item]
                        let user = self.userModelNew[indexPath.item]
        
                        downloadImgaesController.post = post
                        downloadImgaesController.user = user
        
                downloadImgaesController.popoverPresentationController?.sourceView = self.view
                downloadImgaesController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
                downloadImgaesController.popoverPresentationController?.permittedArrowDirections = []

        
    downloadImgaesController.modalPresentationStyle = .custom
    downloadImgaesController.transitioningDelegate = self.halfModalTransitioningDelegate
        self.present(downloadImgaesController, animated: true, completion: nil)
    
    }
    
    var tapCounter : Int = 0

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if isLoadingPost == false {
            self.tapCounter = tapCounter + 1
            
            print("lets see", tapCounter)
            let tabBarIndex = tabBarController.selectedIndex
            
            if tabBarIndex == 0 && tapCounter == 2{
                let nextIndexPath = IndexPath(row: 0, section: 0)
                self.collectionView?.scrollToItem(at: nextIndexPath, at: .top, animated: true)
                
                self.tapCounter = 0
                
            }
            
            if self.tapCounter == 1 {
                let time = DispatchTime.now() + 0.35
                DispatchQueue.main.asyncAfter(deadline: time, execute: {
                    self.tapCounter = 0
                })
            }

        }

    }
    
    func swipetoTop() {
        print("its fucking works")
    }
    
    
    func onswipetButtonTapped(for cell: PostUi) {
        print("Awesome mausam")
        
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }

        let nextIndexPath = IndexPath(row: indexPath.row + 1, section: indexPath.section)

        self.collectionView?.scrollToItem(at: nextIndexPath, at: .top, animated: true)
        
    }
    
    func onTagLabelTapped(post: Post) {
            return
    }
    
    func onFollowingLabelTapped(user: UserModel) {
        return
    }
    
    func onFollowersLabelTapped(user: UserModel) {
        return
    }
    
    
    func onUnblock() {
        handleRefresh()
    }
    
    func onLogoutTapped(for cell: ProfileHeader) {
        return
    }
    
    func onLevelCountTapped(user: UserModel) {
        return
    }
    

    
    func onRocketButtonTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    
    func onOptionButtonTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    func onTitleLabelTappedFromProfile(for cell: PostProfileUi) {
        return
    }
    
    func onEmojiTap(for cell: ProfileHeader) {
        return
    }
    //for depthcontroller
    
    var currentIndexpath:IndexPath!
    public weak var depthPageTransformer:DepthPageTransformer!

    
    func onRocketButtonTapped(for cell: PostUi) {
        print("pritn cbc")
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        DispatchQueue.main.async {
            self.collectionView?.reloadItems(at: [indexPath])
        }
    }
    

    func onLogoutTapped() {
        return
    }
    
    func onEditPorfileTap(user: UserModel) {
        return
    }
    
    func onEditPorfileTap() {
        return
    }
    
    func onOptionButtonTapped(for cell: PostUi) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        alertController.popoverPresentationController?.permittedArrowDirections = []
        
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let AdminId = "FaKyjBJXnzeZAeSYe13XV2WIrp23"
        
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
       
        let post = self.newPosts[indexPath.item]
        let user = self.userModelNew[indexPath.item]
        
        let username = user.u_username ?? ""
        
        let postUid = post.uid ?? ""
        guard let pid = post.id else { return }
    
        if uid == AdminId {
            alertController.addAction(UIAlertAction(title: "Add to explore", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference().child("explore")
                    
                    let cd = Date().timeIntervalSince1970
                    
                    let values = ["uid": postUid, "creationDate": cd] as [String : Any]
                    ref.child(pid).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Done", body: "Keep going Apurva", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                    let notificationMessage = "We have featured your post on our explore page. This will give your more reach. Happy writing :)"
                    guard let toUserId = post.uid else { return }
                    guard let postId = post.id else { return }
                    
                    let ref54 = Database.database().reference().child("notifications").child(toUserId).childByAutoId()
                    
                    
                    let notifValues = ["Message": notificationMessage, "uid":toUserId, "pid":postId, "fromUserId":"b5MKdFi5mkcJgQC12hEOjJOT3MB3", "type":"2312", "creationDate": cd] as [String : Any]
                    
                    ref54.updateChildValues(notifValues)

                    
                }
                
            }))
        }
        
        
        if uid == postUid {
            alertController.addAction(UIAlertAction(title: "Edit post", style: .default, handler: { (_) in
                
                do {
                    
                    let editPostController = EditPostController()
                    editPostController.post = post
                  
                    editPostController.username = username
                self.navigationController?.pushViewController(editPostController,
                                                                  animated: true)
                }
                
            }))
            
            alertController.addAction(UIAlertAction(title: "Delete Post", style: .destructive, handler: { (_) in
                do {
                    self.myActivityIndicator.startAnimating()

                    UtilityAPI.deletePost(post: post)
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.success)
                        view.configureDropShadow()
                        let iconText = "👻"
                        view.configureContent(title: "Done", body: "Your post is deleted.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                    DispatchQueue.main.async {
                        self.newPosts.remove(at: indexPath.item)
                        self.userModelNew.remove(at: indexPath.item)
                        self.collectionView?.deleteItems(at: [indexPath])
                    }

                    self.myActivityIndicator.stopAnimating()
                }
                
            }))
            
        } else {
            alertController.addAction(UIAlertAction(title: "Report For Spam", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference()
                    
                    guard let postId = post.id else { return }
                    let cd = Date().timeIntervalSince1970
                    
                    let values = ["fromUserId":uid, "creationTime": cd] as [String : Any]
                    ref.child("report").child("spam").child(postId).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Thank You", body: "Thank you for reporting this post. We will check and will send you update within 24 hours.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                }
                
            }))
            
            alertController.addAction(UIAlertAction(title: "Report For Plagiarism", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference()
                    
                    guard let postId = post.id else { return }
                    let cd = Date().timeIntervalSince1970
                    
                    let values = ["fromUserId":uid, "creationTime": cd] as [String : Any]
                    ref.child("report").child("plagiarism").child(postId).updateChildValues(values)

                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Thank You", body: "Thank you for reporting this post. We will check and will send you update within 24 hours.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                }
                
            }))

            

        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)

    }
    
    
    func checkIfKeyExists(){
        
        if UserDefaults.standard.object(forKey: "currentItemReadCount") == nil {
            UserDefaults.standard.set(0, forKey: "currentItemReadCount")
        }

    }
    
    

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        print("just to keep an eye")

        
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
        
        guard let uid = Auth.auth().currentUser?.uid else { return }

        
        let user = userModelNew[indexPath.item]
        let post = newPosts[indexPath.item]
        previewpostController.post = post
        previewpostController.user = user
        previewpostController.visitorUid = uid
        previewpostController.modalPresentationStyle = .fullScreen
        previewpostController.popoverPresentationController?.sourceView = self.view
        previewpostController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)

        previewpostController.popoverPresentationController?.permittedArrowDirections = []

        previewpostController.transitioningDelegate = self
        previewpostController.modalPresentationStyle = .custom
        previewpostController.modalPresentationCapturesStatusBarAppearance = true
        self.present(previewpostController, animated: true, completion: nil)

    }
    
    func onStoryViewTapped(user:UserModelNew, post: PostNew) {
        print("bc")
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
        
        previewpostController.post = post
        previewpostController.user = user
        previewpostController.popoverPresentationController?.sourceView = self.view
        previewpostController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        previewpostController.popoverPresentationController?.permittedArrowDirections = []
        
        present(previewpostController, animated: true, completion: nil)
    }
    
    func onTopicButtonTapped() {
        print("topic button tapped")
    }
    
    var postUi: PostUi?

    let postCellId = "postCellId"
    
    func onTitleLabelTapped(for cell: PostUi){
        print("title button tapped")
        
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.newPosts[indexPath.item]
        
        guard let postId = post.id else { return }
    
        let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
        postController.userId = post.uid
        postController.postId = postId
        navigationController?.pushViewController(postController, animated: true)
        
        
    }
    
    func onNameLabelTapped(for cell: PostUi){
        print("rocket test")

        guard let indexPath = collectionView?.indexPath(for: cell) else { return }

        let user = self.userModelNew[indexPath.item]
        print("user is", user)
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.user = user
        self.navigationController?.pushViewController(profileController,
                                                      animated: true)
        
    }
    

    func onloveButtonTapped(loc: CGPoint, for cell: PostUi){
        print("love button tapped")

        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        var post = self.posts[indexPath.item]
        let postUserId = post.user.uid
        guard let postId = post.id else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Database.database().reference().child("love").child(postId)
        let ref3 = Database.database().reference().child("posts").child(postUserId).child(postId)
        
        let values = [uid: post.hasLiked == true ? 0 : 1]
        
        ref.updateChildValues(values) { (err, _) in
            
            if let err = err {
                print("Failed to like post:", err)
                return
            }
            
            print("Successfully liked post.")
            
            post.hasLiked = true
            
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                let value = snapshot.value as? NSDictionary
                let cc = value?.count ?? 0
                
                let loveCount = ["loveCount":cc]
                let taps = 1
                let updates = [uid: taps]
                
                ref.updateChildValues(updates)

                ref3.updateChildValues(loveCount)
                post.loveCount = cc
                self.posts[indexPath.item] = post
                
                self.collectionView?.reloadItems(at: [indexPath])
            })
            
        }
       
    }
    
  
    
    var scene: AnimationScene!
    var size: CGSize!
    
    lazy var rocketView: UIImageView = {
        let image = UIImageView()
        image.image = #imageLiteral(resourceName: "ithaka_rocket")
        
        return image
    }()
    
    lazy var disButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ithaka_rocket"), for: .normal)
    
        button.tag = 111
        return button
    }()

   
    
    lazy var finaView: UICollectionView? = {
        return UICollectionView(frame: self.view.bounds, collectionViewLayout: DepthPageTransformer())
    }()
//
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    
    override func viewDidLoad() {
        super.viewDidLoad()
            self.collectionView?.backgroundColor = UIColor.white
            tabBarController?.delegate = self
        
             myActivityIndicator.backgroundColor = .white
            myActivityIndicator.color = .black
        
            view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant:0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)

        
            myActivityIndicator.startAnimating()
        
      
        collectionView?.register(HomeHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")

        
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "DalekPinpointBold", size: 21)!]

//        let titleimgage = UIImage(named: "ithakaTitleImage.png")
//
//        navigationItem.titleView = UIImageView(image: titleimgage)
        navigationItem.title = "Ithaka"
        collectionView?.delegate = self
    
        self.navigationController?.navigationBar.isTranslucent = false
    
        collectionView?.register(PostUi.self, forCellWithReuseIdentifier: postCellId)
//        addPosts()
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: AddIthakaFinalController.updateFeedNotificationName, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: EditPostController.updateFeedNotificationName, object: nil)
        


        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        
        collectionView?.refreshControl = refreshControl
        
    
        
        
        guard let fcmToken = Messaging.messaging().fcmToken else { return }
        let token: [String: AnyObject] = [fcmToken: fcmToken as AnyObject]
        self.postToken(Token: token)
        
        ///for swipe
        self.navigationController?.navigationBar.backgroundColor = UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
    
//        upd()
        NotificationCenter.default.addObserver(self, selector: #selector(self.BadgeWasUpdated), name: NSNotification.Name(rawValue: "com.api4242.ithaka"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(handleCommentUpdate), name: CommentsController.updateCommentsCountName, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: AddIthakaFinalController.updateFeedNotificationName, object: nil)
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleReadBackStory), name: PreviewPostController.updateReadStoryNotificationName, object: nil)

        
        checkIfKeyExists()
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.05
            
            collectionView?.contentInset = UIEdgeInsetsMake(30, ph, 100, ph)

            
        default:
            print("i am Everyone except ipad")
            collectionView?.contentInset = UIEdgeInsetsMake(20, 15, 100, 15)

            
        }

        



//        setupPointsTable()        
        
//        setupItemsForMe()
        
        loadPosts()
        
//
        
//        setupAllUserNameTable()
        
//        setupAllwritersTables()
        
        checkIfItemClaimed()

        NotificationCenter.default.addObserver(self, selector: #selector(hashtagTappedFromPost), name: PreviewPostController.PushHashtagPage, object: nil)
       
        NotificationCenter.default.addObserver(self, selector: #selector(usernameTappedFromPost), name: PreviewPostController.PushProfilePage, object: nil)

        
    }
    
    @objc func hashtagTappedFromPost(_ notification: NSNotification){
        if let hash = notification.userInfo?["hashtag"] as? String {
            self.hashTagTapped(hash: hash)
        }
    }

    @objc func usernameTappedFromPost(_ notification: NSNotification){
        if let uname = notification.userInfo?["username"] as? String {
           
            self.usernameTapped(username: uname)
            
        }
    }
    
    @objc func handleReadBackStory(_ notification: NSNotification){
        
        if let itemName = notification.userInfo?["itemName"] as? String {
            
            let itemsStoryController = ItemsStoryController(collectionViewLayout: UICollectionViewFlowLayout())
            itemsStoryController.itemCode = itemName
            self.navigationController?.pushViewController(itemsStoryController, animated: true)

        }


    }
    
    func checkIfItemClaimed(){
        
        if UserDefaults.standard.object(forKey: "FirstItemRecieved") == nil {
            guard let uid = Auth.auth().currentUser?.uid else { return }
            
            Api.User.observeUser(withId: uid) { (usermodelnew) in
                let fc = usermodelnew?.firstItemClaim
                
                if fc == true {
                    let chestController = ChestChildController()
                    chestController.delegate = self
                    
                    self.addChildViewController(chestController)
                    self.view.addSubview(chestController.view)
                    chestController.didMove(toParentViewController: self)
                    

        }
        
            }

        }

    }
    
    func setupAllwritersTables() {
        
        let ref = Database.database().reference().child("users")
        ref.observeSingleEvent(of: .value) { (snapshot) in
            if let vv = snapshot.value as? NSDictionary {
                vv.forEach({ (snapshot) in
                    let adv = ["firstItemClaimed": false]
                    ref.child(snapshot.key as! String).updateChildValues(adv)
                })
            }
            
        
            
            
        }

    }
    
    func setupAllUserNameTable() {
        let ref = Database.database().reference().child("allPublicUsernames")
        
        let currentUserNames = Database.database().reference().child("users")
        
        
        currentUserNames.observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            
            value.forEach({ (snapshot) in
                guard let value = snapshot.value as? NSDictionary else { return }

                let username = value["u_username"] as? String ?? "aa"
                
                print("user name is ", username)
                print("uid  is ", snapshot.key)

//              let vv = [username: true] as [String : Any]
//              ref.childByAutoId().updateChildValues(vv)
                

            })

        }
    }
    

    func setupItemsForMe(){
        let ref = Database.database().reference().child("myItems")
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let values = ["ci21":23, "ci13":11, "ri5": 2]
        
        ref.child(uid).updateChildValues(values)
    }
    
    func setupPointsTable(){
        let ref = Api.PointsAPII.Points_REF
        
        let userRef = Database.database().reference().child("users")
        
        userRef.observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            
            value.forEach({ (snapshot) in
                 let uid = snapshot.key
                
                print("snappy snap is ", snapshot)
                
                guard let value = snapshot.value as? NSDictionary else { return }
                let rc = value["profileProgress"] ?? 0
                
                let valueRock = ["CollectedPoints": rc]

                
                ref.child(uid as! String).updateChildValues(valueRock)
                
            })
        }
    }
    
    
    @objc func BadgeWasUpdated() {
        self.tabBarController?.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)
    }
    
    
    @objc func handleCommentUpdate() {
        collectionView?.reloadData()
    }

    
    func upd() {
        let ref = Database.database().reference().child("users")
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let ld = snapshot.value as? NSDictionary else { return }
            
            ld.forEach({ (snapshot) in
                let uidkey = snapshot.key
                
                let vv = ["uid":uidkey]
                
                let ref1 = Database.database().reference().child("posts").child(uidkey as! String)
                
                ref1.observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    guard let lds = snapshot.value as? NSDictionary else { return }
                    
                    lds.forEach({ (snapshot) in
                        ref1.child(snapshot.key as! String).updateChildValues(vv)

                    })

                })
            })
        }
    }
    
    //what could be the better plan for this? No ! Not that==
    func doit(){
        let ref1 = Database.database().reference().child("users")
        let ref2 = Database.database().reference().child("posts")
        let ref3 = Database.database().reference().child("rockets")

        ref1.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            value?.forEach({ (snapshot) in
                print("first all the user ids are", snapshot.key)
                
                ref2.child(snapshot.key as! String).observeSingleEvent(of: .value, with: { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    
                    value?.forEach({ (snapshot) in
                        print("first all the post ids are", snapshot.key)
                        let value5 = snapshot.value as? NSDictionary
                        let rc = value5!["rocketCount"] ?? 0
                        
                        let valueRock = ["rocketCount": rc]
                        
                        ref3.child(snapshot.key as! String).updateChildValues(valueRock)

                    })
                })
                
            })
        }
    }
    
    fileprivate var isLoadingPost = false

     func loadPosts() {
        self.isLoadingPost = true
        guard let uid = Auth.auth().currentUser?.uid else { return }

        Api.Feed.getRecentFeed(withId: uid, start: newPosts.first?.timestamp, limit: 3) { (results) in
            guard let results = results else { return }
            if results.count > 0 {
                results.forEach({ (result) in
                    if result.0.title == nil {
                        self.myActivityIndicator.stopAnimating()

                        self.collectionView?.reloadSections([0,0])
                        return
                    } else {
                        self.newPosts.append(result.0)
                        self.userModelNew.append(result.1)
                    }
                
                })
            }
            self.isLoadingPost = false
            
            DispatchQueue.main.async {
                self.myActivityIndicator.stopAnimating()
                self.collectionView?.reloadSections([0])

            }


        }
    }
    
 
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
            guard let uid = Auth.auth().currentUser?.uid else { return }

            guard !self.isLoadingPost else {
                return
            }
            
            self.isLoadingPost = true

            guard (self.newPosts.last?.timestamp) != nil else {
                self.isLoadingPost = false
                return
            }
            self.myActivityIndicator.startAnimating()

            Api.Feed.getOldFeed(withId: uid, start: (newPosts.last?.timestamp)!, limit: 3, completionHandler: { (results) in
                

                guard let results = results else { return }
                if results.count == 0 {
                    self.myActivityIndicator.stopAnimating()
                    self.collectionView?.reloadData()
                    self.isLoadingPost = false

                    return
                }
                for result in results {
                    if result.0.id == nil {
                        return
                    } else {
                        self.newPosts.append(result.0)
                        self.userModelNew.append(result.1)
                    }
                }
                
                self.myActivityIndicator.stopAnimating()

                self.collectionView?.reloadData()
                self.isLoadingPost = false

                   })
            
            
        }
    }
    
    
    let BlackLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        
        return view
    }()

  
    
    func setStatusBarBackgroundColor(color: UIColor) {
        
        guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
        statusBar.backgroundColor = color
    }

    func postToken(Token: [String: AnyObject]) {
        let ref = Database.database().reference()
        guard let uid = Auth.auth().currentUser?.uid else { return }

        guard let fcmToken = Messaging.messaging().fcmToken else { return }
        
        ref.child("fcmToken").child(uid).child(fcmToken).setValue(Token)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "DalekPinpointBold", size: 21)!]
        self.tabBarController?.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        collectionView?.collectionViewLayout.invalidateLayout()
    }
    
    @objc func handleUpdateFeed() {
        handleRefresh()
    }
    
    @objc func handleRefresh() {
        newPosts.removeAll()
        userModelNew.removeAll()
        loadPosts()
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
            self.myActivityIndicator.stopAnimating()

        }
    }
    
     func fetchAllPosts() {
//        fetchFollowingUserIds()
    }
    
    var isFinishedPaging = false
    var posts = [Post]()
    var newPosts = [PostNew]()
    var userModelNew = [UserModelNew]()
    var user: UserModel?

    
    var users = [UserModel]()
    
    
    fileprivate func setupAddIthaka(){
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8-plus_math_filled").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleAddIthaka))
    
    }
    
    @objc func handleAddIthaka() {
        let addIthakaController = AddIthakaAPI.addIthakaController
        addIthakaController.modalPresentationStyle = .custom
        addIthakaController.transitioningDelegate = self
        present(addIthakaController, animated: true, completion: nil)
    
   }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("new posts count are", newPosts.count)

        return newPosts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return view.frame.height-100
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    var checkForFirstItemClaim: Bool?
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
   
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postCellId, for: indexPath) as! PostUi
    
        let post = newPosts[indexPath.item]
        
        let user = userModelNew[indexPath.item]
        
        cell.post = post
        cell.user = user
        cell.textViewTap = self
        cell.delegate = self
        

//   collectionView.collectionViewLayout.invalidateLayout()
        return cell
    }
    
    var tagheight = 0

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.10
            
            return CGSize(width: view.frame.width - ph, height: CGFloat(330))

            
        default:
            print("i am Everyone except ipad")
            return CGSize(width: view.frame.width - 30, height: CGFloat(330))

            
        }
        

        

    }
    
    func showProfile() {
        let profileController = ProfileController()
        navigationController?.pushViewController(profileController, animated: true)
    }
    

}


extension HomeController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? PreviewPostController {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal,
                               interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard
                let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController,
                interactionController.inProgress
                else {
                    return nil
            }
            return interactionController
    }
}



extension HomeController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return SystemPushAnimator(type: .navigation)
        case .pop:
            return SystemPopAnimator(type: .navigation)
        default:
            return nil
        }
    }

}

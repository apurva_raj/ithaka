//
//  DownloadImagesController.swift
//  ithaka
//
//  Created by Apurva Raj on 26/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Photos
import KILabel
import SwiftMessages

class DownloadImagesController: UIViewController, UITextViewDelegate, UIDocumentInteractionControllerDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        return true
    }
    
    
    var user: UserModelNew? {
        didSet {
            guard let name = user?.u_username else { return }
            print("name is", name)
            nameLabel.text = name
            
            nameLabel1.text = name
            
            guard let profileImageUrl = user?.profileImageUrl else { return }
            
            profileImageView.loadImage(urlString: profileImageUrl)
            profileImageView1.loadImage(urlString: profileImageUrl)

        }
    }
    
    var post: PostNew? {
        didSet{
            guard let story = post?.story else { return }
            
            storyLabel.text = story
            
            storyLabel1.text = story
            
            guard let postItem = post?.postSetItem else {
                
                guard let profileItem = user?.profileSetItem  else { return }
                profileItemView.image = UIImage(named: profileItem + ".png")
                profileItemView1.image = UIImage(named: profileItem + ".png")

                return }

            profileItemView.image = UIImage(named: postItem + ".png")
            profileItemView1.image = UIImage(named: postItem + ".png")

            let sf = view.frame.width
            
            var storyFont = 21.0

            var storyHeightNew = story.height(withConstrainedWidth: sf-25, font: UIFont.systemFont(ofSize: CGFloat(storyFont)))

            let b = CGFloat(sf-75)
            
            while (storyHeightNew > b) {
                storyFont = storyFont - 1

                let storyHeightNews = story.height(withConstrainedWidth: sf-25, font: UIFont.systemFont(ofSize: CGFloat(storyFont)))
                
                storyLabel.font = UIFont.systemFont(ofSize: CGFloat(storyFont))
                storyHeightNew = storyHeightNews
            
            }
         

            
            var story1Font = 21

            var storyHeight1New = story.height(withConstrainedWidth: 600-70, font: UIFont.systemFont(ofSize: CGFloat(story1Font)))
            

            
            while (storyHeight1New > 400) {
                story1Font = story1Font - 1
                
                let storyHeight1News = story.height(withConstrainedWidth: 600-70, font: UIFont.systemFont(ofSize: CGFloat(story1Font)))
                
                storyLabel1.font = UIFont.systemFont(ofSize: CGFloat(story1Font))
                storyHeight1New = storyHeight1News
                
            }


        }
    }
    var af: Int? = 0
    
    
    let profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 15
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = UIColor.clear
        
        return imageView
    }()
    
    
    let profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "ri44")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 10
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        return imageView
    }()

    let profileImageView1: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 30
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = UIColor.clear
        
        return imageView
    }()
    
    
    let profileItemView1: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "ci22")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 15
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        return imageView
    }()


    let storyLabel: UILabel = {
        let textview = UILabel()
        textview.textColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.85)
        textview.numberOfLines = 0

        return textview
    }()
  
    let nameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .black
        label.isUserInteractionEnabled = false
        label.font = UIFont.boldSystemFont(ofSize: 16)

        return label
    }()
    
    
    let nameLabel1: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .left
        label.textColor = .black
        label.isUserInteractionEnabled = false
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.text = "ithaka user"
        return label
    }()

    
    let ithakaLogo: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "ithakaapp_logo_trans").withRenderingMode(.alwaysOriginal)
        return view
    }()

    let ithakaLogo1: UIImageView = {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "ithakaapp_logo_trans").withRenderingMode(.alwaysOriginal)
        return view
    }()

    override func viewWillAppear(_ animated: Bool) {
    }
    
    let storyLabel1: UILabel = {
        let textview = UILabel()
        textview.textColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.85)
        textview.font = UIFont.systemFont(ofSize: 25)
        //        textview.contentInset = UIEdgeInsetsMake(-5, 0, 0, 0)
        textview.numberOfLines = 0
        
        return textview
    }()
    


    let firstUIView: UIView = {

        let fview = UIView()
        fview.backgroundColor = .white
        fview.layer.cornerRadius = 15.0
        fview.layer.shadowColor = UIColor.gray.cgColor
        fview.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        fview.layer.shadowRadius = 12.0
        fview.layer.shadowOpacity = 0.5
        fview.layer.borderColor = UIColor.gray.cgColor
        fview.layer.borderWidth = 1
        fview.clipsToBounds = true
        return fview
    }()
    
    let secUIView: UIView = {
        
        let sview = UIView()
        sview.backgroundColor = .white
        sview.layer.borderColor = UIColor.black.cgColor
        sview.layer.borderWidth = 3

        return sview
    }()
    
    func shareInstagram(imageShare: UIImage) {
        
        
        let objectsToShare: [AnyObject] = [ imageShare ]
        let activityViewController = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
        
    
    }
    
    let uui: UIView = {
        let v = UIView()
        v.backgroundColor = .white
        return v
    }()


    lazy var squarePicImageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .white
        iv.contentMode = .scaleToFill
        return iv
    }()
    
    lazy var storyPicImageView: UIImageView = {
        let iv = UIImageView()
        iv.backgroundColor = .white
        iv.contentMode = .scaleToFill
        
        iv.layer.borderColor = UIColor.black.cgColor
        iv.layer.borderWidth = 1

        return iv
    }()
    
    
    override func viewDidAppear(_ animated: Bool) {
//        let image = UIImage.init(view: secUIView)
//        storyPicImageView.image = image

    }
    
    lazy var shareSquareButton: UILabel = {
        let button = UILabel()
        button.isUserInteractionEnabled = true
        button.text = "Share"
        button.textAlignment = .center
        button.font = UIFont.systemFont(ofSize: 20)
        button.textColor = self.view.tintColor
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleShare))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)

        return button
    }()
    
    @objc func handleShare(){
        print("shared")
        
        myActivityIndicator.startAnimating()
        
        let image = UIImage.init(view: mainUIView)

        self.shareInstagram(imageShare: image)
        
        myActivityIndicator.stopAnimating()

    }


    lazy var downloadSqureButton: UILabel = {
        let button = UILabel()
        button.isUserInteractionEnabled = true
        button.text = "Download story size image"
        button.font = UIFont.systemFont(ofSize: 16)
        button.textColor = self.view.tintColor
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleDownload))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        
        return button
    }()
    
    @objc func handleDownload(){
        
        self.dismiss(animated: true, completion: nil)
        
        SwiftMessages.show {
            let view = MessageView.viewFromNib(layout: .CardView)
            view.configureTheme(.warning)
            view.configureDropShadow()
            let iconText = "🙂"
            view.configureContent(title: "Downloaded", body: "Check your Photos App to the image", iconText: iconText)
            view.button?.isHidden = true
            
            return view
        }
        
        let image = UIImage.init(view: mainUIView)

        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
    
    }

    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        
        return button
    }()
    @objc func handleDismiss(){
        dismiss(animated: true, completion: nil)
        oneview.backgroundColor = .white
    }
    
    let oneview = UIView()

    let cView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return v
    }()
    let yView: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
        v.clipsToBounds = true
        return v
    }()
    let yView1: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.rgb(red: 255, green: 241, blue: 45, alpha: 1)
        
        return v
    }()
    
    let grayView1: UIView = {
        let v = UIView()
        v.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return v
    }()
    let grayView1s: UIButton = {
        let v = UIButton()
        return v
    }()


    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)


    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.white

        
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.8)
        myActivityIndicator.color = .white
        
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: self.view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant:0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        
        myActivityIndicator.startAnimating()

        
        setupSquarePicView()
        
//        setupStoryPicView()
        setupNewView()
        
        view.bringSubview(toFront: firstUIView)

        oneview.backgroundColor = .black
        
        
        myActivityIndicator.stopAnimating()
        
        self.view.layer.cornerRadius = 10
        self.view.clipsToBounds = true
        
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)

        setupTopBar()

    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
                self.handleDismiss()
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
     func setupTopBar() {
        
        let newview = UIView()
        newview.backgroundColor = .white
        
        self.view.addSubview(newview)
        
        newview.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        let stopButton: UIButton = {
            let button = UIButton()
            button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
            button.tintColor = UIColor.blue
            button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)

            return button
        }()
        let shareButton: UIButton = {
            let button = UIButton()
            button.setImage(#imageLiteral(resourceName: "icons8-installing-updates-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
            button.tintColor = UIColor.blue
            button.addTarget(self, action: #selector(handleDownload), for: .touchUpInside)

            return button
        }()

        
        newview.addSubview(shareButton)
        newview.addSubview(stopButton)
        
        stopButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        
        shareButton.anchor(top: newview.topAnchor, left: nil, bottom: nil, right: newview.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 30, heightConstant: 30)

    }
    
    func setupSquarePicView() {
        view.addSubview(firstUIView)
        
        let newH = (view.frame.height/2) - (view.frame.width-40)/2
        
        firstUIView.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: newH/2, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: view.frame.width, heightConstant: view.frame.width)
        
        
        firstUIView.addSubview(storyLabel)
        firstUIView.addSubview(yView)
        
        
        storyLabel.anchor(top: firstUIView.topAnchor, left: firstUIView.leftAnchor, bottom: nil, right: firstUIView.rightAnchor, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: view.frame.width-75)
        yView.anchor(top: storyLabel.bottomAnchor, left: firstUIView.leftAnchor, bottom: firstUIView.bottomAnchor, right: firstUIView.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 50)
        
        yView.addSubview(profileImageView)
        yView.addSubview(profileItemView)

        yView.addSubview(nameLabel)
        yView.addSubview(ithakaLogo)
        
        profileImageView.anchor(top: nil, left: yView.leftAnchor, bottom: yView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 10, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        
        profileItemView.anchor(top: nil, left: profileImageView.rightAnchor, bottom: profileImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: -10, bottomConstant: 3, rightConstant: 0, widthConstant: 20, heightConstant: 20)

        
        nameLabel.anchor(top: nil, left: profileItemView.rightAnchor, bottom: yView.bottomAnchor, right: nil, topConstant: 0, leftConstant: 10, bottomConstant:15, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        ithakaLogo.anchor(top: nil, left: nil, bottom: yView.bottomAnchor, right: yView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 2, rightConstant: 10, widthConstant: 50, heightConstant: 50)
        

        view.addSubview(shareSquareButton)
        shareSquareButton.anchor(top: firstUIView.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        
        

    }
    
    let mainUIView: UIView = {
        
        let sview = UIView()
        sview.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        return sview
    }()

    let SubmainUIView: UIView = {
        
        let fview = UIView()
        fview.backgroundColor = .white
        fview.layer.cornerRadius = 20.0
        fview.layer.shadowColor = UIColor.gray.cgColor
        fview.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        fview.layer.shadowRadius = 12.0
        fview.layer.shadowOpacity = 0.5
        fview.clipsToBounds = true
        return fview
    }()
    

    func setupNewView() {
        view.addSubview(mainUIView)
        mainUIView.frame = CGRect(x: 0, y: 2000, width: 600, height: 1067)
        
        mainUIView.addSubview(SubmainUIView)
        
        let newH =  (1067/2) - (600-50)/2
        

        SubmainUIView.anchor(top: mainUIView.topAnchor, left: mainUIView.leftAnchor, bottom: nil, right: mainUIView.rightAnchor, topConstant: CGFloat(newH), leftConstant: 25, bottomConstant: 0, rightConstant: 25, widthConstant: mainUIView.frame.width - 50, heightConstant: mainUIView.frame.width - 50)


        SubmainUIView.addSubview(storyLabel1)

        storyLabel1.anchor(top: SubmainUIView.topAnchor, left: SubmainUIView.leftAnchor, bottom: nil, right: SubmainUIView.rightAnchor, topConstant: 20, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: mainUIView.frame.width - 140)
        
        SubmainUIView.addSubview(yView1)
        yView1.anchor(top: nil, left: SubmainUIView.leftAnchor, bottom: SubmainUIView.bottomAnchor, right: SubmainUIView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 80)
        
        yView1.addSubview(nameLabel1)
        yView1.addSubview(profileImageView1)
        yView1.addSubview(profileItemView1)
        
        yView1.addSubview(ithakaLogo1)

        
        
        profileImageView1.anchor(top: nil, left: yView1.leftAnchor, bottom: yView1.bottomAnchor, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 15, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        
        profileItemView1.anchor(top: nil, left: profileImageView1.rightAnchor, bottom: profileImageView1.bottomAnchor, right: nil, topConstant: 0, leftConstant: -10, bottomConstant: 3, rightConstant: 0, widthConstant: 30, heightConstant: 30)
        
        
        nameLabel1.anchor(top: nil, left: profileItemView1.rightAnchor, bottom: yView1.bottomAnchor, right: nil, topConstant: 0, leftConstant: 10, bottomConstant:20, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
 
        
        ithakaLogo1.anchor(top: nil, left: nil, bottom: yView1.bottomAnchor, right: yView1.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 20, widthConstant: 100, heightConstant: 100)



    }
    
    func setupStoryPicView() {
        
        view.addSubview(secUIView)
        view.sendSubview(toBack: secUIView)
        secUIView.frame = CGRect(x: 0, y: 2000, width: 1080, height: 1920)
        
        
        secUIView.addSubview(storyLabel1)
        secUIView.addSubview(yView1)
        yView1.anchor(top: nil, left: secUIView.leftAnchor, bottom: secUIView.bottomAnchor, right: secUIView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 130)

        yView1.addSubview(nameLabel1)
        yView1.addSubview(ithakaLogo1)
      
        nameLabel1.anchor(top: nil, left: yView1.leftAnchor, bottom: yView1.bottomAnchor, right: nil, topConstant: 0, leftConstant: 50, bottomConstant:55, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        ithakaLogo1.anchor(top: nil, left: nil, bottom: yView1.bottomAnchor, right: yView1.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 30, rightConstant: 50, widthConstant: 140, heightConstant: 140)

        view.addSubview(grayView1)
        
        grayView1.anchor(top: shareSquareButton.bottomAnchor, left: view.leftAnchor, bottom: nil, right: nil, topConstant: 35, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: view.frame.width, heightConstant: 120)
   
        grayView1.addSubview(storyPicImageView)
        grayView1.addSubview(downloadSqureButton)

        storyPicImageView.anchor(top: grayView1.topAnchor, left: grayView1.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: 25, bottomConstant: 20, rightConstant: 0, widthConstant: 50, heightConstant: 80)
        downloadSqureButton.anchor(top: storyPicImageView.topAnchor, left: storyPicImageView.rightAnchor, bottom: nil, right:nil, topConstant: 30, leftConstant: 35, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

    }
    
}
    

//
//  FollowAPI.swift
//  ithaka
//
//  Created by Apurva Raj on 05/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseDatabase

class FollowAPI {
    var REF_FOLLOWERS = Database.database().reference().child("followers")
    var REF_FOLLOWING = Database.database().reference().child("following")

}

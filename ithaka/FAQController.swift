//
//  FAQController.swift
//  ithaka
//
//  Created by Apurva Raj on 09/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FAQView

class FAQController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    let items = [FAQItem(question: "Why the name Ithaka?", answer: "You may want to google “Ithaka Poem” Or we have also added it on front logIn page. You may have noticed it while opening the app first time.. That poem is the reason this App exist. It aligns with the vision in which we extremely believe in."),
                 FAQItem(question: "What are these virtual items?", answer: "We have added over 100+ items(We’ll be keep adding more and more with time and new updates). These items are port of our little Ithaka World. We have common items, unCommon items and rare items. All the items have backstory to give it more meaning. And all of them have some utility in App."),
                 FAQItem(question: "What’s the use of these items?", answer: "You have to have one item to write a story. That item decides to minimum words required to a story. Once you share it, the item is attached to your story and is gone from your profile item list. \n\n   Collect as many items as you can. Some of them are only to showoff. While of the items are active. i.e. Some keys unlock new quests. Soon, we will have whole economy around this item.\n\n  Also, soon you will see some of your items turning from inactive to active items. You may be able to boost your stories with active items and get more views."),
                 FAQItem(question: "What is that level thing on my profile?", answer: "For each story you share, for each comments you get and for each like you get, you get some points. Those points converts into level. Higher profile level get you higher visibility and unlocks new quests for you."),
                 FAQItem(question: "What is Mini Quest?", answer: "Mini Quest is just a fun way to make you write more and earn more rewards.  Complete all three levels to get more rewards."),
                 FAQItem(question: "What is Monster Quest?", answer: "Ah! Monster quest is a crazy experiment. Unfortunately, it doesn’t work well if we don’t have lots of users. That’s why It is having bit higher requirement to unlock it.\n\nSome of the items are weapon items. Monsters will appear and you are to apply weapons and call your friends to join you if you want to fight bigger monsters. Ultimately, you will be fighting monsters - with your words. You and your friends will be writing on given topic and within given time simultaneously to defeat monster and have fun. Pro tip - Finish it within given time or else, monster will take your weapons and run away."),
                 FAQItem(question: "Will there be other exciting quests except mini and monster quest?", answer: "Absolutely.Yes!"),

                 FAQItem(question: "I want to talk with the person behind this App",
                         answer: "Sure. You can email ar@ithaka.app or send a DM on Instagram @api_42 or tweeter @ApurvaRaj42 \n\n Feel free to send any suggestions, feedback, any issues you are facing or just good words. It will be always appreciated.")]
    
    lazy var iconLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(13)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = .gray
        label.isUserInteractionEnabled = false
        label.text = "icons by icons8 and flaticon"
        
        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = .white

        let faqViews = FAQView(frame: view.frame, title: "Anorak's Scroll",  items: items)
        faqViews.backgroundColor = .white
        faqViews.separatorColor = .white
        setupTopBar()

        view.addSubview(faqViews)
        faqViews.anchor(top: newview.bottomAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: view.frame.width, heightConstant: view.frame.height - 180)
        

    }
    
    
    let newview = UIView()
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        return button
    }()
    
    func setupTopBar() {
        
        newview.backgroundColor = .white
        
        let newview1 = UIView()
        newview1.backgroundColor = .white
        self.view.addSubview(newview1)
        newview1.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        view.addSubview(newview)
        
        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        
        newview.addSubview(backButton)
        backButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        view.bringSubview(toFront: backButton)
        

    }
    
    @objc func handleBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    

}


class helpCard: UICollectionViewCell {
    
    weak var delegate: AddIthakaItemSlect?

    lazy var mainbg: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        view.isUserInteractionEnabled = true

        let tap = UITapGestureRecognizer(target: self, action: #selector(questLabelTapped))
        tap.numberOfTapsRequired = 1
        view.addGestureRecognizer(tap)
        

        return view
    }()
    
    lazy var mainbg2: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 15
        view.clipsToBounds = true
        view.isUserInteractionEnabled = true
        
        
        return view
    }()
    

    
    lazy var questImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "Helpbook")
        imageView.isUserInteractionEnabled = true
        imageView.tag = 4
        let tap = UITapGestureRecognizer(target: self, action: #selector(questLabelTapped))
        tap.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tap)

        return imageView
    }()
    
    lazy var feedbackImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "ri44")
        imageView.isUserInteractionEnabled = true
        imageView.tag = 42
        let tap = UITapGestureRecognizer(target: self, action: #selector(questLabelTapped))
        tap.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tap)
        
        return imageView
    }()

    lazy var questLabel: UILabel = {
        let label = UILabel()
        label.font =  UIFont.boldSystemFont(ofSize: 13)
        label.textAlignment = .center
        label.text = "Help"
        label.tag = 4
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(questLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
    }()
    
    lazy var feedbackLabel: UILabel = {
        let label = UILabel()
        label.font =  UIFont.boldSystemFont(ofSize: 13)
        label.textAlignment = .center
        label.text = "Feedback"
        label.tag = 42
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(questLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        return label
    }()

    @objc func questLabelTapped(sender:UITapGestureRecognizer){
        print("Send")
        self.delegate?.dailyQuestCardTapped(tag:sender.view?.tag ?? 0)
        
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.addSubview(mainbg)
        self.addSubview(mainbg2)
        

        mainbg.addSubview(questImage)
        mainbg.addSubview(questLabel)
        mainbg2.addSubview(feedbackImage)
        mainbg2.addSubview(feedbackLabel)
        
        mainbg.anchor(top: self.topAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 80, heightConstant: 70)
        
        mainbg2.anchor(top: self.topAnchor, left: nil, bottom: nil, right: mainbg.leftAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 30, widthConstant: 80, heightConstant: 70)
        
        questImage.anchor(top: mainbg.topAnchor, left: mainbg.leftAnchor, bottom: nil, right: mainbg.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 45)
        
        feedbackImage.anchor(top: mainbg2.topAnchor, left: mainbg2.leftAnchor, bottom: nil, right: mainbg2.rightAnchor, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 35)

        self.bringSubview(toFront: feedbackImage)

        questLabel.anchor(top: questImage.bottomAnchor, left: mainbg.leftAnchor, bottom: nil, right: mainbg.rightAnchor, topConstant: 0, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        
        feedbackLabel.anchor(top: feedbackImage.bottomAnchor, left: mainbg2.leftAnchor, bottom: nil, right: mainbg2.rightAnchor, topConstant:3, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        
    
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

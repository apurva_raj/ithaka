//
//  ProfileEditCell.swift
//  ithaka
//
//  Created by Apurva Raj on 22/03/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit
import Photos

class profileEditCell: UICollectionViewCell {
    
    weak var delegate: ProfileEditNewDelegate?
    
    
    var user: UserModelNew?{
        didSet {
            guard let username = user?.username else { return }
            guard let shortBio = user?.shortBio else { return }
            guard let profileItem = user?.profileSetItem else { return }
            guard let profileImgUrl = user?.profileImageUrl else { return }
            
            usernameLabel.text = username
            shortBioTextView.text = shortBio
            
            let im = UIImage(named: profileItem + ".png")
            
            profileItemView.image = im
            profileImageView.loadImage(urlString: profileImgUrl)
        }
    }
    
    lazy var profileImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.layer.borderWidth = 0.5
        imageView.layer.borderColor = UIColor.gray.cgColor
        imageView.layer.cornerRadius = 71
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped))
        tap.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tap)
        
        return imageView
    }()
    
    lazy var profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "5")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 40
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(profileItemImageTapped))
        tap.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tap)
        
        return imageView
    }()
    
    @objc func profileImageTapped(){
        print("tapped")
        
        self.delegate?.onProfileImageTapped()
        
        
    }
    
    
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        print("should selcted")
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        profileImageView.image = image
        print(image)
//        navigationController?.tabBarController?.selectedIndex = 4
//        dismiss(animated:true, completion: nil)
        
    }
    
    lazy var usernameLabel: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.isUserInteractionEnabled = true
        tv.contentInset = UIEdgeInsetsMake(0, 7, 0, 0)
        return tv
    }()
    
    lazy var cLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.isUserInteractionEnabled = true
        label.text = "Change"
        label.textColor = UIColor.rgb(red: 0, green: 112, blue: 201, alpha: 1)
        label.textAlignment = .center
        let tap = UITapGestureRecognizer(target: self, action: #selector(profileImageTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    lazy var cILabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.isUserInteractionEnabled = true
        label.text = "Change"
        label.textColor = UIColor.rgb(red: 0, green: 112, blue: 201, alpha: 1)
        label.textAlignment = .center
        let tap = UITapGestureRecognizer(target: self, action: #selector(profileItemImageTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        
        return label
    }()
    
    @objc func profileItemImageTapped() {
        print("Tapped bitches")
        
        self.delegate?.onProfileItemImageTapped()
        
        
    }
    
    
    let uLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.isUserInteractionEnabled = false
        label.text = "YOUR NAME"
        label.textColor = .gray
        
        return label
    }()
    
    let bLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.isUserInteractionEnabled = false
        label.text = "YOUR BIO IN ONE LINE"
        label.textColor = .gray
        
        return label
    }()
    
    
    
    let shortBioTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 16)
        tv.sizeToFit()
        tv.contentInset = UIEdgeInsetsMake(0, 7, 0, 0)
        
        tv.isEditable = true
        tv.isScrollEnabled = false
        
        return tv
    }()
    
    func backbuttonTapped() {
        print("Step 2")
        self.delegate?.dismiss()
    }
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        
        return button
    }()
    
    lazy var saveButton: UIButton = {
        let button = UIButton()
        button.setTitle("Save", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleSave), for: .touchUpInside)
        
        
        return button
    }()
    
    @objc func handleSave(){
        print("Saving 1")
        
        let username = self.usernameLabel.text ?? ""
        let bio = self.shortBioTextView.text ?? ""
        
        self.delegate?.onProfileEditSaveTapped(username: username, bio: bio)
    }
    

    
    @objc func handleBack(){
        self.backbuttonTapped()
    }

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupTopbar()

        
        addSubview(profileImageView)
        addSubview(profileItemView)
        let lc = (frame.width/2)-71
        profileImageView.anchor(top: self.backButton.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: lc, bottomConstant: 0, rightConstant: 0, widthConstant: 142, heightConstant: 142)
        
        profileItemView.anchor(top: nil, left: profileImageView.rightAnchor, bottom: profileImageView.bottomAnchor, right: nil, topConstant: 0, leftConstant: -25, bottomConstant: 3, rightConstant: 0, widthConstant: 80, heightConstant: 80)
        
        addSubview(cILabel)
        cILabel.anchor(top: profileItemView.bottomAnchor, left: profileItemView.leftAnchor, bottom: nil, right: profileItemView.rightAnchor, topConstant: 2, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)
        
        addSubview(cLabel)
        cLabel.anchor(top: profileImageView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 15)
        self.bringSubview(toFront: cILabel)
        
        addSubview(uLabel)
        uLabel.anchor(top: cLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        
        
        addSubview(usernameLabel)
        usernameLabel.anchor(top: uLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 40)
        
        addSubview(bLabel)
        bLabel.anchor(top: usernameLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 5, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        addSubview(shortBioTextView)
        shortBioTextView.anchor(top: bLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        self.bringSubview(toFront: profileItemView)

        
    }
    
    
    func setupTopbar(){
        let newview = UIView()
        newview.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        
        let newview1 = UIView()
        newview1.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        addSubview(newview1)
        newview1.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.safeAreaLayoutGuide.topAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        addSubview(newview)
        
        newview.anchor(top: self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        
        newview.addSubview(backButton)
        newview.addSubview(saveButton)

        backButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        saveButton.anchor(top: newview.topAnchor, left: nil, bottom: nil, right: newview.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)

        
        self.bringSubview(toFront: backButton)
        
    }
    

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

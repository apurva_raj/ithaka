//
//  PostApi.swift
//  ithaka
//
//  Created by Apurva Raj on 04/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

class PostApi {
    var REF_POSTS = Database.database().reference().child("posts")
    
    func observePosts(completion: @escaping (PostNew) -> Void) {
        REF_POSTS.observe(.childAdded) { (snapshot: DataSnapshot) in
            if let dict = snapshot.value as? [String: Any] {
                let newPost = PostNew.transformPostPhoto(dictionary: dict, key: snapshot.key)
                completion(newPost)
            }
        }
    }
    
    func observePost(withId id: String, completion: @escaping (PostNew) -> Void) {
        REF_POSTS.child(id).observeSingleEvent(of: DataEventType.value, with: {
            snapshot in
            if let dict = snapshot.value as? [String: Any] {
                let post = PostNew.transformPostPhoto(dictionary: dict, key: snapshot.key)
                completion(post)
            }
        })
    }
    
    func observeSinglePost(withUId uid: String, withPid pid: String, completion: @escaping (PostNew?) -> Void) {
        
            REF_POSTS.child(uid).child(pid).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                
                if snapshot.exists() {
                    if let dict = snapshot.value as? [String: Any] {
                        let postNew = PostNew.transformPostPhoto(dictionary: dict, key: snapshot.key)
                        
                        guard let current_uid = Auth.auth().currentUser?.uid else { return }

                        Database.database().reference().child("rockets").child(snapshot.key).child(current_uid).observeSingleEvent(of: .value, with: { (snapshot) in
                            if let value = snapshot.value as? Bool, value == true {
                                postNew.isLiked = true
                            } else {
                                postNew.isLiked = false
                            }
                        })
                        completion(postNew)
                    }
                }
        }
    }
    
    
    func observeSinglePost1(withUId uid: String, withPid pid: String, completion: @escaping (PostNew?) -> Void) {
        
        REF_POSTS.child(uid).child(pid).observeSingleEvent(of: DataEventType.value) { (snapshot) in
            
            if snapshot.exists() {
                if let dict = snapshot.value as? [String: Any] {
                    let postNew = PostNew.transformPostPhoto(dictionary: dict, key: snapshot.key)
                    
                    guard let current_uid = Auth.auth().currentUser?.uid else { return }
                    
                    Database.database().reference().child("rockets").child(snapshot.key).child(current_uid).observeSingleEvent(of: .value, with: { (snapshot) in
                        if let value = snapshot.value as? Bool, value == true {
                            postNew.isLiked = true
                        } else {
                            postNew.isLiked = false
                        }
                    })
                    completion(postNew)
                }
            } else {
                completion(nil)
            }
        }
    }

    
    
    func incrementLikes(userId: String, postId: String, onSucess: @escaping (PostNew) -> Void, onError: @escaping (_ errorMessage: String?) -> Void) {
        
        let postRef = Database.database().reference().child("rockets").child(postId)
        let postRef1 = Api.NewPost.REF_POSTS.child(userId).child(postId)
        let usersRef = Database.database().reference().child("users").child(userId)
        
        postRef.runTransactionBlock({ (currentData: MutableData) -> TransactionResult in
            let uui = Auth.auth().currentUser?.uid
            if var post = currentData.value as? [String : AnyObject], let uid = uui {
                

                var rocketCount = post["rocketCount"] as? Int ?? 0
                if let _ = post[uid] {
                    rocketCount -= 1
                    post.removeValue(forKey: uid)
                } else {
                    rocketCount += 1
                    post[uid] = true as AnyObject
                }
                post["rocketCount"] = rocketCount as AnyObject?
                print("dd", post)

                print("rc", post)
                let rocketValue = ["rocketCount": rocketCount]
                postRef1.updateChildValues(rocketValue)
                currentData.value = post
                
                Api.PointsAPII.increasePoints(n_points: 0, UserId: userId, catagory: 0)

                return TransactionResult.success(withValue: currentData)
            }
            return TransactionResult.success(withValue: currentData)
        }) { (error, committed, snapshot) in
            if let error = error {
                onError(error.localizedDescription)
            }
            if let dict = snapshot?.value as? [String: Any] {
                let post = PostNew.transformPostPhoto(dictionary: dict, key: snapshot!.key)
                onSucess(post)
            }
        }
        
        
    }
    
    
}

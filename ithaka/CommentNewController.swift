//
//  CommentNewController.swift
//  ithaka
//
//  Created by Apurva Raj on 03/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase

class CommentNewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, commentDelegate, CommentInputAccessoryViewDelegate {
   
    func didSubmit(for comment: String) {
        print("comment is ", comment)
    }
    
    func onLikeButtonTapped(for cell: CommentCell) {
        print("pritn cbc")
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        DispatchQueue.main.async {
            self.collectionView?.reloadItems(at: [indexPath])
        }
    }
    
   
    
    func onProfileTapped(for cell: CommentCell) {
        return
    }
    
    
    
    var post: PostNew?

    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)

    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Comments"

        self.collectionView?.contentInsetAdjustmentBehavior = .never
        collectionView?.backgroundColor = UIColor.rgb(red: 255, green: 255, blue: 255, alpha: 1)
        collectionView?.delegate = self
        
        collectionView?.backgroundColor = .white
        collectionView?.alwaysBounceVertical = true
        collectionView?.keyboardDismissMode = .interactive
        
        collectionView?.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: -50, right: 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsets(top: 0, left: 0, bottom: -50, right: 0)

        
        collectionView?.register(CommentCell.self, forCellWithReuseIdentifier: "cellId")
        
        
        fetchNewComments()

    }
    
    var commentNew = [CommentNew]()
    var usersNew = [UserModelNew]()
    
    fileprivate func fetchNewComments() {
        
            guard let postId = self.post?.id else { return }
            Api.Comment.getRecentComments(withId: postId, limit: 10, completionHandler: { (results) in
                guard let results = results else { self.myActivityIndicator.stopAnimating()
                    return }
                
                if results.count > 0 {
                    results.forEach({ (result) in
                        self.commentNew.append(result.0)
                        self.usersNew.append(result.1)
                        
                    })
                }
            })
        
        collectionView?.reloadData()
            
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(commentNew.count)
        
        return commentNew.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
        let dummyCell = CommentCell(frame: frame)
        dummyCell.comment = commentNew[indexPath.item]
        dummyCell.layoutIfNeeded()
        
        let targetSize = CGSize(width: view.frame.width, height: 1000)
        let estimatedSize = dummyCell.systemLayoutSizeFitting(targetSize)
        
        let height = max(40 + 8 + 8, estimatedSize.height)
        return CGSize(width: view.frame.width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellId", for: indexPath) as! CommentCell
        //        cell.delegate = self
        cell.cDelegate = self
        cell.comment = self.commentNew[indexPath.item]
        return cell
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    lazy var containerView: CommentInputAccessoryView = {
        let frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
        let commentInputAccessoryView = CommentInputAccessoryView(frame: frame)
        commentInputAccessoryView.delegate = self
        return commentInputAccessoryView
    }()

    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    

    override var inputAccessoryView: UIView? {
        get {
            return containerView
        }
    }
}

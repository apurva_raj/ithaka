//
//  TitleHeader.swift
//  ithaka
//
//  Created by Apurva Raj on 01/11/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import UIKit

class TitleHeader: UICollectionViewCell {
    
    weak var delegate: SearchDelegate?

    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 30)
        label.numberOfLines = 0
        label.textAlignment = .left
        label.isUserInteractionEnabled = false
        label.textColor = .black

        return label
    }()
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.layoutIfNeeded()
        self.layoutSubviews()
        
        addSubview(titleLabel)
        
        titleLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 70)
        

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  ExploreTwoTitles.swift
//  ithaka
//
//  Created by Apurva Raj on 27/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class ExploreTwoTitles: UICollectionViewCell {
    
    
    weak var delegate: exploreDelegate?
    
    lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        label.textColor = .black
        label.text = "Read stories from"
        
        return label
    }()
    

    lazy var titleLabel1: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 24)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        label.textColor = .black
        label.backgroundColor = .white
        label.padding = UIEdgeInsetsMake(10, 10, 10, 10)
        label.setupShadow()
        label.clipsToBounds = true
        
        label.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(dailyTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)
        

        return label
    }()
    
    @objc func dailyTapped(){
        self.delegate?.onDailyQuestFeed()
    }
    
    lazy var titleLabel2: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 18)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.isUserInteractionEnabled = false
        label.textColor = .black
        label.text = "Monster Battle"
        label.backgroundColor = .white
        label.clipsToBounds = true
        label.padding = UIEdgeInsetsMake(10, 10, 10, 10)
        label.setupShadow()
        label.isUserInteractionEnabled = true

        let tap = UITapGestureRecognizer(target: self, action: #selector(monsterTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        
        return label
    }()
    
    @objc func monsterTapped(){
        self.delegate?.onMonsterBattleFeed()
    }

    override var layer: CALayer {
        let layer = super.layer
        layer.zPosition = 0 // make the header appear below the collection view scroll bar
        return layer
    }

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)

        addSubview(titleLabel)
        addSubview(titleLabel1)

        
        let attachment:NSTextAttachment = NSTextAttachment()
        attachment.image =  #imageLiteral(resourceName: "book")
        
        attachment.setImageHeight(height: 42)
        
        let attachmentString:NSAttributedString = NSAttributedString(string:" Daily Quest")
        let attributedString:NSMutableAttributedString = NSMutableAttributedString(attachment: attachment)
        
        
        attributedString.append(attachmentString)
        
        titleLabel1.attributedText = attributedString

        
        titleLabel.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        titleLabel1.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 55)

        
        
//        let stackView = UIStackView(arrangedSubviews: [titleLabel1, titleLabel2])
//        stackView.axis = .horizontal
//        stackView.distribution = .fillEqually
//        stackView.spacing = 20
//        addSubview(stackView)
//
//        stackView.anchor(top: titleLabel.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 20, bottomConstant: 0, rightConstant: 20, widthConstant: 0, heightConstant: 50)


    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//
//  RocketListController.swift
//  ithaka
//
//  Created by Apurva Raj on 30/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase

class RocketListController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    
    var backbutton: String?{
        didSet{
            if backbutton == "lost" {
                let dismissButton: UIButton = {
                    let button = UIButton()
                    button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
                    button.tintColor = .white
                    button.backgroundColor = .black
                    button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
                    
                    return button
                }()

                let oneview = UIView()
                oneview.backgroundColor = .black

                view.addSubview(dismissButton)
                view.addSubview(oneview)

                dismissButton.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
                oneview.anchor(top: view.safeAreaLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

                
            }
        }
    }
    
    @objc func handleDismiss(){
        dismiss(animated: true, completion: nil)
        
    }

    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    let cellId = "cellId"

    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.center = view.center
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 50, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()
        
        collectionView?.delegate = self
        collectionView?.backgroundColor = .white
        
        collectionView?.register(RocketList.self, forCellWithReuseIdentifier: cellId)
        
        collectionView?.register(TitleHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        
        
        collectionView?.contentInset = UIEdgeInsetsMake(10, 0, 15, 15)
        
    }
    
   
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-30, height: 55)
    }

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! TitleHeader
        header.titleLabel.text = "Liked by"
        header.backgroundColor = .white
        
        return header
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: view.frame.width, height: 60)
        
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! RocketList
        
        cell.user = users[indexPath.item]
        
        return cell
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("users count are", users.count)
        return users.count
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let userNew = users[indexPath.item]
        print(userNew)
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = userNew.uid
        navigationController?.pushViewController(profileController,animated: true)
        
    }

    var users = [UserModelNew]()
    var user: UserModelNew?
    var post: PostNew?{
        didSet{
            fetchUser()
        }
    }
    
    func fetchUser() {
        guard let pid = post?.id else { return }
        
        let ref = Database.database().reference().child("rockets").child(pid)
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let dictionaries = snapshot.value as? [String: Any] else { return }
            
            dictionaries.forEach({ (key, value) in
                print("key is", key)
                
                Database.database().reference().child("users").child(key).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    guard let userDictionary = snapshot.value as? [String: Any] else { return }
                    
                    if key == "rocketCount" {
                        self.myActivityIndicator.stopAnimating()
                        self.collectionView?.reloadData()

                    } else {
                        let user = UserModelNew.transformUser(dictionary: userDictionary, key: key)
                        user.uid = snapshot.key
                        self.users.append(user)
                        self.collectionView?.reloadData()

                    }
                    
                    
                })
                self.myActivityIndicator.stopAnimating()
                
                
            })
            
        }
        
    
    }
    
}


class RocketList: UICollectionViewCell {
    
    var user: UserModelNew? {
        didSet {
            usernameLabel.text = user?.u_username ?? user?.username
            
            guard let profileImageUrl = user?.profileImageUrl else { return }
            
            profileImageView.loadImage(urlString: profileImageUrl)
        }
    }
    
    let profileImageView: CustomImageView = {
        let iv = CustomImageView()
        iv.contentMode = .scaleAspectFill
        iv.clipsToBounds = true
        return iv
    }()
    
    let usernameLabel: UILabel = {
        let label = UILabel()
        label.text = "Username"
        label.font = UIFont.boldSystemFont(ofSize: 14)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(profileImageView)
        addSubview(usernameLabel)
        
        profileImageView.anchor(top: nil, left: leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 20, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        profileImageView.layer.cornerRadius = 50 / 2
        profileImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        usernameLabel.anchor(top: topAnchor, left: profileImageView.rightAnchor, bottom: bottomAnchor, right: rightAnchor, topConstant: 0, leftConstant: 8, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)

        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

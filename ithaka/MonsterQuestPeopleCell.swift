//
//  MonsterQuestPeopleCell.swift
//  ithaka
//
//  Created by Apurva Raj on 04/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit

class MonsterQuestPeopleCell: UICollectionViewCell {
    
    let profileImageView1: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "mith")
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 30
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    
    lazy var p1name: UILabel = {
        let label = UILabel()
        label.text = "mathildenaum"
        label.font = UIFont.systemFont(ofSize: 11)
        label.textAlignment = .center
        label.textColor = UIColor.black
        
        return label
    }()

    
    let profileImageView2: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "add-user-button").withRenderingMode(.alwaysOriginal)
        imageView.tintColor = UIColor.gray
//        imageView.layer.borderWidth = 1
//        imageView.layer.cornerRadius = 30
//        imageView.contentMode = .scaleAspectFill
//        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    let profileImageView3: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "add-user-button").withRenderingMode(.alwaysOriginal)
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    let profileImageView4: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "alisha")
        imageView.image = #imageLiteral(resourceName: "add-user-button").withRenderingMode(.alwaysOriginal)
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    
    let addquestlogo: UIImageView = {
        let imageView = UIImageView()
        imageView.image = #imageLiteral(resourceName: "swordsnshield").withRenderingMode(.alwaysOriginal)
        
        imageView.isUserInteractionEnabled = true
        
        return imageView
    }()
    

    
    lazy var addItemLabel1: UILabel = {
        let label = UILabel()
        label.text = "Choose your weapons"
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.textAlignment = .left
        return label
    }()
    lazy var addItemLabel3: UILabel = {
        let label = UILabel()
        label.text = "Add item"
        label.font = UIFont.systemFont(ofSize: 11)
        //        label.textColor = .white
        label.textAlignment = .center
        
        return label
    }()
    lazy var addItemLabel4: UILabel = {
        let label = UILabel()
        label.text = "Add item"
        label.font = UIFont.systemFont(ofSize: 11)
        //        label.textColor = .white
        label.textAlignment = .center
        
        return label
    }()

    lazy var playLabel: UILabel = {
        let label = UILabel()
        label.text = "Play"
        label.font = UIFont.systemFont(ofSize: 24)
        //        label.textColor = .white
        label.textAlignment = .center
        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        return label
    }()
    
    lazy var p2name: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 11)
        label.textAlignment = .center
        label.textColor = UIColor.black

        return label
    }()

    lazy var p3name: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 11)
        label.textAlignment = .center
        label.textColor = UIColor.black

        return label
    }()

    lazy var p4name: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = UIFont.systemFont(ofSize: 11)
        label.textAlignment = .center
        label.textColor = UIColor.black

        return label
    }()
    lazy var lineView: UIView = {
        let iv = UIView()
        iv.backgroundColor = UIColor.black
        return iv
    }()

    lazy var bg1: UIView = {
        let iv = UIView()
        iv.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        iv.layer.cornerRadius = 5
        return iv
    }()

    
    lazy var addweapontext: UILabel = {
        let label = UILabel()
        label.text = "Tap to choose your weapons"
        label.font = UIFont.systemFont(ofSize: 18)
        label.textAlignment = .center
        label.textColor = UIColor.black
        
        return label
    }()

    lazy var weaponBox: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        view.layer.cornerRadius = 5
        
        return view
    }()
    



    fileprivate func setupViews(){
        
        
        addSubview(profileImageView1)
        addSubview(profileImageView2)
        addSubview(profileImageView3)
        addSubview(profileImageView4)

    
        addSubview(addquestlogo)
        addSubview(addItemLabel1)
        addSubview(addweapontext)
        addSubview(weaponBox)

        addSubview(p1name)
        addSubview(p2name)
        addSubview(p3name)
        addSubview(p4name)
        
        addSubview(bg1)
        
        addSubview(lineView)

        profileImageView1.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 20, leftConstant: frame.width/2 - 30, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        
        p1name.anchor(top: profileImageView1.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        profileImageView2.anchor(top: profileImageView1.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 45, leftConstant: 30, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 60)
        
        p2name.anchor(top: profileImageView2.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 30, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 0)
        
        profileImageView3.anchor(top: profileImageView1.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 45, leftConstant: frame.width/2 - 30, bottomConstant: 0, rightConstant:
            0 , widthConstant: 60, heightConstant: 60)
       
        p3name.anchor(top: profileImageView3.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: frame.width/2 - 30, bottomConstant: 0, rightConstant: 0, widthConstant: 60, heightConstant: 0)

        profileImageView4.anchor(top: profileImageView1.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 45, leftConstant: 0, bottomConstant: 0, rightConstant: 30, widthConstant: 60, heightConstant: 60)
        p4name.anchor(top: profileImageView4.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 30, widthConstant: 60, heightConstant: 0)
        
        bg1.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: p4name.bottomAnchor, right: self.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: -25, rightConstant: 15, widthConstant: 0, heightConstant: 0)
        self.sendSubview(toBack: bg1)
    
        addquestlogo.anchor(top: bg1.bottomAnchor, left: bg1.leftAnchor, bottom: nil, right: nil, topConstant: 30, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 40, heightConstant: 40)
        
        addweapontext.anchor(top: addquestlogo.topAnchor, left: addquestlogo.rightAnchor, bottom: nil, right: nil, topConstant: 8, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        weaponBox.anchor(top: bg1.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 15, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 70)
        
        self.sendSubview(toBack: weaponBox)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()

        self.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}


//
//  Comment.swift
//  ithaka
//
//  Created by Apurva Raj on 04/10/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//

import Foundation

struct Comment {
    var id: String?

    let user: UserModel
    
    let creationDate: Date

    
    let text: String
    let uid: String
    
    init(user: UserModel, dictionary: [String: Any]) {
        self.user = user
        self.text = dictionary["text"] as? String ?? ""
        self.uid = dictionary["uid"] as? String ?? ""
        
        let secondsFrom1970 = dictionary["creationDate"] as? Double ?? 0
        self.creationDate = Date(timeIntervalSince1970: secondsFrom1970)

    }
}

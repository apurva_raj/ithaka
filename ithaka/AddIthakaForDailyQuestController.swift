//
//  AddIthakaForDailyQuestController.swift
//  ithaka
//
//  Created by Apurva Raj on 07/03/19.
//  Copyright © 2019 Apurva Raj. All rights reserved.
//

import UIKit

class AddIthakaForDailyQuestController: UICollectionViewController, UICollectionViewDelegateFlowLayout, AddIthakaItemSlect {
    
    
    func onItemimagetapped(story: String) {
        let newcontroller = AddIthakaItemSelectionController(collectionViewLayout: UICollectionViewFlowLayout())
        newcontroller.preWrittenText = story
        newcontroller.modalPresentationStyle = .fullScreen
        newcontroller.popoverPresentationController?.sourceView = self.view
        newcontroller.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        newcontroller.popoverPresentationController?.permittedArrowDirections = []
        
        newcontroller.transitioningDelegate = self
        newcontroller.modalPresentationStyle = .custom
        newcontroller.modalPresentationCapturesStatusBarAppearance = true
        
        
        self.present(newcontroller, animated: true, completion: nil)
        
        self.view.endEditing(true)

    }
    
   
    
    var halfModalTransitioningDelegateForIthakaAdd: HalfModalTransitioningDelegateForIthakaAdd?
    
    
    
    func onShareTapped(storytext: String, itemCode: String) {
        print("needed")
        
        print("Self quest tag is ", self.questTag ?? "nil")
        
        let ps = PostStoryModel.transfromPostStory(st: storytext, ic: itemCode, qt: self.questTag, questId: nil, questObjectId: nil, level: nil)

        let publishController = PublishedAndWonController(collectionViewLayout: UICollectionViewFlowLayout())
        self.halfModalTransitioningDelegateForIthakaAdd = HalfModalTransitioningDelegateForIthakaAdd(viewController: self, presentingViewController: publishController)
        publishController.popoverPresentationController?.sourceView = self.view
        publishController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        publishController.popoverPresentationController?.permittedArrowDirections = []
        
        
        publishController.modalPresentationStyle = .custom
        publishController.transitioningDelegate = self.halfModalTransitioningDelegateForIthakaAdd
        publishController.questTag = self.questTag

        publishController.postStory = ps
        
        
        self.present(publishController, animated: true, completion: nil)
        self.view.endEditing(true)
        


    }
    
    var postStory = [PostStoryModel]()
    
    
    func removeItempage() {
        print("nn")
    }
    
    func dailyQuestCardTapped(tag: Int) {
        print("nn")
    }
    
   
    
    var questTag: String?
    
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        

        return button
    }()
    
    @objc func handleBack(){
        self.navigationController?.popViewController(animated: true)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        collectionView?.register(AddIthakaStartCellForDailyQuest.self, forCellWithReuseIdentifier: "AddIthakaStartCellForDailyQuest")
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        collectionView?.delegate = self
        
        setupBackButton()

    }
    
    func setupBackButton() {
        
        view.addSubview(backButton)
        backButton.anchor(top: nil, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 40, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    var imageData: UIImage?

    var currentLockCode: Int?
    var storytext: String?

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddIthakaStartCellForDailyQuest", for: indexPath) as! AddIthakaStartCellForDailyQuest
        

        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height - 300)

    }

    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
}


extension AddIthakaForDailyQuestController: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? PreviewPostController {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal,
                               interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard
                let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController,
                interactionController.inProgress
                else {
                    return nil
            }
            return interactionController
    }
}



extension AddIthakaForDailyQuestController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return SystemPushAnimator(type: .navigation)
        case .pop:
            return SystemPopAnimator(type: .navigation)
        default:
            return nil
        }
    }
    
}

import GTProgressBar


class AddIthakaStartCellForDailyQuest: UICollectionViewCell, UITextViewDelegate {
    
    weak var delegate: AddIthakaItemSlect?
    
    var Questtag: String?{
        didSet{
            
            let qq = Questtag ?? ""
            self.questTag.text = qq
            
        }
    }
    
    var requireChars: Int?{
        didSet{
            let rc = requireChars ?? 40
            
            minimumWordsRequired.text = "\(rc)"
        }
    }
    
    var imageisEmpty: Bool?{
        didSet{
            print("please choose an item to write", imageisEmpty)
            if imageisEmpty == false{
                self.StoryTextView.becomeFirstResponder()
            }
        }
    }

    lazy var questTag: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 11)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.backgroundColor = .white
        label.layer.cornerRadius = 13
        label.clipsToBounds = true
        label.textColor = UIColor.gray
        label.padding = UIEdgeInsetsMake(5, 5, 5, 5)
        
        return label
        
    }()
    

    
    lazy var StoryTextView: UITextView = {
        let tv = UITextView()
        tv.font = UIFont.systemFont(ofSize: 21)
        tv.backgroundColor = .white
        tv.placeholder = "Start writing..."
        tv.isScrollEnabled = true
        tv.tintColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        tv.translatesAutoresizingMaskIntoConstraints = false
        tv.layer.cornerRadius = 15
        tv.clipsToBounds = true
        tv.sizeToFit()
        tv.layoutIfNeeded()
        tv.contentInset = UIEdgeInsetsMake(5, 5, 5, 5)
        tv.backgroundColor = .white
        tv.textContainerInset = UIEdgeInsetsMake(10, 5, 5, 5)
        
        return tv
    }()
    
    let wordsProgressBar: GTProgressBar = {
        let bar = GTProgressBar()
        bar.progress = 0
        bar.barBorderColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        bar.barFillColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 1)
        bar.barBackgroundColor = UIColor.rgb(red: 174, green: 99, blue: 245, alpha: 0.2)
        bar.barBorderWidth = 1
        bar.barFillInset = 2
        bar.labelTextColor = UIColor.darkGray
        bar.progressLabelInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        bar.font = UIFont.boldSystemFont(ofSize: 16)
        bar.labelPosition = GTProgressBarLabelPosition.right
        bar.barMaxHeight = 12
        bar.displayLabel = false
        bar.layer.cornerRadius = 10
        bar.clipsToBounds = true
        
        //        bar.direction = GTProgressBarDirection.anticlockwise
        
        return bar
    }()
    
    
    
    lazy var shareButton: UIButton = {
        let button = UIButton()
        button.setTitle("Share", for: .normal)
        button.addTarget(self, action: #selector(handleShare), for: UIControlEvents.touchUpInside)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        button.setTitleColor(UIColor.gray, for: .normal)
        
        return button
    }()
    
    var currentItemCode: String?

    @objc func handleShare(){
        print("sharing it")
        
        guard let storyText = StoryTextView.text else { return }
        self.shareButton.isEnabled = false
        let ItemCode = currentItemCode ?? ""
        self.delegate?.onShareTapped(storytext: storyText, itemCode: ItemCode)
    }
    
    lazy var lineView: UIView = {
        let iv = UIView()
        iv.backgroundColor = UIColor.white
        iv.layer.cornerRadius = 15
        iv.clipsToBounds = true
        return iv
    }()
    
    lazy var minimumWordsRequiredLabel: UILabel = {
        let label = UILabel()
        label.text = "Minimum Characters Required"
        label.font = UIFont.systemFont(ofSize: 9)
        label.textColor = UIColor.gray
        
        return label
        
    }()
    lazy var minimumWordsRequired: UILabel = {
        let label = UILabel()
        label.text = "40"
        label.font = UIFont.boldSystemFont(ofSize: 12)
        
        return label
        
    }()
    
    lazy var youWillEarnLabel: UILabel = {
        let label = UILabel()
        label.text = "You will earn "
        label.font = UIFont.systemFont(ofSize: 9)
        label.textColor = UIColor.gray
        
        
        return label
        
    }()
    lazy var IWillEarnLabel: UILabel = {
        let label = UILabel()
        label.text = "10 Points & A rare item "
        label.font = UIFont.boldSystemFont(ofSize: 12)
        return label
        
    }()
    
    
    
    
    func textViewDidChange(_ textView: UITextView) {
        print("yeh, it changed")
        
        if self.imageisEmpty == true {
            self.delegate?.onItemimagetapped(story: StoryTextView.text)
        }
        

        let r = requireChars ?? 40
        let c = StoryTextView.text.count
        let pn = CGFloat(r)
        
        let cc = CGFloat(c)
        
        let mc = cc/pn
        
        if r > c {
            let rc = r - c
            self.minimumWordsRequired.text = "\(rc)"
            
        } else if r == c {
            self.minimumWordsRequired.text = "0"
        }
        
        wordsProgressBar.progress = mc
        
        if c > (r - 1) {
            print("bingo")
            self.shareButton.setTitleColor(UIColor.blue, for: .normal)
            self.shareButton.isEnabled = true

        } else {
            self.shareButton.setTitleColor(UIColor.gray, for: .normal)
            self.shareButton.isEnabled = false

        }
    }
    
    lazy var profileItemView: UIImageView = {
        let imageView = UIImageView()
        imageView.layer.borderWidth = 1
        imageView.layer.cornerRadius = 50/2
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.backgroundColor = .white
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(itemTapped))
        tap.numberOfTapsRequired = 1
        imageView.addGestureRecognizer(tap)

        return imageView
    }()

    
    @objc func itemTapped(){
        
        let storytext = self.StoryTextView.text ?? ""
        print("Ssss is ", storytext)
        
        self.delegate?.onItemimagetapped(story: storytext)
    }

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        StoryTextView.delegate = self
        
        addSubview(wordsProgressBar)
        addSubview(questTag)
        addSubview(StoryTextView)
        addSubview(shareButton)
        addSubview(profileItemView)
        addSubview(minimumWordsRequired)
        addSubview(youWillEarnLabel)
        addSubview(IWillEarnLabel)
        addSubview(minimumWordsRequiredLabel)
        
        StoryTextView.becomeFirstResponder()
        
        
        addSubview(lineView)
        
        
        
        questTag.anchor(top:  self.safeAreaLayoutGuide.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 5, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 0)
       
        wordsProgressBar.anchor(top: questTag.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: 10)

        StoryTextView.anchor(top: wordsProgressBar.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 15, widthConstant: 0, heightConstant: self.frame.height - 130)
        
        
        profileItemView.anchor(top: StoryTextView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 25, bottomConstant: 0, rightConstant: 0, widthConstant: 50, heightConstant: 50)
        
        shareButton.anchor(top: StoryTextView.bottomAnchor, left: nil, bottom: nil, right: self.rightAnchor, topConstant: 20, leftConstant: 0, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        minimumWordsRequiredLabel.anchor(top: StoryTextView.bottomAnchor, left: profileItemView.rightAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 10, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        minimumWordsRequired.anchor(top: minimumWordsRequiredLabel.bottomAnchor, left: profileItemView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 15)
        
        youWillEarnLabel.anchor(top: minimumWordsRequired.bottomAnchor, left: profileItemView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        IWillEarnLabel.anchor(top: youWillEarnLabel.bottomAnchor, left: profileItemView.rightAnchor, bottom: nil, right: nil, topConstant: 0, leftConstant: 10, bottomConstant: 0, rightConstant: 25, widthConstant: 0, heightConstant: 0)
        
        
        lineView.anchor(top: nil, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 85, bottomConstant: 0, rightConstant: 85, widthConstant: 0, heightConstant: 2.5)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    

}

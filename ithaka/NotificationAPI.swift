//
//  NotificationAPI.swift
//  ithaka
//
//  Created by Apurva Raj on 06/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseDatabase

class NotificationAPI {

    var REF_NOTIFICATION = Database.database().reference().child("notifications")
    
    func updateBadge(uid: String, badge: Int) {
        
        Api.User.observeUser(withId: uid) { (user) in
            let userbadge = user?.badge ?? 0
            let newvalue = userbadge + badge
            let values = ["badge": newvalue]
            Api.User.REF_USERS.child(uid).updateChildValues(values)
        }
    }
    
    func observeSingleNotification(withUId  uid: String, withPid nid: String,  completion: @escaping (NotificationNew) -> Void) {
        REF_NOTIFICATION.child(uid).child(nid).observeSingleEvent(of: .value) { (snapshot) in
            if let dict = snapshot.value as? [String: Any] {
                let notificationNew = NotificationNew.transform(dict: dict, key: snapshot.key)
                completion(notificationNew)
            }
        }
    }
    
    func getRecentNotification(withId id: String, start timestamp: TimeInterval? = nil, limit: UInt, completionHandler: @escaping ([(NotificationNew, UserModelNew)]?) -> Void) {
        var feedQuery = REF_NOTIFICATION.child(id).queryOrdered(byChild: "creationDate")

        if let latestPostTimestamp = timestamp, latestPostTimestamp > 0 {
            feedQuery = feedQuery.queryStarting(atValue: latestPostTimestamp + 1 , childKey: "creationDate").queryLimited(toLast: limit)
        } else {
            feedQuery = feedQuery.queryLimited(toLast: limit)
        }
        
        feedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects
            let myGroup = DispatchGroup()
            
            
            var results: [(notification: NotificationNew, user: UserModelNew)] = []
            
            for (_, item) in (items as! [DataSnapshot]).enumerated() {
                myGroup.enter()
                let value = item.value as? NSDictionary
                let uid = value!["uid"] as? String ?? ""
                
                self.observeSingleNotification(withUId: uid, withPid: item.key, completion: { (NotificationNew) in
                    
                    let fromuserid = NotificationNew.fromUserId!
                    
                        Api.User.observeUser(withId: fromuserid, completion: { (UserModelNew) in
                            
                            results.append((NotificationNew, UserModelNew!))
                                
                            myGroup.leave()
                            
                        })
                    
                })
            }
            myGroup.notify(queue: .main) {
                results.sort(by: {$0.0.timestamp ?? 0 > $1.0.timestamp ?? 0 })
                completionHandler(results)
            }
            
            
        })
      
    }
    
    func getOldNotificationFeed(withId id: String, start timestamp: TimeInterval, limit: UInt, completionHandler: @escaping ([(NotificationNew, UserModelNew)]?) -> Void) {
        
        let feedOrderQuery = REF_NOTIFICATION.child(id).queryOrdered(byChild: "creationDate")
        
        let feedLimitedQuery = feedOrderQuery.queryEnding(atValue: timestamp - 1, childKey: "creationDate").queryLimited(toLast: limit)
        
        
        feedLimitedQuery.observeSingleEvent(of: .value, with: { (snapshot) in
            let items = snapshot.children.allObjects as! [DataSnapshot]
            
            let myGroup = DispatchGroup()
            
            var results: [(notification: NotificationNew, user: UserModelNew)] = []
            
            for (_, item) in items.enumerated() {
                print(item)
                
                myGroup.enter()
                let value = item.value as? NSDictionary
                let uid = value!["uid"] as? String ?? ""
                self.observeSingleNotification(withUId: uid, withPid: item.key, completion: { (NotificationNew) in
                    
                    let fromuserid = NotificationNew.fromUserId!
                    
                    Api.User.observeUser(withId: fromuserid, completion: { (UserModelNew) in
                        results.append((NotificationNew, UserModelNew!))
                            
                        myGroup.leave()
                        
                    })

                })

            
            }
            myGroup.notify(queue: DispatchQueue.main, execute: {

                results.sort(by: {$0.0.timestamp ?? 0 > $1.0.timestamp ?? 0 })
                completionHandler(results)
            })
            
        })

    }
}

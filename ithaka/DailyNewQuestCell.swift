//
//  DailyNewQuest.swift
//  ithaka
//
//  Created by Apurva Raj on 02/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase


protocol dailyquestTap: class {
    func titleTapped(title: String)
    func rightbuttonTapped()
    func startWritingTapped()
}

class DailyNewQuestCell: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, QuestDailyDelegate {
    
    func onTappedToQuest(title: String) {
        print("yahho ",title)
        self.delegate?.titleTapped(title: title)
        
    }
    
    func rightbuttonTapped(){
        print("tt")
        self.delegate?.rightbuttonTapped()
    }
    
    weak var delegate: dailyquestTap?
    
    var titleText: NSAttributedString?{
        didSet {
            startWriting.attributedText = titleText
        }
    }
    

    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        
        layout.minimumLineSpacing = 0

        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        cv.contentInset = UIEdgeInsetsMake(50, 0, 50, 0)
        
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    
    
    lazy var startWriting: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        label.backgroundColor = UIColor.white
        label.padding = UIEdgeInsetsMake(10, 10, 10, 10)
        label.isUserInteractionEnabled = true
        label.clipsToBounds = true
        label.layer.cornerRadius = 15
        label.numberOfLines = 0
        label.setLineSpacing(lineSpacing: 0.5)
        let tap = UITapGestureRecognizer(target: self, action: #selector(startWritingTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
    }()
    
    lazy var startWriting2: UILabel = {
        let label = UILabel()
        label.text = "Tap to start writing"
        label.font = UIFont.boldSystemFont(ofSize: 25)
        label.textAlignment = .center
        label.textColor = .black
        label.backgroundColor = UIColor.white
        label.padding = UIEdgeInsetsMake(10, 10, 10, 10)
        label.clipsToBounds = true
        label.layer.cornerRadius = 15
        label.numberOfLines = 0
        label.isUserInteractionEnabled = true

        label.alpha = 0
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(startWritingTapped))
                tap.numberOfTapsRequired = 1
                label.addGestureRecognizer(tap)

        return label
    }()

    @objc func startWritingTapped() {
        print("work")
        self.delegate?.startWritingTapped()
    }
    
    
    private var bottomConstraint = NSLayoutConstraint()
    private var headerHeightConstraint = NSLayoutConstraint()
    private let popupOffset: CGFloat = 100

    
    let maxHeaderHeight: CGFloat = 100;
    let minHeaderHeight: CGFloat = 50;
    var previousScrollOffset: CGFloat = 0;


    override func setupViews() {
        super.setupViews()
        
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        addSubview(myActivityIndicator)
        
        backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        addSubview(collectionView)
        

        setupfont()

        collectionView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        collectionView.register(viewfordailyquest.self, forCellWithReuseIdentifier: "viewfordailyquest")
        collectionView.register(QuestTitle.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "QuestTitle")
        
        fetchQuests()
        
    
    }
    
    func setupfont(){
        let attributedText = NSMutableAttributedString(string: "Tap to start writing", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 25)])

        attributedText.append(NSAttributedString(string: "\n\n"))
        
        attributedText.append(NSAttributedString(string: "What's in your heart?", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11), NSAttributedStringKey.foregroundColor: UIColor.gray]))
        
        startWriting.attributedText = attributedText
        
    }
    
    func setupfontSmall(){
        let attributedText = NSMutableAttributedString(string: "Tap to start writing", attributes: [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 18)])
        
        attributedText.append(NSAttributedString(string: "\n\n", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)]))
        
        attributedText.append(NSAttributedString(string: "What's in your heart?", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 9), NSAttributedStringKey.foregroundColor: UIColor.gray]))
        
        startWriting.attributedText = attributedText
        
    }

    
    
//    header animation starts

    
        func scrollViewDidScroll(_ scrollView: UIScrollView) {
            
        let scrollDiff = scrollView.contentOffset.y - self.previousScrollOffset

        let absoluteTop: CGFloat = 0;
        let absoluteBottom: CGFloat = scrollView.contentSize.height - scrollView.frame.size.height;

        let isScrollingDown = scrollDiff > 0 && scrollView.contentOffset.y > absoluteTop
        let isScrollingUp = scrollDiff < 0 && scrollView.contentOffset.y < absoluteBottom


        if canAnimateHeader(scrollView) {

            // Calculate new header height
            var newHeight = self.headerHeightConstraint.constant
            if isScrollingDown {
                newHeight = max(self.minHeaderHeight, self.headerHeightConstraint.constant - abs(scrollDiff))
                
                self.startWriting.numberOfLines = 1
                self.setupfontSmall()

            } else if isScrollingUp {
                newHeight = min(self.maxHeaderHeight, self.headerHeightConstraint.constant + abs(scrollDiff))
                if newHeight > 80 {
                    self.startWriting.numberOfLines = 0
                    self.setupfont()
                }
            }

            // Header needs to animate
            if newHeight != self.headerHeightConstraint.constant {
                self.headerHeightConstraint.constant = newHeight
                self.updateHeader()
                self.setScrollPosition(self.previousScrollOffset)
            }

            self.previousScrollOffset = scrollView.contentOffset.y
        }
    }
    
    var dragOriginY: CGFloat = 0
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        dragOriginY = scrollView.contentOffset.y

    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.scrollViewDidStopScrolling()
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            self.scrollViewDidStopScrolling()
        }
    }

    func scrollViewDidStopScrolling() {
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let midPoint = self.minHeaderHeight + (range / 2)

        if self.headerHeightConstraint.constant > midPoint {
            self.expandHeader()
        } else {
            self.collapseHeader()
        }
    }

    func canAnimateHeader(_ scrollView: UIScrollView) -> Bool {
        // Calculate the size of the scrollView when header is collapsed
        let scrollViewMaxHeight = scrollView.frame.height + self.headerHeightConstraint.constant - minHeaderHeight

        // Make sure that when header is collapsed, there is still room to scroll
        return scrollView.contentSize.height > scrollViewMaxHeight
    }

    func collapseHeader() {
        self.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.minHeaderHeight
            self.updateHeader()
            self.layoutIfNeeded()
            self.startWriting2.alpha = 1
            self.startWriting.numberOfLines = 1
            self.setupfontSmall()

        })
    }

    func expandHeader() {
        self.layoutIfNeeded()
        UIView.animate(withDuration: 0.2, animations: {
            self.headerHeightConstraint.constant = self.maxHeaderHeight
            self.updateHeader()
            self.layoutIfNeeded()
            self.startWriting.numberOfLines = 0
            self.setupfont()

        })
    }

    func setScrollPosition(_ position: CGFloat) {
        self.collectionView.contentOffset = CGPoint(x: self.collectionView.contentOffset.x, y: position)
    }

    func updateHeader() {
        
        let range = self.maxHeaderHeight - self.minHeaderHeight
        let openAmount = self.headerHeightConstraint.constant - self.minHeaderHeight
        
        let closeAmount = self.headerHeightConstraint.constant - openAmount

    }

    
//    header animation ends
    
    
    var dailyQuest = [DailyQuestModel]()
    var dailyQuest1 = [DailyQuestModel]()
    
    var todayQuest: String?
    var fquestheight: CGFloat?

    func fetchQuests() {
        let ref = Database.database().reference().child("dailyQuest")
        
        var qqs = [DailyQuestModel]()
        
        
        ref.queryLimited(toLast: 30).observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? NSDictionary else { return }
            
            
            value.forEach({ (snapshot) in
                let vv = snapshot.value
                print(snapshot.key)
                print("snapshot.key", vv)
                
                let quest = DailyQuestModel.init(key: snapshot.key as! String, dictionary: vv as! [String : Any])
                qqs.append(quest)
                self.dailyQuest.append(quest)
                
                
            })
            
            DispatchQueue.main.async {
                qqs.sort(by: {$0.timestamp! > $1.timestamp!})
                self.dailyQuest = qqs
                self.collectionView.reloadData()
                self.myActivityIndicator.stopAnimating()
            }
            
            
        }
        
        
        
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dailyQuest.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "viewfordailyquest", for: indexPath) as! viewfordailyquest
        cell.delegate = self
        cell.dailyQuest = dailyQuest[indexPath.item]
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let quest = dailyQuest[indexPath.item]
        let qtitle = quest.quest?.height(withConstrainedWidth:  frame.width - 50, font: UIFont.systemFont(ofSize: 20))
        
        return CGSize(width: frame.width, height: qtitle! + 60)
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        // You need this because this delegate method will run at least
        // once before the header is available for sizing.
        // Returning zero will stop the delegate from trying to get a supplementary view
        return CGSize(width: frame.width, height: 50)

    }
     func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "QuestTitle", for: indexPath) as! QuestTitle
        header.imageView.image = #imageLiteral(resourceName: "book")
        header.rightView.isHidden = false
        header.delegate = self
        return header

    }

    
}

extension NSTextAttachment {
    func setImageHeight(height: CGFloat) {
        guard let image = image else { return }
        let ratio = image.size.width / image.size.height
        
        bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y - 15, width: ratio * height, height: height)
    }
    
    func setImageHeightForSmall(height: CGFloat) {
        guard let image = image else { return }
        let ratio = image.size.width / image.size.height
        
        bounds = CGRect(x: bounds.origin.x, y: bounds.origin.y - 10, width: ratio * height, height: height)
    }

}

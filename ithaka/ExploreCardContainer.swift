//
//  ExploreCardContainer.swift
//  ithaka
//
//  Created by Apurva Raj on 11/07/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase

class ExploreCardContainer: UICollectionViewCell, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource, QuestMainDelegate {
    func didTapQuestTitle(id: String, title: String) {
        delegate?.onTapQuestTitle(id: id, title: title)
    }
    
    func didTapQuestTitle(id: String) {
        return

    }
    
    
    
    func didTapQuestType(for cell: QuestCards) {
//        delegate?.onTypeTapped(for: self)
        
    }
    
    weak var delegate: QuestDelegate?
    
    
    let grayLine: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.gray
        
        return view
    }()
    
    
    let scrollCollectionView: UICollectionView = {
        let layout = SnappingCollectionViewLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let view = UICollectionView(frame: .zero, collectionViewLayout: layout)
        view.backgroundColor = .white
        view.decelerationRate = UIScrollViewDecelerationRateFast
        view.showsHorizontalScrollIndicator = false
        view.showsVerticalScrollIndicator = false
        //        view.isPagingEnabled = true
        
//        view.contentInset = UIEdgeInsetsMake(-50, 0, -60, 20)
        
        return view
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        addSubview(scrollCollectionView)
        
//        self.scrollCollectionView.contentInset = UIEdgeInsetsMake(-20, 0, 0, 0)
        
        scrollCollectionView.delegate = self
        scrollCollectionView.dataSource = self
        
        scrollCollectionView.register(ExploreCards.self, forCellWithReuseIdentifier: "ExploreCards")
        scrollCollectionView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: self.frame.width, heightConstant: self.frame.height)
        fetchQuests()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return questTitle.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 130, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCards", for: indexPath) as! ExploreCards
        let questTitles = questTitle[indexPath.item]
        cell.quest = questTitles
        cell.delegate = self

        return cell
    }
    
    var questTitle = [QuestTitleModel]()
    fileprivate func fetchQuests() {
        let ref = Database.database().reference().child("QuestTitles")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let values = snapshot.value as? [String: Any] else { return }
            
            values.forEach({ (snapshot) in
                
                guard let value = snapshot.value as? [String: Any] else { return }
                let key = snapshot.key
                let quest = QuestTitleModel(id: key, dictionary:value)
                
                self.questTitle.append(quest)
                self.delegate?.stopLoadingAnimation()
                self.scrollCollectionView.reloadData()
            })
        }
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}


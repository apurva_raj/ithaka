//
//  QuestLevelController.swift
//  ithaka
//
//  Created by Apurva Raj on 28/05/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase
import SwiftMessages

class QuestLevelController: UICollectionViewController, UICollectionViewDelegateFlowLayout, StageDelegate {
    
    var questMainTitle: String?
    
    func onQ1Tapped(title: String, id: String) {
//        let addIthakaController = AddIthakaStoryController()
//        addIthakaController.questTag = title
//        addIthakaController.questId = questId
//        addIthakaController.level = level
//        addIthakaController.questObjId = id
//        addIthakaController.questMainTitle = questMainTitle
//        navigationController?.pushViewController(addIthakaController, animated: true)
        
        let quest = title

        let addIthakaController = AddIthakaForMiniQuestController(collectionViewLayout: UICollectionViewFlowLayout())
        addIthakaController.questTag = "Quest: " + quest
        addIthakaController.questId = questId
        addIthakaController.level = level
        addIthakaController.questObjId = id
        addIthakaController.questMainTitle = questMainTitle
        
 self.navigationController?.pushViewController(addIthakaController, animated: true)

        
    }
    
   
    func onQ1Tapped() {
        let addIthakaController = AddIthakaStoryController()
    navigationController?.pushViewController(addIthakaController, animated: true)
    }
    
    func onQTapped(level: Int, checkpoint: Int, title: String) {
        let addIthakaController = AddEmojiController()
        addIthakaController.questTitle = title
        addIthakaController.level = level
        addIthakaController.checkPoint = checkpoint
    
        navigationController?.pushViewController(addIthakaController, animated: true)

    }
 
    @objc func BoySailerTapped(){
        print("Man avatar selcted")
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let ref = Database.database().reference().child("users").child(uid)
        
        let value = ["Avatar":11]
        ref.updateChildValues(value)
        
        self.dismiss(animated: true, completion: nil)
    }
    
    fileprivate func templateController(title: String, unselectedImage: UIImage, selectedImage: UIImage,
                                        rootViewController: UIViewController = UIViewController()) -> UINavigationController {
        let viewController = rootViewController
        let navController = UINavigationController(rootViewController: viewController)
        navController.tabBarItem.image = unselectedImage
        navController.tabBarItem.selectedImage = selectedImage
        navController.tabBarItem.title = title
        return navController
    }
    let WaterImageView: UIButton = {
        let button = UIButton(type: .custom)
        button.setImage(#imageLiteral(resourceName: "uci18"), for: .normal)
        
        return button
    }()
    let cellId = "cellId"

    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icons8-multiply-50").withRenderingMode(.alwaysTemplate), for: .normal)
        button.imageView?.tintColor = .black
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissButtonTapped))
        tap.numberOfTapsRequired = 1
        button.addGestureRecognizer(tap)
        button.backgroundColor = .white
        return button
    }()
    
 
    @objc func dismissButtonTapped(){
        
        print("bc")
//        dismiss(animated: true, completion: nil)
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.popViewController(animated: true)
    }
    
    var level: Int?
    var questId: String?
    
    lazy var backButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "ic_keyboard_arrow_left").withRenderingMode(.alwaysTemplate), for: .normal)
        button.setTitle("Back", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.tintColor = UIColor.blue
        button.addTarget(self, action: #selector(handleBack), for: .touchUpInside)
        button.titleEdgeInsets = UIEdgeInsetsMake(0, -13, 2, 0)
        
        
        return button
    }()
    
    @objc func handleBack(){
        self.navigationController?.popViewController(animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTopbar()
        collectionView?.backgroundColor = UIColor.rgb(red: 240, green: 245, blue: 251, alpha: 1)

        collectionView?.register(stageListTitles.self, forCellWithReuseIdentifier: "stageListTitles")

        collectionView?.delegate = self
        collectionView?.dataSource = self
        collectionView?.isScrollEnabled = true
        
        collectionView?.contentInset = UIEdgeInsetsMake(60, 20, 70, 20)
        
        setupInfo()
        fetchlist()
        
        collectionView?.allowsSelection = true
    }
    
    func setupTopbar(){
        let newview = UIView()
        newview.backgroundColor = UIColor.rgb(red: 240, green: 245, blue: 251, alpha: 1)
        
        
        let newview1 = UIView()
        newview1.backgroundColor = UIColor.rgb(red: 240, green: 245, blue: 251, alpha: 1)
        
        self.view.addSubview(newview1)
        newview1.anchor(top: self.view.topAnchor, left: self.view.leftAnchor, bottom: self.view.safeAreaLayoutGuide.topAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        
        view.addSubview(newview)
        
        newview.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, left: self.view.leftAnchor, bottom: nil, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 45)
        
        
        newview.addSubview(backButton)
        backButton.anchor(top: newview.topAnchor, left: newview.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: 15, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        view.bringSubview(toFront: backButton)
        
    }
    let info: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .center
        label.backgroundColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.8)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    func setupInfo() {
        
        view.addSubview(info)

        info.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 60)
        
        
        if level == 7 {
            info.text = "Completing any 3 of the above will result in quest complete. And you will also recieve extra bonus points."
        } else {
            info.text = "Completing any 3 of the above will unlock next level"
        }
        
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "stageListTitles", for: indexPath) as! stageListTitles
        let qt = QuestListTitles[indexPath.item]
        cell.quest = qt
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let quest =  QuestListTitles[indexPath.item]
        
        
        let apporxWidthofTitleText = view.frame.width - 70
        let size = CGSize(width: apporxWidthofTitleText, height: 1000)
        let attributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 19)]
        let estimatedFrame = NSString(string: quest.title).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        let titleEstHeight = estimatedFrame.height
        
        return CGSize(width: view.frame.width - 40, height: titleEstHeight + 60)

    }
    
   
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return QuestListTitles.count
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
 
    var QuestListTitles = [QuestStageObjectives]()
    var questUserData = [QuestUserData]()
    
    func fetchlist() {
        let levelnumber = "level\(level ?? 1)"
        let ref = Database.database().reference().child("QuestLevels").child(questId!).child(levelnumber)
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        guard let questid = questId else { return }

        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let values = snapshot.value as? [String: Any] else { return }
            values.forEach({ (snapshot) in
                guard let value = snapshot.value as? [String: Any] else { return }
                
                let key = snapshot.key
                var questls = QuestStageObjectives(id: snapshot.key, dictionary: value)

                let ref1 = Database.database().reference().child("QuestUserData").child(currentUid).child(questid).child(levelnumber)
                ref1.observeSingleEvent(of: .value, with: { (snapshot) in
                   
                    if snapshot.exists() {
                        guard let value1 = snapshot.value as? [String: Any] else { return }
                        
                        if value1[key] != nil {
                            questls.isWritten = true
                        } else {
                            questls.isWritten = false
                        }
                        
                        self.QuestListTitles.append(questls)
                        self.collectionView?.reloadData()
                        

                    } else {
                        self.QuestListTitles.append(questls)
                        self.collectionView?.reloadData()

                    }

                })

                
            })

        }
        
    }
    
    func fetchUserQuestData() {
        guard let currentUid = Auth.auth().currentUser?.uid else { return }
        guard let questid = questId else { return }
        let ref = Database.database().reference().child("QuestUserData").child(currentUid).child(questid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let value = snapshot.value as? [String: Any] else { return }
            
            let questData = QuestUserData(uid: currentUid, id: questid, dictionary: value)
            
            print("lets see what happens now", questData)
            self.questUserData.append(questData)
            self.collectionView?.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }

}

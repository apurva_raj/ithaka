//
//  ProfilePreviewController.swift
//  ithaka
//
//  Created by Apurva Raj on 01/08/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import GoogleSignIn
import SwiftMessages
import UIKit.UIGestureRecognizerSubclass


private enum State {
    case closed
    case open
}

extension State {
    var opposite: State {
        switch self {
        case .open: return .closed
        case .closed: return .open
        }
    }
}


class ProfilePreviewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, MyCustomDelegate, ProfileEditDelegate, UIViewControllerTransitioningDelegate, ProfileDelegate, textViewTap, profileItem2ndStageTapped, removeChild {
   
    
    func removeChild() {
        if self.childViewControllers.count > 0{
            let viewControllers:[UIViewController] = self.childViewControllers
            for viewContoller in viewControllers{
                viewContoller.willMove(toParentViewController: nil)
                viewContoller.view.removeFromSuperview()
                viewContoller.removeFromParentViewController()
            }
        }

    }
    
    func readbackstory(itemCode: String?) {
        print("nothing")
    }
    
    
    func shwothedamnkeypage(){
        print("tapped")
        
        
        
        let chestController = ItemDiscoveredController()
        chestController.typecode = 24
        chestController.delegate = self
        addChildViewController(chestController)
        view.addSubview(chestController.view)
        chestController.didMove(toParentViewController: self)

    }

    func profileItemTapped(itemcode: String) {
        itemTapped(itemCode: itemcode)
    }
    
   
    func itemTapped(itemCode: String) {
        print("Successful Lift Off")
        
        let itemsStoryController = ItemsStoryController(collectionViewLayout: UICollectionViewFlowLayout())
       itemsStoryController.itemCode = itemCode
        navigationController?.pushViewController(itemsStoryController, animated: true)

    }
    
    
    
    func profileItemlistView1Tapped() {
        
        print("Worr")
        
        let itemsStoryController = ItemsStoryController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(itemsStoryController, animated: true)
    }
    
    
    func onRocketCountButtonTapped(for cell: PostUi) {
        let rocketListController = RocketListController(collectionViewLayout: UICollectionViewFlowLayout())
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        let post = self.newPosts[indexPath.item]
        rocketListController.post = post
        navigationController?.pushViewController(rocketListController, animated: false)

    }
    
    func onFollowersLabelTapped(user: UserModelNew) {
        
        let followingController = FollowingController(collectionViewLayout: UICollectionViewFlowLayout())
        followingController.user = user
        navigationController?.pushViewController(followingController, animated: true)
        
    }
    
    func onFollowingLabelTapped(user: UserModelNew) {
        let followersController = FollowerController(collectionViewLayout: UICollectionViewFlowLayout())
        followersController.user = user
        navigationController?.pushViewController(followersController, animated: true)
        
    }
    func didTapNewComment(post: PostNew) {
        print("yahho")
        
        let commentsController = CommentsController(collectionViewLayout: UICollectionViewFlowLayout())
        commentsController.post = post
        
        self.navigationController?.pushViewController(commentsController, animated: false)

    }
    
    func didTapComment(post: PostNew) {
        return
    }
    
    func onEditPorfileTap(user: UserModelNew) {
        print("chalu")
        
        let profileEditController = ProfileEditController(collectionViewLayout: UICollectionViewFlowLayout())
        profileEditController.user = user
        navigationController?.pushViewController(profileEditController, animated: true)

    }
    
    func onNameIdLabelTapped(userId: String) {
        return
    }
    
    
    func usernameTapped(username: String) {
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_username = Database.database().reference().child("users")
        
        ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.exists() {
                profileController.userName = username
                self.navigationController?.pushViewController(profileController, animated: true)
            } else {
                print("north remembers")
                
            }
        }

    }
    
    func hashTagTapped(hash: String) {
        let hh = HashTagPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_hash = Database.database().reference().child("Hashtags").child(hash)
        
        ref_hash.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                hh.hashtag = hash
                self.navigationController?.pushViewController(hh, animated: true)
                
            } else {
                print("stupid")
            }
        }
        
    }
    
    func onShareButtonTapped(for cell: PostUi) {
        print("yaho")
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        let post = self.newPosts[indexPath.item]
        let user = self.user
        let downloadImgaesController = DownloadImagesController()
        downloadImgaesController.post = post
        downloadImgaesController.user = user
        
        self.present(downloadImgaesController, animated: true, completion: nil)

    }
    
    func onStoryViewTapped(user: UserModelNew, post: PostNew) {
        print("bc")
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
        
        previewpostController.post = post
        previewpostController.user = user
        previewpostController.popoverPresentationController?.sourceView = self.view
        previewpostController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        previewpostController.popoverPresentationController?.permittedArrowDirections = []
        
        present(previewpostController, animated: true, completion: nil)

    }
    
    
    func swipetoTop() {
        return
    }
    
    func onswipetButtonTapped(for cell: PostUi) {
        return
    }
    
    
    func onTagLabelTapped(post: Post) {
        return
    }
    

    
    func onUnblock() {
        handleRefresh()
    }
    
    
    func onLevelCountTapped(user: UserModel) {
        
        let user = user
        let profileLevelController = ProfileLevelController()
        profileLevelController.user = user
        
        navigationController?.pushViewController(profileLevelController, animated: true)
        
        //        present(profileLevelController, animated: true, completion: nil)
        
    }
    
    func onRocketButtonTappedFromProfile(for cell: PostProfileUi) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.newPosts[indexPath.item]
        guard let postId = post.id else { return }
        guard let postUserId = post.uid else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Database.database().reference().child("rockets").child(postId)
        
        let ref2 = Database.database().reference().child("users").child(postUserId)
        let ref3 = Database.database().reference().child("posts").child(postUserId).child(postId)
        
        
        let values = [uid : 1]
        
        
        print("Lift off success.")
        
        self.newPosts[indexPath.item] = post
        
        //            self.finaView?.reloadItems(at: [indexPath])
        
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            
            if snapshot.hasChild(uid) {
                print("it has", uid)
                
                SwiftMessages.show {
                    let view = MessageView.viewFromNib(layout: .CardView)
                    view.configureTheme(.warning)
                    view.configureDropShadow()
                    let iconText = "🙄"
                    view.configureContent(title: "Oops", body: "You already sent the rocket. It can't come back now", iconText: iconText)
                    view.button?.isHidden = true
                    
                    return view
                }
                
            } else {
                
                let gifcontroller = GifController()
                self.addChildViewController(gifcontroller)
                self.view.addSubview(gifcontroller.view)
                gifcontroller.didMove(toParentViewController: self)
                
                let when = DispatchTime.now() + 4
                DispatchQueue.main.asyncAfter(deadline: when) {
                    print("Start remove sibview")
                    gifcontroller.willMove(toParentViewController: nil)
                    
                    // Remove the child
                    gifcontroller.removeFromParentViewController()
                    
                    // Remove the child view controller's view from its parent
                    gifcontroller.view.removeFromSuperview()
                    
                }
                ref.updateChildValues(values)
                if(uid == postUserId) {
                    print("color bc")
                } else {
                    print("bhai")
                    let ref9 = Database.database().reference().child("users").child(uid)
                    
                    ref9.observeSingleEvent(of: .value, with: { (snapshot) in
                        let value = snapshot.value as? NSDictionary
                        let username = value?["username"] as? String ?? ""
                        
                        print("username is", username)
                        
                        guard let postTitle = post.title else { return }
                        
                        let notificationMessage = username + "sent a rocket to your story: " + postTitle
                        
                        let ref10 = Database.database().reference().child("notifications").child(postUserId).childByAutoId()
                        
                        let notifValues = ["Message": notificationMessage, "uid":postUserId, "pid":postId, "fromUserId":uid, "type":"11", "creationDate": Date().timeIntervalSince1970] as [String : Any]
                        
                        ref10.updateChildValues(notifValues)
                        
                    })
                    
                }
                
                ref3.observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    let value2 = snapshot.value as? NSDictionary
                    
                    let rcount = value2?["rocketCount"] as? Int ?? 0
                    
                    let rfinalcount = rcount+1
                    
                    let values22 = ["rocketCount" : rfinalcount]
                    print(values22)
                    ref3.updateChildValues(values22)
                    
                    ref2.observeSingleEvent(of: .value, with: { (snapshot) in
                        let value3 = snapshot.value as? NSDictionary
                        
                        let rcount3 = value3?["rocketCount"] as? Int ?? 0
                        
                        let totalrocket = rcount3 + 1
                        
                        let totalrocketadd = ["rocketCount": totalrocket]
                        
                        ref2.updateChildValues(totalrocketadd)
                        self.newPosts[indexPath.item] = post
                        DispatchQueue.main.async {
                            self.collectionView?.reloadItems(at: [indexPath])
                        }
                    })
                    
                    //                        print(rcount)
                    //                        let cc2 = value?.count ?? 0
                    //
                    //                        let finalCount = cc+cc2
                    //
                    //                        print(cc2, "  yahhooo")
                    //
                    //                        let addedToUser = ["rocketCount": finalCount]
                    //                        ref3.updateChildValues(addedToUser)
                    
                })
                
                print("succcesssss!")
                
            }
        })
        
        
    }
    
    func onOptionButtonTappedFromProfile(for cell: PostProfileUi) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        alertController.popoverPresentationController?.permittedArrowDirections = []
        
        alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
            
            guard let uid = Auth.auth().currentUser?.uid else { return }
            let AdminId = "FaKyjBJXnzeZAeSYe13XV2WIrp23"
            
            if uid == AdminId {
                alertController.addAction(UIAlertAction(title: "Add new quest", style: .destructive, handler: { (_) in
                    
                    do {

                        let nc = AddDailyQuestController()
                        self.navigationController?.pushViewController(nc, animated: true)
                    }
                    
                }))
            
            }
            do {
                let ref = Database.database().reference()
                
                guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
                let post = self.newPosts[indexPath.item]
                guard let postId = post.id else { return }
                

                //remove from user auth feed
                Api.Feed.REF_FEED.child(uid).child(postId).removeValue()
                
                //remove from followers feed
                let followref = Database.database().reference().child("following").child(uid)
                followref.observeSingleEvent(of: .value, with: { (snapshot) in
                    let arraySnapshot = snapshot.children.allObjects as! [DataSnapshot]
                    arraySnapshot.forEach({ (child) in
                        Api.Feed.REF_FEED.child(child.key).child(postId).removeValue()
                    })
                })
                
                //remove from explore feed
                
                Api.Feed.REF_EXPLORE_POSTS.child(postId).removeValue()
                
                //remove from hashtagfeed
                guard let story = post.story else { return }
                guard let title = post.title else { return }
                
                let titlewords = title.components(separatedBy: CharacterSet(charactersIn: "#@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_").inverted)
                let storywords = story.components(separatedBy: CharacterSet(charactersIn: "#@ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_").inverted)
                for var titleword in titlewords {
                    if titleword.hasPrefix("#") {
                        titleword = titleword.trimmingCharacters(in: CharacterSet.punctuationCharacters)
                        print(titleword)
                        let hashref = Database.database().reference().child("Hashtags").child(titleword.lowercased()).child(postId)
                        
                        hashref.removeValue()
                    }
                }
                for var storyword in storywords {
                    if storyword.hasPrefix("#") {
                        storyword = storyword.trimmingCharacters(in: CharacterSet.punctuationCharacters)
                        print(storyword)
                        let hashref = Database.database().reference().child("Hashtags").child(storyword.lowercased()).child(postId)
                        
                        hashref.removeValue()
                        
                    }
                }
                
                //remove from notification
                //remove from post table
                ref.child("posts").child(uid).child(postId).removeValue(completionBlock: { (err, ref) in
                    if let err = err {
                        print(err)
                    } else {
                        self.handleRefresh()
                    }
                })
                
                //remove from quest poins
                //remove from quest data entry
                //remove from user table post count
                
                ref.child("users").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    let progressCount = value?["profileProgress"] as? Int ?? 0
                    let finalcount = progressCount - 5
                    
                    print("pc is", progressCount)
                    print("fc is", finalcount)
                    let points = ["profileProgress": finalcount]
                    
                    ref.child("users").child(uid).updateChildValues(points)
                    
                })
                // -1 the postcount in user table
                Api.User.REF_CURRENT_USER?.observeSingleEvent(of: .value, with: { (snapshot) in
                    let value = snapshot.value as? NSDictionary
                    let postCount = value?["postCount"] as? Int ?? 0
                    let profileProgressCount = value?["profileProgress"] as? Int ?? 0
                    
                    let postCountNewValue = postCount - 1
                    let profileProgressCountNewValue = profileProgressCount - 5
                    let pvalue = ["postCount": postCountNewValue, "profileProgress": profileProgressCountNewValue]
                    
                    Api.User.REF_CURRENT_USER?.updateChildValues(pvalue)
                    
                })
                
            }
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
        
    }
    
    func onTitleLabelTappedFromProfile(for cell: PostProfileUi) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.newPosts[indexPath.item]
        
        guard let postId = post.id else { return }
        
        let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
        postController.userId = post.uid
        postController.postId = postId
        navigationController?.pushViewController(postController, animated: true)    }
    
    
    func onEmojiTap(for cell: ProfileHeader) {
        print("emoji")
        
    }
    
    
    
    func onRocketButtonTapped(for cell: PostUi) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        DispatchQueue.main.async {
            self.collectionView?.reloadItems(at: [indexPath])
        }

    }
    
    
    func onLogoutTapped(for cell: ProfileHeader) {
        
        guard let currentUserId = user?.uid else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let AdminId = "FaKyjBJXnzeZAeSYe13XV2WIrp23"

        if (currentUserId == uid) {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alertController.popoverPresentationController?.sourceView = self.view
            alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            
            alertController.popoverPresentationController?.permittedArrowDirections = []
            //        alertController.popoverPresentationController?.barButtonItem
            
            
            if uid == AdminId {
                alertController.addAction(UIAlertAction(title: "Add new quest", style: .destructive, handler: { (_) in
                    
                    do {
                        let nc = AddDailyQuestController()
                        self.navigationController?.pushViewController(nc, animated: true)

                    }
                    
                }))
                
                alertController.addAction(UIAlertAction(title: "See All Posts", style: .destructive, handler: { (_) in
                    
                    do {
                        let nc = AdminPostController(collectionViewLayout: UICollectionViewFlowLayout())
                        self.navigationController?.pushViewController(nc, animated: true)
                        
                    }
                    
                }))
                


                
            }

            alertController.addAction(UIAlertAction(title: "Edit Profile", style: .default, handler: {
                (_) in
                
                do {
                    
                    let profileEditController = ProfileEditController(collectionViewLayout: UICollectionViewFlowLayout())
                        profileEditController.user = self.user
                    self.navigationController?.pushViewController(profileEditController, animated: true)
                }
                
            }))
            
            
            alertController.addAction(UIAlertAction(title: "Feedback", style: .default, handler: {
                (_) in
                
                do {
                    
                    let feedbackController = FeedbackController()
                    self.navigationController?.pushViewController(feedbackController, animated: true)
                }
                
            }))
            
            alertController.addAction(UIAlertAction(title: "FAQ", style: .default, handler: {
                (_) in
                
                do {
                    
                    let newController = FAQController(collectionViewLayout: UICollectionViewFlowLayout())
                    self.navigationController?.pushViewController(newController, animated: true)
                }
                
            }))
            alertController.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: { (_) in
                
                do {
                    try Auth.auth().signOut()
                    
                    //what happens? we need to present some kind of login controller
                    let welcomeController = Welcome1Controller()
                    let navController = UINavigationController(rootViewController: welcomeController)
                    
                    
                    self.present(navController, animated: true, completion: nil)
                    
                } catch let signOutErr {
                    print("Failed to sign out:", signOutErr)
                }
                
                
            }))
            
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            present(alertController, animated: true, completion: nil)
            
            
        } else {
            print("report this person")
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alertController.popoverPresentationController?.sourceView = self.view
            alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            
            alertController.popoverPresentationController?.permittedArrowDirections = []
            
            
            

            alertController.addAction(UIAlertAction(title: "Report", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference()
                    
                    let cd = Date().timeIntervalSince1970

                    let values = ["fromuserId": uid, "creationDate":cd] as [String : Any]
                    ref.child("reportProfile").child(currentUserId).updateChildValues(values)

                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Thank You", body: "Thank you for reporting this user. We will check and will send you update within 24 hours.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                }
                
            }))
            
            alertController.addAction(UIAlertAction(title: "Block", style: .destructive, handler: { (_) in
                
                do {
                    guard let uid = Auth.auth().currentUser?.uid else { return }
                    let ref = Database.database().reference()
                    
                    
                    let values = [currentUserId: 1]
                    ref.child("BlockedProfile").child(uid).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Okay!", body: "This user is now blocked. You won't be able to see or interact with each other's content and profile.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    self.handleRefresh()
                    self.navigationController?.dismiss(animated: true, completion: nil)
                    
                }
                
            }))
            
            
            if uid == AdminId {
                alertController.addAction(UIAlertAction(title: "New Ithakian", style: .default, handler: { (_) in
                    
                    do {
                        
                        let ref1 = Database.database().reference().child("NewWritersList")
                        let value = [currentUserId: 1]
                        ref1.updateChildValues(value)
                        
                        
                    }
                    
                }))

            }
            
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func onEditPorfileTap(user: UserModel) {
        let profileEditController = ProfileEditController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(profileEditController, animated: true)
    }
    
    func onOptionButtonTapped(for cell: PostUi) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        alertController.popoverPresentationController?.permittedArrowDirections = []
        
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let AdminId = "FaKyjBJXnzeZAeSYe13XV2WIrp23"
        
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        
        let post = self.newPosts[indexPath.item]
        let postUid = post.uid ?? ""
        
        let username = self.user?.u_username ?? ""

        guard let pid = post.id else { return }
        
        if uid == AdminId {
            alertController.addAction(UIAlertAction(title: "Add to explore", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference().child("explore")
                    
                    let cd = post.timestamp ?? 0
                    
                    
                    let values = ["uid": postUid, "creationDate": cd] as [String : Any]
                    ref.child(pid).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Done", body: "Keep going Apurva", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    let notificationMessage = "We have featured your post on our explore page. This will give your more reach. Happy writing :)"
                    guard let toUserId = post.uid else { return }
                    guard let postId = post.id else { return }
                    
                    let ref54 = Database.database().reference().child("notifications").child(toUserId).childByAutoId()
                    
                    
                    let notifValues = ["Message": notificationMessage, "uid":toUserId, "pid":postId, "fromUserId":"b5MKdFi5mkcJgQC12hEOjJOT3MB3", "type":"2312", "creationDate": cd] as [String : Any]
                    
                    ref54.updateChildValues(notifValues)

                    
                }
                
            }))
        }
        
        
        if uid == postUid {
            alertController.addAction(UIAlertAction(title: "Edit post", style: .default, handler: { (_) in
                
                do {
                    
                    let editPostController = EditPostController()
                    editPostController.post = post
                    editPostController.username = username
                    
 self.navigationController?.pushViewController(editPostController,
                                                                  animated: true)
                }
                
            }))
            
            alertController.addAction(UIAlertAction(title: "Delete Post", style: .destructive, handler: { (_) in
                do {
                    self.myActivityIndicator.startAnimating()
                    
                    UtilityAPI.deletePost(post: post)
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.success)
                        view.configureDropShadow()
                        let iconText = "👻"
                        view.configureContent(title: "Done", body: "Your post is deleted.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                    DispatchQueue.main.async {
                        self.newPosts.remove(at: indexPath.item)
                        self.collectionView?.deleteItems(at: [indexPath])
                    }
                    
                    self.myActivityIndicator.stopAnimating()
                }
                
            }))
            
        } else {
            alertController.addAction(UIAlertAction(title: "Report For Spam", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference()
                    
                    guard let postId = post.id else { return }
                    let cd = Date().timeIntervalSince1970
                    
                    let values = ["fromUserId":uid, "creationTime": cd] as [String : Any]
                    ref.child("report").child("spam").child(postId).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Thank You", body: "Thank you for reporting this post. We will check and will send you update within 24 hours.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                }
                
            }))
            
            alertController.addAction(UIAlertAction(title: "Report For Plagiarism", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference()
                    
                    guard let postId = post.id else { return }
                    let cd = Date().timeIntervalSince1970
                    
                    let values = ["fromUserId":uid, "creationTime": cd] as [String : Any]
                    ref.child("report").child("spam").child(postId).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Thank You", body: "Thank you for reporting this post. We will check and will send you update within 24 hours.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                }
                
            }))

            
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)

        
    }
    
    
    
    func onStoryViewTapped(post: Post) {
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
        //        previewpostController.post = post
        
        //        self.addChildViewController(previewpostController)
        //        self.view.addSubview(previewpostController.view)
        //        previewpostController.didMove(toParentViewController: self)
        //
        
        navigationController?.pushViewController(previewpostController, animated: true)
        //        present(previewpostController, animated: true, completion: nil)
        
    }
    
    func onNameLabelTapped(for cell: PostUi) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.newPosts[indexPath.item]
        
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = post.uid
        self.navigationController?.pushViewController(profileController,
                                                      animated: true)
        
    }
    
    
    func didTapComment(post: Post) {
        
    }
    
    func onloveButtonTapped(loc: CGPoint, for cell: PostUi) {
        return
    }
    
    func onTitleLabelTapped(for cell: PostUi) {
        return
    }
    
    
    var userId: String?
    
    var userName: String?
    
    let cellId = "cellId"
    let postCellId = "postCellId"
    
    
    
    
    fileprivate func setupAddIthaka(){
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8-plus_math_filled").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleAddIthaka))
        
    }
    
    @objc func handleAddIthaka() {
        let addIthakaController = AddIthakaAPI.addIthakaController
        addIthakaController.modalPresentationStyle = .custom
        addIthakaController.transitioningDelegate = self
        present(addIthakaController, animated: true, completion: nil)
    }
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var user: UserModelNew?
    
    var pointsCollected: Bool?
    
    fileprivate func fetchUserFromUsername() {
        
        let uu = userName ?? ""
        
        if uu == "" {
            let uid = self.userId
                ?? (Auth.auth().currentUser?.uid ?? "")
            Api.User.observeUser(withId: uid) { (UserModelNew) in
                self.user = UserModelNew
                self.navigationItem.title = self.user?.username
                self.loadPosts()
                
            }
            
        }
        else {
            Api.User.observeUserByUsername(username: uu, completion: { (UserModelNew) in
                print("user is fucked up ", UserModelNew)
                self.user = UserModelNew
                

                
                self.navigationItem.title = self.user?.username
                self.loadPosts()
                
            })
        }
        
    }
    func checkBlock() {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        guard let currentUid = userId else { return }
        
        print("CurrentId is", currentUid)
        
        let ref = Database.database().reference().child("BlockedProfile").child(uid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
        }
    }
    
    
    lazy var wonItemButton: UIImageView = {
        let v = UIImageView()
        v.backgroundColor = UIColor.clear
        v.isUserInteractionEnabled = true
        v.image = #imageLiteral(resourceName: "Red Bag")
        let tap = UITapGestureRecognizer(target: self, action: #selector(testButtonTapped))
        tap.numberOfTapsRequired = 1
        v.addGestureRecognizer(tap)
        return v
    }()
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant:0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()
        
        
        checkBlock()

        
        collectionView?.backgroundColor = UIColor.white
        
        collectionView?.register(ProfileHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        
        collectionView?.register(PostUi.self, forCellWithReuseIdentifier: postCellId)
        collectionView?.register(ProfileMyItemsListCell.self, forCellWithReuseIdentifier: "ProfileMyItemsListCell")

        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: ProfileEditController.updateProfileNotificatioName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: AddIthakaFinalController.updateFeedNotificationName, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: EditPostController.updateFeedNotificationName, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleReadBackStory), name: PreviewPostController.updateReadStoryNotificationName, object: nil)


        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
        
        //        fetchUser()
        fetchUserFromUsername()
        
//        printCounts()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "STHeitiSC-Medium", size: 18) ?? UIFont.systemFont(ofSize: 18)]

        
        setupPoints()
        collectionView?.setNeedsLayout()
        collectionView?.layoutIfNeeded()
//
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.05
            
            self.collectionView?.contentInset = UIEdgeInsetsMake(0, ph, 0, ph)

            
        default:
            print("i am Everyone except ipad")
            
             
        }

        NotificationCenter.default.addObserver(self, selector: #selector(hashtagTappedFromPost), name: PreviewPostController.PushHashtagPage, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(usernameTappedFromPost), name: PreviewPostController.PushProfilePage, object: nil)

    }
    
    
    @objc func hashtagTappedFromPost(_ notification: NSNotification){
        if let hash = notification.userInfo?["hashtag"] as? String {
            self.hashTagTapped(hash: hash)
        }
    }
    
    @objc func usernameTappedFromPost(_ notification: NSNotification){
        if let uname = notification.userInfo?["username"] as? String {
            
            self.usernameTapped(username: uname)
            
        }
    }

    
    var collectedPoints: Int?
    var latestUsername: String?
    
    func setupPoints(){
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        let ref = Api.PointsAPII.Points_REF.child(uid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.exists() {
                guard let value = snapshot.value else { return }
                
                let points = Points.transform(dict: value as! [String : Any]
                    , key: uid)
                let cp = points.collectedPoints ?? 0
                self.collectedPoints = cp
                
                
            } else {
                self.collectedPoints = 0
                self.pointsCollected = true
                
            }
        }
    }
    
    @objc func handleReadBackStory(_ notification: NSNotification){
        
        if let itemName = notification.userInfo?["itemName"] as? String {
            
            let itemsStoryController = ItemsStoryController(collectionViewLayout: UICollectionViewFlowLayout())
            itemsStoryController.itemCode = itemName
            self.navigationController?.pushViewController(itemsStoryController, animated: true)
            
        }
        
        
    }
    

    
    
    var halfModalTransitioningDelegate: HalfModalTransitioningDelegate?

    @objc func testButtonTapped(){
        print("yahho")
        
        let cc =  CollectPointsViewCellController(collectionViewLayout: UICollectionViewFlowLayout())
        
        self.halfModalTransitioningDelegate = HalfModalTransitioningDelegate(viewController: self, presentingViewController: cc)
        
        
        cc.popoverPresentationController?.sourceView = self.view
        cc.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        cc.popoverPresentationController?.permittedArrowDirections = []
        
        
        cc.modalPresentationStyle = .custom
        cc.transitioningDelegate = self.halfModalTransitioningDelegate
        self.present(cc, animated: true, completion: nil)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    // popup Ends
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.tabBarController?.tabBar.items?[3].badgeValue = String(UIApplication.shared.applicationIconBadgeNumber)

    }
    
    fileprivate var isLoadingPost = false
    var newPosts = [PostNew]()
    
    func loadPosts() {
        isLoadingPost = true
        print("loading started")
        guard let uid = user?.uid else { return }
        
        
        Api.Profile.getRecentProfileFeed(withId: uid, start: newPosts.first?.timestamp, limit: 3) { (results) in
            guard let results = results else { self.myActivityIndicator.stopAnimating()
                return }
            
            if results.count > 0 {
                results.forEach({ (result) in
                    self.newPosts.append(result)
                    
                })
            }
            self.isLoadingPost = false
            self.myActivityIndicator.stopAnimating()
            self.collectionView?.reloadSections([0,1])
        }
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        
        let post = newPosts[indexPath.item]
        
        previewpostController.post = post
        previewpostController.user = user
        previewpostController.visitorUid = uid
        previewpostController.modalPresentationStyle = .fullScreen
        previewpostController.popoverPresentationController?.sourceView = self.view
        previewpostController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        previewpostController.popoverPresentationController?.permittedArrowDirections = []
        
        previewpostController.transitioningDelegate = self
        previewpostController.modalPresentationStyle = .custom
        previewpostController.modalPresentationCapturesStatusBarAppearance = true
        self.present(previewpostController, animated: true, completion: nil)

    }
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
            guard let uid = user?.uid else { return }
            
            guard !isLoadingPost else {
                return
            }
            isLoadingPost = true
            
            guard (self.newPosts.last?.timestamp) != nil else {
                isLoadingPost = false
                return
            }
            self.myActivityIndicator.startAnimating()
            
            Api.Profile.getOldFeed(withId: uid, start: (newPosts.last?.timestamp)!, limit: 3, completionHandler: { (results) in
                guard let results = results else { return }
                
                if results.count == 0 {
                    self.myActivityIndicator.stopAnimating()

                    return
                }
                if results.count > 0 {
                    results.forEach({ (result) in
                        self.newPosts.append(result)
                        
                    })
                }
                
                self.myActivityIndicator.stopAnimating()
                
                self.collectionView?.reloadData()
                self.isLoadingPost = false
            })
            
            
        }
    }
    
    fileprivate func printCounts() {
        let ref = Database.database().reference().child("users")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let usersList = snapshot.value as? [String: Any] else { return }
            
            //listing out all the ids of users
            print("user count is", usersList.count)
        }
        
        let refP = Database.database().reference().child("posts")
        refP.observeSingleEvent(of: .value) { (snapshot) in
            guard let postsList = snapshot.value as? [String: Any] else { return }
            
            var cc = 0
            postsList.forEach({ (snapshot) in
                let value = snapshot.value as? NSDictionary
                guard let count = (value?.count) else  { return }
                cc = cc + count
                
            })
            
            print("Total post count is ", cc)
            
        }
    }
    
    @objc func handleUpdateFeed() {
        handleRefresh()
    }
    
    @objc func handleRefresh() {
        print("Handling refresh..")
        
        users.removeAll()
        newPosts.removeAll()
        
//        itemNameAndCount.removeAll()
        fetchUserFromUsername()
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
        }
    }
    
    
    var isFinishedPaging = false
    var users = [UserModel]()
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch indexPath.section {
        case 0:
            let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! ProfileHeader
            header.user = self.user
            header.profileDelegate = self
            header.delegate = self
            
            header.collectedPoints = self.collectedPoints

            
            guard let authId = Auth.auth().currentUser?.uid else { return header}
            guard let curretnUser = self.user?.uid else { return header }
            
            print("auth is is", authId)
            print("auth is is", curretnUser)
            
            if authId == curretnUser {
                view.addSubview(wonItemButton)
                
                switch UIDevice.current.userInterfaceIdiom {
                    
                case .pad:
                    print("i am ipad")
                    let ph = self.view.frame.width*0.05
                    
                    wonItemButton.anchor(top: nil, left: nil, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 45, rightConstant: ph, widthConstant: 100, heightConstant: 100)
                    
                    
                default:
                    print("i am Everyone except ipad")
                    wonItemButton.anchor(top: nil, left: nil, bottom: self.view.bottomAnchor, right: self.view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 15, rightConstant: 35, widthConstant: 65, heightConstant: 65)
                    
                    
                }
                
            }

            
            
            
            return header

        default:
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.1
            

            switch section {
            case 0:
                let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 1500))
                label.numberOfLines = 0
                label.font = UIFont.boldSystemFont(ofSize: 16)
                
                label.lineBreakMode = NSLineBreakMode.byWordWrapping
                label.text = user?.shortBio
                label.sizeToFit()
                
                let height = label.frame.height + 180 + 150 + 20 + 10
                return CGSize(width: view.frame.width - ph, height: height)
                
            default:
                return .zero
            }
            
        default:

            switch section {
            case 0:
                let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width - 30, height: 1500))
                label.numberOfLines = 0
                label.font = UIFont.boldSystemFont(ofSize: 16)
                
                label.lineBreakMode = NSLineBreakMode.byWordWrapping
                label.text = user?.shortBio
                label.sizeToFit()
                
            
                let height = label.frame.height + 180 + 150 + 20 + 10 + 10
                return CGSize(width: view.frame.width, height: height)
                
            default:
                return .zero
            }
            
        }
        
    }
    
    
    //showing owenr's posts
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            return 1

        default:
            return newPosts.count

        }
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileMyItemsListCell", for: indexPath) as! ProfileMyItemsListCell
            cell.user = user
            cell.delegate = self
            
            return cell

        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postCellId, for: indexPath) as! PostUi
            
            
            let post = newPosts[indexPath.item]
            
            //        let user = userModelNew[indexPath.item]
            
            cell.post = post
            cell.user = user
            
            cell.delegate = self
            cell.textViewTap = self
            return cell

        }
        
    }
    var tagheight = 0
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        switch UIDevice.current.userInterfaceIdiom {
            
        case .pad:
            print("i am ipad")
            let ph = self.view.frame.width*0.1
            
            switch indexPath.section {
            case 0:
                return CGSize(width: view.frame.width - 30 - ph, height: 120)
                
            default:
                return CGSize(width: view.frame.width - 30 - ph, height: 330)
                
            }

            
        default:
            switch indexPath.section {
            case 0:
                return CGSize(width: view.frame.width - 30, height: 120)
                
            default:
                return CGSize(width: view.frame.width - 30, height: 330)
                
            }

            
        }
        
    }
    
    
}


class InstantPanGestureRecognizer: UIPanGestureRecognizer {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent) {
        if (self.state == UIGestureRecognizerState.began) { return }
        super.touchesBegan(touches, with: event)
        self.state = UIGestureRecognizerState.began
    }
    
}

extension ProfilePreviewController {
    
    func animationController(forPresented presented: UIViewController,
                             presenting: UIViewController,
                             source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return FadePushAnimator(type: .modal)
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        var interactionController: UIPercentDrivenInteractiveTransition?
        if let viewController = dismissed as? PreviewPostController {
            interactionController = viewController.interactionController
        }
        return FadePopAnimator(type: .modal,
                               interactionController: interactionController)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning)
        -> UIViewControllerInteractiveTransitioning? {
            
            guard
                let animator = animator as? FadePopAnimator,
                let interactionController = animator.interactionController as? LeftEdgeInteractionController,
                interactionController.inProgress
                else {
                    return nil
            }
            return interactionController
    }
}



extension ProfilePreviewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              animationControllerFor operation: UINavigationControllerOperation,
                              from fromVC: UIViewController,
                              to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        switch operation {
        case .push:
            return SystemPushAnimator(type: .navigation)
        case .pop:
            return SystemPopAnimator(type: .navigation)
        default:
            return nil
        }
    }
    
}




//
//  ViewController.swift
//  ithaka
//
//  Created by Apurva Raj on 16/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//
import UIKit
import Firebase
import GoogleSignIn

class WelcomeController: UIViewController, GIDSignInUIDelegate {
    
    let btnSignIn : GIDSignInButton = {
        let btnSignIn = GIDSignInButton(frame: CGRect(origin: .zero, size: CGSize(width: 230, height: 48)))
        btnSignIn.style = GIDSignInButtonStyle.standard
        
        return btnSignIn
    }()
    
    
    let ithakaLogo: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "ithaka_font")
        
        return imageview
    }()
    
    
    
    let logoContainerView: UIView = {
        let view = UIView()
        
        let logoImageView = UIImageView(image: #imageLiteral(resourceName: "ithaka_font"))
        logoImageView.contentMode = .scaleAspectFill
        
        view.addSubview(logoImageView)
        logoImageView.anchor(top: nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        view.backgroundColor = .black
        return view
    }()
    
    lazy var taglineLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(13)
        label.text = "By signing up you indicate that you have read and agree to the Terms of Service and Privacy Policy..."
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = .gray
        label.isUserInteractionEnabled = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(termsLabelTapped))
        tap.numberOfTapsRequired = 1
        label.addGestureRecognizer(tap)

        return label
    }()
    
    @objc func termsLabelTapped(_ recognizer: UITapGestureRecognizer){
        
        
        UIApplication.shared.open(URL(string : "http://ithakaapp.com/termsandprivacy.html")!, options: [:], completionHandler: { (status) in
            
        })
}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        
        myActivityIndicator.center = view.center
        
        view.addSubview(myActivityIndicator)
        
        myActivityIndicator.startAnimating()

        view.backgroundColor = .white
        navigationItem.title = "Welcome"
        
        view.addSubview(logoContainerView)
        view.addSubview(taglineLabel)
        view.addSubview(btnSignIn)
        
        
        logoContainerView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: view.frame.height/2-50, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
//        taglineLabel.anchor(top: logoContainerView.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 80, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
        btnSignIn.anchor(top: nil, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 40, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        taglineLabel.anchor(top: btnSignIn.bottomAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, topConstant: 2, leftConstant: 5, bottomConstant: 0, rightConstant: 5, widthConstant: 0, heightConstant: 0)
        
        myActivityIndicator.stopAnimating()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        GIDSignIn.sharedInstance().uiDelegate = self
        self.navigationController?.isNavigationBarHidden = true

    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        
        return true
    }
}


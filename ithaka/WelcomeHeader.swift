//
//  WelcomeHeader.swift
//  ithaka
//
//  Created by Apurva Raj on 29/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//


import UIKit


class WelcomeHeader: UICollectionViewCell {
    
    let ithakaLogo: UIImageView = {
        let imageview = UIImageView()
        imageview.image = #imageLiteral(resourceName: "api42")
        
        
        return imageview
    }()
  

    
    let logoContainerView: UIView = {
        let view = UIView()
        
        let logoImageView = UIImageView(image: #imageLiteral(resourceName: "api42"))
        logoImageView.contentMode = .scaleAspectFill
        
        view.addSubview(logoImageView)
        logoImageView.anchor(top: nil, left: nil, bottom: nil, right: nil, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 150, heightConstant: 150)
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        
        view.backgroundColor = .black
        return view
    }()
    
    let taglineLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(17)
        label.text = "Keep ithaka always in your mind"
        label.textAlignment = .center
        return label
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        addSubview(logoContainerView)
        addSubview(taglineLabel)
        
        logoContainerView.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 150, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        taglineLabel.anchor(top: logoContainerView.bottomAnchor, left: self.leftAnchor, bottom: nil, right: self.rightAnchor, topConstant: 80, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

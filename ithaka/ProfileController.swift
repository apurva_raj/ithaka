//
//  ProfileController.swift
//  ithaka
//
//  Created by Apurva Raj on 17/08/17.
//  Copyright © 2017 Apurva Raj. All rights reserved.
//


import UIKit
import Firebase
import FirebaseStorage
import GoogleSignIn
import SwiftMessages


class ProfileController: UICollectionViewController, UICollectionViewDelegateFlowLayout, MyCustomDelegate, ProfileEditDelegate, UIViewControllerTransitioningDelegate, ProfileDelegate, textViewTap {
    
    
    func shwothedamnkeypage() {
        print("yolo")

    }
    
    
    func profileItemTapped(itemcode: String) {
        print("yolo")
    }
    
    func profileItemlistView1Tapped() {
        
        print("Worr")
        
        let itemsStoryController = ItemsStoryController(collectionViewLayout: UICollectionViewFlowLayout())
        navigationController?.pushViewController(itemsStoryController, animated: true)

    }
    
    func onRocketCountButtonTapped(for cell: PostUi) {
        let rocketListController = RocketListController(collectionViewLayout: UICollectionViewFlowLayout())
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        let post = self.newPosts[indexPath.item]
        rocketListController.post = post
        navigationController?.pushViewController(rocketListController, animated: false)

    }
    
    func onFollowersLabelTapped(user: UserModelNew) {
        
        let followingController = FollowingController(collectionViewLayout: UICollectionViewFlowLayout())
        followingController.user = user
        navigationController?.pushViewController(followingController, animated: true)

    }
    
    func onFollowingLabelTapped(user: UserModelNew) {
        let followersController = FollowerController(collectionViewLayout: UICollectionViewFlowLayout())
        followersController.user = user
        navigationController?.pushViewController(followersController, animated: true)

    }
    
    func didTapNewComment(post: PostNew) {
        return
    }
    
    func didTapComment(post: PostNew) {
        return
    }
    
    func onEditPorfileTap(user: UserModelNew) {
        print("chalu")

    }
    
    func onNameIdLabelTapped(userId: String) {
        return
    }
    
    
    func usernameTapped(username: String) {
        let profileController = ProfilePreviewController(collectionViewLayout: UICollectionViewFlowLayout())
       let ref_username = Database.database().reference().child("users")
        
        ref_username.queryOrdered(byChild: "u_username").queryEqual(toValue: username).observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.exists() {
                profileController.userName = username
                self.navigationController?.pushViewController(profileController, animated: true)
            } else {
                print("north remembers")
                
            }
        }

    }
    
    func hashTagTapped(hash: String) {
        let hh = HashTagPostsController(collectionViewLayout: UICollectionViewFlowLayout())
        let ref_hash = Database.database().reference().child("Hashtags").child(hash)
        
        ref_hash.observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.exists() {
                hh.hashtag = hash
                self.navigationController?.pushViewController(hh, animated: true)
                
            } else {
                print("stupid")
            }
        }
    }
    
    func onShareButtonTapped(for cell: PostUi) {
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        let post = self.newPosts[indexPath.item]
        let downloadImgaesController = DownloadImagesController()
        downloadImgaesController.post = post
        self.present(downloadImgaesController, animated: true, completion: nil)
    }
    
    func onStoryViewTapped(user: UserModelNew, post: PostNew) {
        return
    }
    
    
    func swipetoTop() {
        return
    }
    
    func onswipetButtonTapped(for cell: PostUi) {
        return
    }
    
    
    func onTagLabelTapped(post: Post) {
        return
    }
    
    
    
    func onUnblock() {
       handleRefresh()
        }
    
    
    func onLevelCountTapped(user: UserModel) {
        
        let user = user
        let profileLevelController = ProfileLevelController()
        profileLevelController.user = user
        
        navigationController?.pushViewController(profileLevelController, animated: true)
        
//        present(profileLevelController, animated: true, completion: nil)

    }
    
    func onRocketButtonTappedFromProfile(for cell: PostProfileUi) {
        
        
    }
    
    func onOptionButtonTappedFromProfile(for cell: PostProfileUi) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        alertController.popoverPresentationController?.permittedArrowDirections = []
        
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let AdminId = "FaKyjBJXnzeZAeSYe13XV2WIrp23"
        
        guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
        
        let post = self.newPosts[indexPath.item]
        let postUid = post.uid ?? ""
        guard let pid = post.id else { return }
        
        if uid == AdminId {
            alertController.addAction(UIAlertAction(title: "Add to explore", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference().child("explore")
                    
                    let cd = post.timestamp ?? 0
                    
                    
                    let values = ["uid": postUid, "creationDate": cd] as [String : Any]
                    ref.child(pid).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Done", body: "Keep going Apurva", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                    let notificationMessage = "We have featured your post on our explore page. This will give your more reach. Happy writing :)"
                    guard let toUserId = post.uid else { return }
                    guard let postId = post.id else { return }

                    let ref54 = Database.database().reference().child("notifications").child(toUserId).childByAutoId()
                    
                    
                    let notifValues = ["Message": notificationMessage, "uid":toUserId, "pid":postId, "fromUserId":"b5MKdFi5mkcJgQC12hEOjJOT3MB3", "type":"2312", "creationDate": cd] as [String : Any]
                    
                    ref54.updateChildValues(notifValues)

                }
                
            }))
        }
        
        
        if uid == postUid {
            alertController.addAction(UIAlertAction(title: "Edit post", style: .default, handler: { (_) in
                
                do {
                    
                    let editPostController = EditPostController()
                    editPostController.post = post
                    self.navigationController?.pushViewController(editPostController,
                                                                  animated: true)
                }
                
            }))
            
            alertController.addAction(UIAlertAction(title: "Delete Post", style: .destructive, handler: { (_) in
                do {
                    self.myActivityIndicator.startAnimating()
                    
                    UtilityAPI.deletePost(post: post)
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.success)
                        view.configureDropShadow()
                        let iconText = "👻"
                        view.configureContent(title: "Done", body: "Your post is deleted.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                    DispatchQueue.main.async {
                        self.newPosts.remove(at: indexPath.item)
                        self.users.remove(at: indexPath.item)
                        self.collectionView?.deleteItems(at: [indexPath])
                    }
                    
                    self.myActivityIndicator.stopAnimating()
                }
                
            }))
            
        } else {
            alertController.addAction(UIAlertAction(title: "Report", style: .destructive, handler: { (_) in
                
                do {
                    let ref = Database.database().reference()
                    
                    guard let postId = post.id else { return }
                    let values = [postUid:true] as [String : Any]
                    ref.child("report").child(postId).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Thank You", body: "Thank you for reporting this post. We will check and will send you update within 24 hours.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                }
                
            }))
            
            
        }
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)

    }
    
    func onTitleLabelTappedFromProfile(for cell: PostProfileUi) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.newPosts[indexPath.item]
        
        guard let postId = post.id else { return }
        
        let postController = PostController(collectionViewLayout: UICollectionViewFlowLayout())
        postController.userId = post.uid
        postController.postId = postId
        navigationController?.pushViewController(postController, animated: true)    }
    
    
    func onEmojiTap(for cell: ProfileHeader) {
        print("emoji")

    }
    
   
    
    func onRocketButtonTapped(for cell: PostUi) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        
        DispatchQueue.main.async {
            self.collectionView?.reloadItems(at: [indexPath])
        }

    }
    

    func onLogoutTapped(for cell: ProfileHeader) {
        
        guard let currentUserId = user?.uid else { return }
        guard let uid = Auth.auth().currentUser?.uid else { return }

        if (currentUserId == uid) {
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alertController.popoverPresentationController?.sourceView = self.view
            alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            
            alertController.popoverPresentationController?.permittedArrowDirections = []
            //        alertController.popoverPresentationController?.barButtonItem
            
            alertController.addAction(UIAlertAction(title: "Edit Profile", style: .default, handler: {
                (_) in
                
                do {
                    
                    let profileEditController = ProfileEditController(collectionViewLayout: UICollectionViewFlowLayout())
//                    profileEditController.user = self.user
                    self.navigationController?.pushViewController(profileEditController, animated: true)
                }
                
            }))

            
            alertController.addAction(UIAlertAction(title: "Feedback", style: .default, handler: {
                (_) in
                
                do {
                    
                    let feedbackController = FeedbackController()
                    self.navigationController?.pushViewController(feedbackController, animated: true)
                }
                
            }))

            alertController.addAction(UIAlertAction(title: "FAQ", style: .default, handler: {
                (_) in
                
                do {
                    
                    let newController = FAQController(collectionViewLayout: UICollectionViewFlowLayout())
                    self.navigationController?.pushViewController(newController, animated: true)
                }
                
            }))
            alertController.addAction(UIAlertAction(title: "Log Out", style: .destructive, handler: { (_) in
                
                do {
                    try Auth.auth().signOut()
                    
                    //what happens? we need to present some kind of login controller
                    let welcomeController = WelcomeController()
                    let navController = UINavigationController(rootViewController: welcomeController)
                    
                    
                    self.present(navController, animated: true, completion: nil)
                    
                } catch let signOutErr {
                    print("Failed to sign out:", signOutErr)
                }
                
                
            }))
            
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            
            present(alertController, animated: true, completion: nil)
            

        } else {
          print("report this person")
            
            let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            alertController.popoverPresentationController?.sourceView = self.view
            alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            
            alertController.popoverPresentationController?.permittedArrowDirections = []

            
            alertController.addAction(UIAlertAction(title: "Report", style: .destructive, handler: { (_) in
                
                do {
                    guard let uid = Auth.auth().currentUser?.uid else { return }
                    let ref = Database.database().reference()
                    

                    let values = [uid: true]
                    ref.child("reportProfile").child(currentUserId).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Thank You", body: "Thank you for reporting this user. We will check and will send you update within 24 hours.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    
                }
                
            }))
            
            alertController.addAction(UIAlertAction(title: "Block", style: .destructive, handler: { (_) in
                
                do {
                    guard let uid = Auth.auth().currentUser?.uid else { return }
                    let ref = Database.database().reference()
                    
                    
                    let values = [currentUserId: 1]
                    ref.child("BlockedProfile").child(uid).updateChildValues(values)
                    
                    SwiftMessages.show {
                        let view = MessageView.viewFromNib(layout: .CardView)
                        view.configureTheme(.warning)
                        view.configureDropShadow()
                        let iconText = "🙂"
                        view.configureContent(title: "Okay!", body: "This user is now blocked. You won't be able to see or interact with each other's content and profile.", iconText: iconText)
                        view.button?.isHidden = true
                        
                        return view
                    }
                    self.handleRefresh()
                    self.navigationController?.dismiss(animated: true, completion: nil)
                    
                }
                
            }))

            
            
            
            alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func onEditPorfileTap(user: UserModel) {
        return
    }
    func onOptionButtonTapped(for cell: PostUi) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alertController.popoverPresentationController?.sourceView = self.view
        alertController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        alertController.popoverPresentationController?.permittedArrowDirections = []
        
        alertController.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (_) in
            
            do {
                
                guard let indexPath = self.collectionView?.indexPath(for: cell) else { return }
                let post = self.newPosts[indexPath.item]
                
                UtilityAPI.deletePost(post: post)
                
                DispatchQueue.main.async {
                    self.collectionView?.reloadData()
                }

            }
            
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    
    
    func onStoryViewTapped(post: Post) {
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
//        previewpostController.post = post
        
//        self.addChildViewController(previewpostController)
//        self.view.addSubview(previewpostController.view)
//        previewpostController.didMove(toParentViewController: self)
//
        
        navigationController?.pushViewController(previewpostController, animated: true)
//        present(previewpostController, animated: true, completion: nil)
        
    }
    
    func onNameLabelTapped(for cell: PostUi) {
        guard let indexPath = collectionView?.indexPath(for: cell) else { return }
        
        let post = self.newPosts[indexPath.item]
        
        let profileController = ProfileController(collectionViewLayout: UICollectionViewFlowLayout())
        profileController.userId = post.uid
        self.navigationController?.pushViewController(profileController,
                                                      animated: true)
        
    }
    
    
    func didTapComment(post: Post) {
        
    }
    
    func onloveButtonTapped(loc: CGPoint, for cell: PostUi) {
        return
    }
    
    func onTitleLabelTapped(for cell: PostUi) {
        return
    }
  
    
    var userId: String?
    
    var userName: String?
    
    let cellId = "cellId"
    let postCellId = "postCellId"
    

 
    
    fileprivate func setupAddIthaka(){
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "icons8-plus_math_filled").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleAddIthaka))
        
    }
    
    @objc func handleAddIthaka() {
        let addIthakaController = AddIthakaAPI.addIthakaController
        addIthakaController.modalPresentationStyle = .custom
        addIthakaController.transitioningDelegate = self
        present(addIthakaController, animated: true, completion: nil)
    }
    
    let myActivityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
    var user: UserModelNew?
    
    
    fileprivate func fetchUserFromUsername() {
        
        let uu = userName ?? ""
        
        if uu == "" {
            let uid = self.userId
                ?? (Auth.auth().currentUser?.uid ?? "")
            Api.User.observeUser(withId: uid) { (UserModelNew) in
                self.user = UserModelNew
                self.navigationItem.title = self.user?.username
                self.loadPosts()
                
            }

        }
         else {
            Api.User.observeUserByUsername(username: uu, completion: { (UserModelNew) in
                print("user is fucked up ", UserModelNew)
                self.user = UserModelNew
                self.navigationItem.title = self.user?.username
                self.loadPosts()

            })
        }
    }
    func checkBlock() {
        guard let uid = Auth.auth().currentUser?.uid else { return }

        guard let currentUid = userId else { return }
        
        print("CurrentId is", currentUid)

        let ref = Database.database().reference().child("BlockedProfile").child(uid)
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? NSDictionary
            print("snapshot is", value)
        }
    }
    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setImage(#imageLiteral(resourceName: "icons8-delete-filled-50").withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = .white
        button.backgroundColor = .black
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)

        return button
    }()
    
    @objc func handleDismiss(){
        dismiss(animated: true, completion: nil)
        oneview.backgroundColor = .white
    }
    let oneview = UIView()

    var presentBtn: Int?


    override func viewDidLoad() {
        super.viewDidLoad()
        myActivityIndicator.hidesWhenStopped = true
        myActivityIndicator.backgroundColor = .white
        myActivityIndicator.color = .black
        view.addSubview(myActivityIndicator)
        myActivityIndicator.anchor(top: nil, left: view.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 10, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        myActivityIndicator.startAnimating()

        self.collectionView?.contentInset = UIEdgeInsetsMake(15, 0, 100, 0)

        checkBlock()
        oneview.backgroundColor = .black
        
        view.addSubview(dismissButton)
        view.addSubview(oneview)
        
        dismissButton.anchor(top: nil, left: view.self.leftAnchor, bottom: view.safeAreaLayoutGuide.bottomAnchor, right: view.self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 30)
        oneview.anchor(top: view.safeAreaLayoutGuide.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)


        collectionView?.backgroundColor = .white

        collectionView?.register(ProfileHeader.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerId")
        
        collectionView?.register(PostUi.self, forCellWithReuseIdentifier: postCellId)
       
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: ProfileEditController.updateProfileNotificatioName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: AddIthakaFinalController.updateFeedNotificationName, object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(handleUpdateFeed), name: EditPostController.updateFeedNotificationName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleCommentUpdate), name: CommentsController.updateCommentsCountName, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleReadBackStory), name: PreviewPostController.updateReadStoryNotificationName, object: nil)


        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh), for: .valueChanged)
        collectionView?.refreshControl = refreshControl
    
//        fetchUser()
        fetchUserFromUsername()

//        printCounts()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "STHeitiSC-Medium", size: 18) ?? UIFont.systemFont(ofSize: 18)]

        collectionView?.setNeedsLayout()
        collectionView?.layoutIfNeeded()

//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "DalekPinpointBold", size: 21)!]
    }
    
    
    @objc func handleReadBackStory(_ notification: NSNotification){
        
        if let itemName = notification.userInfo?["itemName"] as? String {
            
            let itemsStoryController = ItemsStoryController(collectionViewLayout: UICollectionViewFlowLayout())
            itemsStoryController.itemCode = itemName
            self.navigationController?.pushViewController(itemsStoryController, animated: true)
            
        }
        
        
    }
    

    
    @objc func handleCommentUpdate() {
        collectionView?.reloadData()
    }
    
    fileprivate var isLoadingPost = false
    var newPosts = [PostNew]()

    func loadPosts() {
        isLoadingPost = true
        print("loading started")
        guard let uid = user?.uid else { return }
        
        
        Api.Profile.getRecentProfileFeed(withId: uid, start: newPosts.first?.timestamp, limit: 3) { (results) in
            guard let results = results else { self.myActivityIndicator.stopAnimating()
                return }

            if results.count > 0 {
                results.forEach({ (result) in
                    self.newPosts.append(result)
                
                })
            }
            self.isLoadingPost = false
            self.myActivityIndicator.stopAnimating()
            self.collectionView?.reloadData()
            
        }

    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let previewpostController = PreviewPostController(collectionViewLayout: UICollectionViewFlowLayout())
        
        let post = newPosts[indexPath.item]
        
        
        previewpostController.post = post
        previewpostController.user = user
        
        previewpostController.popoverPresentationController?.sourceView = self.view
        previewpostController.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
        
        previewpostController.popoverPresentationController?.permittedArrowDirections = []
        
        present(previewpostController, animated: true, completion: nil)
        
    }

    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y >= scrollView.contentSize.height - self.view.frame.size.height {
            guard let uid = user?.uid else { return }

            guard !isLoadingPost else {
                return
            }
            isLoadingPost = true
            
            guard (self.newPosts.last?.timestamp) != nil else {
                isLoadingPost = false
                return
            }
            self.myActivityIndicator.startAnimating()
            
            Api.Profile.getOldFeed(withId: uid, start: (newPosts.last?.timestamp)!, limit: 3, completionHandler: { (results) in
                guard let results = results else { return }
                
                if results.count == 0 {
                    self.myActivityIndicator.stopAnimating()
                    return
                }
                if results.count > 0 {
                    results.forEach({ (result) in
                        self.newPosts.append(result)
                        
                    })
                }
                
                self.myActivityIndicator.stopAnimating()
                
                self.collectionView?.reloadData()
                self.isLoadingPost = false
            })
            
            
        }
    }

    
    fileprivate func printCounts() {
        let ref = Database.database().reference().child("users")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            guard let usersList = snapshot.value as? [String: Any] else { return }
            
            //listing out all the ids of users
            print("user count is", usersList.count)
        }
        
        let refP = Database.database().reference().child("posts")
        refP.observeSingleEvent(of: .value) { (snapshot) in
            guard let postsList = snapshot.value as? [String: Any] else { return }
            
            var cc = 0
            postsList.forEach({ (snapshot) in
                let value = snapshot.value as? NSDictionary
                guard let count = (value?.count) else  { return }
                cc = cc + count
                
            })
            
            print("Total post count is ", cc)
            
        }
    }

    @objc func handleUpdateFeed() {
        handleRefresh()
    }
    
    @objc func handleRefresh() {
        print("Handling refresh..")

        users.removeAll()
        newPosts.removeAll()
        
        fetchUserFromUsername()
        DispatchQueue.main.async {
            self.collectionView?.reloadData()
            self.collectionView?.refreshControl?.endRefreshing()
        }
    }
 
    
    var isFinishedPaging = false
    var users = [UserModel]()
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "headerId", for: indexPath) as! ProfileHeader
        header.user = self.user
        header.profileDelegate = self
        header.delegate = self
        
        return header
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 16)

        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = user?.shortBio
        label.sizeToFit()
        
        let height = label.frame.height + 180 + 250 + 20 + 25 + 70
        return CGSize(width: view.frame.width, height: height)
    }
    
    
    //showing owenr's posts
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newPosts.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: postCellId, for: indexPath) as! PostUi
        
        
        let post = newPosts[indexPath.item]
        
//        let user = userModelNew[indexPath.item]
        
        cell.post = post
        cell.user = user
        
        cell.delegate = self
        cell.textViewTap = self
    
        return cell
    }
    var tagheight = 0

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let post = newPosts[indexPath.item]
        
        let apporxWidthofTitleText = view.frame.width - 40 - 15
        let size = CGSize(width: apporxWidthofTitleText, height: 1000)
        let attributes = [NSAttributedStringKey.font: UIFont.boldSystemFont(ofSize: 20)]
        let estimatedFrame = NSString(string: post.title!).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        
        let titleView:UITextView = UITextView(frame: CGRect(x: 65, y: 0, width: collectionView.frame.width, height: CGFloat.greatestFiniteMagnitude))
        titleView.font = UIFont.boldSystemFont(ofSize: 20)
        titleView.text = post.title
        titleView.sizeToFit()
        
        let titleEstHeight = estimatedFrame.height
        let tagcount = post.tag?.count
        if tagcount == 0 {
            self.tagheight = 0
            
        } else {
            self.tagheight = 20
            
        }
        
        let apporxWidthofTitleTextstory = view.frame.width - 45 - 30

        let storysize = CGSize(width: apporxWidthofTitleTextstory, height: CGFloat.greatestFiniteMagnitude)
        let storyAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 17)]
        let storyEstimatedFrame = NSString(string: post.story!).boundingRect(with: storysize, options: .usesLineFragmentOrigin, attributes: storyAttributes, context: nil)
        
        let storyHeight = storyEstimatedFrame.height
        
        
        let finalHeight = 160 + 140 + 32 + titleEstHeight + CGFloat(tagheight) - 30 - 30 + 10 + 5 + 5 + 7

        return CGSize(width: view.frame.width, height: 330)
    }
    
    
}



//
//  MonsterQuestCell.swift
//  ithaka
//
//  Created by Apurva Raj on 02/12/18.
//  Copyright © 2018 Apurva Raj. All rights reserved.
//

import UIKit
import FirebaseDatabase


protocol monstertap: class {
    func playtapped()
}


class MonsterQuestCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(collectionView)
        addSubview(playLabel)
        
        //        addSubview(mainbg)
        
        //        mainbg.anchor(top: self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: frame.width, heightConstant: frame.height)
        //        playLabel.anchor(top: nil, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 10, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 100)
        
        self.addSubview(playbutton)
        
        playbutton.anchor(top: nil, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 15, leftConstant: 40, bottomConstant: 15, rightConstant: 40, widthConstant: 0, heightConstant: 70)
        
        addConstraintsWithFormat("H:|[v0]|", views: collectionView)
        addConstraintsWithFormat("V:|[v0]|", views: collectionView)
        
        collectionView.register(MonsterTitle.self, forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "MonsterTitle")
        
        collectionView.register(MonsterQuestDurationCell.self, forCellWithReuseIdentifier: "MonsterQuestDurationCell")
        collectionView.register(MonsterQuestPeopleCell.self, forCellWithReuseIdentifier: "MonsterQuestPeopleCell")
        collectionView.register(MonsterQuestPlayCell.self, forCellWithReuseIdentifier: "MonsterQuestPlayCell")
        
        
        collectionView.backgroundView = mainbg

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    weak var delegate: monstertap?
    
    
        lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        cv.contentInset = UIEdgeInsetsMake(50, 0, 50, 0)
        cv.contentMode = .scaleAspectFill
        cv.dataSource = self
        cv.delegate = self
    
        return cv
    }()
    
    lazy var playLabel: UIView = {
        let label = UIView()
        label.backgroundColor = UIColor.rgb(red: 242, green: 242, blue: 242, alpha: 1)
        
        return label
    }()
    
    lazy var playbutton : UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "Play")
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(playTapped))
        tap.numberOfTapsRequired = 1
        iv.addGestureRecognizer(tap)
        
        iv.isUserInteractionEnabled = true
        
        return iv
    }()

    @objc func playTapped() {
        print("one is working")
        self.delegate?.playtapped()
    }
    
    
    let mainbg : UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "snowy02_preview-1")
        return iv
    }()
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MonsterQuestPeopleCell", for: indexPath) as! MonsterQuestPeopleCell
            cell.backgroundColor = UIColor.clear

            return cell

        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MonsterQuestDurationCell", for: indexPath) as! MonsterQuestDurationCell
            cell.backgroundColor = UIColor.clear

            return cell
        
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MonsterQuestMainView", for: indexPath) as! MonsterQuestMainView
            return cell

        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize(width: frame.width, height: frame.height*0.20)

        } else {
            return .zero
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "MonsterTitle", for: indexPath) as! MonsterTitle
        header.backgroundColor = UIColor.clear
        return header
        
    }
}

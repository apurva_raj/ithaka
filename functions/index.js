const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();


exports.sendPushNotification = functions.database.ref('notifications/{toUserId}/{autoId}').onWrite((change, context) => {

const afterData = change.after.val();
  const toUserId = afterData.uid;
  const payload = {
    notification: {
      body: afterData.Message,
      badge: afterData.badge,
      sound: 'default',

    }
  };
  return admin.database().ref('fcmToken').child(toUserId).once('value').then(allToken => {
    if (allToken.val()) {
      const token = Object.keys(allToken.val());
      return admin.messaging().sendToDevice(token, payload).then(response => {

      });
    };
  });



});
